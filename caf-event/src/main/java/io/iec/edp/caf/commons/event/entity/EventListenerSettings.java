//package io.iec.edp.caf.commons.event.entity;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * EventListenerSettings 合并后配置
// *
// * @author Leon Huo
// */
//public class EventListenerSettings {
//    private List<EventManagerData> eventManagers = new ArrayList<>();
//
//    public List<EventManagerData> getEventManagers() {
//        return this.eventManagers;
//    }
//
//    public void setEventListenerConfigurations(List<EventManagerData> eventManagers) {
//        this.eventManagers = eventManagers;
//    }
//
//    public EventListenerSettings(EventManagerProperties properties, List<EventManagerData> eventManagerDatas) {
//        if (properties != null && properties.getEventManagers() != null)
//            this.eventManagers.addAll(properties.getEventManagers());
//        if (eventManagerDatas != null) {
//            for (EventManagerData data : eventManagerDatas) {
//                EventManagerData existeData = this.get(data.getName());
//                if (existeData == null)
//                    this.eventManagers.add(data);
//                else {
//                    for (EventListenerData listenerData : data.getListeners()) {
//                        existeData.getListeners().removeIf(s -> s.getName() == listenerData.getName());
//                        existeData.getListeners().add(listenerData);
//                    }
//                }
//            }
//        }
//    }
//
//    private EventManagerData get(String name) {
//        for (EventManagerData data : this.eventManagers) {
//            if (data.getName().equals(name)) {
//                return data;
//            }
//        }
//        return null;
//    }
//}
