/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event.entity;

/**
 * @author Leon Huo
 */
public class EventErrorDefinition {
    /**
     * 错误编码
     */
    private String errorCode;
    /**
     * 参数
     */
    private Object[] errorParams;
    /**
     * 资源名
     */
    private String resourceName = "Inspur.Ecp.Caf.Event.Properties.ErrorInfo";
    /**
     * 错误前缀
     */
    private String eventPrefix = "EVENT";
    /**
     * 错误配置代码
     */
    private String errorConfigurationCode = "EventCore";

    /**
     * 错误信息
     */
    private String errorMessage;

    /**
     * 无参构造
     */
    public EventErrorDefinition() {

    }

    /**
     * 有参构造
     *
     * @param eventError 错误枚举
     */
    public EventErrorDefinition(EventError eventError) {
        this.errorCode = eventError.toString();
    }

    /**
     * 有参构造
     *
     * @param error       错误枚举
     * @param errorParams 参数
     */
    public EventErrorDefinition(EventError error, Object[] errorParams) {
        this(error);
        this.errorParams = errorParams;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object[] getErrorParams() {
        return errorParams;
    }

    public void setErrorParams(Object[] errorParams) {
        this.errorParams = errorParams;
    }

    public String getResourceName() {
        return this.resourceName;
    }

    public String getEventPrefix() {
        return this.eventPrefix;
    }

    public String getErrorConfigurationCode() {
        return this.errorConfigurationCode;
    }

    public String getErrorMessage() {
        errorMessage = String.format("%s-%s", this.eventPrefix, this.errorCode);
        return errorMessage;
    }
}
