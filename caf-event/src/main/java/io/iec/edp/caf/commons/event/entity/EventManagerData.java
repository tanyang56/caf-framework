//package io.iec.edp.caf.commons.event.entity;
//
//import lombok.Data;
//
//import java.util.List;
//
///**
// * @author Leon Huo
// * @Date: 2021/5/6
// */
//@Data
//public class EventManagerData {
//    private String name;
//    /**
//     * 事件监听者对象名称
//     */
//    private List<EventListenerData> listeners;
//
//    /**
//     * 无参构造函数
//     */
//    public EventManagerData() {
//
//    }
//
//    /**
//     * 构造函数
//     */
//    public EventManagerData(String name, List<EventListenerData> listeners) {
//        this.name = name;
//        this.listeners = listeners;
//    }
//}
