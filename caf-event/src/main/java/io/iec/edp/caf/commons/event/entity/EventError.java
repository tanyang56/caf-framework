/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event.entity;

/**
 * @author Leon Huo
 * @Date: 2021/5/6
 */
public enum EventError {
    /**
     * 事件管理类未识别错误
     */
    ManagerUnIdentify(1001),

    /**
     * 事件管理类类名无效
     */
    ManagerClassInvalid(1002),

//    /**
//     * 事件管理类程序集未找到
//     */
//    ManagerAssemblyNotFound(1003),
//
    /**
     * 事件管理类类型在指定程序集未找到
     */
    ManagerClassNotFound(1003),

    /**
     * 事件管理类指定的类型未实现错误处理接口
     */
    ManagerClassNotImplInterface(1004),

    /**
     * 事件管理类指定的配置不存在
     */
    ManagerConfigNotExist(1005),

    /**
     * 事件监听者未识别错误
     */
    ListenerUnIdentify(1006),

    /**
     * 事件监听者类类名无效
     */
    ListenerClassInvalid(1007),

//    /**
//     * 事件监听者类程序集未找到
//     */
//    ListenerAssemblyNotFound(1009),

    /**
     * 事件监听者类类型在指定程序集未找到
     */
    ListenerClassNotFound(1008),

    /**
     * 事件监听者类指定的类型未实现错误处理接口
     */
    ListenerClassNotImplInterface(1009),

    /**
     * 事件监听者类指定的配置不存在
     */
    ListenerConfigNotExist(1010),
    /**
     * 实例化失败
     */
    InstantiationOfImplementationClassFailed(1011);

    private int i;

    EventError(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return this.i + "";
    }
}
