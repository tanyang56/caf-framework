/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event.config;

import io.iec.edp.caf.commons.event.entity.EventManagerProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * EventListenerSettings 合并后配置
 * @author lijing
 * @data 2019/9/4 18:40
 */
//todo 老的这个被com.inspur.edp.bef.bemanager.befdtconsistencycheckevent.BefDtConsistencyCheckConf依赖
public class EventListenerSettings{
    private List<EventManagerData> eventManagers =new ArrayList<>();

    public List<EventManagerData> getEventManagers() {
        return this.eventManagers;
    }

    public void setEventListenerConfigurations(List<io.iec.edp.caf.commons.event.config.EventManagerData> eventManagers) {
        this.eventManagers = eventManagers;
    }

    public EventListenerSettings(EventManagerProperties properties,List<EventManagerData> eventManagerDatas){
        if(properties!=null && properties.getEventManagers()!=null)
            this.eventManagers.addAll(properties.getEventManagers());
        if(eventManagerDatas!=null){
            for (EventManagerData data : eventManagerDatas){
                EventManagerData existeData = this.get(data.getName());
                if(existeData==null)
                    this.eventManagers.add(data);
                else
                {
                    if(existeData.getListeners()==null)
                        existeData.setListeners(new ArrayList<>());

                    for(EventListenerData listenerData:data.getListeners()){
                        existeData.getListeners().removeIf(s-> s.getName()==listenerData.getName());
                        existeData.getListeners().add(listenerData);
                    }
                }
            }
        }
    }

    private EventManagerData get(String name){
        for (EventManagerData data : this.eventManagers){
            if(data.getName().equals(name)){
                return data;
            }
        }
        return null;
    }
}