/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event.startupevent;

import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.exception.entity.DefaultExceptionProperties;
import io.iec.edp.caf.commons.exception.entity.ExceptionErrorCode;

public class StartupEventManager extends EventManager {

    /**
     * 内部枚举类
     */
    enum RpcInvokeEventType {
        preInvoke,
    }

    /**
     * 触发事件
     *
     * @param e
     */
    public void fireStartupStartingEvent(StartupStartingArgs e) {
        this.fire(RpcInvokeEventType.preInvoke, e);
    }


    /**
     * 实现抽象方法getCode(),获取事件管理对象的编码
     *
     * @return 事件管理对象的编码
     */
    @Override
    public String getEventManagerName() {
        return "CAFStartupEventManager";
    }

    /**
     * 当前的事件管理者是否处理输入的事件监听者对象
     *
     * @param listener 事件监听者
     * @return 若处理，则返回true,否则返回false
     */
    @Override
    public boolean isHandlerListener(IEventListener listener) {
        return listener instanceof StartupListner;
    }

    /**
     * 注册事件
     *
     * @param listener 监听者
     */
    @Override
    public void addListener(IEventListener listener) {
        if (listener instanceof StartupListner == false) {
            throw new CAFRuntimeException(DefaultExceptionProperties.SERVICE_UNIT,
                    DefaultExceptionProperties.RESOURCE_FILE,
                    ExceptionErrorCode.classNotImplInterface,
                    new String[]{"StartupListner"},
                    null);
        }
        StartupListner demoEventListener = (StartupListner) listener;
        this.addEventHandler(RpcInvokeEventType.preInvoke, demoEventListener, "startupStarting");
    }

    /***
     * 注销事件
     * @param listener  监听者
     */
    @Override
    public void removeListener(IEventListener listener) {
        if (listener instanceof StartupListner == false) {
            throw new CAFRuntimeException(DefaultExceptionProperties.SERVICE_UNIT,
                    DefaultExceptionProperties.RESOURCE_FILE,
                    ExceptionErrorCode.classNotImplInterface,
                    new String[]{"StartupListner"},
                    null, ExceptionLevel.Error, false);
        }
        StartupListner demoEventListener = (StartupListner) listener;
        this.removeEventHandler(RpcInvokeEventType.preInvoke, demoEventListener, "startupStarting");
    }
}