/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event;

import io.iec.edp.caf.commons.event.EventManager;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 事件管理类集合
 *
 * @author Leon Huo
 * @Date: 2021/5/6
 */
@Data
//todo 被io.iec.edp.caf.runtime.event.FrameworkEventBroker依赖
public class EventManagerCollection {
    /**
     * 事件管理类集合
     */
    private List<EventManager> managerList = new ArrayList<>();

    /**
     * 无参构造函数
     */
    public EventManagerCollection() {

    }

    /**
     * 根据索引获取事件管理者对象
     *
     * @param index 索引
     * @return 事件管理者
     */
    public EventManager get(int index) {
        return managerList.get(index);
    }

    /**
     * 根据编码获取事件管理者对象
     *
     * @param code 编码
     * @return 事件管理者
     */
    public EventManager get(String code) {
        for (EventManager manager : managerList) {
            if (manager.getEventManagerName().equals(code)) {
                return manager;
            }
        }
        return null;
    }

    /**
     * 增加一个事件管理者
     *
     * @param manager 事件管理者
     */
    public void add(EventManager manager) {
        if (manager != null) {
            managerList.add(manager);
        }
    }

    /**
     * 获取EventManager元素个数
     *
     * @return EventManager元素个数
     */
    public int count() {
        return managerList.size();

    }
}
