//package io.iec.edp.caf.commons.event.properties;
//
//
//import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
//import org.springframework.core.env.PropertiesPropertySource;
//import org.springframework.core.env.PropertySource;
//import org.springframework.core.io.support.DefaultPropertySourceFactory;
//import org.springframework.core.io.support.EncodedResource;
//
//import java.io.IOException;
//import java.util.Properties;
//
///**
// * CompositePropertySourceFactory 既支持加载yml文件又支持加载propertites文件
// * @author lijing
// * @data 2019/8/16 16:39
// */
//public class CompositePropertySourceFactory extends DefaultPropertySourceFactory
//{
//    @Override
//    public PropertySource<?> createPropertySource(String name, EncodedResource resource)throws IOException
//    {
//        String sourceName = name != null ? name : resource.getResource().getFilename();
//        if (!resource.getResource().exists()) {
//            return new PropertiesPropertySource(sourceName, new Properties());
//        } else if (sourceName.endsWith(".yml") || sourceName.endsWith(".yaml")) {
//            Properties propertiesFromYaml = loadYml(resource);
//            return new PropertiesPropertySource(sourceName, propertiesFromYaml);
//        } else {
//            return super.createPropertySource(name, resource);
//        }
//    }
//    /**
//     * load yaml file to properties
//     *
//     * @param resource
//     * @return
//     * @throws IOException
//     */
//    private Properties loadYml(EncodedResource resource) throws IOException {
//        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
//        factory.setResources(resource.getResource());
//        factory.afterPropertiesSet();
//        return factory.getObject();
//    }
//
//
//
//}
//
