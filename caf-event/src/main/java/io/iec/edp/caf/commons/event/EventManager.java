/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event;

/**
 * 事件管理类
 * 所有事件管理类的基类
 * 维护了一个事件句柄列表，提供了基本的事件注册和取消注册以及激发的方法
 */
public abstract class EventManager {

    /**
     * 获取事件管理对象的编码
     *
     * @return 管理对象编码
     */
    public abstract String getEventManagerName();

    /**
     * 事件集合
     */
    protected EventHandlerSet eventSet = EventHandlerSet.synchronizedSet(new EventHandlerSet());

    /**
     * 注册一个事件观察者
     */
    protected void addEventHandler(Object eventKey, IEventListener listener, String methodName) {
        this.eventSet.addHandler(eventKey, listener, methodName);
    }

    /**
     * 取消一个事件观察者
     */
    protected void removeEventHandler(Object eventKey, IEventListener listener, String methodName) {
        this.eventSet.removeHandler(eventKey, listener, methodName);
    }

    /**
     * 激发一个特定的事件
     */
    protected void fire(Object eventKey, CAFEventArgs e) {
        this.eventSet.fire(eventKey, e);
    }

    /**
     * 当前的事件管理者是否处理输入的事件监听者对象
     *
     * @param listener 事件监听者
     * @return 若处理，则返回true,否则返回false
     */
    public abstract boolean isHandlerListener(IEventListener listener);

    /**
     * 增加一个监听者
     *
     * @param listener 监听者
     */
    public abstract void addListener(IEventListener listener);

    /**
     * 从监听者列表中去掉指定的监听者
     *
     * @param listener 监听者
     */
    public abstract void removeListener(IEventListener listener);
}
