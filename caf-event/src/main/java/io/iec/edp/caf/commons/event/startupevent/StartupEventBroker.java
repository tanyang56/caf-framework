/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.event.startupevent;

import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

import java.util.HashMap;

public class StartupEventBroker  extends EventBroker {

    private StartupEventManager manager;

    public StartupEventBroker(StartupEventManager manager, EventListenerSettings settings) {
        super(settings);
        this.manager = manager;
        this.init();
    }

    /**
     * 触发事件
     *
     * @return
     */
    public void startupStartingEvent(HashMap<String, Object> parameters) {
//        this.init();
        StartupStartingArgs e = new StartupStartingArgs();
        e.setStartingParam(parameters);
        this.manager.fireStartupStartingEvent(e);
    }

    /**
     * 初始化
     */
    @Override
    protected void onInit() {
        this.eventManagerCollection.add(manager);
    }
}
