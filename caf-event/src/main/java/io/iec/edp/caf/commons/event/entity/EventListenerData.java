//package io.iec.edp.caf.commons.event.entity;
//
//import lombok.Data;
//
///**
// * @author Leon Huo
// * @Date: 2021/5/2
// */
//@Data
//public class EventListenerData implements Cloneable{
//
//    /**
//     * 事件监听者对象名称
//     */
//    private  String name;
//
//    /**
//     * 事件监听者对象的实现类
//     */
//    private String implClassName;
//    /**
//     * 无参构造函数
//     */
//    public EventListenerData(){
//
//    }
//    @Override
//    public Object clone(){
//        EventListenerData data= null;
//        try {
//            data = (EventListenerData) super.clone();
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//        }
//
//        data.setName(this.getName());
//        data.setImplClassName(this.getImplClassName());
//        return data;
//    }
//}
