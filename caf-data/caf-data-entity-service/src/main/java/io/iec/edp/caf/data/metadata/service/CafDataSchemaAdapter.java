/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.service;


import io.iec.edp.caf.data.orm.metadata.edm.Schema;
import lombok.var;

import java.util.*;

/**
 * CAF层面统一管理所有转换器Adapters
 */
public class CafDataSchemaAdapter {

    //所有转换器adapter
    private final List<SchemaAdapter> adapters;

    //基于id模式的所有adapter
    //预警，消息，打印的扩展在使用
    private final Map<String,SchemaAdapter> keysAdapters;

    public CafDataSchemaAdapter() {
        //加载所有SchemaAdapter实现类
        this.adapters = loadSchemaAdapters();
        this.keysAdapters = loadSchemaAdaptersForKey();
    }

    /**
     * 加载所有的SchemaAdapter
     *
     * @return
     */
    private List<SchemaAdapter> loadSchemaAdapters() {
        //加载SchemaAdapter的所有实现类
        ServiceLoader<SchemaAdapter> loader = ServiceLoader.load(SchemaAdapter.class);
        //存储所有实现类
        List<SchemaAdapter> schemaAdapters = new ArrayList<>();
        for (SchemaAdapter adapter : loader) {
            schemaAdapters.add(adapter);
        }
        return schemaAdapters;
    }

    private Map<String,SchemaAdapter> loadSchemaAdaptersForKey() {
        //加载SchemaAdapter的所有实现类
        ServiceLoader<SchemaAdapter> loader = ServiceLoader.load(SchemaAdapter.class);
        //存储所有实现类
        Map<String,SchemaAdapter> schemaAdapters = new HashMap<>();
        for (SchemaAdapter adapter : loader) {
            if(adapter.getIdentity()!=null && adapter.getIdentity().length()>0){
                schemaAdapters.put(adapter.getIdentity(),adapter);
            }
        }
        return schemaAdapters;
    }

    /**
     * 调用对应Schema适配器，返回Schema
     *
     * @param obj 传入参数
     * @return Schema
     */
    public Schema convertSchema(Object obj) {
        //遍历adapter，
        for (SchemaAdapter adapter : this.adapters) {
            Schema schema = adapter.convert(obj);
            if (schema != null) {
                return schema;
            }
        }
        //没有对应转换器，返回null
        return null;
    }

    /**
     * 根据schemaid和参数获取schema
     * @param schemaId schema adapter唯一标识符
     * @param map 扩展参数集合
     * @return
     */
    public Schema getSchema(String schemaId,Map<String,Object> map){
        var adapter = this.keysAdapters.get(schemaId);
        return adapter==null?null:adapter.getSchema(map);
    }

    /**
     * 根据schemaid和参数获取schema的数据
     * @param schemaId schema adapter唯一标识符
     * @param map 扩展参数集合
     * @return
     */
    public List<Object> getSchemaData(String schemaId,Map<String,Object> map){
        var adapter = this.keysAdapters.get(schemaId);
        return adapter==null?null:adapter.getSchemaData(map);
    }



}
