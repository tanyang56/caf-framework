/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.service;


import io.iec.edp.caf.data.orm.metadata.edm.Schema;

import java.util.List;
import java.util.Map;

/**
 * spi接口：Schema转换器
 *
 * @author Vincent Man
 * @date 2021-05-15
 */
public interface SchemaAdapter {
    /**
     * 定义可扩展转换Schema接口
     *
     * @param obj 转换对象
     * @return Schema 统一数据结构
     * @author Vincent Man
     */
    Schema convert(Object obj);

    /**
     * 可扩展获取Schema接口
     * @param schemaMap
     * @return
     */
    Schema getSchema(Map<String,Object> schemaMap);

    /**
     * 可扩展获取Schema 数据接口
     * @param dataMap
     * @return
     */
    List<Object> getSchemaData(Map<String,Object> dataMap);

    /**
     * 可扩展Schema适配器唯一标识接口
     * @return
     */
    String getIdentity();
}
