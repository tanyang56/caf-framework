/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.service;


import io.iec.edp.caf.data.orm.metadata.edm.Schema;

import java.util.Map;

/**
 * 后端统一调用
 * @author Vincent Man
 * @date 2021-05-18
 */
public class CafDataSchemaAdapterService {
    /**
     * 后端统一调用()
     * @param obj
     * @return
     */
    public static Schema getSchema(Object obj){
        return new CafDataSchemaAdapter().convertSchema(obj);
    }

    /**
     * 根据schemaid和参数获取schema
     * @param schemaId schema adapter唯一标识符
     * @param map 扩展参数集合
     * @return
     */
    public static Schema getSchema(String schemaId, Map<String,Object> map){
        return new CafDataSchemaAdapter().getSchema(schemaId,map);
    }

    /**
     * 根据schemaid和参数获取schema数据
     * @param schemaId schema adapter唯一标识符
     * @param map 扩展参数集合
     * @return
     */
    public static Schema getSchemaData(String schemaId, Map<String,Object> map){
        return new CafDataSchemaAdapter().getSchema(schemaId,map);
    }

}
