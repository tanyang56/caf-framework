/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.transaction.service;

import java.time.OffsetDateTime;

/**
 * 未提交事务外部调用接口:为想要记录未提交事务的业务提供方法
 *
 * @author manwenxing01
 * @date 2022-06-29
 */
public interface DBTransactionService {

    /**
     * 保存未提交事务信息
     *
     * @param path    未提交事务的标识
     * @param message 提示信息
     * @param time    发生时间
     * @param type    未提交事务类型
     */
    void save(String path, String message, OffsetDateTime time, String type);

}
