///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.commons.transaction;
//
//import io.iec.caf.data.jpa.repository.config.EnableCafJpaRepositories;
//import io.iec.edp.caf.commons.utils.CapLazyEncryptor;
//import io.iec.edp.caf.commons.utils.CapPropertyPlaceholderConfigurer;
//import io.iec.edp.caf.commons.utils.SpringBeanUtils;
//import org.jasypt.encryption.pbe.PBEStringEncryptor;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.BeanFactoryAware;
//import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.boot.system.ApplicationHome;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.context.EnvironmentAware;
//import org.springframework.context.annotation.*;
//import org.springframework.core.env.Environment;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//
//import java.nio.file.Paths;
//import java.util.Objects;
//import java.util.Properties;
//
///**
// * @author wangyandong
// * @date 2019/9/3 15:31
// *
// */
//@Configuration
//@EntityScan({"io.iec.edp.caf.commons.transaction"})
//@EnableJpaRepositories("io.iec.edp.caf.commons.transaction")
//public class TransactionAutoConfiguration implements EnvironmentAware, BeanFactoryAware, ApplicationContextAware {
//    private Environment environment;
//
//    private BeanFactory beanFactory;
//
//
//    /**
//     * 实现ApplicationContextAware接口的回调方法。设置上下文环境
//     *
//     * @param applicationContext
//     */
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) {
//        SpringBeanUtils.setApplicationContext(applicationContext);
//    }
//
//    @Override
//    public void setApplicationContext(BeanFactory beanFactory) throws BeansException {
//        this.beanFactory = beanFactory;
//    }
//
//    @Override
//    public void setEnvironment(Environment environment) {
//        this.environment = environment;
//    }
//
//}
