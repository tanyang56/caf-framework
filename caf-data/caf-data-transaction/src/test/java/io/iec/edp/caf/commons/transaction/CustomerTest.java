///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.commons.transaction;
//
//import io.iec.edp.caf.commons.utils.SpringBeanUtils;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {
//    TransactionAutoConfiguration.class
//})
//@EnableAutoConfiguration
//public class CustomerTest {
//
//    @Autowired
//    CountryRepository repo;
//
//    @Before
//    public void setUp() throws Exception {
//
//    }
//
//
//    @After
//    public void tearDown() throws Exception {
//
//    }
//    /**
//     * 编程式事务嵌套
//     */
//    @Test
//    public void internalCommit() {
//
////        repo.deleteById("123456");
//        //第1层
//        JpaTransaction tran = JpaTransaction.getTransaction();
//        try {
//            tran.begin();
//
//            //第2层
//            JpaTransaction tran2 = JpaTransaction.getTransaction();
//            try {
//                tran2.begin(TransactionPropagation.NOT_SUPPORTED);
//                CountryRepository repo1 = SpringBeanUtils.getBean(CountryRepository.class);
//                Country c = new Country();
//                c.setId("1234567");
//                c.setCode("1234567");
//                c.setName("1234567");
//                repo1.save(c);
//                tran2.commit();
//            }catch (Throwable e){
//                tran2.rollback();
//            }
//
//            tran.rollback();
//        }catch (Throwable e){
//            tran.rollback();
//        }
//
//        Country country = repo.findById("1234567").get();
//        Assert.assertNotNull(country);
//        repo.deleteById("1234567");
//    }
//
//    @Test
//    public void wholecommit() {
//
//        //第1层
//        JpaTransaction tran = JpaTransaction.getTransaction();
//        try {
//            tran.begin();
//
//            //第2层
//            JpaTransaction tran2 = JpaTransaction.getTransaction();
//            try {
//                tran2.begin(TransactionPropagation.REQUIRED);
//                CountryRepository repo1 = SpringBeanUtils.getBean(CountryRepository.class);
//                Country c = new Country();
//                c.setId("123456");
//                c.setCode("123456");
//                c.setName("123456");
//                repo1.save(c);
//                tran2.commit();
//            }catch (Throwable e){
//                tran2.rollback();
//            }
//
//            tran.commit();
//        }catch (Throwable e){
//            tran.rollback();
//        }
//
//        Country country = repo.findById("123456").get();
//        Assert.assertNotNull(country);
//        repo.deleteById("123456");
//
//    }
//
//    @Test
//    public void internalRollback() {
//        //第1层
//        JpaTransaction tran = JpaTransaction.getTransaction();
//        try {
//            tran.begin();
//
//            //第2层
//            JpaTransaction tran2 = JpaTransaction.getTransaction();
//            try {
//                tran2.begin(TransactionPropagation.REQUIRED);
//
//                CountryRepository repo1 = SpringBeanUtils.getBean(CountryRepository.class);
//                Country c = new Country();
//                c.setId("123456");
//                c.setCode("123456");
//                c.setName("123456");
//                repo1.save(c);
//
//                tran2.rollback();
//            }catch (Throwable e){
//                tran2.rollback();
//            }
//
//            tran.rollback();
//        }catch (Throwable e){
//            tran.rollback();
//        }
//
//        Country country = repo.findById("123456").orElse(null);
//        Assert.assertNull(country);
//    }
//
//    /**
//     * 外层不回滚
//     */
//    @Test
//    @Transactional(rollbackFor = Exception.class)
//    @Rollback(true)
//    public void internalRollback2() {
//        //第1层
//        JpaTransaction tran = JpaTransaction.getTransaction();
//        try {
//            tran.begin(TransactionPropagation.REQUIRED);
//            CountryRepository repo1 = SpringBeanUtils.getBean(CountryRepository.class);
//            Country c = new Country();
//            c.setId("123456");
//            c.setCode("123456");
//            c.setName("123456");
//            repo1.save(c);
//            tran.commit();
//        }catch (Throwable e) {
//            tran.rollback();
//        }
//
//        Country country = repo.findById("123456").orElse(null);
//        Assert.assertNotNull(country);
//    }
//
//    /**
//     * 外层不回滚
//     */
//    @Test
//    public void resRequire_New() {
//        //第1层
//        JpaTransaction tran = JpaTransaction.getTransaction();
//        try {
//            tran.begin();
//
//            for(int i=0;i<10;i++) {
//                //第2层
//                JpaTransaction tran2 = JpaTransaction.getTransaction();
//                try {
//                    tran2.begin(TransactionPropagation.REQUIRED);
//                    CountryRepository repo1 = SpringBeanUtils.getBean(CountryRepository.class);
//                    Country c = new Country();
//                    c.setId("123456");
//                    c.setCode("123456");
//                    c.setName("123456");
//                    repo1.save(c);
//                    tran2.rollback();
//                } catch (Throwable e) {
//                    tran2.rollback();
//                }
//            }
//
//            tran.rollback();
//        }catch (Throwable e){
//            tran.rollback();
//        }
////
////        io.iec.edp.caf.commons.transaction.Country country = repo.findById("123456").get();
////        Assert.assertNotNull(country);
//    }
//}