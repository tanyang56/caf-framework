/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.jdbc.connection;

import org.springframework.util.Assert;

import java.sql.*;

/**
 * 直接需要操作Connection进行数据库操作的方法集
 * 封装的Connection和DatabaseMetaData方法
 */
public class ConnectionOperator {
    private final Connection connection;
    private final DatabaseMetaData databaseMetaData;

    public ConnectionOperator(Connection connection)throws SQLException{
        this.connection = connection;
        this.databaseMetaData = connection.getMetaData();
    }

    /**
     * 运行存储过程
     * @param sql
     * @param resultSetType 可缺省
     * @param resultSetConcurrency 可缺省
     * @param resultSetHoldability 可缺省
     * @return CallableStatement
     * @throws SQLException
     */
    public CallableStatement prepareCall(String sql,
                                         int resultSetType,
                                         int resultSetConcurrency,
                                         int resultSetHoldability)throws SQLException{
        Assert.isTrue(!connection.isClosed(), "Connection is already closed.");
        return connection.prepareCall(sql, resultSetType, resultSetConcurrency,resultSetHoldability);
    }

    public CallableStatement prepareCall(String sql,
                                         int resultSetType,
                                         int resultSetConcurrency)throws SQLException{
        Assert.isTrue(!connection.isClosed(), "Connection is already closed.");
        return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
    }
    public CallableStatement prepareCall(String sql)throws SQLException{
        Assert.isTrue(!connection.isClosed(), "Connection is already closed.");
        return connection.prepareCall(sql);
    }

    /**
     * 获取表信息
     * @param catalog
     * @param schemaPattern
     * @param tableNamePattern
     * @param types
     * @return ResultSet
     * @throws SQLException
     */
    public ResultSet getTables(String catalog, String schemaPattern,
                               String tableNamePattern, String types[])throws SQLException{
        return databaseMetaData.getTables(catalog, schemaPattern,tableNamePattern,types);
    }

    /**
     *
     * @param catalog
     * @param schemaPattern
     * @param procedureNamePattern
     * @return
     * @throws SQLException
     */
    public ResultSet getProcedures(String catalog, String schemaPattern,
                                   String procedureNamePattern)throws  SQLException{
        return databaseMetaData.getProcedures(catalog,schemaPattern,procedureNamePattern);
    }

    /**
     *
     * @param catalog
     * @param schemaPattern
     * @param procedureNamePattern
     * @param columnNamePattern
     * @return
     * @throws SQLException
     */
    public ResultSet getProcedureColumns(String catalog,
                                         String schemaPattern,
                                         String procedureNamePattern,
                                         String columnNamePattern)throws  SQLException{

        return databaseMetaData.getProcedureColumns(catalog,schemaPattern, procedureNamePattern,columnNamePattern);
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public String getDatabaseProductName()throws SQLException{
        return databaseMetaData.getDatabaseProductName();
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public String getDatabaseProductVersion()throws SQLException{
        return databaseMetaData.getDatabaseProductVersion();
    }
    /**
     *
     * @return
     * @throws SQLException
     */
    public String getSearchStringEscape()throws SQLException{
        return databaseMetaData.getSearchStringEscape();
    }



}
