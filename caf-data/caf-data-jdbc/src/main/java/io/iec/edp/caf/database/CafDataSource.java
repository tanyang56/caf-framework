/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.database;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.SmartDataSource;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * 内部类 包级私有 封装connection
 */
@Deprecated
@Slf4j
class CafDataSource implements SmartDataSource {
    private final Connection connection;

    public CafDataSource(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.connection;
    }

    @Override
    public Connection getConnection(String s, String s1) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public <T> T unwrap(Class<T> aClass) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public void setLogWriter(PrintWriter printWriter) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public void setLoginTimeout(int i) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("Unsupported method");
    }

    @Override
    public boolean shouldClose(Connection con) {
        return false;
    }

    void release() {
        try {
            if(this.connection!=null) {
                this.connection.close();
                if(log.isInfoEnabled())
                    log.info("线程"+Thread.currentThread().getId()+"连接已手动关闭");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
