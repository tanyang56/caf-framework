//package io.iec.edp.caf.database;
//
//import lombok.SneakyThrows;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.sql.SQLException;
//
//@RestController
//@RequestMapping("datatest")
//public class TestController {
//
//    @GetMapping("testdatabase")
//    public void testdatabase() throws SQLException {
//        for(int i=0;i<10;i++) {
//            new Thread(new Runnable() {
//                @SneakyThrows
//                @Override
//                public void run() {
//                    DatabaseManager dbm = DatabaseManager.getInstance();
//                    dbm.begin();
//                    Database db = dbm.getDatabase();
//                    try {
//
//                        db.beginTrans();
//                        db.execute("create TEMP TABLE  pg_temp_01(keys int PRIMARY key) ON COMMIT DROP");
//                        db.execute("INSERT into pg_temp_01 values(1)");
//
////                db.execute("create TEMP TABLE  pg_temp_01(keys int PRIMARY key) ON COMMIT PRESERVE ROWS");
//                        db.commit();
//
//                    } catch (SQLException var7) {
//                        db.rollBack();
//                        throw new RuntimeException(var7);
//                    } finally {
//                        dbm.end();
//                    }
//                }
//            }).start();
//
//        }
//    }
//
//
//    @GetMapping("testmultithread")
//    public void testmultithread() throws SQLException {
//        DatabaseManager dbm = DatabaseManager.getInstance();
//        dbm.begin();
//        Database db = dbm.getDatabase();
//
//        try
//        {
//            db.beginTrans();
//
//            for(int i=0;i<50;i++) {
//                int finalI = i;
//                new Thread(new Runnable() {
//                    @SneakyThrows
//                    @Override
//                    public void run() {
//                        if(finalI==87){
//                            db.rollBack();
//                        }else{
//                            db.execute("INSERT into rxbtest values('"+ finalI +"')");
//                        }
//                    }
//                }).start();
//            }
//
//            db.commit();
//        }catch (Throwable e){
//            db.rollBack();
//        }finally {
//            dbm.end();
//        }
//
//    }
//
//    @GetMapping("testmultithread2")
//    public void testmultithread2() throws SQLException {
//        for(int i=0;i<50;i++) {
//            int finalI = i;
//            new Thread(new Runnable() {
//                @SneakyThrows
//                @Override
//                public void run() {
//                    DatabaseManager dbm = DatabaseManager.getInstance();
//                    dbm.begin();
//                    Database db = dbm.getDatabase();
//                    db.beginTrans();
//                    try{
//                        if(finalI==87){
//                            db.rollBack();
//                        }else{
//                            db.execute("INSERT into rxbtest values('"+ finalI +"')");
//                        }
//                        db.commit();
//                    }catch (Exception e){
//                        db.rollBack();
//                    }finally {
//                        dbm.end();
//                    }
//
//
//                }
//            }).start();
//        }
//    }
//
//
//    @GetMapping("testmultithread3")
//    public void testmultithread3() throws SQLException {
//        DatabaseManager dbm = DatabaseManager.getInstance();
//        dbm.begin();
//        Database db = dbm.getDatabase();
//
//        try
//        {
//            db.beginTrans();
//
//            for(int i=0;i<50;i++) {
//                int finalI = i;
//                new Thread(new Runnable() {
//                    @SneakyThrows
//                    @Override
//                    public void run() {
//                        db.execute("INSERT into rxbtest values('"+ finalI +"')");
//                    }
//                }).start();
//            }
//
//            db.commit();
//        }catch (Throwable e){
//            db.rollBack();
//        }finally {
//            dbm.end();
//        }
//
//    }
//
//    @GetMapping("testmultithread4")
//    public void testmultithread4() throws SQLException {
//        for(int i=0;i<50;i++) {
//            int finalI = i;
//            new Thread(new Runnable() {
//                @SneakyThrows
//                @Override
//                public void run() {
//                    DatabaseManager dbm = DatabaseManager.getInstance();
//                    dbm.begin();
//                    Database db = dbm.getDatabase();
//                    db.beginTrans();
//                    try{
//                        db.execute("INSERT into rxbtest values('"+ finalI +"')");
//                        db.commit();
//                    }catch (Exception e){
//                        db.rollBack();
//                    }finally {
//                        dbm.end();
//                    }
//                }
//            }).start();
//        }
//    }
//
//}
