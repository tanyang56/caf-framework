/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

import io.iec.edp.caf.commons.dataaccess.DbType;

/**
 * @author wangyandong
 * @date 2021/05/19 16:15
 *
 */
public class DataSourceTypeRecognizer {
    /**
     * 根据JdbcUrl识别数据库类型
     *
     * @param jdbcUrl
     * @return
     */
    public static DbType recognize(String jdbcUrl) {
        if (jdbcUrl.indexOf("oracle:") != -1) {
            return DbType.Oracle;
        } else if (jdbcUrl.indexOf("sqlserver:") != -1) {
            return DbType.SQLServer;
        } else if (jdbcUrl.indexOf("postgresql:") != -1) {
            return DbType.PgSQL;
        } else if (jdbcUrl.indexOf("dm:") != -1) {
            return DbType.DM;
        } else if (jdbcUrl.indexOf("highgo:") != -1) {
            return DbType.HighGo;
        } else if (jdbcUrl.indexOf("oscar:") != -1) {
            return DbType.Oscar;
        } else if (jdbcUrl.indexOf("kingbase") != -1) {
            return DbType.Kingbase;
        } else if (jdbcUrl.indexOf("gbase:") != -1) {
            return DbType.Gbase;
        } else if (jdbcUrl.indexOf("mysql:") != -1) {
            return DbType.MySQL;
        } else if (jdbcUrl.indexOf("db2:") != -1) {
            return DbType.DB2;
        } else if (jdbcUrl.indexOf("opengauss:") !=-1){
            //可能判断不了 og使用的pg的url
            return DbType.OpenGauss;
        } else if(jdbcUrl.indexOf("oceanbase") !=-1){
            return DbType.OceanBase;
        } else if(jdbcUrl.indexOf("gbase8s") !=-1){
            return DbType.GBase8s;
        } else if(jdbcUrl.indexOf("gbase8c") !=-1){
            return DbType.GBase8c;
        }
        return DbType.Unknown;
    }
}
