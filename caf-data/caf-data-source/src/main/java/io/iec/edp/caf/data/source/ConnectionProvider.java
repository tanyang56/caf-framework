/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 多租户数据源提供程序
 * @author wangyandong
 * @date 2021/05/13 13:18
 *
 */
public interface ConnectionProvider {

    DataSource getDataSource(String identifier);

    /**
     * 根据标识符获取连接
     * @param identifier 标识符
     * @return
     * @throws SQLException
     */
    Connection getConnection(String identifier) throws SQLException;

    /**
     * 关闭连接
     * @param conn
     * @throws SQLException
     */
    void releaseConnection(Connection conn) throws SQLException;

    /**
     * 是否支持积极的释放
     * @return
     */
    boolean supportsAggressiveRelease();
}