/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

/**
 * Common class for all {@link UriBasedServiceInfo}s
 *
 * @author Ramnivas Laddad
 *
 */
public abstract class UriBasedServiceInfo {
	private UriInfo uriInfo;

	private static UriInfoFactory uriFactory = new StandardUriInfoFactory();

	public UriBasedServiceInfo(String scheme, String host, int port, String username, String password, String path, String query) {
		this.uriInfo = getUriInfoFactory().createUri(scheme, host, port, username, password, path, query);
		this.uriInfo = validateAndCleanUriInfo(uriInfo);
	}

	public UriBasedServiceInfo(String uriString) {
		this.uriInfo = getUriInfoFactory().createUri(uriString);
		this.uriInfo = validateAndCleanUriInfo(uriInfo);
	}

	/**
	 * For URI-based (@link ServiceInfo}s which don't conform to the standard URI
	 * format, override this method in your own ServiceInfo class to return a {@link UriInfoFactory} which will create the
	 * appropriate URIs.
	 *
	 * @return your special UriInfoFactory
	 */
	public UriInfoFactory getUriInfoFactory() {
		return uriFactory;
	}

	/**
	 * 返回Uri
	 * @return
	 */
	public String getUri() {
		return uriInfo.getUriString();
	}

	public String getUserName() {
		return uriInfo.getUserName();
	}

	public String getPassword() {
		return uriInfo.getPassword();
	}

	public String getHost() {
		return uriInfo.getHost();
	}

	public int getPort() {
		return uriInfo.getPort();
	}

	public String getPath() {
		return uriInfo.getPath();
	}

	public String getQuery() {
		return uriInfo.getQuery();
	}

	/**
	 * 协议
	 * @return
	 */
	public String getScheme() {
		return uriInfo.getScheme();
	}

	/**
	 * Validate the URI and clean it up by using defaults for any missing information, if possible.
	 *
	 * @param uriInfo
	 *            uri info based on parsed payload
	 * @return cleaned up uri info
	 */
	protected UriInfo validateAndCleanUriInfo(UriInfo uriInfo) {
		return uriInfo;
	}

	protected UriInfo getUriInfo() {
		return uriInfo;
	}

	@Override
	public String toString() {
		// TODO: when using a simple URI string (see comments in getUri), the result of uriInfo.getUriString()
		//       would display the password which does not seem ideal.
		return getClass().getSimpleName() + "[" + getScheme() + "://" + getUserName() + ":****@" + getHost() + ":" + getPort()
			+ "/" + getPath() + "]";
	}
}
