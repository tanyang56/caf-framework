/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.gauss;

import io.iec.edp.caf.data.source.RelationalServiceInfo;

public class OpenGaussServiceInfo extends RelationalServiceInfo {
    public static final String OPENGAUSS_SCHEME = "opengauss";
    public static final String OPENGAUSS_DRIVER_CLASS_NAME = "org.opengauss.Driver";

    public OpenGaussServiceInfo(String uriString) {
        super(uriString);
    }

    public OpenGaussServiceInfo(String host, int port, String username, String password, String path, String query) {
        super(OPENGAUSS_SCHEME, host, port, username, password, path, query);
    }

    /**
     * 高斯数据库url使用的schema是postgresql
     * @return
     */
    @Override
    protected String buildJdbcUrl() {
        return String.format("%s%s://%s%s/%s%s", JDBC_PREFIX, OPENGAUSS_SCHEME, getHost(), formatPort(),
                getPath(),formatQuery());
    }
    /**
     * 获取驱动类名
     * @return
     */
    @Override
    public String getDriverClassName(){
        return OPENGAUSS_DRIVER_CLASS_NAME;
    }
}
