/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess;

import lombok.Data;

/**
 * 简单数据库连接配置
 * @author wangyandong
 * @date 2020/04/22 下午 03:24
 *
 */
@Data
public class SimpleDbConfigData {
    /// <summary>
    /// 编号。
    /// </summary>
    private String id;


    /// <summary>
    /// 数据库类型描述。
    /// </summary>
    private DbType dbType;


    /// <summary>
    /// 加密后的数据库连接字符串
    /// </summary>
    private String ConnectionString;
}
