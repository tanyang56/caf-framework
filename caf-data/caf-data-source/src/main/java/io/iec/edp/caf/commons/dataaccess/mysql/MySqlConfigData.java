/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.mysql;

import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.dataaccess.GSPDbConfigData;

/**
 *
 * @author wangyandong
 * @date 2020/06/05 09:45
 *
 */
public class MySqlConfigData extends GSPDbConfigData {

    //private final String DefaultPort = "3306";

    /// <summary>
    /// 构造函数。
    /// </summary>
    public MySqlConfigData()
    {
        super();
        this.setDbType(DbType.MySQL);
    }
}