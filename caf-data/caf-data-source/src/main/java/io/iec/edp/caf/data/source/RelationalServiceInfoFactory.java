/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.data.source.gauss.OpenGaussServiceInfo;
import io.iec.edp.caf.data.source.gbase8c.Gbase8cServiceInfo;
import io.iec.edp.caf.data.source.gbase8s.Gbase8sServiceInfo;
import io.iec.edp.caf.data.source.oracle.OracleServiceInfo;
import io.iec.edp.caf.data.source.oscar.OscarServiceInfo;
import io.iec.edp.caf.data.source.pgsql.PostgresqlServiceInfo;
import io.iec.edp.caf.data.source.sqlserver.SqlServerServiceInfo;
import io.iec.edp.caf.data.source.db2.DB2ServiceInfo;
import io.iec.edp.caf.data.source.dm.DmServiceInfo;
import io.iec.edp.caf.data.source.gbase.GbaseServiceInfo;
import io.iec.edp.caf.data.source.highgo.HighGoServiceInfo;
import io.iec.edp.caf.data.source.kingbase.KingbaseServiceInfo;
import io.iec.edp.caf.data.source.mysql.MysqlServiceInfo;
import io.iec.edp.caf.data.source.ocean.OceanBaseServiceInfo;
import lombok.extern.slf4j.Slf4j;

/**
 * 关系型数据源创建工厂
 * @author wangyandong
 * @date 2021/05/02 15:52
 *
 */
@Slf4j
public class RelationalServiceInfoFactory {

    /**
     * 根据
     * @param uriString
     * @return
     */
    public static RelationalServiceInfo create(String uriString){
        UriInfo uriInfo = new UriInfo(uriString);
        RelationalServiceInfo serviceInfo = null;
        switch (uriInfo.getScheme()){
            case DB2ServiceInfo.DB2_SCHEME:
                serviceInfo = new DB2ServiceInfo(uriString);
                break;
            case DmServiceInfo.DM_SCHEME:
                serviceInfo = new DmServiceInfo(uriString);
                break;
            case GbaseServiceInfo.GBASE_SCHEME:
                serviceInfo = new GbaseServiceInfo(uriString);
                break;
            case KingbaseServiceInfo.KINGBASE_SCHEME:
                serviceInfo = new KingbaseServiceInfo(uriString);
                break;
            case MysqlServiceInfo.MYSQL_SCHEME:
                serviceInfo = new MysqlServiceInfo(uriString);
                break;
            case OracleServiceInfo.ORACLE_SCHEME:
                serviceInfo = new OracleServiceInfo(uriString);
                break;
            case OscarServiceInfo.OSCAR_SCHEME:
                serviceInfo = new OscarServiceInfo(uriString);
                break;
            case PostgresqlServiceInfo.POSTGRES_SCHEME:
                serviceInfo = new PostgresqlServiceInfo(uriString);
                break;
            case SqlServerServiceInfo.SQLSERVER_SCHEME:
                serviceInfo = new SqlServerServiceInfo(uriString);
                break;
            case HighGoServiceInfo.HIGHGO_SCHEME:
                serviceInfo = new HighGoServiceInfo(uriString);
                break;
            case OpenGaussServiceInfo.OPENGAUSS_SCHEME:
                serviceInfo = new OpenGaussServiceInfo(uriString);
                break;
            case OceanBaseServiceInfo .OCEANBASE_SCHEME:
                serviceInfo = new OceanBaseServiceInfo(uriString);
                break;
            case Gbase8sServiceInfo.GBASE8S_SCHEME:
                serviceInfo = new Gbase8sServiceInfo(uriString);
                break;
            case Gbase8cServiceInfo.GBASE8C_SCHEME:
                serviceInfo = new Gbase8cServiceInfo(uriString);
                break;
        }
        return serviceInfo;
    }

    /**
     * 根据各属性信息构造关系型数据库的连接信息
     * @param dbType 数据库类型
     * @param host 主机
     * @param port 端口
     * @param userName 用户名
     * @param password 密码
     * @param database
     * @param query
     * @return
     */
    public static RelationalServiceInfo create(
            DbType dbType, String host, int port, String userName, String password,
            String database, String query){
        RelationalServiceInfo serviceInfo = null;
        log.error("dbytype:"+dbType);
        switch (dbType){
            case DB2:
                serviceInfo = new DB2ServiceInfo(host,port,userName,password,database,query);
                break;
            case DM:
                serviceInfo = new DmServiceInfo(host,port,userName,password,database,query);
                break;
            case Gbase:
                serviceInfo = new GbaseServiceInfo(host,port,userName,password,database,query);
                break;
            case Kingbase:
                serviceInfo = new KingbaseServiceInfo(host,port,userName,password,database,query);
                break;
            case MySQL:
                serviceInfo = new MysqlServiceInfo(host,port,userName,password,database,query);
                break;
            case Oracle:
                serviceInfo = new OracleServiceInfo(host,port,userName,password,database,query);
                break;
            case Oscar:
                serviceInfo = new OscarServiceInfo(host,port,userName,password,database,query);
                break;
            case PgSQL:
                serviceInfo = new PostgresqlServiceInfo(host,port,userName,password,database,query);
                break;
            case SQLServer:
                serviceInfo = new SqlServerServiceInfo(host,port,userName,password,database,query);
                break;
            case HighGo:
                serviceInfo = new HighGoServiceInfo(host,port,userName,password,database,query);
                break;
            case OpenGauss:
                serviceInfo = new OpenGaussServiceInfo(host,port,userName,password,database,query);
                break;
            case OceanBase:
                serviceInfo = new OceanBaseServiceInfo(host,port,userName,password,database,query);
                break;
            case GBase8s:
                serviceInfo = new Gbase8sServiceInfo(host,port,userName,password,database,query);
                break;
            case GBase8c:
                serviceInfo = new Gbase8cServiceInfo(host,port,userName,password,database,query);
                break;
        }
        return serviceInfo;
    }
}
