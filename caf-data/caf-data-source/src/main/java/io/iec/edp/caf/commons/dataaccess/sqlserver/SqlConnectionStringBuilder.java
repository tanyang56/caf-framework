/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.sqlserver;

import io.iec.edp.caf.commons.dataaccess.DbConnectionStringBuilder;

/**
 * @author wangyandong
 * @date 2020/04/22 下午 03:13
 *
 */
public class SqlConnectionStringBuilder extends DbConnectionStringBuilder {
    @Override
    public String getServer(){
        return (String)this.get("Data Source");
    }
    @Override
    public String getDatabase(){
        return (String)this.get("initial catalog");
    }
    @Override
    public String getUser(){
        return (String)this.get("User Id");
    }

    @Override
    public int getPoolSize(){
        Object o = this.get("Max Pool Size");
        if(o==null)
            return 10;
        return Integer.parseInt(o.toString());
    }

    /**
     * 构造函数
     * @param connectionString
     */
    public SqlConnectionStringBuilder(String connectionString){
        super(connectionString);
    }
}
