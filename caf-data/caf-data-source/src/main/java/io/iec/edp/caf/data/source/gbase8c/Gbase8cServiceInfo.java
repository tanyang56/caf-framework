package io.iec.edp.caf.data.source.gbase8c;

import io.iec.edp.caf.data.source.RelationalServiceInfo;

public class Gbase8cServiceInfo extends RelationalServiceInfo {
    public static final String GBASE8C_SCHEME = "gbase8c";
    public static final String GBASE8C_DRIVER_CLASS_NAME = "cn.gbase8c.Driver";

    public Gbase8cServiceInfo(String host, int port, String username, String password, String path, String query) {
        super(GBASE8C_SCHEME, host, port, username, password, path, query);
    }

    public Gbase8cServiceInfo(String uriString) {
        super(uriString);
    }

    /**
     * gbase8c数据库url
     * @return
     */
    @Override
    protected String buildJdbcUrl() {
        return String.format("%s%s://%s%s/%s%s", JDBC_PREFIX, GBASE8C_SCHEME, getHost(), formatPort(),
                getPath(),formatQuery());
    }

    @Override
    public String getDriverClassName() {
        return GBASE8C_DRIVER_CLASS_NAME;
    }
}
