/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

/**
 * 关系型数据库服务描述信息
 * @author Ramnivas Laddad
 * @author Scott Frederick
 */
public abstract class RelationalServiceInfo extends UriBasedServiceInfo {

	public static final String JDBC_PREFIX = "jdbc:";

	protected final String jdbcUrl;
	protected final String scheme;

	/**
	 *
	 * @param scheme
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @param path
	 */
	public RelationalServiceInfo(String scheme, String host, int port, String username, String password, String path,
								 String query) {
		super(scheme, host, port, username, password, path, query);
		this.scheme = scheme;
		this.jdbcUrl = this.buildJdbcUrl();
	}

	/**
	 * 构造函数
	 * @param uriString
	 */
	public RelationalServiceInfo(String uriString) {
		super(uriString);
		this.scheme = this.getScheme();
		this.jdbcUrl = this.buildJdbcUrl();
	}

	/**
	 * 获取驱动类名
	 * @return
	 */
	public abstract String getDriverClassName();

	/**
	 * 返回JdbcUrl
	 * @return
	 */
	public String getJdbcUrl() {
		return jdbcUrl == null ? buildJdbcUrl() : jdbcUrl;
	}

	/**
	 * 返回加密后的JdbcUrl
	 * @return
	 */
	public String getEncryptedJdbcUrl() {
		return String.format("%s%s", JDBC_PREFIX, UriEncryptor.encrypt(getUri()));
	}

	protected String buildJdbcUrl() {
		return String.format("%s%s://%s%s/%s%s", JDBC_PREFIX, scheme, getHost(), formatPort(),
				getPath(),formatQuery());
	}

	protected String formatPort() {
		if (getPort() != -1) {
			return String.format(":%d", getPort());
		}
		return "";
	}

	protected String formatQuery() {
		if (getQuery() != null) {
			return String.format("?%s", getQuery());
		}
		return "";
	}
}
