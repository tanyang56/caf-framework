/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source.dm;

import io.iec.edp.caf.data.source.RelationalServiceInfo;

/**
 * 达梦数据库连接信息
 */
public class DmServiceInfo extends RelationalServiceInfo {
	public static final String DM_SCHEME = "dm";
	public static final String DM_DRIVER_CLASS_NAME = "dm.jdbc.driver.DmDriver";
	public DmServiceInfo(String uriString) {
		super(uriString);
	}

	public DmServiceInfo(String host, int port, String username, String password, String path, String query) {
		super(DM_SCHEME, host, port, username, password, path, query);
	}

	@Override
	protected String buildJdbcUrl() {
		return String.format("jdbc:%s://%s:%d",
				DM_SCHEME,
				getHost(), getPort());
	}


	/**
	 * 获取驱动类名
	 * @return
	 */
	@Override
	public String getDriverClassName(){
		return DM_DRIVER_CLASS_NAME;
	}
}
