/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess;

/**
 * 数据库类型
 * @author wangyandong
 * @date 2019/10/23 16:53
 *
 */
//todo 被com.inspur.edp.cef.repository.adaptor.KeyWordsManager依赖
public enum DbType {
    SQLServer,
    Oracle,
    PgSQL,
    MySQL,
    DM,
    HighGo,
    Gbase,
    Kingbase,
    Oscar,
    DB2,
    OpenGauss,
    OceanBase,
    GBase8s,
    GBase8c,
    Unknown(-1);

    private DbType(int i) {
    }

    private DbType() {
    }
}
