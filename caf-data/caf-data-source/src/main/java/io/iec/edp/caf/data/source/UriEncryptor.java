/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

import io.iec.edp.caf.commons.utils.StringUtils;

/**
 * Uri加密机
 * @author wangyandong
 * @date 2021/05/02 14:46
 *
 */
class UriEncryptor {

    /**
     * 对明文UriString进行加密处理
     * @param uriString
     * @return
     */
    static String encrypt(String uriString){
        if(StringUtils.isEmpty(uriString))
            return uriString;

        return Utils.encrypt(uriString);
    }


}
