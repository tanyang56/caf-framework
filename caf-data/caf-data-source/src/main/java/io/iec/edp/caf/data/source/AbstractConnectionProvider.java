/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;

import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 多租户数据源提供程序抽象类
 * @author wangyandong
 * @date 2021/05/13 13:37
 *
 */
@Slf4j
public abstract class AbstractConnectionProvider implements ConnectionProvider {
    public AbstractConnectionProvider() {
    }

    /**
     * 选取数据源
     * @param identifier
     * @return【
     */
    protected abstract DataSource selectDataSource(String identifier);

    @Override
    public DataSource getDataSource(String identifier) {
        return this.selectDataSource(identifier);
    }

    /**
     * 根据标识符获取连接
     * @param identifier 标识符
     * @return
     * @throws SQLException
     */
    public Connection getConnection(String identifier) throws SQLException {
        log.info("\nThread "+Thread.currentThread().getId()+"is get db connection，the connection info："+this.selectDataSource(identifier).toString());
        return this.selectDataSource(identifier).getConnection();
    }

    /**
     * 关闭连接
     * @param connection
     * @throws SQLException
     */
    public void releaseConnection(Connection connection) throws SQLException {
        connection.close();
    }

    /**
     * 是否支持积极地释放连接
     * @return
     */
    public boolean supportsAggressiveRelease() {
        return true;
    }

}
