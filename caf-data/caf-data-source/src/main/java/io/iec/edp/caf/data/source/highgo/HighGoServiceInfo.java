/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source.highgo;

import io.iec.edp.caf.data.source.RelationalServiceInfo;

/**
 * 南大通用数据库连接信息
 */
public class HighGoServiceInfo extends RelationalServiceInfo {
	public static final String HIGHGO_SCHEME = "highgo";
	public static final String HIGHGO_DRIVER_CLASS_NAME = "com.highgo.jdbc.Driver";

	public HighGoServiceInfo(String uriString) {
		super(uriString);
	}

	public HighGoServiceInfo(String host, int port, String username, String password, String path, String query) {
		super(HIGHGO_SCHEME, host, port, username, password, path, query);
	}

	/**
	 * 获取驱动类名
	 * @return
	 */
	@Override
	public String getDriverClassName(){
		return HIGHGO_DRIVER_CLASS_NAME;
	}
}
