/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.dm;

import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.dataaccess.GSPDbConfigData;
import io.iec.edp.caf.data.source.Utils;

import java.text.MessageFormat;

/**
 * @author wangyandong
 * @date 2020/04/22 上午 10:46
 *
 */
public class DmDbConfigData extends GSPDbConfigData {

    //server=10.24.18.61;user id = GSNETCORE; password=1234567890;schema=GSNETCORE;port=5236;encoding=GB18030;database=DMSERVER
    private final String ConnFormat = "SERVER={0};PORT={1};USER Id={2};SCHEMA={2};PASSWORD={3};connpooling=True";
    private final String ConnFormatWithPoolSize = "SERVER={0};PORT={1};USER Id={2};SCHEMA={2};PASSWORD={3};connpooling=True;PoolSize={4}";
    /// <summary>
    /// 构造函数。
    /// </summary>
    public DmDbConfigData()
    {
        super();
        this.setDbType(DbType.DM);
    }

    //public string Schema { get; set; }

    /// <summary>
    /// 数据库连接的配置字符串。
    /// </summary>
    @Override
    public String getConnectionString(){

        String result = null;
        String source = this.getSource();
        if (source!=null && !"".equals(source)) {
            String[] sourceArr = source.split(",|:",2);
            if (sourceArr.length != 2)
                throw new IllegalArgumentException(String.format("无效的数据库源:%s", source));

            String host = sourceArr[0];
            String port = sourceArr[1];

            if (this.getMaxPoolSize() == 0)
                result = MessageFormat.format(ConnFormat, host, port, this.getUserId(), this.getPassword());
            else
                result = MessageFormat.format(ConnFormatWithPoolSize, host, port, this.getUserId(), this.getPassword(), this.getMaxPoolSize());
        }
        return result;
    }

    /// <summary>
    /// 加密后的数据库连接字符串。
    /// </summary>
    @Override
    public String getEncryptedConString() {
        String result = "";
        String source = this.getSource();
        if (source!=null && !"".equals(source)) {
            String[] sourceArr = source.split(",|:",2);
            if (sourceArr.length != 2)
                throw new IllegalArgumentException(String.format("无效的数据库源:%s", source));

            String host = Utils.encrypt(sourceArr[0]);
            String port = sourceArr[1];

            if (this.getMaxPoolSize() == 0)
                result = MessageFormat.format(ConnFormat, host, port, Utils.encrypt(this.getUserId()),
                        Utils.encrypt(this.getPassword()));
            else
                result = MessageFormat.format(ConnFormatWithPoolSize, host, port, Utils.encrypt(this.getUserId()),
                        Utils.encrypt(this.getPassword()), this.getMaxPoolSize());
        }
        return result;
    }
}
