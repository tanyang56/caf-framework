/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides a base class for strongly typed connection string builders.
 * @author wangyandong
 * @date 2020/04/22 下午 02:34
 *
 */
public class DbConnectionStringBuilder {

    private String connectionString;
    private Map<String,String> pairs = null;

    /**
     * 构造函数
     * @param connectionString
     */
    public DbConnectionStringBuilder(String connectionString){
        this.setConnectionString(connectionString);
    }

    /**
     * 分隔符
     */
    @Setter
    @Getter
    private String token = ";";

    /**
     * 根据
     * @param keyword
     * @return
     */
    public Object get(String keyword) {
        if (keyword == null || this.pairs == null)
            return null;

        String key = keyword.toLowerCase().trim();
        return this.pairs.getOrDefault(key, null);
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
        pairs = new HashMap<>();
        if (connectionString == null)
            return;

        String[] arr = connectionString.split(this.token);
        if (arr == null)
            return;

        String item = null;
        for (int i=0;i<arr.length;i++) {
            item = arr[i];
            String[] nameValue = item.split("=",2);
            if (nameValue != null && nameValue.length == 2)
                pairs.put(nameValue[0].toLowerCase().trim(), nameValue[1]);
        }
    }


    public String getServer(){
        return (String)this.get("Server");
    }

    public int getPort(){
        Object o = this.get("Port");
        if(o==null)
            return -1;
        return Integer.parseInt(o.toString());
    }

    public String getUser(){
        return (String)this.get("User");
    }

    public String getPassword(){
        return (String)this.get("Password");
    }

    public String getDatabase(){
        return (String)this.get("Database");
    }

    public int getConnectionTimeout(){
        Object o = this.get("ConnectionTimeout");
        if(o==null)
            return 30;
        return Integer.parseInt(o.toString());
    }

    public int getCommandTimeout(){
        Object o = this.get("CommandTimeout");
        if(o==null)
            return 120;
        return Integer.parseInt(o.toString());
    }

    public int getPoolSize(){
        Object o = this.get("MaxPoolSize");
        if(o==null)
            return 10;
        return Integer.parseInt(o.toString());
    }
}
