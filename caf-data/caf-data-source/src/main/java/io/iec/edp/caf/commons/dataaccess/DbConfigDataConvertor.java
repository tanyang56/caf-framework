/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess;

/**
 *
 * @author wangyandong
 * @date 2020/04/22 下午 03:22
 * @email wangyandong@inspur.com
 */
@Deprecated
public class DbConfigDataConvertor {

    /// <summary>
    /// 根据数据源信息获取IGSPDatabase接口
    /// </summary>
    /// <param name="dataSource">数据源信息</param>
    /// <returns>IGSPDatabase接口 data/Source为null返回Null</returns>
    public static GSPDbConfigData Convert(SimpleDbConfigData dataSource){
        return io.iec.edp.caf.data.source.DbConfigDataConvertor.convertFromDotNET(dataSource);
    }

    /// <summary>
    /// 将ADO.NET形式的连接字符串转为通用配置对象
    /// </summary>
    /// <param name="dataSource">数据源信息</param>
    /// <returns>IGSPDatabase接口 data/Source为null返回Null</returns>
    public static JDBCConnectionInfo Convert(GSPDbConfigData configData){
        return io.iec.edp.caf.data.source.DbConfigDataConvertor.convert(configData);
    }

    /**
     * 将configData转为JDBC的数据库连接对象
     * @param configData
     * @return
     */
    public static JDBCConnectionInfo convert(SimpleDbConfigData configData){
        return io.iec.edp.caf.data.source.DbConfigDataConvertor.convert(configData);
    }
}
