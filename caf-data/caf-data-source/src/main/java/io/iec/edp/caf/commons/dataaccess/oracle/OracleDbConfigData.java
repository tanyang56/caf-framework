/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.oracle;

import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.dataaccess.GSPDbConfigData;
import io.iec.edp.caf.data.source.Utils;

import java.text.MessageFormat;

/**
 * @author wangyandong
 * @date 2020/04/22 上午 10:47
 *
 */
public class OracleDbConfigData extends GSPDbConfigData {
    private static final String ConnFormat = "Data Source={0};Persist Security Info=True;User ID={1};Password={2};Enlist=true;POOLING=True";
    private static final String ConnFormatWithPoolSize = "Data Source={0};Persist Security Info=True;User ID={1};Password={2};Enlist=true;POOLING=True;Max Pool Size={3}";

    //private const string ConnFormat = "User ID={0};password={1};Data Source={2}";
    //private const string ConnFormatWithPoolSize = "User ID={0};password={1};Data Source={2};Max Pool Size={3}";

    /// <summary>
    /// 构造函数。
    /// </summary>
    public OracleDbConfigData() {
        super();
        this.setDbType(DbType.Oracle);
    }

    /// <summary>
    /// 数据库连接的配置字符串。
    /// </summary>
    public String getConnectionString() {
        String result = null;
        if (this.getMaxPoolSize() == 0)
            result = MessageFormat.format(ConnFormat, this.getSource(), this.getUserId(), this.getPassword());
        else
            result = MessageFormat.format(ConnFormatWithPoolSize, this.getSource(), this.getUserId(), this.getPassword(), this.getMaxPoolSize());
        return result;
    }

    /// <summary>
    /// 加密后的数据库连接字符串。
    /// </summary>
    public String getEncryptedConString() {
        String result = "";

        if (this.getMaxPoolSize() == 0)
            result = MessageFormat.format(ConnFormat, Utils.encrypt(this.getSource()),
                    Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()));
        else
            result = MessageFormat.format(ConnFormatWithPoolSize, Utils.encrypt(this.getSource()),
                    Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()), this.getMaxPoolSize());

        return result;
    }
}
