/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess;

import io.iec.edp.caf.data.source.Utils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.text.MessageFormat;

/**
 * 数据库连接配置基类，主要适用于与.NET Core兼容场景
 * @author wangyandong
 * @date 2020/04/22 上午 10:34
 *
 */
@Data
public class GSPDbConfigData {
    private final String ConnFormat = "Server={0};Database={1};User={2};Password={3};ConnectionTimeout={4}";
    private final String ConnFormatWithPoolSize = "Server={0};Database={1};User={2};Password={3};ConnectionTimeout={4};MaxPoolSize={5}";

    /// <summary>
    /// 数据库配置编号。
    /// </summary>
    private String code;

    /// <summary>
    /// 数据库类型描述。
    /// </summary>
    private DbType dbType;

    /// <summary>
    /// 用户名。
    /// </summary>
    private String userId;

    /// <summary>
    /// 明文口令
    /// </summary>
    private String password;

    /// <summary>
    /// 数据源。
    /// </summary>
    private String source;

    /// <summary>
    /// 服务提供者。
    /// </summary>
    private String provider;

    /// <summary>
    /// 指代数据库名。
    /// </summary>
    private String catalog;

    ///<summary>
    ///指定数据库连接TimeOut
    ///</summary>
    private int connectTimeout = 30;

    ///<summary>
    ///指定sql执行TimeOut
    ///</summary>
    private int commandTimeout = 30;

    ///<summary>
    ///指定连接池最大连接数
    ///默认值为0，即外部不指定，此时池大小依赖于ADO.NET的默认值
    ///</summary>
    private int maxPoolSize = 100;

    /// <summary>
    /// 数据库配置名称。
    /// </summary>
    private String description;

    /// <summary>
    /// 数据库连接字符串。
    /// </summary>
    @Setter(AccessLevel.NONE)
    private String connectionString;

    /// <summary>
    /// 加密后的数据库连接字符串。
    /// </summary>
    @Setter(AccessLevel.NONE)
    private String encryptedConString;

    /// <summary>
    /// 数据库连接的配置字符串。
    /// </summary>
    public String getConnectionString() {
        String result = this.connectionString;

        if (this.getMaxPoolSize() == 0)
            result = MessageFormat.format(ConnFormat, this.getSource(), this.getUserId(), this.getPassword());
        else
            result = MessageFormat.format(ConnFormatWithPoolSize, this.getSource(), this.getUserId(), this.getPassword(), this.getMaxPoolSize());

        return result;
    }

    /// <summary>
    /// 加密后的数据库连接字符串。
    /// 加密时将 Server:Port 整体加密，解密时需要整体解密再分解开
    /// </summary>
    public String getEncryptedConString() {
        String result = "";

        if (this.getMaxPoolSize() == 0)
            result = MessageFormat.format(ConnFormat, Utils.encrypt(this.getSource()),Utils.encrypt(this.getCatalog()),
                    Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()),this.getConnectTimeout());
        else
            result = MessageFormat.format(ConnFormat, Utils.encrypt(this.getSource()),Utils.encrypt(this.getCatalog()),
                    Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()),this.getConnectTimeout(),
                    this.getMaxPoolSize());

        return result;
    }
}
