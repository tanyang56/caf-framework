/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.source;


/**
 * An interface implemented in order to create {@link UriInfo}s.
 *
 * If your {@link} needs to deal with special URIs and you are extending
 * { UriBasedServiceInfo} then you can implement this factory interface in order
 * to return a custom factory from your ServiceInfo by overriding
 * { UriBasedServiceInfo#getUriInfoFactory()}.
 *
 * @author Jens Deppe
 */
public interface UriInfoFactory {

	/**
	 * Create a {@link UriInfo} based on a URI string
	 *
	 * @param uriString the URI string to parse
	 * @return a {@link UriInfo}
	 */
	UriInfo createUri(String uriString);

	/**
	 * Create a {@link UriInfo} based on explicit components of the URI
	 *
	 * @param scheme   the URI scheme for this service
	 * @param host     the host for this service
	 * @param port     the port for this service
	 * @param username the authentication username for this service
	 * @param password the authentication password for this service
	 * @param path     the path to this service resource on the server
	 * @param query    the parameter for this service
	 * @return a {@link UriInfo}
	 */
	UriInfo createUri(String scheme, String host, int port, String username, String password, String path, String query);
}
