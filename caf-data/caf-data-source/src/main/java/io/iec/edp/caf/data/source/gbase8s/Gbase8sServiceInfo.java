package io.iec.edp.caf.data.source.gbase8s;

import io.iec.edp.caf.data.source.RelationalServiceInfo;

public class Gbase8sServiceInfo extends RelationalServiceInfo {
    public static final String GBASE8S_SCHEME = "gbase8s";
    public static final String GBASE8S_DRIVER_CLASS_NAME = "cn.gbase8s.Driver";


    public Gbase8sServiceInfo(String host, int port, String username, String password, String path, String query) {
        super(GBASE8S_SCHEME, host, port, username, password, path, query);
    }

    public Gbase8sServiceInfo(String uriString) {
        super(uriString);
    }

    /**
     * 高斯数据库url使用的schema是postgresql
     * @return
     */
    @Override
    protected String buildJdbcUrl() {
        return String.format("%s%s://%s%s/%s%s", JDBC_PREFIX, GBASE8S_SCHEME, getHost(), formatPort(),
                getPath(),formatQuery());
    }

    @Override
    public String getDriverClassName() {
        return GBASE8S_DRIVER_CLASS_NAME;
    }
}
