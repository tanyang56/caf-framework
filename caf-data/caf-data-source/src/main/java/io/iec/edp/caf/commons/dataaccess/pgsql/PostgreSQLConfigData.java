/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.dataaccess.pgsql;

import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.dataaccess.GSPDbConfigData;
import io.iec.edp.caf.data.source.Utils;

import java.text.MessageFormat;

/**
 * @author wangyandong
 * @date 2020/04/22 下午 01:42
 *
 */
public class PostgreSQLConfigData extends GSPDbConfigData {
    private static final String DefaultPort = "5432";
    private static final String ConnFormat = "Server={0};Port={1};Database={2};User Id={3};Password={4};Enlist=true;Pooling=True;";
    private static final String ConnFormatWithPoolSize = "Server={0};Port={1};Database={2};User Id={3};Password={4};MaxPoolSize={5};Pooling=True;Enlist=true";
    /// <summary>
    /// 构造函数。
    /// </summary>
    public PostgreSQLConfigData(){
        super();
        this.setDbType(DbType.PgSQL);
    }

    /// <summary>
    /// 数据库连接的配置字符串。
    /// </summary>
    @Override
    public String getConnectionString() {

        String result = null;
        String source = this.getSource();
        if (source!=null && !"".equals(source)) {
            String[] sourceArr = source.split(",|:",2);
            if (sourceArr.length != 2)
                throw new IllegalArgumentException(String.format("无效的数据库源:%s", source));

            String host = sourceArr[0];
            String port = sourceArr[1];

            if (this.getMaxPoolSize() == 0)
                result = MessageFormat.format(ConnFormat, host, port, this.getCatalog(), this.getUserId(), this.getPassword());
            else
                result = MessageFormat.format(ConnFormatWithPoolSize, host, port, this.getCatalog(), this.getUserId(), this.getPassword(), this.getMaxPoolSize());
        }
        return result;
    }
    /// <summary>
    /// 加密后的数据库连接字符串。
    /// </summary>
    @Override
    public String getEncryptedConString() {
        String result = "";
        String source = this.getSource();
        if (source != null && !"".equals(source)) {
            String[] sourceArr = source.split(",|:", 2);
            if (sourceArr.length != 2)
                throw new IllegalArgumentException(String.format("无效的数据库源:%s", source));

            String host = Utils.encrypt(sourceArr[0]);
            String port = sourceArr[1];

            if (this.getMaxPoolSize() == 0)
                result = MessageFormat.format(ConnFormat, host, port, Utils.encrypt(this.getCatalog()),
                        Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()));
            else
                result = MessageFormat.format(ConnFormatWithPoolSize, host, port, Utils.encrypt(this.getCatalog()),
                        Utils.encrypt(this.getUserId()), Utils.encrypt(this.getPassword()), this.getMaxPoolSize());
        }
        return result;
    }
}
