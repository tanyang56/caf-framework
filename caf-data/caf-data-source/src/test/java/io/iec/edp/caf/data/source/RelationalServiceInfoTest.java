///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.source;
//
//import io.iec.edp.caf.commons.dataaccess.DbType;
//import io.iec.edp.caf.commons.dataaccess.JDBCConnectionInfo;
//import io.iec.edp.caf.commons.dataaccess.SimpleDbConfigData;
//import io.iec.edp.caf.data.source.mysql.MysqlServiceInfo;
//import io.iec.edp.caf.data.source.pgsql.PostgresqlServiceInfo;
//import org.junit.Test;
//import org.springframework.util.Assert;
//
///**
// * 测试关系型数据库测试
// * @author wangyandong
// * @date 2021/05/01 15:31
// *
// */
//public class RelationalServiceInfoTest {
//    @Test
//    public void mySqlTest1(){
//        MysqlServiceInfo info = new MysqlServiceInfo("127.0.0.1",3305,"iGIXB6","aaaaaa","iGIXB6","useUnicode=true&characterEncoding=utf8");
//        System.out.println(info.getUri());
//    }
//
//    @Test
//    public void mySqlTest2(){
//        String uriString="mysql://iGIXB6:aaaaaa@localhost/testDB?useUnicode=true&characterEncoding=utf8";
//        MysqlServiceInfo info = new MysqlServiceInfo(uriString);
//        System.out.println(info.getJdbcUrl());
//    }
//
//    @Test
//    public void uriStringEncryptAndDecrypt(){
//        PostgresqlServiceInfo info = (PostgresqlServiceInfo)RelationalServiceInfoFactory.create(
//          DbType.PgSQL,"127.0.0.1", 5432,"iGIXB6","123456a?:/+-%&!","iGIXB6",null
//        );
//        String encryptedJdbcUrl = info.getEncryptedJdbcUrl();
//
//        SimpleDbConfigData configData = new SimpleDbConfigData();
//        configData.setDbType(DbType.PgSQL);
//        configData.setId("aaa");
//        configData.setConnectionString(encryptedJdbcUrl);
//        JDBCConnectionInfo connectionInfo = DbConfigDataConvertor.convert(configData);
//
//        Assert.isTrue(!info.getJdbcUrl().equals(encryptedJdbcUrl));
//        Assert.isTrue(connectionInfo.getUserName().equals(info.getUserName()));
//        Assert.isTrue(connectionInfo.getPassword().equals(info.getPassword()));
//        Assert.isTrue(connectionInfo.getJdbcUrl().equals(info.getJdbcUrl()));
//    }
//}
