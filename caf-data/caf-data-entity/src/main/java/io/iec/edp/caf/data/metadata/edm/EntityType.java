/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.edm;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 实体类型描述（包含主键）
 * @author wangyandong
 * @date 2020/12/28 17:12
 *
 */

@Data
@Deprecated
//todo 被预警依赖
public class EntityType {

    /**
     * 属性名
     */
    private String code;

    /**
     * 实体语义化名
     */
    private String name;

    /**
     * 是否是主实体
     */
    private boolean master;

    /// <summary>Gets the list of properties for this <see cref="T:System.Data.Metadata.Edm.EntityType" />.</summary>
    /// <returns>A collection of type <see cref="T:System.Data.Metadata.Edm.ReadOnlyMetadataCollection`1" /> that contains the list of properties for this <see cref="T:System.Data.Metadata.Edm.EntityType" />.</returns>
    private List<Property> properties;

    /**
     * 主键集合
     */
    private String[] keys;

    /// <summary>Gets the navigation properties of this <see cref="T:System.Data.Metadata.Edm.EntityType" />.</summary>
    /// <returns>A collection of type <see cref="T:System.Data.Metadata.Edm.ReadOnlyMetadataCollection`1" /> that contains the list of navigation properties on this <see cref="T:System.Data.Metadata.Edm.EntityType" />.</returns>
    /**
     * 导航属性集合
     */
    private List<NavigationProperty> navigationProperties;
}
