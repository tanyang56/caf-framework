/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.edm;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 实体数据模型Schema描述类
 * @author wangyandong
 * @date 2020/12/28 17:11
 *
 */
@Data
@Deprecated
//todo 被预警依赖,不能加@Builder注解，外界是new 出来的
public class Schema {
    /**
     * schema的name
     */
    private String name;
    /**
     * 实体schema描述
     */
    private String description;

    /**
     * 实体集列表
     */
    private List<EntitySet> entitySets;

    /**
     * 实体类型列表
     */
    private List<EntityType> entityTypes;

    /**
     * 实体关系列表
     */
    private List<Association> associations;
}
