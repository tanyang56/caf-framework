/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.edm;

import lombok.Builder;
import lombok.Data;

/**
 * 实体属性描述类型
 * @author wangyandong
 * @date 2020/12/28 17:13
 *
 */
@Data
@Deprecated
//todo 被预警依赖
public class Property {
    /**
     * 属性名
     */
    private String code;

    /**
     * 属性语义化名
     */
    private String name;

    /**
     * 属性值类型
     */
    private DataType dataType;

    /**
     * 是否允许为空值
     */
    private boolean nullable;

    /**
     * 属性默认值
     */
    private Object defaultValue;
}
