/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.metadata.edm;

import lombok.Builder;
import lombok.Data;

/**
 * 实体关联关系
 * @author wangyandong
 * @date 2020/12/28 19:24
 *
 */
@Data
@Deprecated
//todo 被预警依赖
public class Association {
    /**
     * 关联名称
     */
    private String name;

    /**
     * 主对象类型
     */
    private String masterType;

    /**
     * 子对象类型
     */
    private String childType;

    /**
     * 主对象字段
     */
    private String masterProperty;
    /**
     * 从对象字段
     */
    private String childProperty;
}
