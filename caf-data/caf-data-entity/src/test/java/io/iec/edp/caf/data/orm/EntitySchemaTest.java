///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.orm;
//
//import io.iec.edp.caf.data.orm.metadata.edm.EntitySet;
//import io.iec.edp.caf.data.orm.metadata.edm.EntityType;
//import io.iec.edp.caf.data.orm.metadata.edm.Property;
//import io.iec.edp.caf.data.orm.metadata.edm.Schema;
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author wangyandong
// */
//public class EntitySchemaTest {
//    @Test
//    public void createEntitySchema(){
//        List<Property> properties = new ArrayList<>();
//        properties.add(Property.builder().build());
//        EntityType et = EntityType.builder()
//                .code("Book")
//                .name("Book")
//                .master(true)
//                .properties(properties)
//                .build();
//
//        List<EntityType> ets = new ArrayList<>();
//        ets.add(et);
//        EntitySet entitySet = EntitySet.builder()
//                .code("Books")
//                .name("图书")
//                .entityType("Book")
//                .build();
//        List<EntitySet> ess = new ArrayList<>();
//        ess.add(entitySet);
//        Schema schema = Schema.builder()
//                .name("print")
//                .entitySets(ess)
//                .entityTypes(ets)
//                  .build();
//
//        Assert.assertNotNull(schema);
//    }
//}
