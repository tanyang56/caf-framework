/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hibernate.dialect;

import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * 增加NCLOB和LongVarchar字段注册
 * @author wangyandong
 */
public class CAFSQLServerDialect extends SQLServer2012Dialect {
    public CAFSQLServerDialect() {
        this.registerHibernateType(Types.LONGNVARCHAR, StandardBasicTypes.STRING.getName());
        this.registerHibernateType(Types.NCLOB, StandardBasicTypes.STRING.getName());
        this.registerHibernateType(Types.CHAR, StandardBasicTypes.STRING.getName());
        this.registerHibernateType(Types.NCHAR, StandardBasicTypes.STRING.getName());
    }
}
