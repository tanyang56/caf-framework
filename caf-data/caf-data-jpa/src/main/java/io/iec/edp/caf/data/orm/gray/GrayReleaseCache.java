package io.iec.edp.caf.data.orm.gray;

import io.iec.edp.caf.commons.runtime.env.enums.GrayReleaseState;

import java.util.concurrent.CopyOnWriteArrayList;


public class GrayReleaseCache {
    private static GrayReleaseState grayReleaseState = GrayReleaseState.NORMAL;

    private static CopyOnWriteArrayList<String> grayReleaseTables = new CopyOnWriteArrayList<>();

    public static GrayReleaseState getGrayReleaseState() {
        return grayReleaseState;
    }

    public static void setGrayReleaseState(GrayReleaseState grayReleaseState) {
        GrayReleaseCache.grayReleaseState = grayReleaseState;
    }

    public static CopyOnWriteArrayList<String> getGrayReleaseTables() {
        return grayReleaseTables;
    }

    public static void setGrayReleaseTables(CopyOnWriteArrayList<String> grayReleaseTables) {
        GrayReleaseCache.grayReleaseTables = grayReleaseTables;
    }
}
