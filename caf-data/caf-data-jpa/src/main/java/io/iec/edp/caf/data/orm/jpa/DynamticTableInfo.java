/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.orm.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态表信息
 * @author wangyandong
 * @date 2019/9/6 9:49
 *
 */
public class DynamticTableInfo {

    /**
     * 实体国际化信息列表
     */
    private Map<String, List<String>> tableColumns;
    /**
     * 动态表名称列表，key为源表名称
     */
    private Map<String,String> dynamicTableNames;

    /**
     * creator
     */
    public DynamticTableInfo(){

    }

    public Map<String, List<String>> getTableColumns() {
        if(this.tableColumns==null)
            this.tableColumns = new HashMap<>();
        return tableColumns;
    }

    public void setTableColumns(Map<String, List<String>> tableColumns) {
        this.tableColumns = tableColumns;
    }


    public Map<String, String> getDynamicTableNames() {
        return dynamicTableNames;
    }

    public void setDynamicTableNames(Map<String, String> dynamicTableNames) {
        this.dynamicTableNames = dynamicTableNames;
    }
}
