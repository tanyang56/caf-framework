/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.data.jpa.repository.support;

import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangyandong
 */
public class CafJpaLanguageThreadHolder {
    private static final ThreadLocal<Map<String,String>> contextHolder = new InheritableThreadLocal();

    CafJpaLanguageThreadHolder() {
    }

    public static void clear() {
        contextHolder.remove();
    }

    public static Map<String,String> getLanguages() {
        Map<String,String> map = (Map<String,String>)contextHolder.get();
        if (map == null) {
            map = new HashMap<>();
            contextHolder.set(map);
        }
        return map;
    }

    public static void setLanguages(Map<String,String> map) {
        Assert.notNull(map, "Only non-null SecurityContext instances are permitted");
        contextHolder.set(map);
    }
}
