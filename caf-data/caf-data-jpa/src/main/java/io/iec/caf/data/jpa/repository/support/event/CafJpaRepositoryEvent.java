/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.data.jpa.repository.support.event;

import org.springframework.context.ApplicationEvent;

public class CafJpaRepositoryEvent extends ApplicationEvent {
    private CafJpaRepositoryEventArgs args;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param args the object on which the event initially occurred or with
     *             which the event is associated (never {@code null})
     */
    public CafJpaRepositoryEvent(CafJpaRepositoryEventArgs args) {
        super(args);
        this.args = args;
    }

    public CafJpaRepositoryEventArgs getEventArgs() {
        return this.args;
    }
}

