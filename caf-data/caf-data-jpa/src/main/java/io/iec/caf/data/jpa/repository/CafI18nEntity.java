/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.data.jpa.repository;

import lombok.Builder;
import lombok.NonNull;

import java.util.Optional;

/**
 * This is {@link CafI18nEntity}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@Builder
public class CafI18nEntity<E> {

    @NonNull
    private final E entity;
    // @NonNull
    private final CafI18nColumnDict dict;

    public E getEntity() {
        return entity;
    }

    public Optional<CafI18nColumnDict> getDict() {
        return Optional.ofNullable(dict);
    }

    // public String getI18nColumn(String fieldName, String langSuffix) {
    //     return null;
    // }
    //
    // public Map<String, String> getFlatI18nColumns() {
    //     return null;
    // }

}
