/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.data.jpa.repository.support.event;

import io.iec.caf.data.jpa.repository.CafI18nColumnDict;

import java.util.Map;

public class CafJpaRepositoryEventArgs {
    private Map<String, String> i18nColumnDict;

    private Object entity;

    private CafI18nColumnDict columnDict;

    public CafJpaRepositoryEventArgs(){

    }

    public CafJpaRepositoryEventArgs(Map<String, String> i18nColumnDict,Object entity,CafI18nColumnDict columnDict){
        this.i18nColumnDict = i18nColumnDict;
        this.entity = entity;
        this.columnDict = columnDict;
    }

    public Map<String, String> getI18nColumnDict() {
        return i18nColumnDict;
    }
    public void setI18nColumnDict(Map<String, String> i18nColumnDict) {
        this.i18nColumnDict = i18nColumnDict;
    }

    public CafI18nColumnDict getColumnDict() {
        return columnDict;
    }
    public void setColumnDict(CafI18nColumnDict columnDict) {
        this.columnDict = columnDict;
    }


    public void setEntity(Object a){
        this.entity = a;
    }
    public Object getEntity(){ return this.entity;}
}
