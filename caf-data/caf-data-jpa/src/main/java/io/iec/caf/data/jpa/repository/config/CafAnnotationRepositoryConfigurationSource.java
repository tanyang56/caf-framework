/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.data.jpa.repository.config;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.repository.config.AnnotationRepositoryConfigurationSource;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.util.Locale;

/**
 * @author wangyandong
 */
public class CafAnnotationRepositoryConfigurationSource extends AnnotationRepositoryConfigurationSource {

    private BootstrapMode bootstrapMode = BootstrapMode.DEFAULT;

    /**
     * Creates a new {@link AnnotationRepositoryConfigurationSource} from the given {@link AnnotationMetadata} and
     * annotation.
     *
     * @param metadata must not be {@literal null}.
     * @param annotation must not be {@literal null}.
     * @param resourceLoader must not be {@literal null}.
     * @param environment must not be {@literal null}.
     * @param registry must not be {@literal null}.
     * @param generator can be {@literal null}.
     */
    public CafAnnotationRepositoryConfigurationSource(AnnotationMetadata metadata, Class<? extends Annotation> annotation,
                                                   ResourceLoader resourceLoader, Environment environment, BeanDefinitionRegistry registry,
                                                   @Nullable BeanNameGenerator generator) {

        super(metadata,annotation,resourceLoader,environment,registry,generator);
        this.configureBootstrapMode(environment);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.data.repository.config.RepositoryConfigurationSource#getBootstrapMode()
     */
    @Override
    public BootstrapMode getBootstrapMode() {

        try {
            BootstrapMode mode = getAttributes().getEnum("bootstrapMode");
            //当元数据属性默认Default，并且环境配置非Default时，以Default为准
            if(mode == BootstrapMode.DEFAULT && this.bootstrapMode!=BootstrapMode.DEFAULT)
                mode = this.bootstrapMode;
            return mode;
        } catch (IllegalArgumentException o_O) {
            return BootstrapMode.DEFAULT;
        }
    }

    private void configureBootstrapMode(Environment environment) {
        String property = environment.getProperty("spring.data.jpa.repositories.bootstrap-mode");
        if (StringUtils.hasText(property)) {
            this.bootstrapMode = BootstrapMode.valueOf(property.toUpperCase(Locale.ENGLISH));
        }
    }
}
