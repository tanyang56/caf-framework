create table tenant_entity
(
    id   integer      not null,
    name varchar(255),
    name_zh varchar(255),
    name_en varchar(255),
    primary key (id)
);
create table user
(
    id         integer      not null,
    first_name varchar(255),
    first_name_zh varchar(255),
    first_name_en varchar(255),
    last_name  varchar(255),
    last_name_zh  varchar(255),
    last_name_en  varchar(255),
    full_name  varchar(255),
    full_name_zh  varchar(255),
    full_name_en  varchar(255),
    primary key (id)
);
create table user_tenants
(
    user_entity_id integer not null,
    tenants_id     integer not null
);
create sequence seq_tenant_id start with 100 increment by 10;
create sequence seq_user_id start with 100 increment by 10;
alter table user_tenants
    add constraint FK5giqgtst2ccywg7q837br2gt8 foreign key (tenants_id) references tenant_entity;
alter table user_tenants
    add constraint FKk9cjhgstjxrdr6sxc57jwq38s foreign key (user_entity_id) references user;


insert into user (id, full_name, full_name_en, full_name_zh, first_name, first_name_en, first_name_zh, last_name, last_name_en, last_name_zh)
values
    (1, '乔·维克 (John Wick)', 'John Wick', '乔·维克','乔 (John)', 'John', '乔', '维克 (Wick)', 'Wick', '维克'),
    (2, '维格·塔拉索 (Viggo Tarasov)', 'Viggo Tarasov', '维格·塔拉索','维格 (Viggo)', 'Viggo', '维格', '塔拉索 (Tarasov)', 'Tarasov', '塔拉索');

insert into tenant_entity (id, name, name_zh, name_en)
values ( 1, '大陆酒店 (Hotel Continental)', '大陆酒店', 'Hotel Continental' );

insert into user_tenants ( user_entity_id, tenants_id )
values
    ( 1, 1 ),
    ( 2, 1 );



create table contact_entity
(
    dtype         varchar(31) not null,
    id            integer     not null,
    first_name     varchar(255),
    first_name_zh  varchar(255),
    first_name_en  varchar(255),
    last_name     varchar(255),
    last_name_zh  varchar(255),
    last_name_en  varchar(255),
    address       varchar(255),
    address_zh    varchar(255),
    address_en    varchar(255),
    contract      varchar(255),
    contract_zh   varchar(255),
    contract_en   varchar(255),
    primary key (id)
);
create sequence seq_customer_id start with 100 increment by 10;

insert into contact_entity (dtype, id, first_name, first_name_zh, first_name_en, last_name, last_name_zh, last_name_en, address, address_zh, address_en, contract, contract_zh, contract_en)
values
    ( 'CustomerEntity', 1, '桑提诺 (Santino)', '桑提诺', 'Santino', 'Antonio', '安东尼奥', 'Antonio', '意大利 (Italy)', '意大利', 'Italy', '血契 (Blood Marker)', '血契', 'Blood Marker'),
    ( 'CustomerEntity', 2, '海伦 (Helen)', '海伦', 'Helen', 'Wick', '安东尼奥', 'Wick', '意大利 (Italy)', '意大利', 'Italy', '婚姻 (Married)', '婚姻', 'Married');

/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

--  i18n columns collection test

create table store_entity
(
    dtype         varchar(31) not null,
    id            varchar(64) not null,
    name          varchar(255),
    name_zh       varchar(255),
    name_en       varchar(255),
    city          varchar(255),
    city_zh       varchar(255),
    city_en       varchar(255),
    street        varchar(255),
    street_zh     varchar(255),
    street_en     varchar(255),
    main_business      varchar(255),
    main_business_zh   varchar(255),
    main_business_en   varchar(255),
    primary key (id)
);

INSERT INTO store_entity (dtype, id, name, name_zh, name_en, city, city_zh, city_en, street, street_zh, street_en, main_business, main_business_zh, main_business_en)
VALUES
    ('HardwareStoreEntity', '1', '老麦克五金店', '老麦克五金店', 'The Old Mike', '济南', '济南', 'Jinan', '浪潮路', '浪潮路', 'Langchao Rd', '铰链', '铰链', 'Hinge'),
    ('HardwareStoreEntity', '2', '小汤姆五金店', '小汤姆五金店', 'The Little Mike', '南京', '南京', 'Nanjing', '热河路', '热河路', 'Rehe Rd', '阀门', '阀门', 'valve');


create table employee_entity
(
    id            integer not null,
    name_zh       varchar(255),
    name_en       varchar(255),
    primary key (id)
);
create sequence seq_employee_id start with 100 increment by 10;