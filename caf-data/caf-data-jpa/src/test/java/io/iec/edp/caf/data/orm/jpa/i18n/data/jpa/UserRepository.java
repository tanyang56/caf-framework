/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.orm.jpa.i18n.data.jpa;

import io.iec.caf.data.jpa.repository.CafJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * This is {@link UserRepository}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
public interface UserRepository extends CafJpaRepository<UserEntity, Integer> {

    List<UserEntity> findUserEntityByNameLike(String name);

    @Query("FROM UserEntity WHERE name = :name")
    List<UserEntity> queryUsingHqlWithSingleCondition(@Param("name") String name);

    List<UserEntity> findUserEntityByNameLikeAndFirstNameIn(String name, List<String> firstNames);

    @Query("FROM UserEntity WHERE name = :name AND firstName IN :firstNames")
    List<UserEntity> queryUsingHqlWithMultipleCondition(@Param("name") String name, @Param("firstNames") List<String> firstNames);


    // Check: Page
    Page<UserEntity> findUserEntityByNameLikeAndLastNameIn(String name, List<String> lastNames, Pageable page);

    @Query("SELECT u FROM UserEntity u WHERE u.name = :name AND u.lastName IN :lastNames")
    Page<UserEntity> queryUsingHqlWithMultipleConditionInPage(@Param("name") String name, @Param("lastNames") List<String> lastNames, Pageable page);


    // Check: Sub-query
    @Query("SELECT u FROM UserEntity u WHERE u.firstName = (SELECT u1.firstName FROM UserEntity u1 WHERE u1.firstName = :firstName)")
    List<UserEntity> queryUsingHqlWithSubQuery(@Param("firstName") String firstName);

    // Check: Join fetch
    @Query("SELECT u FROM UserEntity u LEFT JOIN FETCH u.tenants WHERE u.firstName = :firstName")
    List<UserEntity> queryUsingHqlWithJoinFetch(@Param("firstName") String firstName);

    // TO_FINISH: Embedded

    // TO_FINISH: Inheritance

    // TO_FINISH: Criteria

}
