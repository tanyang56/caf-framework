/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.orm.jpa.i18n.data.jpa;

import lombok.Data;

import javax.persistence.*;

/**
 * This is {@link TenantEntity}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@Data
@Entity
@Table
public class TenantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TENANT_ID")
    @SequenceGenerator(name = "SEQ_TENANT_ID", sequenceName = "SEQ_TENANT_ID", initialValue = 100)
    private Integer id;

    @Column(nullable = false)
    private String name;

}
