///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.orm.jpa.i18n;
//
//import io.iec.caf.data.jpa.repository.CafI18nColumnDict;
//import io.iec.caf.data.jpa.repository.CafI18nEntity;
//import io.iec.caf.data.jpa.repository.support.CafJpaLanguageThreadHolder;
//import io.iec.edp.caf.common.JSONSerializer;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.TenantEntity;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.UserEntity;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.UserRepository;
//import io.iec.edp.caf.data.orm.jpa.i18n.config.CafI18nDataTestConfiguration;
//import org.hibernate.persister.entity.PersistersContext;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.util.StringUtils;
//
//import java.util.*;
//
///**
// * This is {@link BasicI18nTest}.
// *
// * @author yisiqi
// * @since 1.0.0
// */
//@SpringBootTest(classes = CafI18nDataTestConfiguration.class)
//@EnableAutoConfiguration
//public class BasicI18nTest {
//
//    @Autowired
//    UserRepository userRepo;
//
//
//    @BeforeEach
//    void init() {
//        PersistersContext.setCurrentLangSuffix(null);
//    }
//
//
//    private void updateI18nContext(String suffix) {
//        Map<String, List<String>> map = new HashMap<>();
//        map.put(UserEntity.class.getCanonicalName(), Arrays.asList("first_name", "last_name", "full_name"));
//        map.put(TenantEntity.class.getCanonicalName(), Collections.singletonList("name"));
//        PersistersContext.setCurrentLangSuffix(suffix);
//        PersistersContext.resetI18nEntityColumnsMap(map);
//    }
//
//    @Test
//    void testSerialze(){
//        CafI18nColumnDict dict = new CafI18nColumnDict();
//        dict.put("name","en","test");
//
//        String json = JSONSerializer.serialize(dict);
//        CafI18nColumnDict dict2 = JSONSerializer.deserialize(json,CafI18nColumnDict.class);
//        assert dict2!=null;
//    }
//
//
//    @Test
//    void testFindAll() {
//        List<UserEntity> users;
//
//        users = userRepo.findAll();
//        assert users.size() == 2;
//        assert users.stream()
//                .filter(userEntity -> userEntity.getId() == 1)
//                .findFirst()
//                .orElseThrow(NullPointerException::new)
//                .getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.findAll();
//        assert users.size() == 2;
//        assert users.stream()
//                .filter(userEntity -> userEntity.getId() == 1)
//                .findFirst()
//                .orElseThrow(NullPointerException::new)
//                .getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.findAll();
//        assert users.size() == 2;
//        assert users.stream()
//                .filter(userEntity -> userEntity.getId() == 1)
//                .findFirst()
//                .orElseThrow(NullPointerException::new)
//                .getName().equals("乔·维克");
//    }
//
//    @Test
//    void testFindById() {
//        Optional<UserEntity> user;
//
//        user = userRepo.findById(1);
//        assert user.isPresent() && user.get().getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        user = userRepo.findById(1);
//        assert user.isPresent() && user.get().getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        user = userRepo.findById(1);
//        assert user.isPresent() && user.get().getName().equals("乔·维克");
//    }
//
//    @Test
//    void testFindByNameLike() {
//        List<UserEntity> users;
//
//        users = userRepo.findUserEntityByNameLike("%John%");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.findUserEntityByNameLike("%John%");
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.findUserEntityByNameLike("%乔%");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testQueryUsingHqlWithSingleCondition() {
//        List<UserEntity> users;
//
//        users = userRepo.queryUsingHqlWithSingleCondition("乔·维克 (John Wick)");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.queryUsingHqlWithSingleCondition("John Wick");
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.queryUsingHqlWithSingleCondition("乔·维克");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testFindByNameLikeAndFirstNameIn() {
//        List<UserEntity> users;
//
//        users = userRepo.findUserEntityByNameLikeAndFirstNameIn("%乔%", Arrays.asList("乔 (John)", "约翰 (John)"));
//        assert users != null && users.size() == 1;
//
//        updateI18nContext("_en");
//        users = userRepo.findUserEntityByNameLikeAndFirstNameIn("%John%", Arrays.asList("John", "NoName"));
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.findUserEntityByNameLikeAndFirstNameIn("%乔%", Arrays.asList("乔", "约翰"));
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testUsingHqlWithMultipleCondition() {
//        List<UserEntity> users;
//
//        users = userRepo.queryUsingHqlWithMultipleCondition("乔·维克 (John Wick)", Arrays.asList("乔 (John)", "约翰 (John)"));
//        assert users != null && users.size() == 1;
//
//        updateI18nContext("_en");
//        users = userRepo.queryUsingHqlWithMultipleCondition("John Wick", Arrays.asList("John", "NoName"));
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.queryUsingHqlWithMultipleCondition("乔·维克", Arrays.asList("乔", "约翰"));
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testFindByNameLikeAndLastNameIn() {
//        Page<UserEntity> users;
//
//        users = userRepo.findUserEntityByNameLikeAndLastNameIn("%", Arrays.asList("维克 (Wick)", "威克 (Wick)"), PageRequest.of(0, 1));
//        assert users != null && users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.findUserEntityByNameLikeAndLastNameIn("%John%", Arrays.asList("Wick", "NoName"), PageRequest.of(0, 10));
//        assert users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.findUserEntityByNameLikeAndLastNameIn("%乔%", Arrays.asList("维克", "威克"), PageRequest.of(0, 10));
//        assert users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testUsingHqlWithMultipleConditionInPage() {
//        Page<UserEntity> users;
//
//        users = userRepo.queryUsingHqlWithMultipleConditionInPage("乔·维克 (John Wick)", Arrays.asList("维克 (Wick)", "威克 (Wick)"), PageRequest.of(0, 1));
//        assert users != null && users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.queryUsingHqlWithMultipleConditionInPage("John Wick", Arrays.asList("Wick", "NoName"), PageRequest.of(0, 10));
//        assert users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.queryUsingHqlWithMultipleConditionInPage("乔·维克", Arrays.asList("维克", "威克"), PageRequest.of(0, 10));
//        assert users.getTotalElements() == 1 && users.getContent().get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testUsingHqlWithSubQuery() {
//        List<UserEntity> users;
//
//        users = userRepo.queryUsingHqlWithSubQuery("乔 (John)");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.queryUsingHqlWithSubQuery("John");
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.queryUsingHqlWithSubQuery("乔");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testUsingHqlWithJoinFetch() {
//        List<UserEntity> users;
//
//        users = userRepo.queryUsingHqlWithJoinFetch("乔 (John)");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克 (John Wick)");
//
//        updateI18nContext("_en");
//        users = userRepo.queryUsingHqlWithJoinFetch("John");
//        assert users.size() == 1 && users.get(0).getName().equals("John Wick");
//
//        updateI18nContext("_zh");
//        users = userRepo.queryUsingHqlWithJoinFetch("乔");
//        assert users.size() == 1 && users.get(0).getName().equals("乔·维克");
//    }
//
//    @Test
//    void testUpdate() {
//        UserEntity user = userRepo.findById(2).orElseThrow(NullPointerException::new);
//        user.setLastName("特拉索 (Tarasov)");
//        userRepo.save(user);
//        assert userRepo.findById(2).orElseThrow(NullPointerException::new).getLastName().equals("特拉索 (Tarasov)");
//
//        updateI18nContext("_en");
//        user = userRepo.findById(2).orElseThrow(NullPointerException::new);
//        user.setFirstName("Santino");
//        user.setLastName("Antonio");
//        userRepo.save(user);
//        userRepo.flush();
//        assert user.getLastName().equals("Antonio");
//        assert user.getName().equals("Viggo Tarasov");
//
//        updateI18nContext("_zh");
//        user = userRepo.findById(2).orElseThrow(NullPointerException::new);
//        user.setFirstName("圣蒂诺");
//        user.setLastName("安东尼奥");
//        userRepo.save(user);
//        userRepo.flush();
//        assert user.getLastName().equals("安东尼奥");
//        assert user.getName().equals("维格·塔拉索");
//    }
//
//    @Test
//    void testInsert() {
//        //基努·里维斯 Keanu Reeves
//        UserEntity user = new UserEntity();
//        user.setFirstName("基努 (Keanu)");
//        user.setLastName("里维斯 (Reeves)");
//        user.setName("基努·里维斯 (Keanu Reeves)");
//        user = userRepo.save(user);
//
//        updateI18nContext("_en");
//        UserEntity u = userRepo.findById(user.getId()).orElseThrow(NullPointerException::new);
//        assert StringUtils.isEmpty(u.getFirstName());
//        assert StringUtils.isEmpty(u.getLastName());
//        assert StringUtils.isEmpty(u.getName());
//        // userRepo.deleteById(user.getId());
//
//        user = new UserEntity();
//        user.setFirstName("Keanu");
//        user.setLastName("Reeves");
//        user.setName("Keanu Reeves");
//        TenantEntity tenant = new TenantEntity();
//        tenant.setName("home");
//        user.setTenants(Collections.singleton(tenant));
//        user = userRepo.save(user);
//
//        updateI18nContext(null);
//        u = userRepo.findById(user.getId()).orElseThrow(NullPointerException::new);
//        assert StringUtils.isEmpty(u.getFirstName());
//        assert StringUtils.isEmpty(u.getLastName());
//        assert StringUtils.isEmpty(u.getName());
//
//        updateI18nContext("_zh");
//        u = userRepo.findById(user.getId()).orElseThrow(NullPointerException::new);
//        assert StringUtils.isEmpty(u.getFirstName());
//        assert StringUtils.isEmpty(u.getLastName());
//        assert StringUtils.isEmpty(u.getName());
//
//        updateI18nContext("_en");
//        u = userRepo.findById(user.getId()).orElseThrow(NullPointerException::new);
//        assert u.getFirstName().equals("Keanu");
//        assert u.getLastName().equals("Reeves");
//        assert u.getName().equals("Keanu Reeves");
//    }
//
//    @Test
//    void testFindByIdWithAllI18nColumn() {
//        Optional<CafI18nEntity<UserEntity>> userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        // assert !userI18nEntity.get().getDict().isPresent();
//
//        PersistersContext.setCurrentLangList(Arrays.asList("zh", "en"));
//        Map<String, String> langs = new HashMap<>();
//        langs.put("zh-CHS","zh");
//        langs.put("en","en");
//        CafJpaLanguageThreadHolder.setLanguages(langs);
//        userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        // assert !userI18nEntity.get().getDict().isPresent();
//
//        Map<String, List<String>> columnsMap = new HashMap<>();
//        columnsMap.put(UserEntity.class.getCanonicalName(), Collections.singletonList("first_name"));
//        PersistersContext.resetI18nEntityColumnsMap(columnsMap);
//        userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        assert userI18nEntity.get().getDict().isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").get().equals("乔");
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").get().equals("John");
//
//        columnsMap.put(UserEntity.class.getCanonicalName(), Arrays.asList("first_name", "last_name"));
//        PersistersContext.resetI18nEntityColumnsMap(columnsMap);
//        userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        assert userI18nEntity.get().getDict().isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").get().equals("乔");
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").get().equals("John");
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").get().equals("维克");
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").get().equals("Wick");
//
//        columnsMap.put(UserEntity.class.getCanonicalName(), Arrays.asList("last_name", "first_name"));
//        PersistersContext.resetI18nEntityColumnsMap(columnsMap);
//        userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        assert userI18nEntity.get().getDict().isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").get().equals("乔");
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").get().equals("John");
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").get().equals("维克");
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").get().equals("Wick");
//
//        PersistersContext.setCurrentLangList(Arrays.asList("en", "zh"));
//        userI18nEntity = userRepo.findByIdWithAllI18nColumn(1);
//        assert userI18nEntity.isPresent();
//        assert userI18nEntity.get().getEntity() != null;
//        assert userI18nEntity.get().getDict().isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "zh-CHS").get().equals("乔");
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("first_name", "en").get().equals("John");
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "zh-CHS").get().equals("维克");
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").isPresent();
//        assert userI18nEntity.get().getDict().get().get("last_name", "en").get().equals("Wick");
//    }
//}
