/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.orm.jpa.i18n.data.jpa;

import io.iec.caf.data.jpa.repository.CafJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * This is {@link CustomerRepository}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
public interface CustomerRepository extends CafJpaRepository<CustomerEntity, Integer> {

    Page<CustomerEntity> findByAddressInAndName_FirstNameLike(List<String> addr, String firstName, Pageable page);

    @Query("SELECT c FROM CustomerEntity c WHERE c.address in :addr AND c.name.firstName LIKE :firstName")
    List<CustomerEntity> queryUsingHqlWithConditions(@Param("addr") List<String> addr, @Param("firstName") String firstName);

}
