///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.orm.jpa.i18n;
//
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.HardwareStoreEntity;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.HardwareStoreRepository;
//import io.iec.caf.data.jpa.repository.CafI18nColumnDict;
//import io.iec.caf.data.jpa.repository.support.CafJpaLanguageThreadHolder;
//import io.iec.edp.caf.data.orm.jpa.i18n.config.CafI18nDataTestConfiguration;
//import org.hibernate.persister.entity.PersistersContext;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestInstance;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@SpringBootTest(classes = CafI18nDataTestConfiguration.class)
//@EnableAutoConfiguration
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//public class StoreTest {
//
//	@Autowired
//	HardwareStoreRepository repo;
//
//	@BeforeEach
//	void init() {
//		PersistersContext.setCurrentLangSuffix(null);
//	}
//
//	@Test
//	void testFindAll() {
//		List<HardwareStoreEntity> hardwareStores;
//		HardwareStoreEntity hardStore;
//
//		hardwareStores = repo.findAll();
//		assert hardwareStores.size() == 2;
//		hardStore = hardwareStores.stream().filter(c -> c.getId().equals("1")).findFirst().orElse(null);
//		assert hardStore != null;
//		assert hardStore.getName().equals("老麦克五金店");
//		assert hardStore.getAddress().getCity().equals("济南");
//		assert hardStore.getAddress().getStreet().equals("浪潮路");
//		assert hardStore.getMainBusiness().equals("铰链");
//
//		PersistersContext.setCurrentLangSuffix("_en");
//		hardwareStores = repo.findAll();
//		assert hardwareStores.size() == 2;
//		hardStore = hardwareStores.stream().filter(c -> c.getId().equals("1")).findFirst().orElse(null);
//		assert hardStore != null;
//		assert hardStore.getName().equals("The Old Mike");
//		assert hardStore.getAddress().getCity().equals("Jinan");
//		assert hardStore.getAddress().getStreet().equals("Langchao Rd");
//		assert hardStore.getMainBusiness().equals("Hinge");
//
//	}
//
//	@Test
//	void updateI18n() {
//		List<HardwareStoreEntity> hardwareStores;
//		HardwareStoreEntity hardStore;
//
//		PersistersContext.setCurrentLangSuffix("_en");
//		hardwareStores = repo.findAll();
//		assert hardwareStores.size() == 2;
//		hardStore = hardwareStores.stream().filter(c -> c.getId().equals("1")).findFirst().orElse(null);
//		assert hardStore != null;
//		assert hardStore.getName().equals("The Old Mike");
//		assert hardStore.getAddress().getCity().equals("Jinan");
//		assert hardStore.getAddress().getStreet().equals("Langchao Rd");
//		assert hardStore.getMainBusiness().equals("Hinge");
//
//		CafI18nColumnDict dict = new CafI18nColumnDict();
//		dict.put("name", "en", "Old Mike Junior");
//		dict.put("city", "en", "Beijing");
//		dict.put("street", "en", "Renmin Rd");
//		dict.put("main_business", "en", "Others");
//		Map<String, String> langs = new HashMap<>();
//		langs.put("en","en");
//		langs.put("zh","zh");
//		CafJpaLanguageThreadHolder.setLanguages(langs);
//		repo.save(hardStore, dict);
//		assert hardStore.getId() != null;
//
//		hardwareStores = repo.findAll();
//		assert hardwareStores.size() == 2;
//		hardStore = hardwareStores.stream().filter(c -> c.getId().equals("1")).findFirst().orElse(null);
//		assert hardStore != null;
//		assert hardStore.getName().equals("Old Mike Junior");
//		assert hardStore.getAddress().getCity().equals("Beijing");
//		assert hardStore.getAddress().getStreet().equals("Renmin Rd");
//		assert hardStore.getMainBusiness().equals("Others");
//
//	}
//
//}
