/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.data.orm.jpa;//package io.iec.edp.caf.data.orm.jpa;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//import java.utils.HashMap;
//import java.utils.List;
//import java.utils.Map;
//import java.utils.Optional;
//import java.utils.regex.Matcher;
//import java.utils.regex.Pattern;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {
//        TestAutoConfiguration.class
//})
//public class CustomerTest {
//
//    @Autowired
//    CountryRepository repo;
//    @Autowired
//    EntityManager em;
//
//    @Before
//    public void setUp() throws Exception {
//
//    }
//
//    @After
//    public void tearDown() throws Exception {
//
//    }
//
//    @Test
//    public void QueryTest() {
//        Map<String,String> map = new HashMap<>();
//        map.put("COUNTRY","country2019");
//        try(DynamicTableContext context = DynamicTableContext.createContext(map)){
//            Country user = new Country();
//            user.setId("123123");
//            user.setCode("123123");
//            user.setName("jinan");
//
//            this.repo.save(user);
//
//            Optional<Country> user2 = repo.findById("123123");
//            System.out.println(user2.get().getName());
//        }
//        catch (Exception e) {
//            // TODO: handle exception
//        }
//    }
//
//    @Test
//    public void QueryTest2() {
//
//        Query query = em.createNativeQuery("select * from Country");
//
//        List<Country> users = query.getResultList();
//
//    }
//
//    @Test
//    public void QueryTest3() {
//
//        Country c = this.repo.findByTaskName("123123");
//        System.out.println(c.getName());
//
//    }
//
//    @Test
//    public void QueryTest4() {
//        String sql = "select country0_1_.id as id1_0_0_, country0_.code";
//
//        String regex = "(country[\\d_]+)\\.code";
//        String st = sql.replaceAll(regex,"$1.code_en");
//        String columnName = "code";
//        String newColName = columnName+"_zhs";
//        Pattern p=Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
//        Matcher m=p.matcher(sql);
//        boolean b = m.matches();
//        int k = m.groupCount();
////        for (int i=0;i<m.groupCount();i++){
////            String subString = m.group(i);
////            sql = sql.replaceAll("(?i)"+ subString+columnName, subString+newColName);
////        }
//        System.out.println(sql);
//
//
//    }
//}