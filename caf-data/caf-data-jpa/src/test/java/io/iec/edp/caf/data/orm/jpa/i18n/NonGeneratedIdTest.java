///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.orm.jpa.i18n;
//
//import io.iec.edp.caf.data.orm.jpa.i18n.config.CafI18nDataTestConfiguration;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.EmployeeEntity;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.EmployeeRepository;
//import org.hibernate.persister.entity.PersistersContext;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.*;
//
///**
// * This is {@link NonGeneratedIdTest}.
// *
// * @author yisiqi
// * @since 1.0.0
// */
//@SpringBootTest(classes = CafI18nDataTestConfiguration.class)
//@EnableAutoConfiguration
//public class NonGeneratedIdTest {
//
//    @Autowired
//    private EmployeeRepository repo;
//
//    @BeforeEach
//    public void init() {
//        Map<String, List<String>> map = new HashMap<>();
//        map.put(EmployeeEntity.class.getCanonicalName(), Collections.singletonList("name"));
//        PersistersContext.setCurrentLangSuffix("_en");
//        PersistersContext.resetI18nEntityColumnsMap(map);
//    }
//
//    @Test
//    public void test() {
//        repo.save(new EmployeeEntity(1, "hh"));
//    }
//}
