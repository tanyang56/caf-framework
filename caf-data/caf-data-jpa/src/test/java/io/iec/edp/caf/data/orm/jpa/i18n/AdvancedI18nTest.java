///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.data.orm.jpa.i18n;
//
//import io.iec.edp.caf.data.orm.jpa.i18n.config.CafI18nDataTestConfiguration;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.ContactName;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.CustomerEntity;
//import io.iec.edp.caf.data.orm.jpa.i18n.data.jpa.CustomerRepository;
//import org.hibernate.persister.entity.PersistersContext;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.util.StringUtils;
//
//import java.util.*;
//
///**
// * This is {@link AdvancedI18nTest}.
// *
// * @author yisiqi
// * @since 1.0.0
// */
//@SpringBootTest(classes = CafI18nDataTestConfiguration.class)
//@EnableAutoConfiguration
//public class AdvancedI18nTest {
//
//    @Autowired
//    private CustomerRepository repo;
//
//
//    @BeforeEach
//    void init() {
//        PersistersContext.setCurrentLangSuffix(null);
//    }
//
//
//    private void updateI18nContext(String suffix) {
//        Map<String, List<String>> map = new HashMap<>();
//        map.put(CustomerEntity.class.getCanonicalName(), Arrays.asList("first_name", "last_name", "address", "contract"));
//        PersistersContext.setCurrentLangSuffix(suffix);
//        PersistersContext.resetI18nEntityColumnsMap(map);
//    }
//
//
//    @Test
//    void testFindAll() {
//        List<CustomerEntity> customers;
//        CustomerEntity customer;
//
//        customers = repo.findAll();
//        assert customers.size() == 2;
//        customer = customers.stream().filter(c -> c.getId().equals(1)).findFirst().orElse(null);
//        assert customer != null;
//        assert customer.getName().getFirstName().equals("桑提诺 (Santino)");
//        assert customer.getAddress().equals("意大利 (Italy)");
//        assert customer.getContract().equals("血契 (Blood Marker)");
//
//        updateI18nContext("_en");
//        customers = repo.findAll();
//        assert customers.size() == 2;
//        customer = customers.stream().filter(c -> c.getId().equals(1)).findFirst().orElse(null);
//        assert customer != null;
//        assert customer.getName().getFirstName().equals("Santino");
//        assert customer.getAddress().equals("Italy");
//        assert customer.getContract().equals("Blood Marker");
//
//        updateI18nContext("_zh");
//        customers = repo.findAll();
//        assert customers.size() == 2;
//        customer = customers.stream().filter(c -> c.getId().equals(1)).findFirst().orElse(null);
//        assert customer != null;
//        assert customer.getName().getFirstName().equals("桑提诺");
//        assert customer.getAddress().equals("意大利");
//        assert customer.getContract().equals("血契");
//    }
//
//    @Test
//    void testFindById() {
//        Optional<CustomerEntity> customer;
//
//        customer = repo.findById(1);
//        assert customer.isPresent();
//        assert customer.get().getName().getFirstName().equals("桑提诺 (Santino)");
//        assert customer.get().getAddress().equals("意大利 (Italy)");
//        assert customer.get().getContract().equals("血契 (Blood Marker)");
//
//        updateI18nContext("_en");
//        customer = repo.findById(1);
//        assert customer.isPresent();
//        assert customer.get().getName().getFirstName().equals("Santino");
//        assert customer.get().getAddress().equals("Italy");
//        assert customer.get().getContract().equals("Blood Marker");
//
//        updateI18nContext("_zh");
//        customer = repo.findById(1);
//        assert customer.isPresent();
//        assert customer.get().getName().getFirstName().equals("桑提诺");
//        assert customer.get().getAddress().equals("意大利");
//        assert customer.get().getContract().equals("血契");
//    }
//
//    @Test
//    void testFindByCondition() {
//        Page<CustomerEntity> customers;
//        CustomerEntity customer;
//
//        customers = repo.findByAddressInAndName_FirstNameLike(Collections.singletonList("意大利 (Italy)"), "%桑%San%", PageRequest.of(0, 10));
//        assert customers.getTotalElements() ==1;
//        customer = customers.getContent().get(0);
//        assert customer.getName().getFirstName().equals("桑提诺 (Santino)");
//        assert customer.getAddress().equals("意大利 (Italy)");
//        assert customer.getContract().equals("血契 (Blood Marker)");
//
//        updateI18nContext("_en");
//        customers = repo.findByAddressInAndName_FirstNameLike(Collections.singletonList("Italy"), "%San%", PageRequest.of(0, 10));
//        assert customers.getTotalElements() == 1;
//        customer = customers.getContent().get(0);
//        assert customer.getName().getFirstName().equals("Santino");
//        assert customer.getAddress().equals("Italy");
//        assert customer.getContract().equals("Blood Marker");
//
//        updateI18nContext("_zh");
//        customers = repo.findByAddressInAndName_FirstNameLike(Collections.singletonList("意大利"), "%桑%", PageRequest.of(0, 10));
//        assert customers.getTotalElements() == 1;
//        customer = customers.getContent().get(0);
//        assert customer.getName().getFirstName().equals("桑提诺");
//        assert customer.getAddress().equals("意大利");
//        assert customer.getContract().equals("血契");
//    }
//
//
//
//    @Test
//    void testUsingHqlWithConditions() {
//        List<CustomerEntity> customers;
//        CustomerEntity customer;
//
//        customers = repo.queryUsingHqlWithConditions(Collections.singletonList("意大利 (Italy)"), "%桑%San%");
//        assert customers.size() ==1;
//        customer = customers.get(0);
//        assert customer.getName().getFirstName().equals("桑提诺 (Santino)");
//        assert customer.getAddress().equals("意大利 (Italy)");
//        assert customer.getContract().equals("血契 (Blood Marker)");
//
//        updateI18nContext("_en");
//        customers = repo.queryUsingHqlWithConditions(Collections.singletonList("Italy"), "%San%");
//        assert customers.size() == 1;
//        customer = customers.get(0);
//        assert customer.getName().getFirstName().equals("Santino");
//        assert customer.getAddress().equals("Italy");
//        assert customer.getContract().equals("Blood Marker");
//
//        updateI18nContext("_zh");
//        customers = repo.queryUsingHqlWithConditions(Collections.singletonList("意大利"), "%桑%");
//        assert customers.size() == 1;
//        customer = customers.get(0);
//        assert customer.getName().getFirstName().equals("桑提诺");
//        assert customer.getAddress().equals("意大利");
//        assert customer.getContract().equals("血契");
//    }
//
//    //
//    // @Test
//    // void testUsingHqlWithSubQuery() {
//    //     List<CustomerEntity> customers;
//    //
//    //     customers = repo.queryUsingHqlWithSubQuery("乔 (John)");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("乔·维克 (John Wick)");
//    //
//    //     updateI18nContext("_en", "full_name", "first_name");
//    //     customers = repo.queryUsingHqlWithSubQuery("John");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("John Wick");
//    //
//    //     updateI18nContext("_zh", "full_name", "first_name");
//    //     customers = repo.queryUsingHqlWithSubQuery("乔");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("乔·维克");
//    // }
//    //
//    // @Test
//    // void testUsingHqlWithJoinFetch() {
//    //     List<CustomerEntity> customers;
//    //
//    //     customers = repo.queryUsingHqlWithJoinFetch("乔 (John)");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("乔·维克 (John Wick)");
//    //
//    //     updateI18nContext("_en", "full_name", "first_name");
//    //     customers = repo.queryUsingHqlWithJoinFetch("John");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("John Wick");
//    //
//    //     updateI18nContext("_zh", "full_name", "first_name");
//    //     customers = repo.queryUsingHqlWithJoinFetch("乔");
//    //     assert customers.size() == 1 && customers.get(0).getName().equals("乔·维克");
//    // }
//
//    @Test
//    void testUpdate() {
//        CustomerEntity customer = repo.findById(2).orElseThrow(NullPointerException::new);
//        customer.getName().setLastName("特拉索 (Tarasov)");
//        repo.save(customer);
//        assert repo.findById(2).orElseThrow(NullPointerException::new).getName().getLastName().equals("特拉索 (Tarasov)");
//
//        updateI18nContext("_en");
//        customer = repo.findById(2).orElseThrow(NullPointerException::new);
//        customer.getName().setFirstName("Santino");
//        customer.getName().setLastName("Antonio");
//        repo.save(customer);
//        repo.flush();
//        assert customer.getName().getLastName().equals("Antonio");
//
//        updateI18nContext("_zh");
//        customer = repo.findById(2).orElseThrow(NullPointerException::new);
//        customer.getName().setFirstName("圣蒂诺");
//        customer.getName().setLastName("安东尼奥");
//        repo.save(customer);
//        repo.flush();
//        assert customer.getName().getLastName().equals("安东尼奥");
//    }
//
//    @Test
//    void testInsert() {
//        //基努·里维斯 Keanu Reeves
//        CustomerEntity customer = new CustomerEntity();
//        customer.setName(new ContactName("基努 (Keanu)", "里维斯 (Reeves)"));
//        customer.setAddress("家 (Home)");
//        customer.setContract("无 (NULL)");
//        customer = repo.save(customer);
//
//        updateI18nContext("_en");
//        CustomerEntity c = repo.findById(customer.getId()).orElseThrow(NullPointerException::new);
//        assert c.getName() == null;
//        assert StringUtils.isEmpty(c.getAddress());
//        assert StringUtils.isEmpty(c.getContract());
//
//        customer = new CustomerEntity();
//        customer.setName(new ContactName("Keanu", "Reeves"));
//        customer.setAddress("Home");
//        customer.setContract("NULL");
//        customer = repo.save(customer);
//
//        updateI18nContext(null);
//        c = repo.findById(customer.getId()).orElseThrow(NullPointerException::new);
//        assert c.getName() == null;
//        assert StringUtils.isEmpty(c.getAddress());
//        assert StringUtils.isEmpty(c.getContract());
//
//        updateI18nContext("_zh");
//        c = repo.findById(customer.getId()).orElseThrow(NullPointerException::new);
//        assert c.getName() == null;
//        assert StringUtils.isEmpty(c.getAddress());
//        assert StringUtils.isEmpty(c.getContract());
//
//        updateI18nContext("_en");
//        c = repo.findById(customer.getId()).orElseThrow(NullPointerException::new);
//        assert c.getName() != null;
//        assert c.getName().getFirstName().equals("Keanu");
//        assert c.getName().getLastName().equals("Reeves");
//        assert c.getAddress().equals("Home");
//        assert c.getContract().equals("NULL");
//    }
//
//}
