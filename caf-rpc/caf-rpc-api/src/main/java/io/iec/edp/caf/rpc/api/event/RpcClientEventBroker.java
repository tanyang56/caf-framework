/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.event;

import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

import java.util.HashMap;

/**
 * @author Leon Huo
 */
public class RpcClientEventBroker extends EventBroker {

    private RpcClientEventManager manager;

    public RpcClientEventBroker(RpcClientEventManager manager, EventListenerSettings settings) {
        super(settings);
        this.manager = manager;
        this.init();
    }

    /**
     * 触发事件
     *
     * @return
     */
    public void firePreRpcInvokeEvent(HashMap<String, String> context,
                                      HashMap<String, Object> parameters,
                                      HashMap<String, Object> localContext) {
//        this.init();
        PreRpcInvokeEventArgs e = PreRpcInvokeEventArgs.builder()
                .context(context)
                .parameters(parameters)
                .localContext(localContext)
                .build();
        this.manager.firePreRpcInvokeEvent(e);
    }

    /**
     * 触发事件
     *
     * @return
     */
    public void firePostRpcInvokeEvent(HashMap<String, String> context,
                                       Object methodResult,
                                       HashMap<String, Object> localContext) {
//        this.init();
        PostRpcInvokeEventArgs e = PostRpcInvokeEventArgs.builder()
                .context(context)
                .methodResult(methodResult)
                .localContext(localContext)
                .build();
        this.manager.firePostRpcInvokeEvent(e);
    }

    /**
     * 触发事件
     *
     * @param
     */
    public void fireExceptionRpcInvokeEvent(HashMap<String, String> context,
                                            HashMap<String, Object> parameters,
                                            HashMap<String, Object> localContext,
                                            Exception ex) {
//        this.init();
        ExceptionRpcInvokeEventArgs e = ExceptionRpcInvokeEventArgs.builder()
                .context(context)
                .parameters(parameters)
                .localContext(localContext)
                .exception(ex)
                .build();
        this.manager.fireExceptionRpcInvokeEvent(e);
    }


    /**
     * 初始化
     */
    @Override
    protected void onInit() {
        this.eventManagerCollection.add(manager);
    }
}
