/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.event;

import io.iec.edp.caf.commons.event.IEventListener;

/**
 * RpcClient RpcServer事件监听接口
 * @author wangyandong
 * @date 2019/11/14 2:38
 *
 */
@Deprecated
//todo 被com/inspur/edp/debugtool/client/listener/rpc/RpcTraceConfiguration依赖
public interface IRpcClientEventListener  extends IEventListener {
    /**
      * 方法执行前的事件
      * @param args
      */
    void preInvokeEvent(PreRpcInvokeEventArgs args);

    /**
     * 方法执行后的事件
     * @param args
     */
    void postInvokeEvent(PostRpcInvokeEventArgs args);

    /**
     * 异常时的事件
     * @param args
     */
    void exceptionInvokeEvent(ExceptionRpcInvokeEventArgs args);
}
