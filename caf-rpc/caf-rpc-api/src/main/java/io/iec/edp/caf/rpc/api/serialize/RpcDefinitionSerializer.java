/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.serialize;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 自定义序列化器 用于序列化Definition
 * <p>
 * todo 换掉fastjson
 *
 * @author guowenchang
 */
public class RpcDefinitionSerializer implements RpcCustomSerializer {
    static {
        ParserConfig.getGlobalInstance().addAccept("io.iec.edp.caf.rpc.api");
    }

    @Override
    public String serialize(Object obj) {
        return JSON.toJSONString(obj, SerializerFeature.WriteClassName);
    }

    @Override
    public <T> T deserialize(String s, Class<T> clazz) {
        return JSON.parseObject(s, clazz, Feature.OrderedField);
    }
}
