/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.support;

import lombok.var;

import java.util.HashMap;

public class RpcThreadCacheHolder {

    private static ThreadLocal<HashMap<String,String>> asyncLocalValue = new ThreadLocal<HashMap<String, String>>();


    /**
     * 设置缓存值
     */
    public static void setValue(String key,String value)
    {
        var cache = asyncLocalValue.get();
        if(cache==null){
            cache =  new HashMap<String, String>();
            asyncLocalValue.set(cache);
        }
        cache.put(key,value);

    }

    /**
     * 当前Rpc调用上下文
     * @return
     */
    public static String getValue(String key)
    {
        var cache = asyncLocalValue.get();
        if(cache!=null){
            return cache.get(key);
        }else{
            return "";
        }
    }

    public static void clear(){
        var cache = asyncLocalValue.get();
        if(cache!=null){
            cache.clear();

        }
    }
}
