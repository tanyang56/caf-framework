/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.utils;

import io.iec.edp.caf.rpc.api.filter.RpcClientFilter;
import io.iec.edp.caf.rpc.api.filter.RpcServerFilter;
import io.iec.edp.caf.rpc.api.service.RpcServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RpcFiltersContainer {
//    private static Map<String, Map<String,Object>> serviceBeanDepository = new HashMap<>();
    private static Map<String, List<RpcClientFilter>> clientFilterBeanDepository = new HashMap<>();
    private static Map<String, List<RpcServerFilter>> serverFilterBeanDepository = new HashMap<>();
//    private static List<RpcClientFilter> clientFilterBeanDepository = new ArrayList<>();
//    private static List<RpcServerFilter> serverFilterBeanDepository = new ArrayList<>();


    /**
     * 初始化仓库
     * @param clientFilter
     * @param serverFilter
     */
    public static void initialize(List<RpcClientFilter> clientFilter,
                                  List<RpcServerFilter>  serverFilter){
//        if(serviceBeanDepository.size()>0) return;
//        serviceBeanDepository.putAll(service);
//        clientFilterBeanDepository.addAll(clientFilter);
//        serverFilterBeanDepository.addAll(serverFilter);
    }

//    public static Map<String,Object> getServices(String mode){
//        return serviceBeanDepository.get(mode);
//    }

    public static Map<String, List<RpcClientFilter>> getClientFilter(){
        return clientFilterBeanDepository;
    }

    public static Map<String, List<RpcServerFilter>> getServerFilter(){
        return serverFilterBeanDepository;
    }

    public static List<RpcClientFilter> getClientFilter(String mode){
        return clientFilterBeanDepository.get(mode);
    }

    public static List<RpcServerFilter> getServerFilter(String mode){
        return serverFilterBeanDepository.get(mode);
    }
}
