/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.iec.edp.caf.rpc.api.common.GspSerializeType;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;

/**
 * @author Leon Huo
 */
public interface RpcServer {
    /**
     * invoke service
     * @param serviceId  service id
     * @param version    service version
     * @param paramArray  service parameters
     * @return
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    Object invokeService(String serviceId, String version, Object[] paramArray, GspSerializeType returnType) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, JsonProcessingException;

//    /**
//     * invoke service
//     * @param serviceId service id
//     * @param version   service version
//     * @param paramDict service parameters
//     * @return
//     */
//    InputStream invokeServiceStream(String serviceId, String version, LinkedHashMap<String, Object> paramDict);
}
