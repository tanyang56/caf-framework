/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.service;

import io.iec.edp.caf.rpc.api.support.Type;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Leon Huo
 */
public interface RpcClient {

    /**
     * invoke service
     * @param <T>
     * @param t         reture type
     * @param serviceId serviceId
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context  service context
     * @return
     * @throws MalformedURLException
     */
    @Deprecated
    <T> T invokeService(Class<T> t, String serviceId, String serviceUnitName, HashMap<String, Object> parameters,
                        HashMap<String, String> context) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, MalformedURLException;

    /**
     * invoke service
     * @param serviceId service id
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @return
     * @throws MalformedURLException
     */
    @Deprecated
    String InvokeService(String serviceId, String serviceUnitName, HashMap<String, Object> parameters,
                         HashMap<String, String> context) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, MalformedURLException;

    /**
     * invoke service
     * @param serviceId service id
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @return
     * @throws MalformedURLException
     */
    @Deprecated
    String invokeService(String serviceId, String serviceUnitName, HashMap<String, Object> parameters,
                         HashMap<String, String> context) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, MalformedURLException;


    /**
     * invoke service
     * @param t return type
     * @param serviceId service id
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @param <T>
     * @return
     */
    <T> T invoke(Class<T> t, String serviceId, String serviceUnitName,
                 LinkedHashMap<String, Object> parameters, HashMap<String, String> context);

    /**
     * invoke service (support Generics)
     *
     * @param t               return type
     * @param serviceId       service id
     * @param serviceUnitName msu code
     * @param parameters      service parameters
     * @param context         service context
     * @return
     */
    <T> T invoke(Type<T> t, String serviceId, String serviceUnitName,
                 LinkedHashMap<String, Object> parameters, HashMap<String, String> context);

    /**
     * invoke service，by remote url
     *
     * @param t                     return type
     * @param serviceId             service id
     * @param serviceUnitName       msu code
     * @param parameters            service parameters
     * @param context               service context
     * @param remoteUrl             remote url
     * @return result
     */
    @Deprecated
    <T> T invoke(Type<T> t,String serviceId, String serviceUnitName, LinkedHashMap<String, Object> parameters,
                  HashMap<String, String> context,String remoteUrl);

    /**
     * invoke service
     * @param serviceId service id
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @return
     */
    String invoke(String serviceId, String serviceUnitName, LinkedHashMap<String, Object> parameters,
                  HashMap<String, String> context);


    /**
     * invoke service
     * @param serviceId service id
     * @param version   service version
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @return
     */
    InputStream invokeStream(String serviceId,String version, String serviceUnitName, LinkedHashMap<String, Object> parameters,
                       HashMap<String, String> context);

    /**
     * invoke service
     * @param stream    input stream
     * @param t         return type
     * @param serviceId service id
     * @param version   service version
     * @param serviceUnitName msu code
     * @param parameters service parameters
     * @param context service context
     * @return
     */
    <T> T  invokeStream(InputStream stream,Type<T> t,String serviceId,String version, String serviceUnitName, LinkedHashMap<String, Object> parameters,
                             HashMap<String, String> context);


    void setTimeout(Integer timeout);
}
