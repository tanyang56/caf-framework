/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.schema;

import io.iec.edp.caf.rpc.api.schema.enums.TypeEnum;

/**
 * Schema工具
 * 提供基本数据类型的Schema 减少对象创建
 *
 * @author guowenchang
 */
public class TypeSchemaConstant {

    public static final TypeSchema BYTE = new TypeSchema(TypeEnum.BYTE);
    public static final TypeSchema SHORT = new TypeSchema(TypeEnum.SHORT);
    public static final TypeSchema INT = new TypeSchema(TypeEnum.INT);
    public static final TypeSchema LONG = new TypeSchema(TypeEnum.LONG);
    public static final TypeSchema FLOAT = new TypeSchema(TypeEnum.FLOAT);
    public static final TypeSchema DOUBLE = new TypeSchema(TypeEnum.DOUBLE);
    public static final TypeSchema BOOL = new TypeSchema(TypeEnum.BOOL);
    public static final TypeSchema STRING = new TypeSchema(TypeEnum.STRING);
    public static final TypeSchema CHAR = new TypeSchema(TypeEnum.CHAR);
    public static final TypeSchema DATE = new TypeSchema(TypeEnum.DATE);
    public static final TypeSchema BIG_INTEGER = new TypeSchema(TypeEnum.BIG_INTEGER);
    public static final TypeSchema BIG_DECIMAL = new TypeSchema(TypeEnum.BIG_DECIMAL);
    public static final TypeSchema VOID = new TypeSchema(TypeEnum.VOID);
    public static final TypeSchema UNKNOWN = new TypeSchema(TypeEnum.UNKNOWN);
    public static final TypeSchema TYPE_SCHEMA = new TypeSchema(TypeEnum.TYPE_SCHEMA);
    public static final TypeSchema OBJECT_SCHEMA = new TypeSchema(TypeEnum.OBJECT_SCHEMA);
    public static final TypeSchema MAP_SCHEMA = new TypeSchema(TypeEnum.MAP_SCHEMA);
    public static final TypeSchema ARRAY_SCHEMA = new TypeSchema(TypeEnum.ARRAY_SCHEMA);
    public static final TypeSchema SCHEMA_FIELD = new TypeSchema(TypeEnum.SCHEMA_FIELD);
    public static final TypeSchema TYPE_ENUM = new TypeSchema(TypeEnum.TYPE_ENUM);
}