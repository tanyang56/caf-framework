/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.local;

import io.iec.edp.caf.rpc.api.entity.RpcServiceDefinition;
import io.iec.edp.caf.rpc.api.entity.RpcServiceMethodDefinition;
import io.iec.edp.caf.rpc.api.service.InternalServiceManageService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Leon Huo
 */
@Slf4j
public class InternalServiceManageServiceImpl implements InternalServiceManageService {

    @Override
    public RpcServiceMethodDefinition getRpcMethodDefinition(String serviceId) {
        List<RpcServiceDefinition> rpcServiceDefinitions = InternalServiceLocalStorage.current.getRpcServiceDefinitions();
        for (RpcServiceDefinition rpcServiceDefinition : rpcServiceDefinitions) {
            for (RpcServiceMethodDefinition rpcServiceMethodDefinition : rpcServiceDefinition.getMethods()) {
                if (serviceId.equalsIgnoreCase(rpcServiceMethodDefinition.getServiceId())) {
                    return rpcServiceMethodDefinition;
                }
            }
        }

        return null;
    }

    @Override
    public List<RpcServiceDefinition> getAllBySU(String su) {
        List<RpcServiceDefinition> instances = InternalServiceLocalStorage.current.getRpcServiceDefinitions();
        if (instances == null || instances.size() == 0)
            return null;
        return instances.stream()
                .filter((n) -> su.equalsIgnoreCase(n.getSu()))
                .collect(Collectors.toList());
    }

}
