/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.local;

import io.iec.edp.caf.rpc.api.entity.RpcServiceDefinition;

import java.util.List;

/**
 * @author Leon Huo
 */
public class InternalServiceLocalStorage {
    public final static InternalServiceLocalStorage current = new InternalServiceLocalStorage();

//    private List<GspRpcServiceDefinition> internalServices;

    private List<RpcServiceDefinition> rpcServiceDefinitions;

    /**
     * 私有构造函数
     */
    private InternalServiceLocalStorage() {
//        this.internalServices = null;
    }

    /**
     * 返回当前应用内的RPC服务列表
     *
     * @return
     */
    public List<RpcServiceDefinition> getRpcServiceDefinitions() {
        return rpcServiceDefinitions;
    }

    /**
     * 设置当前应用内的RPC服务列表
     *
     * @param rpcServiceDefinitions
     */
    public void setRpcServiceDefinitions(List<RpcServiceDefinition> rpcServiceDefinitions) {
        this.rpcServiceDefinitions = rpcServiceDefinitions;
    }

//    /**
//     * 返回本地所有内部服务
//     *
//     * @return
//     */
//    @Deprecated
//    public List<GspRpcServiceDefinition> getInternalServices() {
//        return this.internalServices;
//    }
//
//    /**
//     * 设置当前的内部服务列表
//     */
//    @Deprecated
//    public void setInternalServices(List<GspRpcServiceDefinition> internalServices) {
//        this.internalServices = internalServices;
//    }
}
