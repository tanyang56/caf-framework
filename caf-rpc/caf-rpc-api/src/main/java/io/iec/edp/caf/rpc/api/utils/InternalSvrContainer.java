/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.utils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 内部服务实例对象容器
 *
 * @author Leon Huo
 */
public class InternalSvrContainer {

    private static ConcurrentHashMap<Class<?>, Object> hashMap = new ConcurrentHashMap<>();

    public static Object getService(Class interfaceClass) {
        return hashMap.get(interfaceClass);
    }

    public static <T> void putService(Class<T> interfaceClass, T implementsClass) {
        hashMap.put(interfaceClass, implementsClass);
    }
}
