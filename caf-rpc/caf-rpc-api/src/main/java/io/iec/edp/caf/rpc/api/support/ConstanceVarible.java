/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.support;

public class ConstanceVarible {
    public final static String IS_LOCAL = "local";
    public final static String CURRENT_SERVICE_INTERFACE = "interfaceString";
    public final static String CURRENT_SERVICE_ID = "serviceId";
    public final static String TARGET_SU= "targetSu";
    public final static String CURRENT_REMOTEBASE_URL= "currentRemoteUrl";
    public final static String CURRENT_APPNAME= "appName";
    public final static String CURRENT_SERVICENAME= "serviceName";
    public final static String COOKIE_SESSION_NAME="caf_web_session";

    public final static String ROUTE_TENANTID = "TenantId";
    public final static String ROUTE_FIRSTDIMENSION_KEY = "TenantDim1";
    public final static String ROUTE_SECONDDIMENSION_KEY = "TenantDim2";

    public final static String GSP_CONTEXT_ID="ContextId";
    public final static String GSP_RPC="gsp-rpc";
    public final static String GSP_MSU="ServiceUnitName";
    public final static String GSP_RPC_TENANT="rpc-tenantId";
    public final static String GSP_RPC_CLIENT_ENVENT="gsp-rpc-client-eventcontext";
    public final static String GSP_CONTEXT="gsp-context";
    public final static String GSP_RPC_SERVER="gsp-rpc-server";
    public final static String GSP_RPC_SERVER_ENVENT="gsp-rpc-server-eventcontext";

    public final static String LOG_MSU = "serviceUnitName";
    public final static String BIZ_CONTEXT_ID="BizContextId";
    public final static String NET_SESSION_ID ="NSessionId";
    public final static String SESSION_ID="sessionid";

    public final static String RPC_HTTP_RESPONSE="http_response";
    public final static String RPC_INPUT_STREAM="http_inputstream";

    public final static String RPC_HEADER="rpc_header";

    public final static String RPC_FILTER_MODE_DEFAULT = "official";
}
