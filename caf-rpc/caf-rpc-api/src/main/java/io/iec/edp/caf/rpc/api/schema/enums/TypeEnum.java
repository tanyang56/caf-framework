/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.schema.enums;

/**
 * @author Leon Huo
 */
public enum TypeEnum {
    BYTE(0),
    SHORT(1),
    INT(2),
    LONG(3),
    FLOAT(4),
    DOUBLE(5),
    CHAR(6),
    BOOL(7),
    STRING(8),
    ARRAY(9),
    COLLECTION(10),
    MAP(11),
    ENUM(12),
    DATE(13),
    BIG_INTEGER(14),
    BIG_DECIMAL(15),
    OBJECT(16),
    INTERFACE(17),
    VOID(18),
    UNKNOWN(19),
    TYPE_SCHEMA(20),
    OBJECT_SCHEMA(21),
    MAP_SCHEMA(22),
    ARRAY_SCHEMA(23),
    SCHEMA_FIELD(24),
    TYPE_ENUM(25);

    int value;

    TypeEnum(int val) {
        this.value = val;
    }

    public int getValue() {
        return this.value;
    }
}
