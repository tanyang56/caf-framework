/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.properties;

import lombok.Data;

@Data
public class LocalInvokeProperties {
    //默认是cpu逻辑核数
    private int corePoolSize=Runtime.getRuntime().availableProcessors();
    //默认是cpu逻辑核数*2
    private int maxPoolsize=Runtime.getRuntime().availableProcessors()*2;
    //队列大小暂不支持配置
//    private int queueSize = 1;
}
