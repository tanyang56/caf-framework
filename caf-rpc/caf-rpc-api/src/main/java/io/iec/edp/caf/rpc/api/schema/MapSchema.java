/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.schema;

import io.iec.edp.caf.rpc.api.schema.enums.TypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * Map Schema
 *
 * @author guowenchang
 */
@Getter
@Setter
public class MapSchema extends TypeSchema {
    private TypeSchema keyType;
    private TypeSchema valueType;

    public MapSchema() {
        super(TypeEnum.MAP);
    }

    public MapSchema(TypeSchema keyType, TypeSchema valueType) {
        this();
        this.keyType = keyType;
        this.valueType = valueType;
    }
}