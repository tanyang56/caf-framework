/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.entity;

import io.iec.edp.caf.rpc.api.common.GspSerializeType;
import io.iec.edp.caf.rpc.api.schema.TypeSchema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 *  Rpc返回值定义
 *
 * @author Leon Huo
 */
@Getter
@Setter
public class RpcReturnValueDefinition implements Serializable {

    //类型
    private TypeSchema type;
    //序列化类型
    private GspSerializeType serializeType = GspSerializeType.Json;
    //序列化器
    private String serializer = "";

    public RpcReturnValueDefinition() {
    }
}
