/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.api.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE , ElementType.PACKAGE})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface GspServiceBundle {
    /**
     *关键应用
     * @return
     */
    String applicationName() default "";

    /**
     * 服务单元名
     * @return
     */
    String serviceUnitName() default "";

    /**
     * 服务名
     * @return
     */
    String serviceName() default "";

    /**
     * 服务版本号
     * 默认1.0.0
     * @return
     */
    String serviceVersion() default "1.0.0";

}
