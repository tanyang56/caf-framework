/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.remote.grpc.filter;

import io.iec.edp.caf.rpc.api.annotation.RpcClientInterceptor;
import io.iec.edp.caf.rpc.api.filter.RpcClientFilter;
import io.iec.edp.caf.rpc.api.support.ConstanceVarible;
import io.iec.edp.caf.rpc.api.support.RpcThreadCacheHolder;
import lombok.var;

import java.util.Map;

@RpcClientInterceptor
public class GrpcClientFilter implements RpcClientFilter {
    @Override
    public void doOutFilter(Map<String, Object> reqContext) {

    }

    @Override
    public void doInFilter(Map<String, Object> repContext) {
        var gspContext = (String)repContext.get(ConstanceVarible.GSP_RPC_SERVER_ENVENT);
        if(gspContext!=null && gspContext.length()>0){
            RpcThreadCacheHolder.setValue(ConstanceVarible.GSP_RPC_SERVER_ENVENT,gspContext);
        }
    }

    @Override
    public void doFilter(Map<String, Object> reqContext, Map<String, Object> repContext) {

    }
}
