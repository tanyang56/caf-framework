/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.remote.grpc;

import io.grpc.*;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * gRpc客户端构造器标准方法
 */
public class GrpcChannelBuilder {

    /**
     * 根据ip\port构造Channel
     * @param host
     * @param port
     * @return
     */
    public static ManagedChannelBuilder<?> getChannelBuilder(String host, int port){

        return ManagedChannelBuilder.forAddress(host, port)
                // 使用非安全机制传输
                .usePlaintext();
    }

    /**
     * 根据url构造channel
     * @param remoteUrl
     * @return
     * @throws MalformedURLException
     */
    public static ManagedChannelBuilder<?> getChannelBuilder(String remoteUrl) throws MalformedURLException {
        URL url = new URL(remoteUrl);
        String host = url.getHost();
        int port = url.getPort();
        return getChannelBuilder(host,port);
    }

    public static void addClientInterceptor(ManagedChannelBuilder<?> channelBuilder, ClientInterceptor... interceptors){
        channelBuilder.intercept(interceptors);
    }
}
