/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.client;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.entity.DefaultExceptionProperties;
import io.iec.edp.caf.commons.exception.entity.ExceptionErrorCode;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.service.RpcClient;

import java.lang.reflect.Proxy;
import java.util.HashMap;

/**
 * @author Leon Huo
 */
public class RpcClassHolder {
    private RpcClient rpcClient;

    private HashMap<String, Object> rpcObjectMap = new HashMap<>();

    public RpcClassHolder(RpcClient rpcClient) {
        this.rpcClient = rpcClient;
    }

    public <T> T getRpcClass(Class<T> rpcInterface) {
        return getRpcClass(null, rpcInterface);
    }

    public <T> T getRpcClass(Class<T> rpcInterface, HashMap<String, String> rpcContext) {
        return getRpcClass(null, rpcInterface, rpcContext);
    }

    public <T> T getRpcClass(String serviceUnitName, Class<T> rpcInterface) {
        return getRpcClass(serviceUnitName, rpcInterface, null);
    }

    public <T> T getRpcClass(String serviceUnitName, Class<T> rpcInterface, HashMap<String, String> rpcContext) {
        if (!rpcInterface.isInterface()) {
            throw new CAFRuntimeException(DefaultExceptionProperties.SERVICE_UNIT,
                    DefaultExceptionProperties.RESOURCE_FILE, ExceptionErrorCode.rpcError,
                    new String[]{}, null, ExceptionLevel.Error, true);
        }

        GspServiceBundle annotation = rpcInterface.getAnnotation(GspServiceBundle.class);
        if (annotation == null) {
            throw new CAFRuntimeException(DefaultExceptionProperties.SERVICE_UNIT,
                    DefaultExceptionProperties.RESOURCE_FILE, ExceptionErrorCode.illegalRPCCall,
                    new String[]{}, null, ExceptionLevel.Error, true);
        }

        if (serviceUnitName == null) {
            serviceUnitName = annotation.serviceUnitName();
        }

        String key = serviceUnitName + ':' + rpcInterface.getName();

        Object rpcObject = rpcObjectMap.get(key);
        if (rpcObject == null) {
            rpcObject = Proxy.newProxyInstance(rpcInterface.getClassLoader(), new Class[]{rpcInterface}, new DynamicInvocationHandler(rpcClient, serviceUnitName, rpcContext));
            rpcObjectMap.put(key, rpcObject);
        }

        return (T) rpcObject;
    }
}
