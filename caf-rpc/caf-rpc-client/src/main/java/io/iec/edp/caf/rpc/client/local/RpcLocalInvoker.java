/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.client.local;

import io.iec.edp.caf.rpc.api.entity.RpcServiceMethodDefinition;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @author Leon Huo
 */
public interface RpcLocalInvoker {

    <T> T invokeLocalService(Class<T> clazz, String serviceId, String serviceUnitName, HashMap<String, Object> parameters, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId,HashMap<String, String> eventContext) throws IllegalAccessException, InvocationTargetException;

    InputStream invokeLocalServiceStream(String serviceId,String version, String serviceUnitName, HashMap<String, Object> parameters, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId, HashMap<String, String> eventContext) throws IllegalAccessException, InvocationTargetException;

    <T> T invokeLocalServiceStream(InputStream stream,Class<T> clazz,String serviceId, String version,String serviceUnitName, HashMap<String, Object> parameters, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId, HashMap<String, String> eventContext) throws IllegalAccessException, InvocationTargetException;
}
