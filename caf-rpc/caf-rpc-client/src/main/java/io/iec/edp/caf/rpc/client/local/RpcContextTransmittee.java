/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.client.local;

import io.iec.edp.caf.core.context.ICAFContextService;
import io.iec.edp.caf.rpc.client.entity.RpcContext;

/**
 * this class is rpc context transmitte
 * this class is use to capture context from parent thread
 * and replay this context in sub thread,finally to restore sub thread context;
 */
public interface RpcContextTransmittee {
    RpcContext capture(ICAFContextService cafContextService);

    void replay(ICAFContextService cafContextService,RpcContext newContext);

    void restore(ICAFContextService cafContextService);
}
