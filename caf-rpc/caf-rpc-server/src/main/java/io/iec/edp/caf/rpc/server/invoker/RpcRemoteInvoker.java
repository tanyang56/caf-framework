/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.server.invoker;

import io.iec.edp.caf.rpc.api.entity.RpcServiceDefinition;
import io.iec.edp.caf.rpc.api.entity.RpcServiceMethodDefinition;
import io.iec.edp.caf.rpc.api.support.Type;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @author Leon Huo
 */
public interface RpcRemoteInvoker {

//    Object remoteInvokeByAddress(Class clazz, String serviceId, String address, HashMap<String, Object> parameters,
//                                 Cookie[] cks, RpcServiceMethodDefinition rpcServiceMethodDefinition,HashMap<String, String> eventContext) throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException, NoSuchMethodException, InvocationTargetException;


    <T> T invokeRemoteService(Type<T> type, String serviceId, String serviceUnitName, String remoteUrl,HashMap<String, Object> parameters,
                              Cookie[] cks, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId,HashMap<String, String> eventContext) throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException, NoSuchMethodException, InvocationTargetException;

    InputStream invokeRemoteServiceStream(String serviceId,String version, String serviceUnitName, String remoteUrl, HashMap<String, Object> parameters,
                                          Cookie[] cks, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId, HashMap<String, String> eventContext) throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException, NoSuchMethodException, InvocationTargetException;

    <T> T invokeRemoteServiceStream(InputStream inputStream,Type<T> type,String serviceId,String version, String serviceUnitName, String remoteUrl, HashMap<String, Object> parameters,
                                          Cookie[] cks, RpcServiceMethodDefinition rpcServiceMethodDefinition, Integer tenantId, HashMap<String, String> eventContext) throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException, NoSuchMethodException, InvocationTargetException;

}
