/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.config;

import io.iec.edp.caf.rpc.registry.api.RpcDefinitionQuery;
import io.iec.edp.caf.rpc.registry.api.RpcDefinitionRegistry;
import io.iec.edp.caf.rpc.registry.impl.RpcDefinitionQueryImpl;
import io.iec.edp.caf.rpc.registry.impl.RpcDefinitionRegistryImpl;
import io.iec.edp.caf.rpc.registry.storage.RpcDefinitionStorage;
import io.iec.edp.caf.rpc.registry.storage.database.RpcDefinitionStorageDatabaseImpl;
import io.iec.edp.caf.rpc.registry.storage.database.domain.manager.RpcDefinitionManager;
import io.iec.edp.caf.rpc.registry.storage.database.domain.repository.RpcServiceDefinitionRepository;
import io.iec.edp.caf.rpc.registry.storage.database.domain.repository.RpcServiceMethodDefinitionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Rpc服务注册中心配置
 *
 * @author guowenchang
 * @date 2020-06-30
 */
@Configuration(proxyBeanMethods = false)
@EnableJpaRepositories("io.iec.edp.caf.rpc.registry.storage.database.domain.repository")
@EntityScan("io.iec.edp.caf.rpc.registry.storage.database.domain.entity")
public class RpcRegistryAutoConfiguration {

    private RpcDefinitionStorage rpcDefinitionStorage;

    public RpcRegistryAutoConfiguration(RpcServiceDefinitionRepository rpcServiceDefinitionRepository, RpcServiceMethodDefinitionRepository rpcServiceMethodDefinitionRepository) {
        RpcDefinitionManager manager = new RpcDefinitionManager(rpcServiceDefinitionRepository, rpcServiceMethodDefinitionRepository);
        this.rpcDefinitionStorage = new RpcDefinitionStorageDatabaseImpl(manager);
    }

    @Bean
    public RpcDefinitionRegistry createRpcServiceRegistry() {
        return new RpcDefinitionRegistryImpl(rpcDefinitionStorage);
    }

    @Bean
    public RpcDefinitionQuery createRpcServiceQuery() {
        return new RpcDefinitionQueryImpl(rpcDefinitionStorage);
    }

}
