/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.domain.repository;

import io.iec.edp.caf.rpc.registry.storage.database.domain.entity.RpcServiceMethodDefinitionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface RpcServiceMethodDefinitionRepository extends JpaRepository<RpcServiceMethodDefinitionEntity, String> {
    List<RpcServiceMethodDefinitionEntity> findAllByServiceDefinitionId(String serviceDefinitionId);

    List<RpcServiceMethodDefinitionEntity> findAllByServiceDefinitionIdIn(Collection<String> serviceDefinitionId);
}
