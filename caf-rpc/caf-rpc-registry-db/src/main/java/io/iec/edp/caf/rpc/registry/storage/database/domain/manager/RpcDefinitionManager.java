/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.domain.manager;

import io.iec.edp.caf.rpc.registry.storage.database.domain.entity.RpcServiceDefinitionEntity;
import io.iec.edp.caf.rpc.registry.storage.database.domain.entity.RpcServiceMethodDefinitionEntity;
import io.iec.edp.caf.rpc.registry.storage.database.domain.repository.RpcServiceDefinitionRepository;
import io.iec.edp.caf.rpc.registry.storage.database.domain.repository.RpcServiceMethodDefinitionRepository;

import java.util.List;

public class RpcDefinitionManager {

    private RpcServiceDefinitionRepository serviceRepository;

    private RpcServiceMethodDefinitionRepository methodRepository;

    public RpcDefinitionManager(RpcServiceDefinitionRepository serviceRepository,
                                RpcServiceMethodDefinitionRepository methodRepository) {
        this.serviceRepository = serviceRepository;
        this.methodRepository = methodRepository;
    }

    public Boolean saveRpcServiceDefinition(List<RpcServiceDefinitionEntity> definitionList) {
        serviceRepository.saveAll(definitionList);
        return true;
    }

    public Boolean saveRpcServiceMethodDefinition(List<RpcServiceMethodDefinitionEntity> definitionList) {
        methodRepository.saveAll(definitionList);
        return true;
    }

    public RpcServiceDefinitionEntity getRpcServiceDefinition(String rpcServiceId) {
        return serviceRepository.getOne(rpcServiceId);
    }

    public List<RpcServiceMethodDefinitionEntity> getRpcServiceMethodDefinitions(String rpcServiceId) {
        return methodRepository.findAllByServiceDefinitionId(rpcServiceId);
    }

    public List<String> getRpcServiceDefinitionList() {
        return serviceRepository.findAllIds();
    }

    public List<RpcServiceDefinitionEntity> getRpcServiceDefinitionEntitiesBySu(String su) {
        return serviceRepository.findRpcServiceDefinitionEntitiesBySu(su);
    }

    public List<RpcServiceMethodDefinitionEntity> getRpcServiceMethodDefinitionEntitiesByServiceDefinitionIdIn(List<String> serviceIdList) {
        return methodRepository.findAllByServiceDefinitionIdIn(serviceIdList);
    }
}
