/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.domain.convert;

import io.iec.edp.caf.rpc.api.entity.RpcReturnValueDefinition;
import io.iec.edp.caf.rpc.api.serialize.RpcCustomSerializer;
import io.iec.edp.caf.rpc.api.serialize.RpcDefinitionSerializer;

import javax.persistence.AttributeConverter;

public class RpcReturnValueConverter implements AttributeConverter<RpcReturnValueDefinition, String> {
    private static RpcCustomSerializer rpcCustomSerializer = new RpcDefinitionSerializer();

    public RpcReturnValueConverter() {
    }

    public String convertToDatabaseColumn(RpcReturnValueDefinition attribute) {
        return attribute == null ? "" : rpcCustomSerializer.serialize(attribute);
    }

    public RpcReturnValueDefinition convertToEntityAttribute(String dbData) {
        return dbData != null && dbData.length() != 0 ? rpcCustomSerializer.deserialize(dbData, RpcReturnValueDefinition.class) : null;
    }
}
