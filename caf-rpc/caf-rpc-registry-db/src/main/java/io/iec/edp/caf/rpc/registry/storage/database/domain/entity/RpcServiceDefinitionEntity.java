/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.domain.entity;

import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Rpc服务存储结构定义
 *
 * @author guowenchang
 */
@Data
@Entity
@Table(name = "RpcServiceDefinition")
@Proxy(lazy = false)
public class RpcServiceDefinitionEntity {
    @Id
    //包名加类名加版本号
    private String id;
    //Rpc服务名称
    private String name;
    //描述
    private String description;
    //真实类名
    private String className;
    //版本
    private String version;
    //关键应用
    private String app;
    //服务单元
    private String su;
}