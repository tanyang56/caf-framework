/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.registry.storage.database.domain.entity;

import io.iec.edp.caf.rpc.api.entity.RpcParamDefinition;
import io.iec.edp.caf.rpc.api.entity.RpcReturnValueDefinition;
import io.iec.edp.caf.rpc.registry.storage.database.domain.convert.RpcDefinitionConverter;
import io.iec.edp.caf.rpc.registry.storage.database.domain.convert.RpcReturnValueConverter;
import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Rpc服务方法存储结构定义
 *
 * @author guowenchang
 */
@Data
@Entity
@Table(name = "RpcServiceMethodDefinition")
@Proxy(lazy = false)
public class RpcServiceMethodDefinitionEntity {

    @Id
    //包名+类名+版本号+方法名
    private String id;
    //包名+类名+方法名/自定义
    private String serviceId;
    //名称
    private String name;
    //描述
    private String description;
    //rpcServiceDefinition的id
    private String serviceDefinitionId;

    //参数列表
    @Convert(converter = RpcDefinitionConverter.class)
    private List<RpcParamDefinition> parameters;

    //参数列表
    @Convert(converter = RpcReturnValueConverter.class)
    //返回值
    private RpcReturnValueDefinition returnInfo;
}
