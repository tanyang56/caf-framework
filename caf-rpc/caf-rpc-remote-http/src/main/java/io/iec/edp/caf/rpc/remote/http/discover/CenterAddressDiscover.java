/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.remote.http.discover;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import io.iec.edp.caf.msu.api.client.ServiceDiscovery;
import io.iec.edp.caf.msu.api.entity.MsuProperties;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

import java.util.HashMap;

/**
 * 通过服务中心获取地址
 * <p>
 * todo 加缓存 加重试
 *
 * @author guowenchang
 */
@Slf4j
public class CenterAddressDiscover implements RpcAddressDiscover {

    //服务中心配置
    private MsuProperties msuProperties;

    //服务发现
    private ServiceDiscovery serviceDiscovery;

    public CenterAddressDiscover(MsuProperties msuProperties, ServiceDiscovery serviceDiscovery) {
        this.msuProperties = msuProperties;
        this.serviceDiscovery = serviceDiscovery;
    }

    @Override
    public String getAddress(String serviceUnitName, HashMap<String, String> eventContext) {
        log.info("ServiceDiscovery(RPC) Discovery instance by su [{}]", serviceUnitName);
        String addr = serviceDiscovery.discover(serviceUnitName, eventContext);
        log.info("ServiceDiscovery(RPC) Instance is [{}]", addr);
        return addr;
    }
}
