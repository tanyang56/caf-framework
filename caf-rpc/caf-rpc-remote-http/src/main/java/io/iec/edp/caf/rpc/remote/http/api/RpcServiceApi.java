/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rpc.remote.http.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.iec.edp.caf.rpc.remote.http.entity.RpcStreamParam;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;


import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;

/**
 * @author Leon Huo
 */
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface RpcServiceApi {
    @POST
    @Path("/{serviceId}/{version}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    String invoke(@PathParam("serviceId") String serviceId, @PathParam("version") String version, LinkedHashMap<String, String> paramDict) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, JsonProcessingException;


    @POST
    @Path("/streamdown/{serviceId}/{version}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    void invokeStream(@PathParam("serviceId") String serviceId, @PathParam("version") String version, LinkedHashMap<String, String> paramDict) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, JsonProcessingException;


    @POST
    @Path("/streamup")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    Object invokeStream(
            @Multipart(value = "serviceid",type = MediaType.APPLICATION_JSON) String serviceId,
            @Multipart(value = "version",type = MediaType.APPLICATION_JSON) String version,
            @Multipart(value = "params",type = MediaType.APPLICATION_JSON) LinkedHashMap<String, String> paramDict,
            @Multipart(value = "stream",type = MediaType.APPLICATION_OCTET_STREAM) InputStream stream) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IOException;

}
