/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.context.event;

import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FilterInvokeEventBroker extends EventBroker {

    private FilterInvokeEventManager filterInvokeEventManager;

    public FilterInvokeEventBroker(EventListenerSettings settings) {
        super(settings);
        this.filterInvokeEventManager = new FilterInvokeEventManager();
        this.init();
    }

    public void fireBeforeFilterInvokeEvent(HttpServletRequest request,
                                            HttpServletResponse response) {
        BeforeFilterInvokeEventArgs args = new BeforeFilterInvokeEventArgs();
        args.setRequest(request);
        args.setResponse(response);
        filterInvokeEventManager.fireBeforeFilterInvokeEvent(args);
    }

    public void fireAfterFilterInvokeEvent(HttpServletRequest request,
                                           HttpServletResponse response) {
        AfterFilterInvokeEventArgs args = new AfterFilterInvokeEventArgs();
        args.setRequest(request);
        args.setResponse(response);
        filterInvokeEventManager.fireAfterFilterInvokeEvent(args);
    }

    @Override
    protected void onInit() {
        this.eventManagerCollection.add(filterInvokeEventManager);
    }
}
