/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.context;

import io.iec.caf.data.jpa.repository.support.CafJpaLanguageThreadHolder;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.core.session.CafSession;

import io.iec.edp.caf.core.session.CafSessionContextHolder;
import io.iec.edp.caf.i18n.api.LanguageSuffixProvider;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.i18n.framework.api.language.ILanguageService;
import org.hibernate.persister.entity.PersistersContext;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 上下文设置包装类，方便后台线程设置session及语言上下文
 * @author wangyandong
 * @date 2020/11/23 19:21
 *
 */
public class CAFContextWrapper {
    private final static HashMap<String, Map<String,String>> languageListCache = new HashMap<>();
    private ILanguageService languageService;
    private LanguageSuffixProvider suffixProvider;

    public static CAFContextWrapper current = new CAFContextWrapper();
    private Lock lock = new ReentrantLock();

    protected CAFContextWrapper(){

    }

    /**
     * 根据Session设置Session及语言上下文
     * @param session
     */
    public void setCurrentContext(CafSession session){
        if(session!=null) {
            //将Session设置为当前Session
            CafSessionContextHolder.setCurrentSession(session);

            //设置语言上下文
            String language = session.getLanguage();
            this.setCurrentLanguage(language);
        }
    }

    /**
     * 根据当前语言设置语言上下文
     * @param language
     */
    public void setCurrentLanguage(String language){
        initSuffixProvider();
        String currentLangSuffix = this.suffixProvider.getFieldSuffix(language);

        //设置当前语言后缀
        String ctxLangSuffix = PersistersContext.getCurrentLangSuffix();
        if(!currentLangSuffix.equals(ctxLangSuffix))
            PersistersContext.setCurrentLangSuffix(currentLangSuffix);

        //设置语言列表
        CafJpaLanguageThreadHolder.setLanguages(this.getLanguageList());
    }

    /**
     * 获取语言服务
     * @return
     */
    private ILanguageService getLanguageService(){
        if(this.languageService ==null)
            this.languageService = SpringBeanUtils.getBean(ILanguageService.class);

        return this.languageService;
    }

    /**
     * 获取语言服务
     * @return
     */
    private void initSuffixProvider() {
        if(this.suffixProvider == null)
            this.suffixProvider = SpringBeanUtils.getBean(LanguageSuffixProvider.class);
    }

    /**
     * 获取语言列表
     * @return
     */
    private Map<String,String> getLanguageList(){
        String tenantId = String.valueOf(CAFContext.current.getTenantId());
        if(!languageListCache.containsKey(tenantId)){
            try {
                lock.lock();
                if (!languageListCache.containsKey(tenantId)) {
                    List<EcpLanguage> languages = this.getLanguageService().getEnabledLanguages();
                    Iterator<EcpLanguage> it = languages.iterator();
                    Map<String,String> langs= new HashMap<>();
                    while(it.hasNext()){
                        EcpLanguage item = it.next();
                        if(item != null){
                            langs.put(item.getId(),item.getFieldSuffix());
                        }
                    }
                    languageListCache.put(tenantId,langs);
                }
            } finally {
                lock.unlock();
            }
        }
        return languageListCache.getOrDefault(tenantId,null);
    }
}
