/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.context.event;


import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class FilterInvokeEventManager extends EventManager {
    public FilterInvokeEventManager() {
    }

    public void fireBeforeFilterInvokeEvent(BeforeFilterInvokeEventArgs args){
        this.fire(FilterInvokeEventType.BeforeFilterInvoke,args);
    }

    public void fireAfterFilterInvokeEvent(AfterFilterInvokeEventArgs args){
        this.fire(FilterInvokeEventType.AfterFilterInvoke,args);
    }

    @Override
    public String getEventManagerName(){
        return "FilterInvokeEventManager";
    }

    @Override
    public boolean isHandlerListener(IEventListener listener){
        return listener instanceof FilterInvokeEventListener;
    }

    @Override
    public void addListener(IEventListener listener){
        if( !(listener instanceof FilterInvokeEventListener)){
            throw new RuntimeException("指定的监听者没有实现[FilterInvokeEventListener]接口！！");
        }
        FilterInvokeEventListener filterInvokeEventListener = (FilterInvokeEventListener)listener ;
        this.addEventHandler(FilterInvokeEventType.BeforeFilterInvoke,filterInvokeEventListener, "beforeFilterInvoke");
        this.addEventHandler(FilterInvokeEventType.AfterFilterInvoke,filterInvokeEventListener, "afterFilterInvoke");
    }
    @Override
    public void removeListener(IEventListener listener){
        if( !(listener instanceof FilterInvokeEventListener)){
            throw new RuntimeException("指定的监听者没有实现[FilterInvokeEventListener]接口！！");
        }
        FilterInvokeEventListener filterInvokeEventListener = (FilterInvokeEventListener)listener ;
        this.removeEventHandler(FilterInvokeEventType.BeforeFilterInvoke,filterInvokeEventListener, "beforeFilterInvoke");
        this.removeEventHandler(FilterInvokeEventType.AfterFilterInvoke,filterInvokeEventListener, "afterFilterInvoke");
    }
}
