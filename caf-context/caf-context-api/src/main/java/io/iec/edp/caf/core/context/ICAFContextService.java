/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.context;

import io.iec.edp.caf.core.session.CurrentSessionManager;
import io.iec.edp.caf.i18n.api.CurrentLanguageProvider;

import java.time.OffsetDateTime;

//todo 被com/inspur/edp/svc/cloudprint/devicemanager/config/DeviceManagerConfiguration依赖
public interface ICAFContextService extends CurrentSessionManager, CurrentLanguageProvider {
    /**
     * 获取业务Context
     */
    BizContext getBizContext();

    /**
     * 设置
     * @param context
     */
    void setBizContext(BizContext context);

    /**
     * 获取框架Context
     *
     */
    FrameworkContext getFrameworkContext();

    /**
     * 设置框架上下文
     */
    void setFrameworkContext(FrameworkContext context);

    /**
     * 获取当前的BizContextId
     */
    String getContextId();


    /**
     * 获取当前SU编号
     */
    String getCurrentSU();

    /**
     * 设置当前SU编号
     */
    void setCurrentSU(String su);

    /**
     * 获取Session标识
     */
    String getSessionId();

    /**
     * 获取租户标识
     */
    int getTenantId();

    /**
     * 用户编号
     */
    String getUserId();

    /**
     * 获取实例编号
     */
    String getAppCode();

    /**
     * 获取登录时间
     */
    OffsetDateTime getLoginTime();

    /**
     * 获取系统当前时间
     */
    OffsetDateTime getCurrentDateTime();
}
