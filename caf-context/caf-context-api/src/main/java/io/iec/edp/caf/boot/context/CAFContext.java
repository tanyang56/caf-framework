/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.context;

import com.zaxxer.hikari.HikariConfig;
import io.iec.edp.caf.commons.runtime.CafEnvironment;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.core.context.BizContext;
import io.iec.edp.caf.core.context.CafContextServiceImpl;
import io.iec.edp.caf.core.context.FrameworkContext;
import io.iec.edp.caf.core.context.ICAFContextService;
import io.iec.edp.caf.core.session.CafSession;
import io.iec.edp.caf.core.session.WebSession;
import io.iec.edp.caf.data.source.DataSourceTypeRecognizer;
import io.iec.edp.caf.commons.dataaccess.DbType;

import java.time.OffsetDateTime;

/**
 * CAF上下文
 * @author wangyandong
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CAFContext {

    private static DbType masterDbType = DbType.Unknown;
    /**
     * CAF上下文
     */
    public final static CAFContext current = new CAFContext();

    private ICAFContextService service = null;
    /**
     * 私有构造函数，防止开发人员直接创建
     */
    private CAFContext(){

    }

    /**
     * 获取当前的Session对象
     */
    @Deprecated
    //todo 被FrameworkFilterListener依赖
    public WebSession getSession(){
        CafSession session = this.getCurrentSession();
        if(session instanceof WebSession)
            return (WebSession)session;
        return null;
    }

    //todo 被打印依赖
    public String getRuntimePath(){
        return CafEnvironment.getServerRTPath();
    }
    /**
     * 获取当前的Session对象
     */
    public CafSession getCurrentSession(){
        return this.getService().getCurrentSession();
    }

    /**
     * 获取深复制的cafsession
     * @return
     */
    public CafSession getCopyedCurrentSession(){
        return this.getService().getCopyedCurrentSession();
    }

    /**
     * 获取业务Context
     */
    public BizContext getBizContext() {
        ICAFContextService service = this.getService();
        return service!=null? service.getBizContext() : null;
    }

    public FrameworkContext getFrameworkContext(){
        ICAFContextService service = this.getService();
        return service!=null? service.getFrameworkContext() : null;
    }

  /**
     * 获取当前的BizContextId
     */
    public String getContextId() {
        ICAFContextService service = this.getService();
        return service!=null? service.getContextId() : null;
    }


    /**
     * 获取当前SU编号
     */
    public String getCurrentSU() {
        ICAFContextService service = this.getService();
        return service!=null? service.getCurrentSU() : null;
    }

    /**
     * 设置当前SU编号
     */

    void setCurrentSU(String su) {
        ICAFContextService service = this.getService();
        if(service!=null)
            ((CafContextServiceImpl)service).setCurrentSU(su);
    }

    /**
     * 获取Session标识
     */
    public String getSessionId() {
        ICAFContextService service = this.getService();
        return service!=null? service.getSessionId() : null;
    }

    /**
     * 获取租户标识
     */
    public int getTenantId() {
        ICAFContextService service = this.getService();
        return service!=null? service.getTenantId() : -1;
    }

    /**
     * 用户编号
     */
    public String getUserId(){
        ICAFContextService service = this.getService();
        return service!=null? service.getUserId() : null;
    }

    /**
     * 获取实例编号
     */
    public String getAppCode(){
        ICAFContextService service = this.getService();
        return service!=null? service.getAppCode() : null;
    }

    /**
     * 获取登录时间
     */
    public OffsetDateTime getLoginTime(){
        ICAFContextService service = this.getService();
        return service!=null? service.getLoginTime() : null;
    }

    /**
     * 获取当前语言
     */
    public String getLanguage(){
        ICAFContextService service = this.getService();
        String langauge = null;;
        if(service!=null)
            langauge = service.getLanguage();
        return langauge!=null?langauge:"zh-CHS";
    }

    /**
     * 获取系统当前时间
     */
    public OffsetDateTime getCurrentDateTime(){
        ICAFContextService service = this.getService();
        return service!=null? service.getCurrentDateTime() : null;
    }

    /**
     * 获取当前数据库类型
     * @return
     */
    public DbType getDbType(){
        if (masterDbType == DbType.Unknown) {
            HikariConfig wholeHikariConfig= SpringBeanUtils.getBean(HikariConfig.class);
            String jdbcUrl = wholeHikariConfig.getJdbcUrl();
            masterDbType = DataSourceTypeRecognizer.recognize(jdbcUrl);
        }
        return masterDbType;
    }

    /**
     * 获取当前的数据库类型
     */
    public ICAFContextService getService(){
        if(this.service ==null)
            this.service = SpringBeanUtils.getBean(ICAFContextService.class);
        return this.service;
    }
}
