
/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package io.iec.edp.caf.context.util;

import io.iec.edp.caf.commons.utils.StringUtils;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class BuriedPointMachineUtil {
    private static String processNo = getProcessNo();
    private static String IP;
    private static String hostName;

    public static String getProcessNo() {
        if (StringUtils.isEmpty(processNo)) {
            String name = ManagementFactory.getRuntimeMXBean().getName();
            processNo = name.split("@")[0];
        }

        return processNo;
    }

    private static InetAddress getInetAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException var1) {
            hostName = "unknown host!";
            return null;
        }
    }

    public static String getHostIp() {
        if (StringUtils.isEmpty(IP)) {
            InetAddress netAddress = getInetAddress();
            if (null == netAddress) {
                IP = "N/A";
            } else {
                IP = netAddress.getHostAddress();
            }
        }

        return IP;
    }

    public static String getHostName() {
        if (StringUtils.isEmpty(hostName)) {
            InetAddress netAddress = getInetAddress();
            if (null == netAddress) {
                hostName = "N/A";
            } else {
                hostName = netAddress.getHostName();
            }
        }

        return hostName;
    }

    public static String getHostDesc() {
        return getHostName() + "/" + getHostIp();
    }

    private BuriedPointMachineUtil() {
    }
}
