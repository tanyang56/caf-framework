/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.context;

import io.iec.edp.caf.core.session.Session;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * This is {@link BizContextBuilder}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
public interface BizContextBuilder<C extends BizContext, P extends Session> {

	@SuppressWarnings("unchecked")
	default Class<C> getContextType() {
		int index = 0;
		Type genType = this.getClass().getGenericInterfaces()[0];
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		return (Class) params[index];
		// return (Class<C>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	C build(P parent);

}
