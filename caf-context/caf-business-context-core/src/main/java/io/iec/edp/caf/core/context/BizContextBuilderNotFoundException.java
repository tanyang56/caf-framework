/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.context;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.entity.DefaultExceptionProperties;
import io.iec.edp.caf.commons.exception.entity.ExceptionErrorCode;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
/**
 * This is {@link BizContextBuilderNotFoundException}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@SuppressWarnings("WeakerAccess")
public class BizContextBuilderNotFoundException extends CAFRuntimeException {


    /**
     * 空参构造函数
     */
    public BizContextBuilderNotFoundException() {
        super(DefaultExceptionProperties.SERVICE_UNIT, DefaultExceptionProperties.RESOURCE_FILE, ExceptionErrorCode.bizContextBuilderNotFound, null, null, ExceptionLevel.Error);
        super.setBizException(true);
    }
}
