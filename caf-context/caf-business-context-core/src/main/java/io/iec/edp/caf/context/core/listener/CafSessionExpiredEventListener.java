/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.context.core.listener;

import io.iec.edp.caf.core.context.BizContextManager;
import org.springframework.context.ApplicationListener;
import org.springframework.session.events.SessionExpiredEvent;

/**
 * Session过期事件监听
 *
 * config set notify-keyspace-events Egx
 * @author wangyandong
 * @date 2020/1/1 16:02
 *
 */
public class CafSessionExpiredEventListener implements ApplicationListener<SessionExpiredEvent> {

    private BizContextManager manager;

    public CafSessionExpiredEventListener(BizContextManager manager){
        this.manager = manager;
    }

    @Override
    public void onApplicationEvent(SessionExpiredEvent sessionExpiredEvent) {
        this.manager.destroyBySession(sessionExpiredEvent.getSessionId());
    }
}
