/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.context;

import io.iec.edp.caf.context.core.listener.BizContextListener;

import java.util.concurrent.TimeUnit;

/**
 * BizContext管理接口，供业务调用
 *
 * @author yisiqi
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public interface BizContextManager {

    /**
     * 创建顶级上下文
     *
     * @param contextType 上下文类型定义
     * @param <C>         基于BizContext派生的上下文类型
     * @return 新的上下文
     */
    @Deprecated
    <C extends BizContext> C createRootContext(Class<C> contextType) throws BizContextBuilderNotFoundException;

    /**
     * 在所提供ID的Context下，创建新的上下文
     *
     * @param contextType 上下文类型定义
     * @param parentId    父级ID
     * @param <C>         基于BizContext派生的上下文类型
     * @return 新的上下文
     * @throws BizContextNotFoundException 无法找到指定ID的父级上下文
     */
    @Deprecated
    <C extends BizContext> C createContextUnder(Class<C> contextType, String parentId) throws BizContextNotFoundException, BizContextBuilderNotFoundException;

    /**
     * @param contextType
     * @param parent
     * @param <C>
     * @return
     * @throws BizContextNotFoundException 无法找到指定的父级上下文
     */
    @Deprecated
    <C extends BizContext> C createContextUnder(Class<C> contextType, BizContext parent) throws BizContextNotFoundException, BizContextBuilderNotFoundException;

    /**
     * 创建顶级上下文
     *
     * @param contextType    上下文类型定义
     * @param expiration     过期时间
     * @param unit           时间单位
     * @param <C>            基于BizContext派生的上下文类型
     * @param serializerType 序列化方法
     * @return 新的上下文
     * @throws BizContextBuilderNotFoundException 无法找到指定ID的父级上下文
     */
    <C extends BizContext> C createRootContext(Class<C> contextType, long expiration, TimeUnit unit, String serializerType) throws BizContextBuilderNotFoundException;

    /**
     * 在所提供ID的Context下，创建新的上下文
     *
     * @param contextType    上下文类型定义
     * @param parentId       父级ID
     * @param expiration     过期时间
     * @param unit           时间单位
     * @param serializerType 序列化方法
     * @param <C>            基于BizContext派生的上下文类型
     * @return 新的上下文
     * @throws BizContextBuilderNotFoundException 无法找到指定ID的父级上下文
     */
    <C extends BizContext> C createContextUnder(Class<C> contextType, String parentId, long expiration, TimeUnit unit, String serializerType) throws BizContextNotFoundException, BizContextBuilderNotFoundException;

    /**
     * 在所提供父级Context下，创建新的上下文
     *
     * @param contextType    上下文类型定义
     * @param parent         父级上下文
     * @param expiration     过期时间
     * @param unit           时间单位
     * @param serializerType 序列化方法
     * @param <C>            基于BizContext派生的上下文类型
     * @return 新的上下文
     * @throws BizContextNotFoundException、BizContextBuilderNotFoundException
     */
    <C extends BizContext> C createContextUnder(Class<C> contextType, BizContext parent, long expiration, TimeUnit unit, String serializerType) throws BizContextNotFoundException, BizContextBuilderNotFoundException;

    /**
     * 获取指定类型、ID的上下文
     *
     * @param contextType 上下文类型定义
     * @param contextId   上下文ID
     * @param <C>         基于BizContext派生的上下文类型
     * @return 指定类型、ID的上下文
     * @throws BizContextNotFoundException 无法找到指定类型、ID的上下文
     */
    <C extends BizContext> C fetch(Class<C> contextType, String contextId) throws BizContextNotFoundException, BizContextBuilderNotFoundException;

    /**
     * 获取指定ID的上下文
     *
     * @param contextId 上下文ID
     * @return 指定ID的上下文
     * @throws BizContextNotFoundException 无法找到指定ID的上下文
     */
    BizContext fetch(String contextId) throws BizContextNotFoundException;

    /**
     * 推断指定ID的上下文类型
     *
     * @param contextId 指定的上下文ID
     * @return 指定ID的上下文类型
     * @throws BizContextNotFoundException 无法找到指定ID的上下文
     */
    Class<? extends BizContext> deduce(String contextId) throws BizContextNotFoundException;

    /**
     * 更新上下文类型
     *
     * @param context 指定的上下文
     */
    void update(BizContext context);

    /**
     * 注册上下文监听器
     *
     * @param contextType 监听上下文类型
     * @param listener    上下文监听器
     * @param <C>         基于BizContext派生的上下文类型
     * @return 监听注册ID
     */
    <C extends BizContext> String register(Class<C> contextType, BizContextListener<C> listener);

    /**
     * 注销上下文监听器
     *
     * @param registrationId 监听注册ID
     */
    void unRegister(String registrationId);

    /**
     * 注销上下文监听器
     *
     * @param contextType 监听上下文类型
     * @param listener    上下文监听器
     * @param <C>         基于BizContext派生的上下文类型
     */
    <C extends BizContext> void unRegister(Class<C> contextType, BizContextListener<C> listener);

    /**
     * 销毁上下文
     *
     * @param sessionId 指定上下文ID
     */
    void destroyBySession(String sessionId);

    /**
     * 销毁上下文
     *
     * @param contextId 指定上下文ID
     * @throws BizContextNotFoundException      无法找到指定的上下文
     * @throws BizContextDestroyFailedException 无法销毁指定的上下文
     */
    void destroy(String contextId) throws BizContextNotFoundException, BizContextDestroyFailedException;

    /**
     * 销毁上下文
     *
     * @param context 指定上下文
     * @throws BizContextNotFoundException      无法找到指定的上下文
     * @throws BizContextDestroyFailedException 无法销毁指定的上下文
     */
    void destroy(BizContext context) throws BizContextNotFoundException, BizContextDestroyFailedException;

    /**
     * 销毁上下文
     *
     * @param contextId 指定上下文ID
     * @param cascade   是否级联删除
     * @throws BizContextNotFoundException      无法找到指定的上下文
     * @throws BizContextDestroyFailedException 无法销毁指定的上下文
     */
    void destroy(String contextId, Boolean cascade) throws BizContextNotFoundException, BizContextDestroyFailedException;

    /**
     * 销毁上下文
     *
     * @param context 指定上下文
     * @param cascade 是否级联删除
     * @throws BizContextNotFoundException      无法找到指定的上下文
     * @throws BizContextDestroyFailedException 无法销毁指定的上下文
     */
    void destroy(BizContext context, Boolean cascade) throws BizContextNotFoundException, BizContextDestroyFailedException;

    /**
     * 判断Session是否过期
     *
     * @param contextId
     * @return
     */
    boolean isExpired(String contextId);

    /**
     * 延长业务上下文时间
     *
     * @param contextId 业务上下文id
     */
    void prolong(String contextId);

}
