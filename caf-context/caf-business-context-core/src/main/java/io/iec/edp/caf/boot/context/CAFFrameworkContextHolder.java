/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.context;

import io.iec.edp.caf.core.context.FrameworkContext;
import org.springframework.util.Assert;

import java.util.LinkedList;
import java.util.Optional;

/**
 * This is {@link CAFFrameworkContextHolder}.
 *
 * @author huoliang
 * @since 1.0.0
 */
public class CAFFrameworkContextHolder {

    private static final ThreadLocal<LinkedList<FrameworkContext>> threadLocal = new InheritableThreadLocal<>();


//    @Deprecated
//    public static FrameworkContext getContext() {
//        Optional<FrameworkContext> optionalFrameworkContext = CAFFrameworkContextHolder.getCurrentContext();
//        return optionalFrameworkContext.orElse(null);
//    }

    public static Optional<FrameworkContext> getCurrentContext() {
        LinkedList<FrameworkContext> frameworkContextStack = threadLocal.get();
        if (frameworkContextStack != null && frameworkContextStack.size() > 0) {
            return Optional.of(frameworkContextStack.peekFirst());
        }
        return Optional.empty();
    }

    public static void setCurrentContext(FrameworkContext context) {
        Assert.notNull(context, "Only non-null BizContext instances are permitted");
        LinkedList<FrameworkContext> frameworkContextStack = threadLocal.get();
        if (frameworkContextStack == null) {
            frameworkContextStack = new LinkedList<>();
        }
        frameworkContextStack.push(context);
        threadLocal.set(frameworkContextStack);
    }

    public static void releaseCurrentContext() {
        LinkedList<FrameworkContext> frameworkContextStack = threadLocal.get();
        if (frameworkContextStack != null && frameworkContextStack.size() > 0) {
            frameworkContextStack.pop();
        }
    }

    public static void purge() {
        threadLocal.remove();
    }

}

