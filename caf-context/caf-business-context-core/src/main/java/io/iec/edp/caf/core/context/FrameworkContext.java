/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.context;

import io.iec.edp.caf.core.session.Session;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * This is {@link FrameworkContext}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@Getter
@EqualsAndHashCode(callSuper = true)
//todo 被io/iec/edp/caf/runtime/sessiongroup/config/SessionGroupMgrAutoConfig依赖
public abstract class FrameworkContext<P extends Session> extends Session {

    protected final P parent;
    private final Map<String, Object> items;
    protected BizContextExpirationPolicy expirationPolicy;
    private Object b;
    private String b1;

    private String token;
    private String contentId;
    private String funcId;
    private String funcCode;
    private String appId;
    private String appCode;
    private String appEntrance;
    private String boId;

    protected FrameworkContext(String id, ZonedDateTime creationDate, P parent, BizContextExpirationPolicy expirationPolicy) {
        this(id, creationDate, parent, new HashMap<>(), expirationPolicy);
    }

    protected FrameworkContext(String id, ZonedDateTime creationDate, P parent, Map<String, Object> items, BizContextExpirationPolicy expirationPolicy) {
        super(id, creationDate);
        this.parent = parent;
        this.items = items;
        this.expirationPolicy = expirationPolicy;
        if (items != null) {
            this.token = items.containsKey("token") ? items.get("token").toString() : null;
            this.contentId = items.containsKey("contentId") ? items.get("contentId").toString() : null;
            this.funcId = items.containsKey("funcId") ? items.get("funcId").toString() : null;
            this.funcCode = items.containsKey("funcCode") ? items.get("funcCode").toString() : null;
            this.appId = items.containsKey("appId") ? items.get("appId").toString() : null;
            this.appCode = items.containsKey("appCode") ? items.get("appCode").toString() : null;
            this.appEntrance = items.containsKey("appEntrance") ? items.get("appEntrance").toString() : null;
            this.boId = items.containsKey("boId") ? items.get("boId").toString() : null;
        }
    }

    /**
     * 根据键获取值
     *
     * @param key 键
     * @return 值，如果键不存在，则向上查找，如果找不到返回null。
     */
    public Object get(String key) {
        //首先从当前的items中获取，存在则直接返回
        if (this.items != null && this.items.containsKey(key)) {
            return items.get(key);
        }

        //如果parent不为null && parent派生自BizContext，则向上递归查询
        if (this.getParent() != null && FrameworkContext.class.isAssignableFrom(this.getParent().getClass())) {
            FrameworkContext context = (FrameworkContext) this.getParent();
            if (context != null)
                return context.get(key);
        }
        return null;
    }

    /**
     * 添加k-v变量
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, Object value) {
        if (key != null && !"".equals(key))
            this.items.put(key, value);
    }

    /**
     * 添加k-v变量
     *
     * @param mapItems 键
     */
    public void put(Map<String, Object> mapItems) {
        if (mapItems != null && mapItems.size() > 0) {
            this.getItems().putAll(mapItems);
        }
    }
}
