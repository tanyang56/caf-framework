/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.context.dist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * BizContext的设置
 *
 * @author manwenxing01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisBizContextSetting {

    //过期时间
    private long expiration;

    //时间单位
    private TimeUnit unit;

    //自定义序列化的全限定类名
    private String serializer;

    //业务上下文类型
    private String clazz;

}
