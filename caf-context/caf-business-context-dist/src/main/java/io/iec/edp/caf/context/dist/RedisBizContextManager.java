/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.context.dist;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.runtime.redis.enums.RedisMode;
import io.iec.edp.caf.commons.runtime.redis.properties.CafRedisConfiguration;
import io.iec.edp.caf.commons.runtime.redis.properties.RedisSetting;
import io.iec.edp.caf.commons.utils.InvokeService;
import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.context.core.listener.BizContextListener;
import io.iec.edp.caf.core.context.*;
import io.iec.edp.caf.core.session.CafSession;
import io.iec.edp.caf.core.session.CurrentSessionManager;
import io.iec.edp.caf.core.session.ICafSessionService;
import io.iec.edp.caf.core.session.Session;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * This is {@link RedisBizContextManager}.
 * redis中存储两个关系，过期策略：主动删除 + 定时扫描session移除bizContext
 * - bizContextId             string结构
 * - sessionId ~  contextId   set结构
 *
 * @author wangyandong
 * @since 1.0.0
 */
@Slf4j
public class RedisBizContextManager implements BizContextManager {

    private final static String CTX_PREFIX = "caf:bzctx:";
    private final static String CTX_SESSION_PREFIX = CTX_PREFIX + "s:";
    private final static String CTX_SETTING_PREFIX = CTX_PREFIX + "setting:";

    private final Map<Class<? extends BizContext>, BizContextBuilder> buildersMap = new ConcurrentHashMap<>();
    private final Map<String, List<BizContextListener>> listenersMap = new ConcurrentHashMap<>();
    private final Map<String, BizContextDataSerializer> serializerMap = new ConcurrentHashMap<>();  //序列化实例集合
    private final Map<String, Class> clazzMap = new ConcurrentHashMap();

    private final RedisTemplate redisTemplate;
    private final CurrentSessionManager currentWebSessionManager;
    private final ICafSessionService sessionService;

    //批量清理sessionId ~ contextIds关系的脚本
    private final static DefaultRedisScript DestroyBizContextBySession = new DefaultRedisScript<>(
            "local members = redis.call('SMEMBERS',KEYS[1])\n" +
                    "for i=1, #members do\n" +
                    "  local contextId = string.sub(members[i],2,-2)\n" +
                    "  redis.call('DEL','caf:bzctx:'..contextId)\n" +
                    "  redis.call('DEL','caf:bzctx:setting:'..contextId)\n" +
                    "end;\n" +
                    "redis.call('DEL',KEYS[1])"
    );

    /*
        清理业务上下文lua脚本
    KEYS = { CTX_PREFIX+contextId, CTX_SETTING_PREFILX+contextId, contextId, CTX_SESSION_PREFIX+sessionId, sessionId }
     */
    private final static DefaultRedisScript DestroyBizContext = new DefaultRedisScript<>(
            "redis.call('DEL',KEYS[1])\n" +
                    "redis.call('DEL',KEYS[2])\n" +
                    "if(KEYS[5]~='Null')\n" +
                    "then\n\t" +
                    "   redis.call('SREM',KEYS[4],KEYS[3])\n" +
                    "   if(redis.call('SCARD',KEYS[4])==0)\n" +
                    "   then\n\t" +
                    "       redis.call('DEL',KEYS[4])\n" +
                    "   end\n" +
                    "end"
    );


    public RedisBizContextManager(CurrentSessionManager manager, ICafSessionService sessionService, List<BizContextBuilder> builders, RedisConnectionFactory connectionFactory, CafRedisConfiguration cafRedisConfiguration) {
        this.currentWebSessionManager = manager;
        this.sessionService = sessionService;
        if (builders != null && builders.size() > 0) {
            builders.forEach((b) -> buildersMap.put(b.getContextType(), b));
        }

        //构造redisconnectionfactory
        JedisConnectionFactory connection = null;
        if (cafRedisConfiguration != null) {
            RedisSetting setting = cafRedisConfiguration.get("caf-biz-context");
            if (setting != null && setting.getMode() == RedisMode.STANDALONE) {
                //目前仅支持单体部署
                JedisPoolConfig poolConfig = new JedisPoolConfig();
                poolConfig.setMaxTotal(256);
                poolConfig.setMaxIdle(256);
                poolConfig.setMaxWaitMillis(10000);
                poolConfig.setMinIdle(16);
                poolConfig.setTestOnBorrow(false);
                poolConfig.setTestOnReturn(false);
                poolConfig.setTestWhileIdle(true);
                JedisClientConfiguration clientConfig = JedisClientConfiguration.builder()
                        .usePooling().poolConfig(poolConfig).and().readTimeout(Duration.ofMillis(10000))
                        .build();
                RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
                redisStandaloneConfiguration.setHostName(setting.getHost());
                redisStandaloneConfiguration.setPort(setting.getPort());
                redisStandaloneConfiguration.setPassword(setting.getPassword());
                redisStandaloneConfiguration.setDatabase(setting.getDatabase());
                connection = new JedisConnectionFactory(redisStandaloneConfiguration, clientConfig);
                connection.afterPropertiesSet();
            }
        }
        //业务上下文的Redis存储，通过此RedisTemplate进行操作
        this.redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(connection != null ? connection : connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        // 支持 jdk 1.8 日期
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        om.registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule())
                .registerModule(new ParameterNamesModule());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer(om));
        redisTemplate.afterPropertiesSet();
    }

    /**
     * 根据类型创建根业务上下文
     *
     * @param contextType 上下文类型定义
     * @param <C>         泛型要求
     * @return
     * @throws BizContextBuilderNotFoundException
     * @throws BizContextBuilderNotFoundException
     */
    @Override
    @Deprecated
    @SneakyThrows(BizContextNotFoundException.class)
    public <C extends BizContext> C createRootContext(Class<C> contextType) throws BizContextBuilderNotFoundException {
        return buildNewContext(contextType, null, true);
    }

    /**
     * 根据类型创建业务上下文
     *
     * @param contextType 上下文类型定义
     * @param parentId    父级ID
     * @param <C>
     * @return
     * @throws BizContextNotFoundException
     * @throws BizContextBuilderNotFoundException
     */
    @Override
    @Deprecated
    public <C extends BizContext> C createContextUnder(Class<C> contextType, String parentId) throws BizContextNotFoundException, BizContextBuilderNotFoundException {
        if (parentId == null || parentId.isEmpty())
            throw new BizContextNotFoundException();
        BizContext parent = this.load(parentId, true);
        if (parent == null) {
            throw new BizContextNotFoundException();
        }
        return buildNewContext(contextType, parent, false);
    }

    /**
     * 根据类型创建业务上下文
     *
     * @param contextType 上下文类型定义
     * @param parent      父级上下文
     * @param <C>
     * @return
     * @throws BizContextNotFoundException
     * @throws BizContextBuilderNotFoundException
     */
    @Override
    @Deprecated
    public <C extends BizContext> C createContextUnder(Class<C> contextType, BizContext parent) throws BizContextNotFoundException, BizContextBuilderNotFoundException {
        return createContextUnder(contextType, parent.getId());
    }

    /**
     * 获取业务上下文
     *
     * @param contextType 上下文类型定义
     * @param contextId   上下文ID
     * @param <C>
     * @return
     * @throws BizContextNotFoundException
     */
    @Override
    public <C extends BizContext> C fetch(Class<C> contextType, String contextId) throws BizContextNotFoundException {
        return (C) this.fetchContext(contextType, contextId);
    }

    /**
     * 获取业务上下文
     *
     * @param contextId 上下文ID
     * @return
     * @throws BizContextNotFoundException
     */
    @Override
    public BizContext fetch(String contextId) throws BizContextNotFoundException {
        return (BizContext) this.fetchContext(null, contextId);
    }

    //还原业务上下文
    private <C extends BizContext> C fetchContext(Class<C> contextType, String contextId) throws BizContextNotFoundException {
        C context;
        //获取context的setting
        RedisBizContextSetting setting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + contextId), RedisBizContextSetting.class);
        if (setting != null) {
            contextType = contextType != null ? contextType : getContextType(setting.getClazz());
            //存在setting，获取序列化器
            context = this.load(contextId, true, contextType, setting.getSerializer());

            //Stop to save BizContext automatically
//            save(contextId, context, context.getParentId() == null, false, contextType, setting.getExpiration(), setting.getUnit(), setting.getSerializer());
        } else {
            context = this.load(contextId, true);
        }
        if (context == null) {
            throw new BizContextNotFoundException();
        }
        return context;
    }

    /**
     * 类型推断
     *
     * @param contextId 指定的上下文ID
     * @return
     * @throws BizContextNotFoundException
     */
    @Override
    public Class<? extends BizContext> deduce(String contextId) throws BizContextNotFoundException {
        BizContext context = this.fetch(contextId);
        return context.getClass();
    }

    /**
     * 更新上下文类型
     *
     * @param context 指定的上下文
     */
    @Override
    public void update(BizContext context) {
        if (context == null)
            return;
        //当context存在时，更新一下
        context.setItemsChanged(false);

        //获取setting
        RedisBizContextSetting setting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + context.getId()), RedisBizContextSetting.class);
        if (setting != null) {
            //存在setting，根据数据设置
            //递归，延长parent过期时间
            save(context.getId(), context, context.getParentId() == null, getContextType(setting.getClazz()), setting.getExpiration(), setting.getUnit(), setting.getSerializer());
        } else {
            var oldParent = context.getParent();
            context.setParent(null);
            redisTemplate.opsForValue().setIfPresent(CTX_PREFIX + context.getId(), context);
            context.setParent(oldParent);
        }
    }

    /**
     * 注册ContextType类型
     *
     * @param contextType 监听上下文类型
     * @param listener    上下文监听器
     * @param <C>
     * @return
     */
    @Override
    public <C extends BizContext> String register(Class<C> contextType, BizContextListener<C> listener) {
        String contextTypeName = contextType.getCanonicalName();
        List<BizContextListener> listeners = listenersMap.computeIfAbsent(contextTypeName, k -> new ArrayList<>());
        listeners.add(listener);

        return Base64.getEncoder().encodeToString((contextTypeName + ":" + (listeners.size() - 1)).getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public void unRegister(String registrationId) {
        String[] decodedRegistration = new String(Base64.getDecoder().decode(registrationId), StandardCharsets.UTF_8).split(":");
        if (decodedRegistration.length != 2) {
            return;
        }

        String contextTypeName = decodedRegistration[0];
        int index = Integer.parseInt(decodedRegistration[1]);
        List<BizContextListener> listeners = listenersMap.get(contextTypeName);

        if (listeners == null || listeners.isEmpty() || listeners.get(index) == null) return;

        listeners.remove(index);
    }

    @Override
    public <C extends BizContext> void unRegister(Class<C> contextType, BizContextListener<C> listener) {
        String contextTypeName = contextType.getCanonicalName();
        List<BizContextListener> listeners = listenersMap.get(contextTypeName);
        if (listeners == null || listeners.isEmpty()) return;
        listeners.remove(listener);
    }

    /**
     * 销毁上下文
     *
     * @param sessionId 指定上下文ID
     */
    @Override
    public void destroyBySession(String sessionId) {
        if (sessionId == null || sessionId.isEmpty())
            return;

        //lua执行清理redis相关业务上下文
        if (this.redisTemplate.hasKey(CTX_SESSION_PREFIX + sessionId)) {
            log.info("Start to destroy BizContext by session id : {}", sessionId);
            this.redisTemplate.execute(DestroyBizContextBySession, Collections.singletonList(CTX_SESSION_PREFIX + sessionId));
        }
    }

    /**
     * 销毁业务上下文
     *
     * @param contextId 指定上下文
     * @throws BizContextNotFoundException
     */
    @Override
    public void destroy(String contextId) throws BizContextNotFoundException {
        destroy(contextId, true);
    }

    /**
     * 销毁业务上下文
     *
     * @param context 指定上下文
     * @throws BizContextNotFoundException
     */
    @Override
    public void destroy(BizContext context) throws BizContextNotFoundException {
        destroy(context.getId());
    }

    /**
     * 销毁业务上下文
     *
     * @param contextId 指定上下文
     * @param cascade   是否级联删除
     * @throws BizContextNotFoundException
     */
    @Override
    public void destroy(String contextId, Boolean cascade) throws BizContextNotFoundException {
        if (contextId == null || contextId.isEmpty())
            return;

        List<Object> keys = new ArrayList<>();
        keys.add(0, CTX_PREFIX + contextId);
        keys.add(1, CTX_SETTING_PREFIX + contextId);
        keys.add(2, "\"" + contextId + "\"");       //TODO 存储时redisTemplate的valueSerializer为json，所以值要手动拼接双引号
        CafSession session = currentWebSessionManager.getCurrentSession();
        if (session != null) {
            keys.add(3, CTX_SESSION_PREFIX + session.getId());
            keys.add(4, session.getId());
        } else {
            keys.add(3, CTX_SESSION_PREFIX + "Null");
            keys.add(4, "Null");
            log.info("The current user session information cannot be found when destroying bizContext {} !", contextId);
        }

        this.redisTemplate.execute(DestroyBizContext, keys);
    }

    /**
     * 销毁业务上下文
     *
     * @param context 指定上下文
     * @param cascade 是否级联删除
     * @throws BizContextNotFoundException
     */
    @Override
    public void destroy(BizContext context, Boolean cascade) throws BizContextNotFoundException {
        destroy(context.getId(), cascade);
    }

    /**
     * 判断Session是否过期
     *
     * @param contextId
     * @return
     */
    @Override
    public boolean isExpired(String contextId) {
        BizContext context;
        //获取context的setting
        RedisBizContextSetting setting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + contextId), RedisBizContextSetting.class);
        if (setting != null) {
            //存在setting，获取序列化器
            context = this.load(contextId, false, getContextType(setting.getClazz()), setting.getSerializer());
        } else {
            context = this.load(contextId, false);
        }
        if (context != null && context.getSessionId() != null && !"".equals(context.getSessionId())) {
            return this.sessionService.isExpired(context.getSessionId());
        }
        return true;
    }

    @Override
    public void prolong(String contextId) {
        //获取context的setting
        RedisBizContextSetting setting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + contextId), RedisBizContextSetting.class);
        if (setting != null && setting.getExpiration() > 0L) {
            Class contextType = getContextType(setting.getClazz());
            //存在setting，获取序列化器
            BizContext context = this.load(contextId, true, contextType, setting.getSerializer());
            //存储
            save(contextId, context, context.getParentId() == null, contextType, setting.getExpiration(), setting.getUnit(), setting.getSerializer());
        }
    }

    /**
     * 新建业务上下文
     *
     * @param contextType
     * @param parent
     * @param skipParentCheck
     * @param <C>
     * @return
     * @throws BizContextBuilderNotFoundException
     * @throws BizContextNotFoundException
     */
    @SuppressWarnings("unchecked")
    private <C extends BizContext> C buildNewContext(Class<C> contextType, Session parent, boolean skipParentCheck) throws BizContextBuilderNotFoundException, BizContextNotFoundException {
        //不能忽略Parent检查时，如果parent==null 或 根据parentId取不到，抛出异常
        if (!skipParentCheck && (parent == null || this.load(parent.getId(), true) == null)) {
            throw new BizContextNotFoundException();
        }

        BizContextBuilder builder = buildersMap.get(contextType);
        if (builder == null) {
            throw new BizContextBuilderNotFoundException();
        }

        try {
            C context = (C) builder.build(parent);
            if (this.currentWebSessionManager.getCurrentSession() != null) {
                context.setSessionId(this.currentWebSessionManager.getCurrentSession().getId());
            }
            save(context.getId(), context);
            return context;
        } catch (ClassCastException ex) {
            log.error("Failed to build context of type: {}. Please check the context builder: {}.", contextType.getCanonicalName(), builder.getClass().getCanonicalName());
            return null;
        }
    }

    /**
     * 加载业务上下文
     *
     * @param id         标识
     * @param loadParent 是否加载Parent
     * @param <C>        类型
     * @return
     */
    private <C extends BizContext> C load(String id, boolean loadParent) {
        //获取当前bizContext
        C context = (C) this.redisTemplate.opsForValue().get(CTX_PREFIX + id);
        //递归处理下Parent
        if (context != null && loadParent) {
            this.recursiveLoading(context);
        }
        return context;
    }

    /**
     * 递归加载Parent
     *
     * @param context
     */
    private void recursiveLoading(BizContext context) {
        if (context != null && context.getParentId() != null && !context.getParentId().isEmpty()) {
            if (context.getParent() == null) {
                Session parent = (Session) this.redisTemplate.opsForValue().get(CTX_PREFIX + context.getParentId());
                context.setParent(parent);
            }
            this.recursiveLoading((BizContext) context.getParent());
        }
    }

    /**
     * 保存业务上下文
     *
     * @param id      标识
     * @param context 上下文实体
     */
    private void save(String id, BizContext context) {
        if (id == null || id.isEmpty() || context == null)
            return;
        //1将bizContext记入Redis , 为了不序列化parent，先置空再补充
        var oldParent = context.getParent();
        context.setParent(null);
        this.redisTemplate.opsForValue().set(CTX_PREFIX + id, context);
        context.setParent(oldParent);

        //2将sessionId ~ contextId的关系记入set结构
        this.redisTemplate.opsForSet().add(CTX_SESSION_PREFIX + context.getSessionId(), id);
    }

    //#############重载方法##支持过期时间与自定义序列化##############
    //存储过期时间+单位+序列化方法的数据结构采用？创建一个对象，然后存储各种

    //创建root：1、持久化上下文+过期时间等 2、
    public <C extends BizContext> C createRootContext(Class<C> contextType, long expiration, TimeUnit unit, String serializerType) throws BizContextBuilderNotFoundException {
        return buildContext(contextType, null, true, true, true, expiration, unit, serializerType);
    }

    public <C extends BizContext> C createContextUnder(Class<C> contextType, String parentId, long expiration, TimeUnit unit, String serializerType) throws BizContextNotFoundException, BizContextBuilderNotFoundException {
        if (parentId == null || parentId.isEmpty())
            throw new BizContextNotFoundException();
        BizContext parent = this.load(parentId, true, contextType, serializerType);
        if (parent == null) {
            throw new BizContextNotFoundException();
        }
        return buildContext(contextType, parent, false, false, true, expiration, unit, serializerType);
    }

    public <C extends BizContext> C createContextUnder(Class<C> contextType, BizContext parent, long expiration, TimeUnit unit, String serializerType) throws BizContextNotFoundException, BizContextBuilderNotFoundException {
        return createContextUnder(contextType, parent.getId(), expiration, unit, serializerType);
    }

    /**
     * 新建业务上下文
     */
    @SuppressWarnings("unchecked")
    private <C extends BizContext> C buildContext(Class<C> contextType, Session parent, boolean skipParentCheck, boolean isRoot, boolean isCreate, long expiration, TimeUnit unit, String serializerType) throws BizContextBuilderNotFoundException, BizContextNotFoundException {
        //不能忽略Parent检查时，如果parent==null 或 根据parentId取不到，抛出异常
        if (!skipParentCheck && (parent == null || this.load(parent.getId(), true, contextType, serializerType) == null)) {
            throw new BizContextNotFoundException();
        }

        BizContextBuilder builder = buildersMap.get(contextType);
        if (builder == null) {
            throw new BizContextBuilderNotFoundException();
        }

        try {
            C context = (C) builder.build(parent);
            if (this.currentWebSessionManager.getCurrentSession() != null) {
                context.setSessionId(this.currentWebSessionManager.getCurrentSession().getId());
            }
            save(context.getId(), context, isRoot, contextType, expiration, unit, serializerType);
            return context;
        } catch (ClassCastException ex) {
            log.error("Failed to build context of type: {}. Please check the context builder: {}.",
                    contextType.getCanonicalName(), builder.getClass().getCanonicalName());
            return null;
        }
    }

    /*
    保存业务上下文
    一、root
        1.存储context
        2.存储Setting
        3.isCreate=true则idNumber++
     二、under
        1.存储context
        2.存储Setting
        3.idRoot=false:循环持久化parentContext与parentSetting
        4.isCreate=true:则idNumber++
     三、fetch与update
        1.如一和二
     */
    private void save(String id, BizContext context, boolean isRoot, Class clazz, long expiration, TimeUnit unit, String serializerType) {
        if (id == null || id.isEmpty() || context == null)
            return;
        //获取序列化容器
        BizContextDataSerializer serializer = getSerializer(serializerType);

        //存储设置信息
        String className = clazz.getName();
        if (!this.clazzMap.containsKey(className)) {
            this.clazzMap.put(className, clazz);
        }
        RedisBizContextSetting setting = new RedisBizContextSetting(expiration, unit, serializerType, className);

        //1将bizContext记入Redis , 为了不序列化parent，先置空再补充
        var oldParent = context.getParent();
        context.setParent(null);
        if (expiration > 0L) {
            redisTemplate.opsForValue().set(CTX_PREFIX + id, serializer != null ? serializer.serialize(context) : context, expiration, unit);
            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + id, JSONSerializer.serialize(setting), TimeUnit.MINUTES.convert(expiration, unit), TimeUnit.MINUTES);
        } else {
            redisTemplate.opsForValue().set(CTX_PREFIX + id, serializer != null ? serializer.serialize(context) : context);
            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + id, JSONSerializer.serialize(setting));
        }
        context.setParent(oldParent);
        //非root，则循环持久化
        if (!isRoot)
            cycleSave(context, clazz, expiration, unit);

        //2将seesionId ~ contextId 关系记入redis
        String sessionId = context.getSessionId();
        if (expiration > 0L) {
            if (!isRoot) {
                //under则，对比root与under时间
                RedisBizContextSetting parentSetting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + context.getParentId()), RedisBizContextSetting.class);
                this.redisTemplate.opsForSet().add(CTX_SESSION_PREFIX + sessionId, id);
                this.redisTemplate.expire(CTX_SESSION_PREFIX + sessionId, getMaxExpiration(parentSetting, expiration, unit), TimeUnit.MINUTES);
            } else {
                this.redisTemplate.opsForSet().add(CTX_SESSION_PREFIX + sessionId, id);
                this.redisTemplate.expire(CTX_SESSION_PREFIX + sessionId, expiration, unit);
            }
        } else {
            this.redisTemplate.opsForSet().add(CTX_SESSION_PREFIX + sessionId, id);
        }
    }

    /*
    循环保存context
    一、context包含parentId
        获取parent的setting，通过序列化获取，然后通过context的过期时间来重新设置
     */
    private void cycleSave(BizContext context, Class clazz, long expiration, TimeUnit unit) {
        if (context != null && !("").equals(context.getParentId())) {
            Session parent;
            if (context.getParent() != null) {
                RedisBizContextSetting parentSetting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + context.getParentId()), RedisBizContextSetting.class);
                //存在parent在递归
                BizContextDataSerializer parentSerializer = getSerializer(parentSetting.getSerializer());
                //延长parent时间原则：max(parent自身过期时间,context自身过期时间)
                long expir = getMaxExpiration(parentSetting, expiration, unit);
                //如果无parent，试着加载
                if (parentSerializer != null) {
                    parent = parentSerializer.deserialize(redisTemplate.opsForValue().get(CTX_PREFIX + context.getParentId()), clazz);
                    if (parent != null)
                        if (expiration > 0L) {
                            redisTemplate.opsForValue().set(CTX_PREFIX + context.getParentId(), parentSerializer.serialize(parent), expir, TimeUnit.MINUTES);
                            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + context.getParentId(), JSONSerializer.serialize(parentSetting), expir, TimeUnit.MINUTES);
                        } else {
                            redisTemplate.opsForValue().set(CTX_PREFIX + context.getParentId(), parentSerializer.serialize(parent));
                            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + context.getParentId(), JSONSerializer.serialize(parentSetting));
                        }
                } else {
                    parent = (Session) redisTemplate.opsForValue().get(CTX_PREFIX + context.getParentId());
                    if (parent != null)
                        if (expiration > 0L) {
                            redisTemplate.opsForValue().set(CTX_PREFIX + context.getParentId(), parent, expir, TimeUnit.MINUTES);
                            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + context.getParentId(), JSONSerializer.serialize(parentSetting), expir, TimeUnit.MINUTES);
                        } else {
                            redisTemplate.opsForValue().set(CTX_PREFIX + context.getParentId(), parent);
                            redisTemplate.opsForValue().set(CTX_SETTING_PREFIX + context.getParentId(), JSONSerializer.serialize(parentSetting));
                        }
                }
                //设置parent
                context.setParent(parent);
                //递归
                cycleSave((BizContext) parent, clazz, expiration, unit);
            }
        }
    }

    /*
    加载context（此过程不持久化）
     */
    private <C extends BizContext> C load(String id, boolean loadParent, Class<C> clazz, String serializerType) {
        //TODO 从redis中获取序列化容器 or 使用当前序列化参数  (目前使用参数)

        //获取序列化容器
        BizContextDataSerializer serializer = getSerializer(serializerType);
        //根据id获取BizContext
        C context;
        if (serializer != null)
            context = serializer.deserialize(redisTemplate.opsForValue().get(CTX_PREFIX + id), clazz);
        else
            context = (C) redisTemplate.opsForValue().get(CTX_PREFIX + id);

        //加载parent
        if (context != null && loadParent) {
//            recursiveLoading(clazz, context, serializer);TODO 加载父类时，序列化器使用参数or从redis中获取？从Redis获取更合理、按照目前使用没问题
            recursiveLoading(clazz, context);
        }
        return context;
    }

    //循环加载业务上下文，不持久化
    private void recursiveLoading(Class clazz, BizContext context) {
        //存在parent在递归
        if (context != null && context.getParentId() != null && !context.getParentId().isEmpty()) {
            //获取parent序列化器
            RedisBizContextSetting parentSetting = JSONSerializer.deserialize((String) redisTemplate.opsForValue().get(CTX_SETTING_PREFIX + context.getParentId()), RedisBizContextSetting.class);
            BizContextDataSerializer parentSerializer = getSerializer(parentSetting.getSerializer());
            Session parent = null;
            if (context.getParent() == null) {
                //如果无parent，试着加载
                if (parentSerializer != null)
                    parent = parentSerializer.deserialize(redisTemplate.opsForValue().get(CTX_PREFIX + context.getParentId()), clazz);
                else
                    parent = (Session) redisTemplate.opsForValue().get(CTX_PREFIX + context.getParentId());

                //设置parent
                context.setParent(parent);
            }
            //递归
            recursiveLoading(clazz, (BizContext) parent);
        }
    }

    /**
     * 加载序列化方法
     *
     * @param serializerType 序列化类全限定类名
     * @return 序列化类
     */
    private BizContextDataSerializer getSerializer(String serializerType) {
        if (!StringUtils.isEmpty(serializerType)) {
            if (serializerMap.containsKey(serializerType)) {
                return serializerMap.get(serializerType);
            } else {
                BizContextDataSerializer serializer = null;
                try {
                    serializer = (BizContextDataSerializer) InvokeService.getClass(serializerType).newInstance();
                } catch (Exception e) {
                    log.info("BizContext: Custom serializer instance failed, it will not serializer!");
                }
                serializerMap.put(serializerType, serializer);
                return serializer;
            }
        }
        return null;
    }

    /**
     * 获取BizContext类型
     *
     * @param className BizContext全类名
     */
    private Class getContextType(String className) {
        if (!this.clazzMap.containsKey(className)) {
            Class clazz = null;
            try {
                clazz = InvokeService.getClass(className);
            } catch (Exception e) {
                log.info("BizContext: Reflect Class error!");
            }
            this.clazzMap.put(className, clazz);
            return clazz;
        }

        return this.clazzMap.get(className);
    }

    //获取最大值：比较(parent的时间)和(新的时间)
    private long getMaxExpiration(RedisBizContextSetting parent, long expiration, TimeUnit unit) {
        return Math.max(TimeUnit.MINUTES.convert(parent.getExpiration(), parent.getUnit()), TimeUnit.MINUTES.convert(expiration, unit));
    }

}
