/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.logging.logback.appender;

import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.encoder.Encoder;
import ch.qos.logback.core.net.SyslogConstants;
import ch.qos.logback.core.status.ErrorStatus;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Base class for SyslogAppender.
 * @author wangyandong
 * @date 2021/06/19 16:05
 *
 */
public abstract class CafSyslogAppenderBase <E> extends AppenderBase<E> {

    final static int MAX_MESSAGE_SIZE_LIMIT = 65000;

    /**
     * It is the encoder which is ultimately responsible for writing the event to
     * an {@link OutputStream}.
     */
    protected Encoder<E> encoder;
    /**
     * 日志源分类，暂时用不到
     */
    private String facilityStr;
    private String host;
    private int port = SyslogConstants.SYSLOG_PORT;
    private CafSyslogOutputStream sos;
    private int maxMessageSize;

    @Override
    public void start() {
        int errorCount = 0;
        if (this.encoder == null) {
            addStatus(new ErrorStatus("No encoder set for the appender named \"" + name + "\".", this));
            errorCount++;
        }

        try {
            sos = createOutputStream();

            final int systemDatagramSize = sos.getSendBufferSize();
            if (maxMessageSize == 0) {
                maxMessageSize = Math.min(systemDatagramSize, MAX_MESSAGE_SIZE_LIMIT);
                addInfo("Defaulting maxMessageSize to [" + maxMessageSize + "]");
            } else if (maxMessageSize > systemDatagramSize) {
                addWarn("maxMessageSize of [" + maxMessageSize + "] is larger than the system defined datagram size of [" + systemDatagramSize + "].");
                addWarn("This may result in dropped logs.");
            }
        } catch (UnknownHostException e) {
            addError("Could not create SyslogWriter", e);
            errorCount++;
        } catch (SocketException e) {
            addWarn("Failed to bind to a random datagram socket. Will try to reconnect later.", e);
        }

        if (errorCount == 0) {
            super.start();
        }
    }

    abstract public CafSyslogOutputStream createOutputStream() throws UnknownHostException, SocketException;

    abstract public int getSeverityForEvent(Object eventObject);

    @Override
    protected void append(E eventObject) {
        if (!isStarted()) {
            return;
        }

        try {
            byte[] msg = this.encoder.encode(eventObject);
            if (msg == null) {
                return;
            }
            if(msg.length < maxMessageSize){
                sos.write(msg);
            }else{
                sos.write(msg,0,maxMessageSize);
            }
            sos.flush();
            postProcess(eventObject, sos);
        } catch (IOException ioe) {
            addError("Failed to send diagram to " + host, ioe);
        }
    }

    protected void postProcess(Object event, OutputStream sw) {
        //to do 可以参考SyslogAppender
    }

    /**
     * Returns the integer value corresponding to the named syslog facility.
     *
     * @throws IllegalArgumentException
     *           if the facility string is not recognized
     */
    static public int facilityStringToint(String facilityStr) {
        if ("KERN".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_KERN;
        } else if ("USER".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_USER;
        } else if ("MAIL".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_MAIL;
        } else if ("DAEMON".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_DAEMON;
        } else if ("AUTH".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_AUTH;
        } else if ("SYSLOG".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_SYSLOG;
        } else if ("LPR".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LPR;
        } else if ("NEWS".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_NEWS;
        } else if ("UUCP".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_UUCP;
        } else if ("CRON".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_CRON;
        } else if ("AUTHPRIV".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_AUTHPRIV;
        } else if ("FTP".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_FTP;
        } else if ("NTP".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_NTP;
        } else if ("AUDIT".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_AUDIT;
        } else if ("ALERT".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_ALERT;
        } else if ("CLOCK".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_CLOCK;
        } else if ("LOCAL0".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL0;
        } else if ("LOCAL1".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL1;
        } else if ("LOCAL2".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL2;
        } else if ("LOCAL3".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL3;
        } else if ("LOCAL4".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL4;
        } else if ("LOCAL5".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL5;
        } else if ("LOCAL6".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL6;
        } else if ("LOCAL7".equalsIgnoreCase(facilityStr)) {
            return SyslogConstants.LOG_LOCAL7;
        } else {
            throw new IllegalArgumentException(facilityStr + " is not a valid syslog facility string");
        }
    }

    /**
     * Returns the value of the <b>Host</b> option.
     */
    public String getHost() {
        return host;
    }

    /**
     * The <b>Host</b> option is the name of the the syslog host where log
     * output should go.
     *
     * <b>WARNING</b> If the SyslogHost is not set, then this appender will fail.
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Returns the string value of the <b>Facility</b> option.
     *
     * See {@link #setFacility} for the set of allowed values.
     */
    public String getFacility() {
        return facilityStr;
    }

    /**
     * The <b>Facility</b> option must be set one of the strings KERN, USER, MAIL,
     * DAEMON, AUTH, SYSLOG, LPR, NEWS, UUCP, CRON, AUTHPRIV, FTP, NTP, AUDIT,
     * ALERT, CLOCK, LOCAL0, LOCAL1, LOCAL2, LOCAL3, LOCAL4, LOCAL5, LOCAL6,
     * LOCAL7. Case is not important.
     *
     * <p>
     * See {@link SyslogConstants} and RFC 3164 for more information about the
     * <b>Facility</b> option.
     */
    public void setFacility(String facilityStr) {
        if (facilityStr != null) {
            facilityStr = facilityStr.trim();
        }
        this.facilityStr = facilityStr;
    }

    /**
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * The port number on the syslog server to connect to. Normally, you would not
     * want to change the default value, that is 514.
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     *
     * @return
     */
    public int getMaxMessageSize() {
        return maxMessageSize;
    }

    /**
     * Maximum size for the syslog message (in characters); messages
     * longer than this are truncated. The default value is 65400 (which
     * is near the maximum for syslog-over-UDP). Note that the value is
     * characters; the number of bytes may vary if non-ASCII characters
     * are present.
     */
    public void setMaxMessageSize(int maxMessageSize) {
        this.maxMessageSize = maxMessageSize;
    }

    public Encoder<E> getEncoder() {
        return encoder;
    }

    public void setEncoder(Encoder<E> encoder) {
        this.encoder = encoder;
    }

    @Override
    public void stop() {

        if (sos != null) {
            sos.close();
        }
        super.stop();
    }
}
