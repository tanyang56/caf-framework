/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.logging;//package io.iec.edp.caf.io.iec.edp.caf.logging;
//
//import org.slf4j.MDC;
//import org.springframework.util.StringUtils;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//import java.util.Optional;
//
///**
// * @author wangyandong
// * @date 2021/05/24 13:10
// *
// */
//public class LogTraceFilter extends OncePerRequestFilter {
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        try {
//            String traceId = request.getHeader(CommonConstant.TRACE_ID_HEADER);
//            if (StringUtils.isEmpty(traceId)) {
//                //生成一个管理起来
//                traceId = TraceIdGenerator.generate();
//                MDC.put(CommonConstant.LOG_TRACE_ID, traceId);
//            }
//            MDC.put("msu",request.getMethod());
//        } finally {
//            filterChain.doFilter(request, response);
//        }
//    }
//
//    @Override
//    public void destroy() {
//        MDC.clear();
//    }
//}
