/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.logging;

/**
 * 常量类
 * @author wangyandong
 * @date 2021/05/22 15:46
 *
 */
public class CommonConstant {
    /**
     * Http Header中的标识Key
     */
    public static final String TRACE_ID_HEADER="traceId";

    /**
     * 日志变量Key标识
     */
    public static final String LOG_TRACE_ID = "tid";


    /**
     * 日志变量Key标识
     */
    public static final String LOG_MSU_ID = "msu";
}
