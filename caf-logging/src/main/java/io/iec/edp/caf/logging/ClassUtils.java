/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.logging;

import io.iec.edp.caf.commons.utils.InvokeService;

/**
 * @author wangyandong
 * @date 2021/05/24 10:39
 *
 */
public class ClassUtils {
    /**
     * Determine whether the Class identified by the supplied name is present and can be loaded.
     * 判断由提供的类名(类的全限定名)标识的类是否存在并可以加载
     * 如果类或其中一个依赖关系不存在或无法加载，则返回false
     * @param className 要检查的类的名称
     * @param classLoader 加载该类使用的类加载器
     * 可以是 null, 表明使用默认的类加载器
     * @return 指定的类是否存在
     */
    public static boolean isPresent(String className, ClassLoader classLoader) {
        try {
            InvokeService.getClass(className);
            return true;
        }
        catch (Throwable ex) {
            // Class or one of its dependencies is not present...
            return false;
        }
    }
}
