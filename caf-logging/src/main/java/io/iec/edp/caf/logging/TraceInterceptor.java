/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.logging;//package io.iec.edp.caf.io.iec.edp.caf.logging;
//
//import io.iec.edp.caf.commons.utils.StringUtils;
//import org.slf4j.MDC;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * 拦截器服务
// * @author wangyandong
// * @date 2021/05/24 17:07
// *
// */
//public class TraceInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//        String traceId = request.getHeader(CommonConstant.TRACE_ID_HEADER);
//        if (!StringUtils.isEmpty(traceId)) {
//            MDC.put(CommonConstant.LOG_TRACE_ID, traceId);
//        }
//
//        return true;
//    }
//}