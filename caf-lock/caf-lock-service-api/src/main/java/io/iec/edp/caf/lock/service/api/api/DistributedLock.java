/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.api;

import java.io.Closeable;

/**
 * 分布式锁对象
 * @author wangyandong
 */
public interface DistributedLock extends Closeable {

    /**
     * The name of the resource the lock is for.
     */
    public String getResource();

    /**
     * Whether the lock has been acquired.
     */
    public boolean isAcquired();

    void multiUnlock();

}
