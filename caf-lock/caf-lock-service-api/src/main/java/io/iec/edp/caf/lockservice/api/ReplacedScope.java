/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

/**
 * 锁的可替换范围枚举
 * @author wangyandong
 * @date 2019/7/27 14:01
 *
 */
public enum ReplacedScope {
    /**
     * 独占锁，不允许被替换。
     */
    Exclusive(0),

    /**
     * 进程锁，进程内可以替换 。
     */
    Session(1),

    /**
     * 个人锁，允许被同一个所有者替换。
     */
    Self(2),

    /**
     * 共享锁,前几个都只能在一个对象上加一个锁。
     */
    Share(3);

    /*value值*/
    private int value = 0;

    /**
     * 根据int值返回对应的枚举值
     * @param value
     * @return
     * @author wangyandong
     * @date 14:07 2019/7/27
     */
    private ReplacedScope(int value) {
        this.value = value;
    }
    /**
     *返回枚举对应的int值
     * @return  int值
     * @author 王延东
     * @date 14:09 2019/7/27
     */
    public int value() {
        return this.value;
    }
}
