/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.exception;

/**
 * 错误编号枚举
 * @author wangyandong
 */
public class DLErrorDefinition {

    /**
     * isacquired处报错
     */
    public static final String AddLock_IsAcquired_Error = "Gsp_Svc_DistributedLock_1001";

    /**
     * issession处报错
     */
    public static final String AddLock_IsSession_Error = "Gsp_Svc_DistributedLock_1002";

    /**
     * getlock处报错
     */
    public static final String AddLock_GetLock_Error = "Gsp_Svc_DistributedLock_1003";

    /**
     * 批量删锁处报错
     */
    public static final String RemLock_BathLock_Error = "Gsp_Svc_DistributedLock_1004";

    /**
     * lockDAC中删锁处报错
     */
    public static final String LockDAC_RemLock_Error = "Gsp_Svc_DistributedLock_1005";

    /**
     * 分布式锁StartAutoTime处报错
     */
    public static final String DistributedLock_StartAutoTime_Error = "Gsp_Svc_DistributedLock_1006";

    /**
     * 根据SMCD删锁时报错
     */
    public static final String RemLock_SMCD_Error = "Gsp_Svc_DistributedLock_1007";

    /**
     * 根据lockid删锁时报错
     */
    public static final String RemLock_Lockid_Error = "Gsp_Svc_DistributedLock_1008";

    /**
     * 根据seeionid删锁时报错
     */
    public static final String RemLock_Sessionid_Error = "Gsp_Svc_DistributedLock_1009";

    /**
     * 根据seeionid删锁时报错
     */
    public static final String AddBatchLock_ShareNotSupport_Error = "Gsp_Svc_DistributedLock_1010";
}
