/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.api;

import io.iec.edp.caf.lockservice.api.LockStatus;
import io.iec.edp.caf.lockservice.api.*;
import org.springframework.lang.Nullable;

import java.time.Duration;
import java.util.List;


public interface LockService {
    /**
     *单条数据加锁
     * @param mkId 模块ID
     * @param dataCat 数据种类
     * @param dataId 数据ID
     * @param option 数据锁的设置选项
     * @param funcId 加锁的功能Id
     * @param comment 备注
     * @param distributedLockOptions 分布式锁选项
     * @return boolean 是否加锁成功的标识
     */
    LockResult addLock(String mkId, String dataCat, String dataId, DataLockOptions option, String funcId, String comment, @Nullable DistributedLockOptions distributedLockOptions);

    /**
     *单条数据加锁
     * @param mkId 模块ID
     * @param dataCat 数据种类
     * @param dataId 数据ID
     * @param option 数据锁的设置选项
     * @param funcId 加锁的功能Id
     * @param comment 备注
     * @return boolean 是否加锁成功的标识
     */
    LockResult addLock(String mkId, String dataCat, String dataId, DataLockOptions option, String funcId, String comment);

    /**
     * 批量锁加锁方法
     * @param mkId 模块ID
     * @param dataCat 数据种类
     * @param dataIds 数据ID列表
     * @param groupId 批量锁标识，可空
     * @param option 数据锁的设置选项
     * @param funcId 功能ID
     * @param comment 备注
     * @param distributedLockOptions 分布式锁选项
     * @return
     */
    @Deprecated
    BatchLockResult addBatchLock(String mkId, String dataCat, List<String> dataIds, @Nullable String groupId, DataLockOptions option, String funcId, String comment, @Nullable DistributedLockOptions distributedLockOptions);

    /**
     * 批量锁加锁方法
     * @param mkId 模块ID
     * @param dataCat 数据种类
     * @param dataIds 数据ID列表
     * @param groupId 批量锁标识，可空
     * @param option 数据锁的设置选项
     * @param funcId 功能ID
     * @param comment 备注
     * @return
     */
    @Deprecated
    BatchLockResult addBatchLock(String mkId, String dataCat, List<String> dataIds, @Nullable String groupId, DataLockOptions option, String funcId, String comment);

    /**
     * 批量锁加锁方法
     * @param mkId 模块ID
     * @param dataCat 数据种类
     * @param dataIds 数据ID列表
     * @param groupId 批量锁标识，可空
     * @param persistenceTime 持续时间
     * @param funcId 功能ID
     * @param comment 备注
     * @return
     */
    BatchLockResult addBatchLock(String mkId, String dataCat, List<String> dataIds, @Nullable String groupId, Duration persistenceTime, String funcId, String comment);

    /// <summary>
    ///
    /// </summary>
    /// <param name="lockID">锁编号</param>
    /// <param name="removeShardLock">删除排他锁、共享锁标识</param>
    /// <param name="sessionId">sessionId</param>
    /// <returns>
    /// 是否解锁的标志，true：表示成功  false：表示失败
    /// <see cref="bool"/>
    /// </returns>

    /**
     * 根据锁编号解锁
     * @param lockID 锁编号
     * @param sharedLock 标记当前锁是排他锁还是共享锁
     * @param sessionId 加锁时的session标识
     * @return 是否成功的标识
     */
    @Deprecated
    boolean removeLock(String lockID, boolean sharedLock, String sessionId);

    /**
     * 根据锁编号解锁
     * @param lockID 锁编号
     * @return 是否成功的标识
     */
    boolean removeLock(String lockID);

    /**
     * 解除所有进程号为sessionId的锁
     * @param sessionId 锁对应的进程号，区别于当前进程号
     * @param tenantId 租户编号
     * @return 是否清除成功，true：表示成功  false：表示失败
     */
    boolean removeSessionLock(String sessionId, int tenantId);

    /**
     * 批量解锁
     * @param groupId 锁批量编号
     * @return 是否删批量锁成功
     */
    boolean removeBatchLock(String groupId);

    /**
     * 删除当前所有已失效的锁
     * @param tenantId 租户标识
     * @return 是否清除成功，true：表示成功  false：表示失败
     */
    boolean removeInvalidLocks(int tenantId);


    boolean removeBatchLockByContext(String contextId);

    /**
     * 根据gropuId获取dataIds
     * @param groupId
     * @return
     */
    List<String>  getDataIds(String groupId);

    /**
     * 检查所是否有效
     * @param lockEntity
     * @return
     */
    LockStatus getLockStatus(LockEntity lockEntity);


}
