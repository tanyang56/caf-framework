/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.utils;

import java.util.Collection;
import java.util.Map;

/**
 * @author wangyandong
 */
public class DataValidator {

    /**
     * 判断字符串是否为空串
     * @param value
     * @param variableName
     */
    public static void checkForEmptyString(String value, String variableName) {
        if(value == null || "".equals(value))
            throw new RuntimeException(String.format("变量名称为 %s 的字符串为空！", variableName));
    }

    /**
     * 判断对象是否为空值
     * @param value
     * @param variableName
     */
    public static void checkForNullReference(Object value, String variableName) {
        if(value == null)
            throw new RuntimeException(String.format("变量名称为 %s 的值为空！", variableName));
    }
    /**
     * 转换为字符串
     * @param object
     * @return
     */
    public static String toString(Object object) {
        return object == null ? "" : object.toString();
    }

    /**
     * 判断集合是否为空
     * @param collection
     * @return
     */
    public boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * 判断字典是否为空
     * @param map
     * @return
     */
    public boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    /**
     * 判断字符串是否问空
     * @param string
     * @return
     */
    public boolean isEmpty(String string) {
        return toString(string).isEmpty();
    }

    /**
     * 判断泛型数组是否为空
     * @param array
     * @param <T>
     * @return
     */
    public <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }
}
