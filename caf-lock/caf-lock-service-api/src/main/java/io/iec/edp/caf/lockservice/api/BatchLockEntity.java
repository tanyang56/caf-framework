/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;

/**
 * 数据锁实体
 * @author wangyandong
 * @date 2020/6/23 10:50
 *
 */
@Entity
@Table(name = "GspBatchLock")
@Data
public class BatchLockEntity {
public BatchLockEntity(){}
    /**
     * 锁Id
     */
    @Id
    private String id;

    /**
     * 锁的模块编号
     */
    private String mkId;

    /**
     *锁的分类编号
     */
    private String categoryId;

    /**
     *数据编号
     */
    private String dataId;

    /**
     *数据编号
     */
    private String groupId;

    /**
     *注释
     */
    @Column(name = "comments")
    private String comment;

    /**
     *锁的功能编号
     */
    private String funcId;

    /**
     *加锁时的会话编号
     */
    private String sessionId;

    /**
     *加锁时的上下文编号
     */
    private String contextId;

    /**
     *用户Code
     */
    private String userId;

    /**
     *加锁时间
     */
    private OffsetDateTime lockTime;

    /**
     * 失效时间
     */
    private OffsetDateTime expiredTime;

    /**
     * su名称
     */
    private String suName;
}
