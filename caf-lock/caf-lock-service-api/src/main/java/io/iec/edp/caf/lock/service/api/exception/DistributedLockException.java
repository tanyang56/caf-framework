/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @author wangyandong
 */
public class DistributedLockException extends CAFRuntimeException {

    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param exceptionCode   异常编号
     * @param resourceFile    资源文件
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     */
    public DistributedLockException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException) {
        super(exceptionCode, resourceFile, exceptionCode, messageParams, innerException);
    }

    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param exceptionCode   异常编号
     * @param resourceFile    资源文件
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     * @param level           异常级别
     */
    public DistributedLockException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException,level);
    }

    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param resourceFile    资源文件
     * @param exceptionCode   异常编号
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     * @param level           异常级别
     * @param bizException    是否业务异常
     */
    public DistributedLockException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level, bizException);
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     */
    public DistributedLockException(String serviceUnitCode, String exceptionCode, String message, Exception innerException) {
        super(serviceUnitCode, exceptionCode, message, innerException);
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     * @param level           异常级别
     */
    public DistributedLockException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, exceptionCode, message, innerException, level);
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     * @param level           异常级别
     * @param bizException    是否业务异常
     */
    public DistributedLockException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, exceptionCode, message, innerException, level,bizException);
    }
}
