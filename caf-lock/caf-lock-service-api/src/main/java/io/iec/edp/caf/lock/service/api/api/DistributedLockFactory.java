/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.api;

import java.time.Duration;
import java.util.List;

/**
 * 分布式锁创建服务
 * @author wangyandong
 */
public interface DistributedLockFactory {

    /**
     * Gets a RedLock using the factory's set of redis endpoints. You should check the IsAcquired property before performing actions.
     * Blocks and retries up to the specified time limits.
     * @param resource The resource string to lock on. Only one RedLock should be acquired for any given resource at once.
     * @param expiryTime How long the lock should be held for.
     * RedLocks will automatically extend if the process that created the RedLock is still alive and the RedLock hasn't been disposed.
     * @return A RedLock object.
     */
    public DistributedLock createLock(String resource, Duration expiryTime);

    /**
     *尝试获取锁，当超过waitTime时仍没有获取到锁时则直接返回无需等待
     */
    public DistributedLock tryCreateLock(Duration waitTime,String resource, Duration expiryTime) throws InterruptedException;

    public DistributedLock createMultiLock(String resoucePrefix, List<String> dataIds, Duration expiryTime);

    public DistributedLock tryCreateLock(String resource) ;


    @Deprecated
    public DistributedLock CreateLock(String resource, Duration expiryTime);
    /**
     * Gets a RedLock using the factory's set of redis endpoints. You should check the IsAcquired property before performing actions.
     * Blocks and retries up to the specified time limits.
     * @param resource The resource string to lock on. Only one RedLock should be acquired for any given resource at once.
     * @param expiryTime How long the lock should be held for.
     * RedLocks will automatically extend if the process that created the RedLock is still alive and the RedLock hasn't been disposed.
     * @param waitTime How long to block for until a lock can be acquired.
     * @param retryTime How long to wait between retries when trying to acquire a lock.
     * @return A RedLock object.
     */
    @Deprecated
    public DistributedLock createLock(String resource, Duration expiryTime, Duration waitTime, Duration retryTime);
}
