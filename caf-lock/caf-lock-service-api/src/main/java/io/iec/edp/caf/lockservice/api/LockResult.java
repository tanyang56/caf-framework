/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

import java.util.List;

/**
 * 加锁结果对象
 * @author wangyandong
 * @date 2019/7/27 15:34
 *
 */
public class LockResult {

    /**
     * 是否加锁成功
     */
    private boolean success;

    /**
     * 锁标识
     */
    private String lockId;

    /**
     * 已加锁列表
     */
    private List<LockEntity> lockedEntities;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getLockId() {
        return lockId;
    }

    public void setLockId(String lockId) {
        this.lockId = lockId;
    }

    public List<LockEntity> getLockedEntities() {
        return lockedEntities;
    }

    public void setLockedEntities(List<LockEntity> lockedEntities) {
        this.lockedEntities = lockedEntities;
    }
}
