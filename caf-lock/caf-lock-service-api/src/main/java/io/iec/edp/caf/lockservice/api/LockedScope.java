/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

/**
 * 锁的作用域范围。用于指定锁在哪一个作用域内有效。
 * 作用域一般分为三个范围，会话、应用实例、功能上下文(暂不提供)。
 * 如果锁的作用域为“会话”，则当用户会话退出时，该锁将失效。锁的作用域与锁的持续时间一起决定了锁的有效范围。
 * @author wangyandong
 * @date 2019/7/27 14:11
 *
 */
public enum LockedScope {
    /**会话级的作用域。*/
    Session(0),

    /** 应用实例级的作用域。*/
    AppInstance(1),

    /** 功能上下文级的作用域。*/
    BizContext(2);

    /*value值*/
    private int value = 0;

    /**
     * 根据int值返回对应的枚举值
     * @param value
     * @return
     * @author wangyandong
     * @date 14:07 2019/7/27
     */
    private LockedScope(int value) {
        this.value = value;
    }
    /**
     *返回枚举对应的int值
     * @return  int值
     * @author wangyandong
     * @date 14:09 2019/7/27
     */
    public int value() {
        return this.value;
    }
}
