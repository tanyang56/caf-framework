/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

import io.iec.edp.caf.lock.service.api.api.DistributedLock;
import io.iec.edp.caf.lock.service.api.api.DistributedLockFactory;

import java.time.Duration;

/**
 * 分布式锁创建服务
 * @author wangyandong
 * @date 2019/7/29 14:29
 *
 */
@Deprecated
//todo 被com.inspur.edp.cdp.coderule.runtime.server.lock.DistributedLockService依赖
public interface IDistributedLockFactory {
    /**
     * Gets a RedLock using the factory's set of redis endpoints. You should check the IsAcquired property before performing actions.
     * Blocks and retries up to the specified time limits.
     * @param resource The resource string to lock on. Only one RedLock should be acquired for any given resource at once.
     * @param expiryTime How long the lock should be held for.
     * RedLocks will automatically extend if the process that created the RedLock is still alive and the RedLock hasn't been disposed.
     * @return A RedLock object.
     */
    public IDistributedLock createLock(String resource, Duration expiryTime);

    @Deprecated
    public IDistributedLock CreateLock(String resource, Duration expiryTime);
    /**
     * Gets a RedLock using the factory's set of redis endpoints. You should check the IsAcquired property before performing actions.
     * Blocks and retries up to the specified time limits.
     * @param resource The resource string to lock on. Only one RedLock should be acquired for any given resource at once.
     * @param expiryTime How long the lock should be held for.
     * RedLocks will automatically extend if the process that created the RedLock is still alive and the RedLock hasn't been disposed.
     * @param waitTime How long to block for until a lock can be acquired.
     * @param retryTime How long to wait between retries when trying to acquire a lock.
     * @return A RedLock object.
     */
    @Deprecated
    public IDistributedLock createLock(String resource, Duration expiryTime, Duration waitTime, Duration retryTime);
    //todo:看是否添加trylock


}
