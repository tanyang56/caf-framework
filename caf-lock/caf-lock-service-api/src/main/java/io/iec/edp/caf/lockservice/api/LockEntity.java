/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lockservice.api;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * 数据锁实体
 * @author wangyandong
 * @date 2019/7/29 10:50
 *
 */
@Entity
@Table(name = "GspLock")
@Data
public class LockEntity {
    public LockEntity(){}
    /**
     * 锁Id
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    /**
     * 锁的模块编号
     */
    private String mkId;

    /**
     *锁的分类编号
     */
    private String categoryId;

    /**
     *数据编号
     */
    private String dataId;

    /**
     * 获取或设置锁可被替换的范围，见<see cref="ReplacedScope"/>。
     * 通过设置锁可被替换范围，可指定当前锁在有效期内时，如果有其他请求（用户自己或其他用户）也希望对同样的数据进行锁定时，当前锁是否可被替换。
     * 如果没有设定，默认值为<c>ReplaceScope.Self</c>。
     */
    private ReplacedScope replacedScope;

    /**
     * 获取或设置锁的作用域范围。
     * 通过锁作用域范围的设置，可设置锁在那个作用域范围内有效。参见<see cref="LockedScope"/>。
     * 如果没有设定，默认值为<c>LockedScope.Session</c>。
     */
    public LockedScope lockedScope;

    /**
     *持续时间
     */
    private Duration keepTime;

    /**
     *注释
     */
    @Column(name = "comments")
    private String comment;

    /**
     *锁的功能编号
     */
    private String funcId;

    /**
     *加锁时的会话编号
     */
    private String sessionId;

    /**
     *加锁时的上下文编号
     */
    private String contextId;

    /**
     *用户Code
     */
    private String userId;

    /**
     *加锁时间
     */
    private OffsetDateTime lockTime;

    /**
     * su名称
     */
    private String suName;
}
