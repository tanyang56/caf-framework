/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.api.repositories;


import io.iec.edp.caf.lockservice.api.LockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 业务锁仓储类
 *
 * @author wangyandong
 */
public interface LockRepo extends JpaRepository<LockEntity, String> {

    List<LockEntity> findByMkIdAndCategoryIdAndDataId(String mkId, String categoryId, String dataId);

    void deleteByMkIdAndCategoryIdAndDataId(String mkId, String categoryId, String dataId);

    void deleteBySessionId(String sessionId);

    List<LockEntity> findByMkIdAndCategoryId(String mkId, String categoryId);

    List<LockEntity>   findByMkIdAndCategoryIdAndSuName(String mkId, String categoryId,String su);

    List<LockEntity> findByMkIdNotInAndCategoryIdNotIn(List<String> mkId,List<String>categoryId);

    @Override
    @Transactional
    @Modifying
    @Query(value = "delete from gsplock where id = :id ",nativeQuery = true)
    void deleteById(@Param("id") String id);


}
