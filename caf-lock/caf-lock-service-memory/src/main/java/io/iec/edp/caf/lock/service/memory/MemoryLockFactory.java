/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.memory;



import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.lock.service.api.api.DistributedLock;
import io.iec.edp.caf.lock.service.api.exception.DLErrorDefinition;
import io.iec.edp.caf.lock.service.api.exception.DistributedLockException;
import io.iec.edp.caf.lockservice.api.IDistributedLock;
import io.iec.edp.caf.lockservice.api.IDistributedLockFactory;

import java.time.Duration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Deprecated
//todo 被com.inspur.edp.cdp.coderule.runtime.server.lock.DistributedLockService依赖
public class MemoryLockFactory implements IDistributedLockFactory {
    private final Lock memLock = new ReentrantLock();

    private static ConcurrentHashMap<String, Lock> map = new ConcurrentHashMap();
    private static ConcurrentHashMap<String, Integer> refs = new ConcurrentHashMap();

    @Override
    public IDistributedLock createLock(String resource, Duration expiryTime) {
        try {
            Lock lock = null;
            synchronized (resource) {
                if (!refs.containsKey(resource)) {
                    refs.put(resource, 1);
                }
                lock = map.computeIfAbsent(resource, k -> new ReentrantLock());
                refs.computeIfPresent(resource, (k, v) -> v + 1);
            }
            lock.tryLock(expiryTime.getSeconds()*1000,TimeUnit.SECONDS);
            return new MemoryLockOld(resource, true, map, refs);
        } catch (Exception e) {
            throw new DistributedLockException("pfcomm", DLErrorDefinition.AddLock_IsAcquired_Error, null, e, ExceptionLevel.Error, false);
        }
    }

    @Override
    public IDistributedLock CreateLock(String resource, Duration expiryTime) {
        return createLock(resource, expiryTime);
    }

    @Override
    public IDistributedLock createLock(String resource, Duration expiryTime, Duration waitTime, Duration retryTime) {
        try {
            Lock lock = null;
            synchronized (resource) {
                if (!refs.containsKey(resource)) {
                    refs.put(resource, 1);
                }
                lock = map.computeIfAbsent(resource, k -> new ReentrantLock());
                refs.computeIfPresent(resource, (k, v) -> v + 1);
            }
            lock.tryLock(expiryTime.getSeconds()*1000,TimeUnit.SECONDS);
            return new MemoryLockOld(resource, true, map, refs);
        } catch (Exception e) {
            throw new DistributedLockException("pfcomm", DLErrorDefinition.AddLock_IsAcquired_Error, null, e, ExceptionLevel.Error, false);
        }
    }
}
