/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.lock.service.redis;

import io.iec.edp.caf.lockservice.api.IDistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

@Deprecated
public class DistributedLockOld implements IDistributedLock {
    private RLock redLock;
    /**加锁资源*/
    private String resource;
    private RedissonClient newClient;
    /**是否获取锁的标识*/
    //private boolean isAcquired;

    /**
     * ctor
     * @param rLock
     */
    public DistributedLockOld(String resource, RLock rLock){
        this.resource = resource;
        this.redLock = rLock;
    }

    /**
     * 是否已正常获取锁
     * @return
     */
    public boolean isAcquired() {
        if (this.redLock != null)
            //return true;
            return this.redLock.isLocked();
        return false;
    }

    /**
     * 获取资源
     * @return 资源信息
     */
    public String getResource() {
        return this.resource;
    }

    @Override
    public void close() {
        if(this.redLock!=null && this.redLock.isLocked()){
//            log.error("分布式锁解锁DistributedLockOld+close解锁资源为"+this.redLock.getName(),new Exception());
            this.redLock.unlock();
        }
    }
}