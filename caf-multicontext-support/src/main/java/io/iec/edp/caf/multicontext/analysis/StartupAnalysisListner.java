/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.analysis;

import io.iec.edp.caf.commons.event.StartupCompletedEvent;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

@Slf4j
@Order(0)
public class StartupAnalysisListner implements ApplicationListener<StartupCompletedEvent> {

    @Autowired
    private Environment env;

    @Override
    public void onApplicationEvent(StartupCompletedEvent event) {

        var isLog = env.getProperty("startup.log",boolean.class);
        if(isLog!=null && isLog) {
            var parallel = System.getProperty("parallel.startup", "false").equals("true");

            if(parallel){
                StartupAnalysisLog.logBeanStepTime();
                StartupAnalysisLog.logRestBeanAnalysis();
                StartupAnalysisLog.logRpcBeanAnalysis();
                StartupAnalysisLog.logBeanAnalysis();
            }else{
                StartupAnalysisLog.logNoParallel();
            }
            StartupAnalysisLog.logBeanTimeAnalysis();

        }
        StartupAnalysisLog.clearBeanAnaylsis();
    }
}
