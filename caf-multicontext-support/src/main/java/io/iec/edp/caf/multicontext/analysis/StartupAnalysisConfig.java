/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.multicontext.analysis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

@Configuration(proxyBeanMethods = false)
@Slf4j
public class StartupAnalysisConfig implements ApplicationContextAware {

    private ApplicationContext context;

    @Bean
    public ApplicationListener bootstrapListener() {
        return new StartupAnalysisListner();
    }

    //todo 只有在非并行下才这么注册
    @Bean
    @ConditionalOnExpression("#{systemProperties['parallel.startup']==null || systemProperties['parallel.startup'].equals('false')}")
    public BeanPostProcessor beanInitFileLogProcessor() {
        StartupAnalysisLog.setApplicationContext(this.context);
        return new StartupAnalysisBeanPostProcesser("All No Parallel");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
