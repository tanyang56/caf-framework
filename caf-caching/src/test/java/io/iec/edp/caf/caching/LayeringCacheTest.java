///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.caching;
//
//import io.iec.edp.caf.caching.api.Cache;
//import io.iec.edp.caf.caching.api.CacheManager;
//import io.iec.edp.caf.caching.properties.RedisCacheProperties;
//import io.iec.edp.caf.caching.setting.LayeringCacheSetting;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import static org.junit.Assert.*;
//
///**
// * 单元测试
// *
// * @author guowenchang
// * @date 2020-05-14
// */
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = CachingConfiguration.class)
//public class LayeringCacheTest {
//
//    @Autowired
//    private CacheManager cacheManager;
//
//    @Autowired
//    private RedisCacheProperties redisCacheSettings;
//
//    private String name = "Seven-Class";
//
//    private LayeringCacheSetting ONLY_FIRST;
//
//    private LayeringCacheSetting ONLY_SECOND;
//
//    private LayeringCacheSetting FIRST_SECOND;
//
//    @Test
//    public void testFirstCache() {
//        //
//        Cache cache = this.cacheManager.getCache(name, ONLY_FIRST);
//        assertNotNull(cache);
//        cache.put("口号", "不负赤子心");
//
//        //string列表
//        List<String> studentNames = new ArrayList<>();
//        studentNames.add("满天星");
//        studentNames.add("康乃馨");
//        cache.put("studentNames", studentNames);
//        List list = cache.get("studentNames", List.class);
//        assertNotNull(list);
//        assertTrue(list.size()==2);
//        assertTrue(list.get(0).equals("满天星"));
//        assertTrue(list.get(1).equals("康乃馨"));
//        assert list.get(0).equals("满天星");
//
//        //PeopleVO
//        List<People> people = new ArrayList<>();
//        people.add(new People(1, "张三", "zs"));
//        people.add(new People(2, "李四", "ls"));
//        cache.put("people", people);
//        //对象类型的List为成为LinkedHashMap
//        List list2 = cache.get("people", List.class);
//        assertNotNull(list2);
//        assertTrue(list2.size()==2);
//    }
//
//    @Test
//    public void testSecondCache() {
//        //
//        Cache cache = this.cacheManager.getCache(name, ONLY_SECOND);
//        assertNotNull(cache);
//        cache.put("口号", "不负赤子心");
//
//        //string列表
//        List<String> studentNames = new ArrayList<>();
//        studentNames.add("满天星");
//        studentNames.add("康乃馨");
//        cache.put("studentNames", studentNames);
//        List list = cache.get("studentNames", List.class);
//        assertNotNull(list);
//        assertTrue(list.size()==2);
//        assertTrue(list.get(0).equals("满天星"));
//        assertTrue(list.get(1).equals("康乃馨"));
//
//        //PeopleVO
//        List<People> people = new ArrayList<>();
//        people.add(new People(1, "张三", "zs"));
//        people.add(new People(2, "李四", "ls"));
//        cache.put("people", people);
//        //对象类型的List为成为LinkedHashMap
//        List list2 = cache.get("people", List.class);
//        assertNotNull(list2);
//        assertTrue(list2.size()==2);
//    }
//
//    @Test
//    public void testFirstAndSecondCache() {
//        //
//        Cache cache = this.cacheManager.getCache(name, FIRST_SECOND);
//        assertNotNull(cache);
//        cache.put("口号", "不负赤子心");
//
//        //string列表
//        List<String> studentNames = new ArrayList<>();
//        studentNames.add("满天星");
//        studentNames.add("康乃馨");
//        cache.put("studentNames", studentNames);
//        List list = cache.get("studentNames", List.class);
//        assertNotNull(list);
//        assertTrue(list.size()==2);
//        assertTrue(list.get(0).equals("满天星"));
//        assertTrue(list.get(1).equals("康乃馨"));
//
//        //PeopleVO
//        List<People> people = new ArrayList<>();
//        people.add(new People(1, "张三", "zs"));
//        people.add(new People(2, "李四", "ls"));
//        cache.put("people", people);
//        //对象类型的List为成为LinkedHashMap
//        List list2 = cache.get("people", List.class);
//        assertNotNull(list2);
//        assertTrue(list2.size()==2);
//    }
//
//    @Before()
//    public void setONLY_FIRST(){
//        this.ONLY_FIRST = new LayeringCacheSetting.Builder()
//                .enableFirstCache()
//                .disableSecondCache()
//                .firstCacheExpireTime(5)
//                .firstCacheTimeUnit(TimeUnit.MINUTES)
//                .allowNullValue(false)
//                .depict("17班")
//                .build();
//    }
//
//    @Before()
//    public void setONLY_SECOND(){
//        this.ONLY_SECOND = new LayeringCacheSetting.Builder()
//                .disableFirstCache()
//                .enableSecondCache()
//                .secondCacheExpireTime(5)
//                .secondCacheTimeUnit(TimeUnit.MINUTES)
//                .allowNullValue(false)
//                .depict("17班")
//                .build();
//    }
//
//    @Before()
//    public void setFIRST_SECOND(){
//        this.FIRST_SECOND = new LayeringCacheSetting.Builder()
//                .enableFirstCache()
//                .firstCacheTimeUnit(TimeUnit.MINUTES)
//                .firstCacheExpireTime(5)
//                .enableSecondCache()
//                .secondCacheExpireTime(7)
//                .secondCacheTimeUnit(TimeUnit.MINUTES)
//                .allowNullValue(false)
//                .depict("17班")
//                .build();
//    }
//
//    @After
//    public void clear() {
//        Cache cache = this.cacheManager.getCache(name, ONLY_FIRST);
//        if(cache!=null){
//            cache.clear();
//            List list = cache.get("studentNames", List.class);
//            assertNull(list);
//        }
//
//
//        cache = this.cacheManager.getCache(name, ONLY_SECOND);
//        if(cache!=null) {
//            cache.clear();
//            List list = cache.get("studentNames", List.class);
//            assertNull(list);
//        }
//
//
//        cache = this.cacheManager.getCache(name, FIRST_SECOND);
//        if(cache!=null) {
//            cache.clear();
//            List list = cache.get("studentNames", List.class);
//            assertNull(list);
//        }
//    }
//
//    @Data
//    @AllArgsConstructor
//    @NoArgsConstructor
//    class People {
//        private int number;
//        private String name;
//        private String code;
//    }
//
//}
