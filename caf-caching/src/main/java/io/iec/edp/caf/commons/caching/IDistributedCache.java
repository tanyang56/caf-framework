/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching;

import java.util.Map;

/**
 * 分布式缓存服务接口
 */
@Deprecated
//todo 被CacheFactory依赖
public interface IDistributedCache {

  /**
   * 获取缓存
   * @param key 键
   * @param serializer 自定义序列化方式，可选的，默认为Json序列化
   * @param clazz 自定义对象的class对象
   * @return 缓存值
   */
  public <T> T get(String key, Class<T> clazz, ICacheSerializer serializer);

  /**
   * 增加缓存
   * @param key 键
   * @param value 类值
   * @param options  缓存过期策略
   * @param serializer 自定义序列化格式，可选的，默认为Json序列化
   */
  public void set(String key, Object value, CacheEntryOptions options, ICacheSerializer serializer);

  /**
   * 增加缓存
   * @param hashKey hash键
   * @param key 键
   * @param value  值
   * @param options 缓存过期策略
   * @param serializer 自定义序列化格式，可选的，默认为Json序列化
   */
  public void hashSet(String hashKey, String key, Object value, CacheEntryOptions options, ICacheSerializer serializer);
  /**
   * 获取缓存
   * @param hashKey hash键
   * @param serializer  自定义序列化格式，可选的，默认为Json序列化
   * @param clazz 自定义对象的class对象
   * @param members  字段
   * @return 缓存值
   */
  public <T> T[] hashMemberGet(String hashKey, String[] members, Class<T> clazz, ICacheSerializer serializer);

  /**
   * 通过hashKey返回所有的字段
   * @param hashKey hash键
   * @return 字段
   */
  public String[] hashKeys(String hashKey);

  /**
   *返回哈希表中的所有域和值
   * @param hashKey hash键
   * @param serializer 自定义序列化格式，可选的，默认为Json序列化
   * @param clazz  自定义对象的class对象
   * @return  所有域和值
   */
 public <T> Map<String, T> hashGetAll(String hashKey, Class<T> clazz, ICacheSerializer serializer);

  /**
   * 删除缓存
   * @param key 键
   */
  public void remove(String key);

  /**
   * 删除缓存
   * @param hashKey hash键
   * @param key  键
   */
  public void hashRemove(String hashKey, String key);

}
