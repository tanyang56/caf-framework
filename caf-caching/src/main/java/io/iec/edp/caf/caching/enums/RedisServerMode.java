/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.enums;

/**
 * Redis服务器模式模式
 *
 * @author guowenchang
 * @date 2020-05-13
 */
public enum RedisServerMode {

    STANDALONE("standalone", "单体模式"),
    SENTINEL("sentinel", "哨兵模式"),
    CLUSTER("cluster", "集群模式");

    private String label;
    private String description;

    RedisServerMode(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public static RedisServerMode getMode(String label) {
        for (RedisServerMode mode : RedisServerMode.values()) {
            if (mode.label.equals(label)) {
                return mode;
            }
        }

        throw new RuntimeException("不支持此种redis模式" + label);
    }
}