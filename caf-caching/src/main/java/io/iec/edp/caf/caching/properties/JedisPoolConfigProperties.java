/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * jedis配置
 *
 * @author wangyandong
 */
@ConfigurationProperties("jedis.pool.config")
@Getter
@Setter
public class JedisPoolConfigProperties {

    /**
     * 最大个数
     */
    private int maxTotal = 256;

    /**
     * 最大闲置个数
     */
    private int maxIdle = 256;

    /**
     * 最小闲置个数
     */
    private int minIdle = 16;

    /**
     * 最大等待时间（毫秒）
     */
    private int maxWaitMillis = 10000;

    /**
     * 获取连接时是否ping
     */
    private boolean testOnBorrow = true;

    /**
     * 返还连接是否ping
     */
    private boolean testOnReturn = false;

    /**
     * 是否周期检查连接
     */
    private boolean testWhileIdle = true;

}