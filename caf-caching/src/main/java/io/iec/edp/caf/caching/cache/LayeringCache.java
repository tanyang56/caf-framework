/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.cache;

import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.listener.RedisPubSubMessage;
import io.iec.edp.caf.caching.enums.RedisPubSubMessageType;
import io.iec.edp.caf.caching.listener.RedisPublisher;
import io.iec.edp.caf.caching.setting.LayeringCacheSetting;
import io.iec.edp.caf.caching.stats.CacheStats;
import io.iec.edp.caf.caching.support.CallableWrapper;
import io.iec.edp.caf.caching.support.NullValue;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;

import java.util.*;

/**
 * 多级缓存
 */
public class LayeringCache extends AbstractValueCache {

    //redis 客户端
    private RedisTemplate<String, Object> redisTemplate;

    //一级缓存
    private AbstractValueCache firstCache;

    //二级缓存
    private AbstractValueCache secondCache;

    //多级缓存配置
    private LayeringCacheSetting layeringCacheSetting;

    //是否使用一级缓存， 默认true
    private boolean useFirstCache = true;

    //是否使用二级缓存， 默认true
    private boolean useSecondCache = true;

    /**
     * 创建一个多级缓存对象
     *
     * @param redisTemplate        redisTemplate
     * @param firstCache           一级缓存
     * @param secondCache          二级缓存
     * @param stats                是否开启统计
     * @param layeringCacheSetting 多级缓存配置
     */
    public LayeringCache(RedisTemplate<String, Object> redisTemplate, AbstractValueCache firstCache, AbstractValueCache secondCache,
                         boolean stats, LayeringCacheSetting layeringCacheSetting) {
        this(redisTemplate, firstCache, secondCache,
                layeringCacheSetting.isUseFirstCache(), layeringCacheSetting.isUseSecondCache(),
                stats, secondCache.getName(), layeringCacheSetting);
    }

    /**
     * 创建一个多级缓存对象
     *
     * @param redisTemplate        redisTemplate
     * @param firstCache           一级缓存
     * @param secondCache          二级缓存
     * @param useFirstCache        是否使用一级缓存，默认是
     * @param stats                是否开启统计，默认否
     * @param name                 缓存名称
     * @param layeringCacheSetting 多级缓存配置
     */
    public LayeringCache(RedisTemplate<String, Object> redisTemplate, AbstractValueCache firstCache, AbstractValueCache secondCache,
                         boolean useFirstCache, boolean useSecondCache,
                         boolean stats, String name, LayeringCacheSetting layeringCacheSetting) {
        super(stats, name, layeringCacheSetting.isAllowNullValue());
        this.redisTemplate = redisTemplate;
        this.firstCache = firstCache;
        this.secondCache = secondCache;
        this.useFirstCache = useFirstCache;
        this.useSecondCache = useSecondCache;
        this.layeringCacheSetting = layeringCacheSetting;
    }

    @Override
    public LayeringCache getNativeCache() {
        return this;
    }

    /**
     * 获取一级缓存
     */
    public Cache getFirstCache() {
        return firstCache;
    }

    /**
     * 获取二级缓存
     */
    public Cache getSecondCache() {
        return secondCache;
    }

    public LayeringCacheSetting getLayeringCacheSetting() {
        return layeringCacheSetting;
    }

    @Override
    public CacheStats getCacheStats() {
        CacheStats cacheStats = new CacheStats();

        if (useFirstCache) {
            cacheStats.addCacheRequestCount(firstCache.getCacheStats().getCacheRequestCount().longValue());
        }

        if (useSecondCache) {
            cacheStats.addCachedMethodRequestCount(secondCache.getCacheStats().getCachedMethodRequestCount().longValue());
            cacheStats.addCachedMethodRequestTime(secondCache.getCacheStats().getCachedMethodRequestTime().longValue());
        }

        if (useFirstCache && useSecondCache) {
            firstCache.getCacheStats().addCachedMethodRequestCount(secondCache.getCacheStats().getCacheRequestCount().longValue());
        }

        setCacheStats(cacheStats);
        return cacheStats;
    }

    @Override
    @Deprecated
    public Object get(Object key) {
        throw new RuntimeException("not support this method");
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        Object result = null;
        if (useFirstCache) {
            result = firstCache.get(key);
            if (result != null || result instanceof NullValue) {
                return (T) fromStoreValue(result);
            }
        }

        if (useSecondCache) {
            result = secondCache.get(key, type);
            putFirstCache(key, result);
        }

        return (T) result;
    }

    @Override
    public <T> T get(Object key, CallableWrapper<T> callableWrapper) {
        Object result = null;
        if (useFirstCache) {
            result = firstCache.get(key);
            if (result != null || result instanceof NullValue) {
                return (T) fromStoreValue(result);
            }
        }

        if (useSecondCache) {
            result = secondCache.get(key, callableWrapper);
            putFirstCache(key, result);
        }

        return (T) fromStoreValue(result);
    }

    @Override
    public void put(Object key, Object value) {
        if (useSecondCache) {
            secondCache.put(key, value);
            // 删除一级缓存
            if (useFirstCache) evictFirstCache(key);
        } else if (useFirstCache) {
            firstCache.put(key, value);
        }
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        Object result;
        if (useSecondCache) {
            result = secondCache.putIfAbsent(key, value);
            // 删除一级缓存
            if (useFirstCache) evictFirstCache(key);
        } else {
            result = putFirstCache(key, value);
        }

        return result;
    }

    @Override
    public void evict(Object key) {
        if (useSecondCache) {
            // 先删除二级缓存再删除一级缓存，否则有并发问题
            secondCache.evict(key);
            // 删除一级缓存
            if (useFirstCache) evictFirstCache(key);
        } else {
            firstCache.evict(key);
        }
    }

    @Override
    public void clear() {
        if (useSecondCache) {
            // 先删除二级缓存再删除一级缓存，否则有并发问题
            secondCache.clear();
            if (useFirstCache) removeFirstCache(null, RedisPubSubMessageType.CLEAR);
        } else {
            firstCache.clear();
        }
    }

    /**
     * 存储hash时
     * 一、如果开启二级：二级存储，同时若开启一级则清空所有应用一级缓存
     * 二、如果未开二级：如果开启一级，一级存储
     */
    public void putAsHash(Object key, Object hashKey, Object hashValue) {
        if (useSecondCache) {
            secondCache.putAsHash(key, hashKey, hashValue);
            if (useFirstCache) evictFirstCache(key);
        } else if (useFirstCache) {
            firstCache.putAsHash(key, hashKey, hashValue);
        }
    }

    /**
     * 获取hash所有keys时
     * 一、如果开启一级：一级获取到则直接返回
     * 二、如果关闭一级或一级未取到：二级获取，如果开启一级则同步一级(该同步每次仅同步处理请求的单个应用)
     */
    public Set<Object> getHashKeys(Object key) {
        if (useFirstCache && useSecondCache) {
            syncHashIfNecessary(key);
            return firstCache.getHashKeys(key);
        } else if (useSecondCache) {
            return secondCache.getHashKeys(key);
        } else {
            return firstCache.getHashKeys(key);
        }
    }

    /**
     * 获取hash中某个键值对
     * 一、如果开启一级：一级取到则返回
     * 二、如果未开一级或一级未取到：二级获取，如果开启一级则同步一级(该同步每次仅同步处理请求的单个应用)
     */
    public <T> T getHashValue(Object key, Object hashKey, Class<T> type) {
        if (useFirstCache && useSecondCache) {
            syncHashIfNecessary(key);
            return firstCache.getHashValue(key, hashKey, type);
        } else if (useSecondCache) {
            return secondCache.getHashValue(key, hashKey, type);
        } else {
            return firstCache.getHashValue(key, hashKey, type);
        }
    }

    /**
     * 获取hash所有的value时
     * 一、如果开启一级：一级取到则直接返回
     * 二、如果(未开一级||一级没取到)&&开启二级：二级获取，如果开启一级，则同步一级(该同步每次仅同步处理请求的单个应用)
     */
    public List<Object> getHashValues(Object key) {
        if (useFirstCache && useSecondCache) {
            syncHashIfNecessary(key);
            return firstCache.getHashValues(key);
        } else if (useSecondCache) {
            return secondCache.getHashValues(key);
        } else {
            return firstCache.getHashValues(key);
        }
    }

    /**
     * 删除hash的部分keys
     * 一、如果开启二级：二级清除; 同时若开启一级则清空所有应用一级缓存
     * 二、如果未开一级：一级删除部分keys
     */
    public void deleteHashKey(Object key, Object... hashKey) {
        if (useSecondCache) {
            secondCache.deleteHashKey(key, hashKey);
            if (useFirstCache) evictFirstCache(key);
        } else if (useFirstCache) {
            firstCache.deleteHashKey(key, hashKey);
        }
    }

    @Override
    public void multiPut(Map<Object, Object> map) {
        if (useSecondCache) {
            secondCache.multiPut(map);
            // 删除一级缓存
            if (useFirstCache) removeFirstCache(map.keySet(), RedisPubSubMessageType.EVICT);
        } else if (useFirstCache) {
            firstCache.multiPut(map);
        }
    }

    @Override
    public Map<Object, Object> multiGet(List<Object> keys) {
        Map<Object, Object> map = new HashMap<>();
        for (Object key : keys) {
            map.put(key, get(key, Object.class));
        }

        return map;
    }

    @Override
    public void multiDel(Collection<Object> keys) {
        if (useSecondCache) {
            secondCache.multiDel(keys);
            if (useFirstCache) {
                //remove current and other server memory cache by redis publish/subscribe
                removeFirstCache(keys, RedisPubSubMessageType.EVICT);
            }
        } else {
            firstCache.multiDel(keys);
        }
    }

    private Object putFirstCache(Object key, Object result) {
        if (useFirstCache) {
            return firstCache.putIfAbsent(key, result);
        }

        return null;
    }

    private void evictFirstCache(Object key) {
        removeFirstCache(Collections.singletonList(key), RedisPubSubMessageType.EVICT);
    }

    private void removeFirstCache(Collection<Object> keys, RedisPubSubMessageType type) {
        if (this.redisTemplate == null) return;

        // 删除一级缓存需要用到redis的Pub/Sub（订阅/发布）模式，否则集群中其他服服务器节点的一级缓存数据无法删除
        RedisPubSubMessage message = new RedisPubSubMessage();
        message.setCacheName(getName());
        switch (type) {
            case EVICT: {
                //清理一级缓存
                firstCache.multiDel(keys);
                message.addKeys(keys);
                message.setMessageType(RedisPubSubMessageType.EVICT);
                break;
            }
            case CLEAR: {
                //清空一级缓存
                firstCache.clear();
                message.setMessageType(RedisPubSubMessageType.CLEAR);
                break;
            }
        }

        // 发布消息
        RedisPublisher.publisher(redisTemplate, new ChannelTopic(getName()), message);
    }

    //二级缓存的hash向一级缓存同步:前提：一、二级缓存都开启
    private void syncHashIfNecessary(Object key) {
        if (firstCache.hashSize(key) != secondCache.hashSize(key)) {
            //一、二级缓存map大小不一致，进行同步
            firstCache.evict(key);
            Set<Object> hashKeys = secondCache.getHashKeys(key);
            for (Object hashKey : hashKeys) {
                firstCache.putAsHash(key, hashKey, secondCache.getHashValue(key, hashKey, Object.class));
            }
        }
    }

}
