/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.manager;

import io.iec.edp.caf.caching.listener.RedisMessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.awt.*;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangyandong
 */
public class RedisClient {
    private static ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
    private static ThreadPoolTaskExecutor subscriptionExecutor = new ThreadPoolTaskExecutor();
    protected RedisTemplate<String, Object> redisTemplate;

    /**
     * redis pub/sub 容器
     */
    protected final RedisMessageListenerContainer container = new RedisMessageListenerContainer();

    static {
        taskExecutor.setThreadNamePrefix("cafCaching-task-");
        // 1.核心线程数
        taskExecutor.setCorePoolSize(2);
        // 2.最大线程数
        taskExecutor.setMaxPoolSize(64);
        // 3.队列最大长度
        taskExecutor.setQueueCapacity(500);
        // 4.线程池维护线程允许的空闲时间，默认60s
        taskExecutor.setKeepAliveSeconds(60);
        // 5.线程池初始化
        taskExecutor.initialize();

        subscriptionExecutor.setThreadNamePrefix("cafCaching-sub-");
        // 1.核心线程数
        subscriptionExecutor.setCorePoolSize(2);
        // 2.最大线程数
        subscriptionExecutor.setMaxPoolSize(64);
        // 3.队列最大长度
        subscriptionExecutor.setQueueCapacity(500);
        // 4.线程池维护线程允许的空闲时间，默认60s
        subscriptionExecutor.setKeepAliveSeconds(60);
        // 5.线程池初始化
        subscriptionExecutor.initialize();
    }

    public RedisClient(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.container.setConnectionFactory(this.redisTemplate.getConnectionFactory());
        this.container.setTaskExecutor(taskExecutor);
        this.container.setSubscriptionExecutor(subscriptionExecutor);
        this.container.afterPropertiesSet();
        this.container.start();
    }

    public void addMessageListener(RedisMessageListener redisMessageListener, Set<String> cacheNames) {
        for (String name : cacheNames) {
            this.container.addMessageListener(redisMessageListener, new ChannelTopic(name));
        }
    }

    public RedisMessageListenerContainer getContainer() {
        return this.container;
    }

    public RedisTemplate getRedisTemplate() {
        return this.redisTemplate;
    }
}
