/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.layeringcache.manager;

import io.iec.edp.caf.commons.layeringcache.cache.Cache;
import io.iec.edp.caf.commons.layeringcache.listener.RedisMessageListener;
import io.iec.edp.caf.commons.layeringcache.setting.LayeringCacheSetting;
import io.iec.edp.caf.commons.layeringcache.stats.CacheStatsInfo;
import io.iec.edp.caf.commons.layeringcache.stats.StatsService;
import io.iec.edp.caf.commons.layeringcache.util.BeanFactory;
import io.iec.edp.caf.commons.utils.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.SmartLifecycle;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 公共的抽象 {@link CacheManager} 的实现.
 */
@Deprecated
//todo 被com.inspur.edp.aif.runtime.adapter.CafServiceManager依赖
public abstract class AbstractCacheManager implements CacheManager, InitializingBean, DisposableBean, SmartLifecycle {

    private Logger logger = LoggerFactory.getLogger(AbstractCacheManager.class);

//    /**
//     * redis pub/sub 容器
//     */
//    private final RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//
    /**
     * redis pub/sub 监听器
     */
    protected final RedisMessageListener messageListener = new RedisMessageListener();

    private volatile boolean running = false;

    /**
     * 缓存容器
     * 外层key是cache_name
     * 里层key是[一级缓存有效时间-二级缓存有效时间-二级缓存自动刷新时间]
     */
    private final ConcurrentMap<String, ConcurrentMap<String, Cache>> cacheContainer = new ConcurrentHashMap<>(16);

    /**
     * 缓存名称容器
     */
    protected volatile Set<String> cacheNames = new LinkedHashSet<>();

    /**
     * CacheManager 容器
     */
    static Set<AbstractCacheManager> cacheManagers = new LinkedHashSet<>();

    /**
     * 是否开启统计
     */
    private boolean stats = true;

    /**
     * redis客户端 按cacheName分片
     */
    protected ConcurrentHashMap<String, RedisClient> redisClientPool = new ConcurrentHashMap<>();

//    /**
//     * redis 客户端
//     */
//    RedisTemplate<String, Object> redisTemplate;

    public static Set<AbstractCacheManager> getCacheManager() {
        return cacheManagers;

    }

    @Override
    public Collection<Cache> getCaches(String name) {
        ConcurrentMap<String, Cache> cacheMap = this.cacheContainer.get(name);
        if (CollectionUtils.isEmpty(cacheMap)) {
            return Collections.emptyList();
        }
        return cacheMap.values();
    }

    @Override
    public Cache getCache(String name) {
        LayeringCacheSetting layeringCacheSetting = LayeringCacheSetting.getDefaultCacheSetting();
        return this.getCache(name, layeringCacheSetting);
    }

    // Lazy cache initialization on access
    @Override
    public Cache getCache(String name, LayeringCacheSetting layeringCacheSetting) {
        // 第一次获取缓存Cache，如果有直接返回,如果没有加锁往容器里里面放Cache
        ConcurrentMap<String, Cache> cacheMap = this.cacheContainer.get(name);
        if (!CollectionUtils.isEmpty(cacheMap)) {
            if (cacheMap.size() > 1) {
                logger.warn("缓存名称为 {} 的缓存,存在两个不同的过期时间配置，请一定注意保证缓存的key唯一性，否则会出现缓存过期时间错乱的情况", name);
            }
            Cache cache = cacheMap.get(layeringCacheSetting.getInternalKey());
            if (cache != null) {
                return cache;
            }
        }

        // 第二次获取缓存Cache，加锁往容器里里面放Cache
        synchronized (this.cacheContainer) {
            cacheMap = this.cacheContainer.get(name);
            if (!CollectionUtils.isEmpty(cacheMap)) {
                // 从容器中获取缓存
                Cache cache = cacheMap.get(layeringCacheSetting.getInternalKey());
                if (cache != null) {
                    return cache;
                }
            } else {
                cacheMap = new ConcurrentHashMap<>(16);
                cacheContainer.put(name, cacheMap);
                // 更新缓存名称
                updateCacheNames(name);
            }

            // 新建一个Cache对象
            Cache cache = getMissingCache(name, layeringCacheSetting);
            if (cache != null) {
                // 装饰Cache对象
                cache = decorateCache(cache);

                // 将新的Cache对象放到容器
                cacheMap.put(layeringCacheSetting.getInternalKey(), cache);
                if (cacheMap.size() > 1) {
                    logger.warn("缓存名称为 {} 的缓存,存在两个不同的过期时间配置，请一定注意保证缓存的key唯一性，否则会出现缓存过期时间错乱的情况", name);
                }

                // 同时开启一级和二级缓存，创建redis监听
                if (useFirstAndSecond(layeringCacheSetting))
                    addMessageListener(name);
            }

            return cache;
        }
    }

    @Override
    public Collection<String> getCacheNames() {
        return this.cacheNames;
    }

    /**
     * 更新缓存名称容器
     *
     * @param name 需要添加的缓存名称
     */
    private void updateCacheNames(String name) {
        cacheNames.add(name);
    }

    /**
     * 获取Cache对象的装饰示例
     *
     * @param cache 需要添加到CacheManager的Cache实例
     * @return 装饰过后的Cache实例
     */
    protected Cache decorateCache(Cache cache) {
        return cache;
    }

    /**
     * 根据缓存名称在CacheManager中没有找到对应Cache时，通过该方法新建一个对应的Cache实例
     *
     * @param name                 缓存名称
     * @param layeringCacheSetting 缓存配置
     * @return {@link Cache}
     */
    protected abstract Cache getMissingCache(String name, LayeringCacheSetting layeringCacheSetting);

    /**
     * 是否同时开启一级和二级缓存
     *
     * @param layeringCacheSetting 缓存设置
     * @return 是否同时开启一二级缓存
     */
    protected abstract boolean useFirstAndSecond(LayeringCacheSetting layeringCacheSetting);

    /**
     * 获取缓存容器
     *
     * @return 返回缓存容器
     */
    public ConcurrentMap<String, ConcurrentMap<String, Cache>> getCacheContainer() {
        return cacheContainer;
    }

    /**
     * 添加消息监听
     *
     * @param name 缓存名称
     */
    protected void addMessageListener(String name) {
        //订阅存储 name 的Redis服务器
        if (redisClientPool.get(name) != null) {
            redisClientPool.get(name).getContainer().addMessageListener(messageListener, new ChannelTopic(name));
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        messageListener.setCacheManager(this);
        messageListener.afterPropertiesSet();

        BeanFactory.getBean(StatsService.class).setCacheManager(this);
        if (getStats()) {
            // 采集缓存命中率数据
            BeanFactory.getBean(StatsService.class).syncCacheStats();
        }
    }

    @Override
    public List<CacheStatsInfo> listCacheStats(String cacheName) {
        return BeanFactory.getBean(StatsService.class).listCacheStats(cacheName);
    }

    @Override
    public void resetCacheStat() {
        BeanFactory.getBean(StatsService.class).resetCacheStat();
    }

//    @Override
//    public void setBeanName(String name) {
//        container.setBeanName("redisMessageListenerContainer");
//    }

    @Override
    public void destroy() throws Exception {
        for (RedisClient redisClient : redisClientPool.values()) {
            redisClient.container.destroy();
        }

        BeanFactory.getBean(StatsService.class).shutdownExecutor();
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void start() {
        this.running = true;
        for (RedisClient redisClient : redisClientPool.values()) {
            redisClient.container.start();
        }
    }

    @Override
    public void stop() {
        this.running = false;
        for (RedisClient redisClient : redisClientPool.values()) {
            redisClient.container.stop();
        }
    }

    @Override
    public boolean isRunning() {
        return this.running;
    }

    @Override
    public int getPhase() {
        return Integer.MAX_VALUE;
    }

    public boolean getStats() {
        return stats;
    }

    public void setStats(boolean stats) {
        this.stats = stats;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public RedisTemplate<String, Object> getRedisTemplate(String cacheName) {
        return this.redisClientPool.get(cacheName) != null ? this.redisClientPool.get(cacheName).redisTemplate : null;
    }

    public Set<RedisTemplate<String, Object>> getRedisTemplates() {
        Set<RedisTemplate<String, Object>> redisTemplateSet = new HashSet<>();
        for (RedisClient redisClient : redisClientPool.values()) {
            redisTemplateSet.add(redisClient.redisTemplate);
        }

        return redisTemplateSet;
    }
}
