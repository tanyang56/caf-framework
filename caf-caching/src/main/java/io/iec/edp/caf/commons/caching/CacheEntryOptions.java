/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching;

import java.time.LocalDateTime;

@Deprecated
//todo 被CacheFactory依赖
public class CacheEntryOptions implements ICacheEntryOptions  {

    /**
     * 绝对过期时间（时间点）
     */
    private LocalDateTime absoluteExpirationTime = null;

    /**
     * 绝对过期时间间隔（秒）
     */
    private int absoluteExpirationInSeconds = -1;


    public LocalDateTime getAbsoluteExpirationTime() {
        return absoluteExpirationTime;
    }

    public void setAbsoluteExpirationTime(LocalDateTime absoluteExpirationTime) {
        this.absoluteExpirationTime = absoluteExpirationTime;
    }

    public int getAbsoluteExpirationInSeconds() {
        return absoluteExpirationInSeconds;
    }

    public void setAbsoluteExpirationInSeconds(int  absoluteExpirationInSeconds) {
        this.absoluteExpirationInSeconds = absoluteExpirationInSeconds;
    }
}
