/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.layeringcache.cache;

import io.iec.edp.caf.commons.layeringcache.listener.RedisPubSubMessage;
import io.iec.edp.caf.commons.layeringcache.listener.RedisPubSubMessageType;
import io.iec.edp.caf.commons.layeringcache.listener.RedisPublisher;
import io.iec.edp.caf.commons.layeringcache.setting.LayeringCacheSetting;
import io.iec.edp.caf.commons.layeringcache.stats.CacheStats;
import io.iec.edp.caf.commons.layeringcache.support.CallableWrapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;

/**
 * 多级缓存
 */
@Deprecated
public class LayeringCache extends AbstractValueAdaptingCache {
//    Logger logger = LoggerFactory.getLogger(LayeringCache.class);

    /**
     * redis 客户端
     */
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 一级缓存
     */
    private AbstractValueAdaptingCache firstCache;

    /**
     * 二级缓存
     */
    private AbstractValueAdaptingCache secondCache;

    /**
     * 多级缓存配置
     */
    private LayeringCacheSetting layeringCacheSetting;

    /**
     * 是否使用一级缓存， 默认true
     */
    private boolean useFirstCache = true;

    /**
     * 是否使用二级缓存， 默认true
     */
    private boolean useSecondCache = true;

    /**
     * 创建一个多级缓存对象
     *
     * @param redisTemplate        redisTemplate
     * @param firstCache           一级缓存
     * @param secondCache          二级缓存
     * @param stats                是否开启统计
     * @param layeringCacheSetting 多级缓存配置
     */
    public LayeringCache(RedisTemplate<String, Object> redisTemplate,
                         AbstractValueAdaptingCache firstCache,
                         AbstractValueAdaptingCache secondCache,
                         boolean stats,
                         LayeringCacheSetting layeringCacheSetting) {
        this(redisTemplate, firstCache, secondCache, layeringCacheSetting.isUseFirstCache(),
                layeringCacheSetting.isUseSecondCache(), stats, secondCache.getName(), layeringCacheSetting);
    }

    /**
     * @param redisTemplate        redisTemplate
     * @param firstCache           一级缓存
     * @param secondCache          二级缓存
     * @param useFirstCache        是否使用一级缓存，默认是
     * @param stats                是否开启统计，默认否
     * @param name                 缓存名称
     * @param layeringCacheSetting 多级缓存配置
     */
    public LayeringCache(RedisTemplate<String, Object> redisTemplate,
                         AbstractValueAdaptingCache firstCache,
                         AbstractValueAdaptingCache secondCache,
                         boolean useFirstCache,
                         boolean useSecondCache,
                         boolean stats,
                         String name,
                         LayeringCacheSetting layeringCacheSetting) {
        super(stats, name);
        this.redisTemplate = redisTemplate;
        this.firstCache = firstCache;
        this.secondCache = secondCache;
        this.useFirstCache = useFirstCache;
        this.useSecondCache = useSecondCache;
        this.layeringCacheSetting = layeringCacheSetting;
    }

    @Override
    public LayeringCache getNativeCache() {
        return this;
    }

    @Override
    @Deprecated
    public Object get(Object key) {
        throw new RuntimeException("not support this method");
//        Object result = null;
//        if (useFirstCache) {
//            result = firstCache.get(key);
//            logger.debug("查询一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
//            if (result != null) {
//                return fromStoreValue(result);
//            }
//        }
//
//        /**
//         * 禁用掉redis的get
//         */
//        if (useSecondCache) {
//            result = secondCache.get(key);
//            putFirstCache(key, result);
//            logger.debug("查询二级缓存,并将数据放到一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
//        }
//
//        return fromStoreValue(result);
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        T result = null;
        if (useFirstCache) {
            result = firstCache.get(key, type);
//            logger.debug("查询一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
            if (result != null) {
                return (T) fromStoreValue(result);
            }
        }

        if (useSecondCache) {
            result = secondCache.get(key, type);
            putFirstCache(key, result);
//            logger.debug("查询二级缓存,并将数据放到一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
        }

        return result;
    }

    @Override
    public <T> T get(Object key, CallableWrapper<T> callableWrapper) {
        Object result = null;
        if (useFirstCache) {
            result = firstCache.get(key);
//            logger.debug("查询一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
            if (result != null) {
                return (T) fromStoreValue(result);
            }
        }

        if (useSecondCache) {
            result = secondCache.get(key, callableWrapper);
            putFirstCache(key, result);
//            logger.debug("查询二级缓存,并将数据放到一级缓存。 key={},返回值是:{}", key, JSONSerializer.serialize(result));
        }

        return (T) fromStoreValue(result);
    }

    @Override
    public void put(Object key, Object value) {
        if (useSecondCache) {
            secondCache.put(key, value);
            // 删除一级缓存
            if (useFirstCache) {
                deleteFirstCache(key);
            }
        } else {
            if (useFirstCache) {
                firstCache.put(key, value);
            }
        }
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        Object result;
        if (useSecondCache) {
            result = secondCache.putIfAbsent(key, value);
            // 删除一级缓存
            if (useFirstCache) {
                deleteFirstCache(key);
            }
        } else {
            result = putFirstCache(key, value);
        }

        return result;
    }

    @Override
    public void evict(Object key) {
        if (useSecondCache) {
            // 删除的时候要先删除二级缓存再删除一级缓存，否则有并发问题
            secondCache.evict(key);
            // 删除一级缓存
            if (useFirstCache) {
                deleteFirstCache(key);
            }
        } else {
            firstCache.evict(key);
        }
    }

    @Override
    public void clear() {
        if (useSecondCache) {
            // 删除的时候要先删除二级缓存再删除一级缓存，否则有并发问题
            secondCache.clear();
            if (useFirstCache) {
                firstCache.clear();
                // 清除一级缓存需要用到redis的订阅/发布模式，否则集群中其他服服务器节点的一级缓存数据无法删除
                RedisPubSubMessage message = new RedisPubSubMessage();
                message.setCacheName(getName());
                message.setMessageType(RedisPubSubMessageType.CLEAR);
                // 发布消息
                RedisPublisher.publisher(redisTemplate, new ChannelTopic(getName()), message);
            }
        } else {
            firstCache.clear();
        }

    }

    private void deleteFirstCache(Object key) {
        if (this.redisTemplate == null) {
            return;
        }

        firstCache.evict(key);

        // 删除一级缓存需要用到redis的Pub/Sub（订阅/发布）模式，否则集群中其他服服务器节点的一级缓存数据无法删除
        RedisPubSubMessage message = new RedisPubSubMessage();
        message.setCacheName(getName());
        message.setKey(key);
        message.setMessageType(RedisPubSubMessageType.EVICT);
        // 发布消息
        RedisPublisher.publisher(redisTemplate, new ChannelTopic(getName()), message);
    }

    /**
     * 获取一级缓存
     *
     * @return FirstCache
     */
    public Cache getFirstCache() {
        return firstCache;
    }

    /**
     * 获取二级缓存
     *
     * @return SecondCache
     */
    public Cache getSecondCache() {
        return secondCache;
    }

    @Override
    public CacheStats getCacheStats() {
        CacheStats cacheStats = new CacheStats();

        if (useFirstCache) {
            cacheStats.addCacheRequestCount(firstCache.getCacheStats().getCacheRequestCount().longValue());
        }

        if (useSecondCache) {
            cacheStats.addCachedMethodRequestCount(secondCache.getCacheStats().getCachedMethodRequestCount().longValue());
            cacheStats.addCachedMethodRequestTime(secondCache.getCacheStats().getCachedMethodRequestTime().longValue());
        }

        if (useFirstCache && useSecondCache) {
            firstCache.getCacheStats().addCachedMethodRequestCount(secondCache.getCacheStats().getCacheRequestCount().longValue());
        }

        setCacheStats(cacheStats);
        return cacheStats;
    }

    public LayeringCacheSetting getLayeringCacheSetting() {
        return layeringCacheSetting;
    }

    @Override
    public boolean isAllowNullValues() {
        if (!useSecondCache) {
            return false;
        }

        return secondCache.isAllowNullValues();
    }

    private Object putFirstCache(Object key, Object result) {
        if (useFirstCache) {
            return firstCache.putIfAbsent(key, result);
        }

        return null;
    }
}
