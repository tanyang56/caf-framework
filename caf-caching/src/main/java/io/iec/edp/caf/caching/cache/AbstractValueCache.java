/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.cache;

import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.stats.CacheStats;
import io.iec.edp.caf.caching.support.NullValue;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

/**
 * Cache 接口的抽象实现类，对公共的方法做了一写实现，如是否允许存NULL值
 * 如果允许为NULL值，则需要在内部将NULL替换成{@link NullValue#INSTANCE} 对象
 */
public abstract class AbstractValueCache implements Cache {

    /**
     * 缓存名称
     */
    private final String name;

    /**
     * 是否开启统计功能
     */
    private boolean stats;

    /**
     * 是否允许缓存null值
     */
    private boolean allowNullValue;

    /**
     * 缓存统计类
     */
    private CacheStats cacheStats = new CacheStats();

    /**
     * 通过构造方法设置缓存配置
     *
     * @param stats 是否开启监控统计
     * @param name  缓存名称
     */
    protected AbstractValueCache(boolean stats, String name, boolean allowNullValue) {
        Assert.notNull(name, "缓存名称不能为NULL");
        this.stats = stats;
        this.name = name;
        this.allowNullValue = allowNullValue;
    }

    /**
     * 获取是否允许存NULL值
     *
     * @return true:允许，false:不允许
     */
    public boolean isAllowNullValues() {
        return this.allowNullValue;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Object key, Class<T> type) {
        return (T) fromStoreValue(get(key));
    }

    /**
     * Convert the given value from the internal store to a user value
     * returned from the get method (adapting {@code null}).
     *
     * @param storeValue the store value
     * @return the value to return to the user
     */
    protected Object fromStoreValue(Object storeValue) {
        if (isAllowNullValues() && storeValue instanceof NullValue) {
            return null;
        }
        return storeValue;
    }

    /**
     * Convert the given user value, as passed into the put method,
     * to a value in the internal store (adapting {@code null}).
     *
     * @param userValue the given user value
     * @return the value to store
     */
    protected Object toStoreValue(Object userValue) {
        if (isAllowNullValues() && userValue == null) {
            return NullValue.INSTANCE;
        }
        return userValue;
    }

    /**
     * 获取是否开启统计
     *
     * @return true：开启统计，false：关闭统计
     */
    public boolean isStats() {
        return stats;
    }

    /**
     * 获取统计信息
     *
     * @return CacheStats
     */
    @Override
    public CacheStats getCacheStats() {
        return cacheStats;
    }

    public void setCacheStats(CacheStats cacheStats) {
        this.cacheStats = cacheStats;
    }

    /**
     * 获取hash的数量大小(此处实现无作用，由一、二级缓存自己去实现)
     *
     * @param key hash名称
     * @return hash大小
     */
    protected int hashSize(Object key) {
        return 0;
    }

    /**
     * 批量获取键值对默认实现（此方法一、二级缓存不需要单独实现）
     *
     * @param keys 键列表
     * @return null
     */
    public Map<Object, Object> multiGet(List<Object> keys) {
        return null;
    }
}
