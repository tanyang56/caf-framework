/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.cache.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import io.iec.edp.caf.caching.cache.AbstractValueCache;
import io.iec.edp.caf.caching.exception.LoaderCacheValueException;
import io.iec.edp.caf.caching.setting.FirstCacheSetting;
import io.iec.edp.caf.caching.support.CallableWrapper;
import io.iec.edp.caf.caching.enums.ExpireMode;
import io.iec.edp.caf.caching.support.NullValue;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * 基于Caffeine实现的一级缓存
 */
public class CaffeineCache extends AbstractValueCache {

    /**
     * 缓存对象
     */
    private final Cache<Object, Object> cache;

    /**
     * 使用name和{@link FirstCacheSetting}创建一个 {@link CaffeineCache} 实例
     *
     * @param name              缓存名称
     * @param firstCacheSetting 一级缓存配置 {@link FirstCacheSetting}
     * @param stats             是否开启统计模式
     */
    public CaffeineCache(String name, FirstCacheSetting firstCacheSetting, boolean stats, boolean allowNullValue) {
        super(stats, name, allowNullValue);
        //一级缓存配置为null，提供默认配置
        if (firstCacheSetting == null)
            firstCacheSetting = new FirstCacheSetting();

        this.cache = getCache(firstCacheSetting);
    }

    @Override
    public Cache<Object, Object> getNativeCache() {
        return this.cache;
    }

    @Override
    public Object get(Object key) {
        if (isStats()) {
            getCacheStats().addCacheRequestCount(1);
        }

        if (this.cache instanceof LoadingCache) {
            return ((LoadingCache<Object, Object>) this.cache).get(key);
        }
        return cache.getIfPresent(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Object key, CallableWrapper<T> callableWrapper) {
        if (isStats()) {
            getCacheStats().addCacheRequestCount(1);
        }

        Object result = this.cache.get(key, k -> loaderValue(key, callableWrapper.getCallable()));
        // 如果不允许存NULL值 直接删除NULL值缓存
        boolean isEvict = !isAllowNullValues() && (result == null || result instanceof NullValue);
        if (isEvict) {
            evict(key);
        }
        return (T) fromStoreValue(result);
    }

    @Override
    public void put(Object key, Object value) {
        Object result = toStoreValue(value);

        // result = null代表： 不允许存null && value=null
        if (result == null) {
            this.cache.invalidate(key);
            return;
        }

        // 非null值
        this.cache.put(key, result);
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        boolean flag = !isAllowNullValues() && (value == null || value instanceof NullValue);
        if (flag) {
            return null;
        }
        Object result = this.cache.get(key, k -> toStoreValue(value));
        return fromStoreValue(result);
    }

    @Override
    public void evict(Object key) {
        this.cache.invalidate(key);
    }

    @Override
    public void clear() {
        this.cache.invalidateAll();
    }

    @Override
    protected int hashSize(Object key) {
        return this.get(key) != null ? ((Map) this.get(key)).size() : 0;
    }

    @Override
    public void putAsHash(Object key, Object hashKey, Object hashValue) {
        if (hashKey != null) {
            //允许为空值 || hashValue有值
            if (isAllowNullValues() || (hashValue != null && !(hashValue instanceof NullValue))) {
                Map<Object, Object> map = (Map) this.get(key);

                //hash采用线程安全的HashTable实现
                if (map == null)
                    map = new Hashtable<>();

                map.put(hashKey, hashValue);
                this.cache.put(key, map);
            }
        }
    }

    @Override
    public Set<Object> getHashKeys(Object key) {
        Map<Object, Object> map = (Map) this.get(key);
        return map != null ? map.keySet() : null;
    }

    @Override
    public <T> T getHashValue(Object key, Object hashKey, Class<T> type) {
        Map<Object, Object> map = (Map) this.get(key);
        if (map != null)
            return (T) map.get(hashKey);
        return null;
    }

    @Override
    public List<Object> getHashValues(Object key) {
        Map<Object, Object> map = (Map) this.get(key);
        return map != null ? new ArrayList<>(map.values()) : null;
    }

    @Override
    public void deleteHashKey(Object key, Object... hashKey) {
        Map<Object, Object> map = (Map) this.get(key);
        for (Object k : hashKey) {
            map.remove(k);
        }
        this.cache.put(key, map);
    }

    @Override
    public void multiPut(Map<Object, Object> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<Object, Object> entry : map.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public void multiDel(Collection<Object> keys) {
        if (keys != null && keys.size() > 0) {
            for (Object key : keys) {
                this.cache.invalidate(key);
            }
        }
    }

    /**
     * 加载数据
     */
    private <T> Object loaderValue(Object key, Callable<T> valueLoader) {
        long start = System.currentTimeMillis();
        if (isStats()) {
            getCacheStats().addCachedMethodRequestCount(1);
        }

        try {
            T t = valueLoader.call();

            if (isStats()) {
                getCacheStats().addCachedMethodRequestTime(System.currentTimeMillis() - start);
            }
            return toStoreValue(t);
        } catch (Exception e) {
            throw new LoaderCacheValueException(key, e);
        }
    }

    /**
     * 根据配置获取本地缓存对象
     *
     * @param firstCacheSetting 一级缓存配置
     * @return {@link Cache}
     */
    private static Cache<Object, Object> getCache(FirstCacheSetting firstCacheSetting) {
        //todo 支持更多设置
        Caffeine<Object, Object> builder = Caffeine.newBuilder();
        builder.initialCapacity(firstCacheSetting.getInitialCapacity());
        builder.maximumSize(firstCacheSetting.getMaximumSize());
        if (ExpireMode.WRITE.equals(firstCacheSetting.getExpireMode())) {
            builder.expireAfterWrite(firstCacheSetting.getExpireTime(), firstCacheSetting.getTimeUnit());
        } else if (ExpireMode.ACCESS.equals(firstCacheSetting.getExpireMode())) {
            builder.expireAfterAccess(firstCacheSetting.getExpireTime(), firstCacheSetting.getTimeUnit());
        }
        // 根据Caffeine builder创建 Cache 对象
        return builder.build();
    }

}
