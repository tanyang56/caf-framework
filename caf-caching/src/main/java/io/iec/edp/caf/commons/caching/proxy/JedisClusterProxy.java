/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching.proxy;

import io.iec.edp.caf.commons.caching.CacheEntryOptions;
import lombok.val;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Jedis集群代理类
 *
 * @author guowenchang
 */
@Deprecated
//todo 被CacheFactory依赖
public class JedisClusterProxy implements JedisProxy {
    private final int DEFAULT_TIMEOUT = 2000;
    private final int DEFAULT_MAX_ATTEMPTS = 5;
    private JedisCluster jedisCluster;
    private boolean closed = false;

    public JedisClusterProxy(JedisPoolConfig jedisPoolConfig, Set<HostAndPort> clusterNodes, int maxRedirects, String password) {
        this.jedisCluster = new JedisCluster(clusterNodes, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT, DEFAULT_MAX_ATTEMPTS, password, jedisPoolConfig);
    }

    @Override
    public String get(String key) {
        return jedisCluster.get(key);
    }

    @Override
    public void set(String formatKey, String value, CacheEntryOptions options) {
        jedisCluster.set(formatKey, value);
        setExpiration(formatKey, jedisCluster, options);
    }

    @Override
    public void hset(String formatKey, String key, String data, CacheEntryOptions options) {
        jedisCluster.hset(formatKey, key, data);
        setExpiration(formatKey, jedisCluster, options);
    }

    @Override
    public List<String> hmget(String formatKey, String[] members) {
        return jedisCluster.hmget(formatKey, members);
    }

    @Override
    public Set<String> hkeys(String formatKey) {
        return jedisCluster.hkeys(formatKey);
    }

    @Override
    public Map<String, String> hgetAll(String formatKey) {
        return jedisCluster.hgetAll(formatKey);
    }

    @Override
    public void del(String formatKey) {
        jedisCluster.del(formatKey);
    }

    @Override
    public void hdel(String formatKey, String key) {
        jedisCluster.hdel(formatKey, key);
    }

    @Override
    public boolean isClosed() {
        return this.closed;
    }

    @Override
    public void close() throws IOException {
        this.closed = true;
        this.jedisCluster.close();
    }

    /**
     * 设置过期策略
     *
     * @param key     缓存键值
     * @param jedis   redis客户端
     * @param options 选项
     */
    private void setExpiration(String key, JedisCluster jedis, CacheEntryOptions options) {
        if (options == null)
            return;

        if (options.getAbsoluteExpirationInSeconds() >= 0)
            jedis.expire(key, options.getAbsoluteExpirationInSeconds());

        if (options.getAbsoluteExpirationTime() != null) {
            val timestamps = options.getAbsoluteExpirationTime();
            // LocalDateTime to epoch seconds
            val seconds = timestamps.atZone(ZoneOffset.UTC).toEpochSecond();
            jedis.expireAt(key, seconds);
        }
    }
}
