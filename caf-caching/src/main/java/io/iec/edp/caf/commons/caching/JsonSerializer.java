/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching;

import io.iec.edp.caf.commons.core.SerializerFactory;
import io.iec.edp.caf.commons.core.enums.SerializeType;
import lombok.var;

@Deprecated
//todo 被CacheFactory依赖
public class JsonSerializer implements ICacheSerializer {
    /**
     * 单例对象
     */
    public static final JsonSerializer Current = new JsonSerializer();

    /**
     * 私有构造函数
     */
    private JsonSerializer(){

    }


    /**
     * 序列化对象
     * @param obj 类
     * @return 序列化后的字符串
     */
    public String serializeObject(Object obj){
        var serializer = SerializerFactory.getSerializer(SerializeType.Json);
        String json=serializer.serializeToString(obj);
        return json;
    }

    /**
     * 反序列化对象
     * @param jsonData json字符串
     * @param clazz 自定义对象的class对象
     * @return 反序列化的对象
     */
    public <T> T deserializeObject(String jsonData, Class<T> clazz){
        if(jsonData!=null && !"".equals(jsonData)){
            var serializer = SerializerFactory.getDeserializer(SerializeType.Json);

            return serializer.deserialize(jsonData,clazz);
        }
        return null;
    }
}
