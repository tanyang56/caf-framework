//package io.iec.edp.caf.commons.layeringcache.config;
//
//import io.iec.edp.caf.caching.manager.LayeringCacheManager;
//import io.iec.edp.caf.caching.aspect.LayeringAspect;
//import io.iec.edp.caf.caching.properties.JedisPoolConfigProperties;
//import io.iec.edp.caf.caching.properties.RedisCacheProperties;
//import io.iec.edp.caf.caching.api.CacheManager;
//import io.iec.edp.caf.caching.properties.LayeringCacheProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//
///**
// * 多级缓存自动配置类
// */
//@Configuration(proxyBeanMethods = false)
////@ConditionalOnBean(RedisTemplate.class)
////@AutoConfigureAfter({RedisAutoConfiguration.class})
//@EnableAspectJAutoProxy
//@EnableConfigurationProperties({JedisPoolConfigProperties.class, LayeringCacheProperties.class, RedisCacheProperties.class})
//@Deprecated
//public class LayeringCacheAutoConfig {
//
//    /**
//     * @param properties
//     * @return
//     */
//    @Bean
//    public CacheManager layeringCacheManager(RedisCacheProperties redisCacheSettings, JedisPoolConfigProperties jedisPoolConfigProperties, LayeringCacheProperties properties) {
//        LayeringCacheManager layeringCacheManager = new LayeringCacheManager(redisCacheSettings, jedisPoolConfigProperties);
//        // 默认开启统计功能
//        layeringCacheManager.setStats(properties.isStats());
//        return layeringCacheManager;
//    }
//
//    @Bean
//    public LayeringAspect layeringAspect() {
//        return new LayeringAspect();
//    }
//}
