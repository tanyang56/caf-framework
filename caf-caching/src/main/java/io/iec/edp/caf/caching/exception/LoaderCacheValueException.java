/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.exception;

import io.iec.edp.caf.commons.core.SerializerFactory;
import io.iec.edp.caf.commons.core.enums.SerializeType;

/**
 *  方法加载缓存值的包装异常
 */
public class LoaderCacheValueException extends RuntimeException {

    private final Object key;
    public LoaderCacheValueException(Object key, Throwable ex){

        super(String.format("加载key为 %s 的缓存数据,执行被缓存方法异常", SerializerFactory.getSerializer(SerializeType.Json).serializeToString(key)), ex);
        this.key = key;
    }

    public Object getKey() {
        return this.key;
    }
}
