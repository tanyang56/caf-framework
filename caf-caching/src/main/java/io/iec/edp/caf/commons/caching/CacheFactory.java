/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching;

import io.iec.edp.caf.commons.layeringcache.properties.CacheSetting;
import io.iec.edp.caf.commons.layeringcache.properties.RedisCacheProperties;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * factory class used to get instances of a specified Cache
 * @author wangyandong
 */
@Deprecated
//todo 被com.inspur.edp.lcm.workspace.core.config.WorkSpaceCoreConfig依赖
public class CacheFactory {

    @Autowired
    private RedisCacheProperties settings;
    @Autowired
    private JedisPoolManager manager;
    private Map<String, IDistributedCache> cacheInstances;
    private Lock lock;

    /**
     * 构造函数
     */
    public CacheFactory(){
        this.cacheInstances = new HashMap<>();
        this.lock = new ReentrantLock();
    }

    /**
     * 根据名称获取分布式缓存
     * @param cacheManagerName 配置名称
     * @return
     */
    public IDistributedCache getDistributedCache(String cacheManagerName)
    {
        if(cacheManagerName==null || "".equals(cacheManagerName)){
            throw new RuntimeException("cacheManagerName为空值");
        }


        if (!this.cacheInstances.containsKey(cacheManagerName))
        {
            lock.lock();
            try {
                if (!this.cacheInstances.containsKey(cacheManagerName))
                {
                    //读取配置
                    CacheSetting data = this.settings.getCacheConfig(cacheManagerName);

                    if(data==null) {
                        data = this.settings.getCacheConfig("default");
                    }

                    if(data ==null)
                    {
                        throw new RuntimeException("缓存配置项不存在，请检查！");
                    }
                    else
                    {
                        IDistributedCache cache = this.getDistributedCache(cacheManagerName, data);
                        this.cacheInstances.put(cacheManagerName, cache);
                    }
                }
            }
            finally {
                lock.unlock();
            }
        }
        return this.cacheInstances.get(cacheManagerName);
    }

    /**
     * 根据名称获取分布式缓存
     *
     * @param name         名称
     * @param cacheSetting 设置项
     * @return
     */
    public IDistributedCache getDistributedCache(String name, CacheSetting cacheSetting)
    {
        return new DistributedRedisCache(name, cacheSetting, manager);
    }
}
