/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 缓存配置项
 */
@ConfigurationProperties("caching-configuration.stats")
public class LayeringCacheProperties {

    /**
     * 是否开启缓存统计
     */
    private boolean stats = false;

    /**
     * 命名空间，必须唯一般使用服务名
     */
    private String namespace;

    /**
     * 启动 LayeringCacheServlet.
     */
    private boolean layeringCacheServletEnabled = false;

    /**
     * contextPath
     */
    private String urlPattern;

    /**
     * 白名单
     */
    private String allow;

    /**
     * 黑名单
     */
    private String deny;

    /**
     * 登录用户账号
     */
    private String loginUsername = "admin";

    /**
     * 登录用户密码
     */
    private String loginPassword = "admin";

    /**
     * 是否启用更新权限
     */
    private boolean enableUpdate = false;

    public boolean isLayeringCacheServletEnabled() {
        return layeringCacheServletEnabled;
    }

    public void setLayeringCacheServletEnabled(boolean layeringCacheServletEnabled) {
        this.layeringCacheServletEnabled = layeringCacheServletEnabled;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    public String getDeny() {
        return deny;
    }

    public void setDeny(String deny) {
        this.deny = deny;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public boolean isStats() {
        return stats;
    }

    public void setStats(boolean stats) {
        this.stats = stats;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public boolean isEnableUpdate() {
        return enableUpdate;
    }

    public void setEnableUpdate(boolean enableUpdate) {
        this.enableUpdate = enableUpdate;
    }
}
