/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.layeringcache.manager;

import io.iec.edp.caf.commons.layeringcache.cache.Cache;
import io.iec.edp.caf.commons.layeringcache.cache.LayeringCache;
import io.iec.edp.caf.commons.layeringcache.cache.caffeine.CaffeineCache;
import io.iec.edp.caf.commons.layeringcache.cache.redis.RedisCache;
import io.iec.edp.caf.commons.layeringcache.properties.CacheSetting;
import io.iec.edp.caf.commons.layeringcache.properties.JedisPoolConfigProperties;
import io.iec.edp.caf.commons.layeringcache.properties.RedisCacheProperties;
import io.iec.edp.caf.commons.layeringcache.setting.LayeringCacheSetting;
import io.iec.edp.caf.commons.runtime.CafEnvironment;
import org.springframework.util.Assert;

/**
 * 缓存管理器
 *
 * @author guowenchang
 * @date 2020-04-28
 */
@Deprecated
//todo 被com.inspur.edp.aif.runtime.adapter.CafServiceManager依赖
public class LayeringCacheManager extends io.iec.edp.caf.commons.layeringcache.manager.AbstractCacheManager {
    private RedisCacheProperties settings;
    private RedisClientManager redisClientManager;
    private Boolean enableRedis = null;

    public LayeringCacheManager(RedisCacheProperties redisCacheSettings, JedisPoolConfigProperties jedisPoolConfigProperties) {
        this.settings = redisCacheSettings;
        this.enableRedis = CafEnvironment.getEnvironment().getProperty("redis.enabled", Boolean.class, true);
        this.redisClientManager = new RedisClientManager(jedisPoolConfigProperties);
        cacheManagers.add(this);
    }

    @Override
    protected Cache getMissingCache(String name, LayeringCacheSetting layeringCacheSetting) {
        layeringCacheSetting.setUseSecondCache(layeringCacheSetting.isUseSecondCache() && enableRedis);
        layeringCacheSetting.setUseFirstCache(layeringCacheSetting.isUseFirstCache() || (!enableRedis));
        Assert.isTrue(layeringCacheSetting.isUseFirstCache() || layeringCacheSetting.isUseSecondCache(), "Please insure at least one level cache is enabled");

        // 创建一级缓存
        CaffeineCache caffeineCache = layeringCacheSetting.isUseFirstCache() ? new CaffeineCache(name, layeringCacheSetting.getFirstCacheSetting(), getStats()) : null;

        if (layeringCacheSetting.isUseSecondCache()) {
            // 创建二级缓存
            RedisClient redisClient = getOrDefaultRedisClient(name);
            RedisCache redisCache = new RedisCache(name, redisClient.redisTemplate, layeringCacheSetting.getSecondaryCacheSetting(), getStats());
            this.redisClientPool.put(name, redisClient);
            return new LayeringCache(redisClient.redisTemplate, caffeineCache, redisCache, super.getStats(), layeringCacheSetting);
        } else {
            return new LayeringCache(null, caffeineCache, null, layeringCacheSetting.isUseFirstCache(),
                    layeringCacheSetting.isUseSecondCache(), super.getStats(), name, layeringCacheSetting);
        }
    }

    @Override
    protected boolean useFirstAndSecond(LayeringCacheSetting layeringCacheSetting) {
        return layeringCacheSetting.isUseFirstCache() && layeringCacheSetting.isUseSecondCache();
    }

    private RedisClient getOrDefaultRedisClient(String name) {
        CacheSetting cacheSetting = this.settings.getCacheConfig(name);
        if (cacheSetting == null) {
            cacheSetting = this.settings.getCacheConfig("default");
        }

        if (cacheSetting == null) {
            throw new RuntimeException("缓存配置项不存在，请检查！");
        }

        return this.redisClientManager.getRedisClient(cacheSetting, this);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
