/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.caching.listener;

import io.iec.edp.caf.caching.enums.RedisPubSubMessageType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * redis pub/sub 消息
 */
public class RedisPubSubMessage implements Serializable {
    /**
     * 缓存名称
     */
    private String cacheName;

    /**
     * 缓存key
     */
    private Collection<Object> keys = new ArrayList<>();

    /**
     * 消息类型
     */
    private RedisPubSubMessageType messageType;

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public Collection<Object> getKeys() {
        return this.keys;
    }

    public void addKey(Object key) {
        this.keys.add(key);
    }

    public void addKeys(Collection<Object> keys) {
        this.keys.addAll(keys);
    }

    public RedisPubSubMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(RedisPubSubMessageType messageType) {
        this.messageType = messageType;
    }
}