/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.caching.proxy;

import io.iec.edp.caf.commons.caching.CacheEntryOptions;
import lombok.val;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Jedis单体应用代理
 *
 * @author guowenchang
 */
@Deprecated
//todo 被CacheFactory依赖
public class JedisPoolProxy implements JedisProxy {
    private JedisPool jedisPool;

    public JedisPoolProxy(JedisPoolConfig jedisPoolConfig, String host, int port, int timeout, String password, int database) {
        this.jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password, database);
    }

    @Override
    public String get(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get(key);
        }
    }

    @Override
    public void set(String formatKey, String value, CacheEntryOptions options) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(formatKey, value);
            setExpiration(formatKey, jedis, options);
        }
    }

    @Override
    public void hset(String formatKey, String key, String data, CacheEntryOptions options) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hset(formatKey, key, data);
            setExpiration(formatKey, jedis, options);
        }
    }

    @Override
    public List<String> hmget(String formatKey, String[] members) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hmget(formatKey, members);
        }
    }

    @Override
    public Set<String> hkeys(String formatKey) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hkeys(formatKey);
        }
    }

    @Override
    public Map<String, String> hgetAll(String formatKey) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hgetAll(formatKey);
        }
    }

    @Override
    public void del(String formatKey) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.del(formatKey);
        }
    }

    @Override
    public void hdel(String formatKey, String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hdel(formatKey, key);
        }
    }

    @Override
    public void close() throws IOException {
        this.jedisPool.close();
    }

    @Override
    public boolean isClosed() {
        return this.jedisPool.isClosed();
    }

    /**
     * 设置过期策略
     *
     * @param key     缓存键值
     * @param jedis   redis客户端
     * @param options 选项
     */
    private void setExpiration(String key, Jedis jedis, CacheEntryOptions options) {
        if (options == null)
            return;

        if (options.getAbsoluteExpirationInSeconds() >= 0)
            jedis.expire(key, options.getAbsoluteExpirationInSeconds());

        if (options.getAbsoluteExpirationTime() != null) {
            val timestamps = options.getAbsoluteExpirationTime();
            // LocalDateTime to epoch seconds
            val seconds = timestamps.atZone(ZoneOffset.UTC).toEpochSecond();
            jedis.expireAt(key, seconds);
        }
    }
}
