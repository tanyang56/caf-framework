/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.middleware.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is {@link CAFBootServerConfigurationProperties}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@Data
@Validated
@ConfigurationProperties("caf-boot.server")
public class CAFBootServerConfigurationProperties {

	private boolean allowBeanInitializationTimeTracking = false;

	@Valid
	private List<MimeMappingItemConfigurationProperties> mimeMapping = new ArrayList<>();

	@Valid
	@NestedConfigurationProperty
	private CAFServerStaticResourcesConfigurationProperties staticResources =
			new CAFServerStaticResourcesConfigurationProperties();

	@Valid
	private String homePage = "/index.html";

	@Data
	@SuppressWarnings("WeakerAccess")
	public static class MimeMappingItemConfigurationProperties {
		@NotNull
		private String extension;
		@NotNull
		private String mimeType;
	}

	@Data
	@SuppressWarnings("WeakerAccess")
	public static class CAFServerStaticResourcesConfigurationProperties {
		private List<String> paths = Collections.singletonList("/**");

		private Boolean enableCache = true;
		private Integer periodOfCacheInSeconds = 36000 * 24;

		private List<URL> externalStaticResources = new ArrayList<>();
	}
}
