/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.middleware.tongweb;

import com.tongweb.springboot.starter.TongWebServletWebServerFactory;
import io.iec.edp.caf.middleware.entity.CAFBootServerConfigurationProperties;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;

import java.util.List;

public class TongWebMimeMapping implements WebServerFactoryCustomizer<TongWebServletWebServerFactory> {

    private final List<CAFBootServerConfigurationProperties.MimeMappingItemConfigurationProperties> mimeMapping;


    public TongWebMimeMapping(List<CAFBootServerConfigurationProperties.MimeMappingItemConfigurationProperties> mimeMapping) {
        this.mimeMapping = mimeMapping;
    }


    @Override
    public void customize(TongWebServletWebServerFactory factory) {
        MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
        this.mimeMapping.forEach(mm -> mappings.add(mm.getExtension(), mm.getMimeType()));
        factory.setMimeMappings(mappings);
    }
}

