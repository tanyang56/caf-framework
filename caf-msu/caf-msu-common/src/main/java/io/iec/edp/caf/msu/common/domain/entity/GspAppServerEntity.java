/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.Date;

/**
 * 应用（APP）实体
 *
 * @author Leon Huo
 */
@Data
@Entity
@Table(name = "gspappserverinstance")
public class GspAppServerEntity {

    //实例标识
    @Id
    private String appName;

    //实例地址
    private String appUrl;

    //实例ip
    private String ip;

    //实例port
    private String port;

    //实例是否健康（暂不使用）
    @Deprecated
    private Boolean healthy;

    //实例心跳时间
    private Date beatTime;

    private OffsetDateTime createdTime;
    private String creator;
    private OffsetDateTime lastModifiedTime;
    private String lastModifier;

}
