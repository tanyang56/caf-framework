/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;

/**
 * 微服务（MSU）实体
 *
 * @author Leon Huo
 */
@Data
@Entity
@Table(name = "gspmsuinstance")
public class GspSuEntity {

    @Id
    private String id;

    //su编号（小写）
    private String su;

    //应用实例名称
    private String app;

    private OffsetDateTime createdTime;
    private String creator;
    private OffsetDateTime lastModifiedTime;
    private String lastModifier;

}
