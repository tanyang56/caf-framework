/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common.domain.repository;

import io.iec.edp.caf.msu.common.domain.entity.GspAppServerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * @author Leon Huo
 */
public interface AppServerRepository extends JpaRepository<GspAppServerEntity, String> {

    @Query(value = "select app.* from gspappserverinstance app left join gspmsuinstance su on su.app=app.appname where su.su= ?1", nativeQuery = true)
    List<GspAppServerEntity> findBySuName(String suName);

    List<GspAppServerEntity> findByAppName(String appServerName);

    void deleteByAppName(String appName);

    //获取数据库时间
    @Query(value = "select distinct CURRENT_TIMESTAMP from GspAppServerEntity", nativeQuery = false)
    Date getCurrentTimestamp();

}
