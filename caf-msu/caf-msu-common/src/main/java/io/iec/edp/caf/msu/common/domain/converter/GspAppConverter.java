/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common.domain.converter;

import io.iec.edp.caf.msu.api.entity.GspAppServerInstance;
import io.iec.edp.caf.msu.common.domain.entity.GspAppServerEntity;
import lombok.var;

/**
 * @author Leon Huo
 */
public class GspAppConverter {

    public static GspAppServerEntity convertToEntity(GspAppServerInstance instance) {
        var entity = new GspAppServerEntity();
        entity.setAppName(instance.getAppName());
        entity.setAppUrl(instance.getAppUrl());
        entity.setIp(instance.getIp());
        entity.setPort(instance.getPort());
        entity.setHealthy(instance.getHealthy());
        entity.setBeatTime(instance.getBeatTime());
        entity.setCreator(instance.getCreator());
        entity.setCreatedTime(instance.getCreatedTime());
        entity.setLastModifiedTime(instance.getLastModifiedTime());
        entity.setLastModifier(instance.getLastModifier());
        return entity;
    }

    public static GspAppServerInstance convertToInstance(GspAppServerEntity entity) {
        var instance = new GspAppServerInstance();
        instance.setAppName(entity.getAppName());
        instance.setAppUrl(entity.getAppUrl());
        instance.setIp(entity.getIp());
        instance.setPort(entity.getPort());
        instance.setHealthy(entity.getHealthy());
        instance.setBeatTime(entity.getBeatTime());
        instance.setCreator(entity.getCreator());
        instance.setCreatedTime(entity.getCreatedTime());
        instance.setLastModifiedTime(entity.getLastModifiedTime());
        instance.setLastModifier(entity.getLastModifier());
        return instance;
    }
}
