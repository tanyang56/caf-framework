/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common;

import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.runtime.CafEnvironment;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.msu.api.ServiceUnitAwareService;
import io.iec.edp.caf.msu.api.entity.CafMsuConfiguration;
import io.iec.edp.caf.msu.api.entity.DeploymentConfiguration;
import io.iec.edp.caf.msu.api.entity.ServiceUnitItem;

import io.iec.edp.caf.sumgr.api.IServiceUnitAware;
import io.iec.edp.caf.sumgr.api.entity.MsuType;
import io.iec.edp.caf.sumgr.api.entity.ServiceUnitInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Leon Huo
 * @Date: 2021/5/12
 */
@Slf4j
@Deprecated
//todo 被com.inspur.edp.dataie.rpc.client.DataIeRestServiceImpl依赖
public class ServiceUnitAwareImpl implements IServiceUnitAware {

    private Lock locker = new ReentrantLock();
    private Boolean initialized = false;
    private List<ServiceUnitInfo> infos;
    private List<String> serviceUnitNames;

    private static final ServiceUnitAwareService suService = SpringBeanUtils.getBean(ServiceUnitAwareService.class);


    /**
     * 当前Server中包含的所有的服务单元信息
     * @return
     */
    @Override
    public List<ServiceUnitInfo> getAllServiceUnits() {
//        this.ensureInitialized();
//        return this.infos;
        List<io.iec.edp.caf.msu.api.entity.ServiceUnitInfo> suInfos = suService.getAllServiceUnits();
        List<ServiceUnitInfo> result = new ArrayList<>();
        suInfos.forEach(info -> {
            result.add(new ServiceUnitInfo(info));
        });
        return result;
    }

    /**
     * 当前已启用的服务单元名称列表
     *
     * @return
     */
    @Override
    public List<String> getEnabledServiceUnits() {
//        this.ensureInitialized();
//        return this.serviceUnitNames;
        return suService.getEnabledServiceUnits();
    }


}
