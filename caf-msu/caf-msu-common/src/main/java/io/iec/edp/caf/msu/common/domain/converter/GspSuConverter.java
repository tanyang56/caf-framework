/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.common.domain.converter;

import io.iec.edp.caf.msu.api.entity.GspSuInstance;
import io.iec.edp.caf.msu.common.domain.entity.GspSuEntity;
import lombok.var;

/**
 * @author Leon Huo
 */
public class GspSuConverter {

    public static GspSuInstance convertToInstance(GspSuEntity entity) {
        var instance = new GspSuInstance();
        instance.setSu(entity.getSu());
        instance.setApp(entity.getApp());
        instance.setCreator(entity.getCreator());
        instance.setCreatedTime(entity.getCreatedTime());
        instance.setLastModifier(entity.getLastModifier());
        instance.setLastModifiedTime(entity.getLastModifiedTime());
        return instance;
    }

    public static GspSuEntity convertToEntity(GspSuInstance instance) {
        var entity = new GspSuEntity();
        entity.setSu(instance.getSu());
        entity.setApp(instance.getApp());
        entity.setCreator(instance.getCreator());
        entity.setCreatedTime(instance.getCreatedTime());
        entity.setLastModifier(instance.getLastModifier());
        entity.setLastModifiedTime(instance.getLastModifiedTime());
        return entity;
    }
}
