/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.server.config;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.naming.NamingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

//种类不能放到starter里，因为msu-server是个独立的单元，在拆分su的情况下，所有su只保留一个该jar包
@Configuration(proxyBeanMethods = false)
@EntityScan({"io.iec.edp.caf.msu.server.storage.database.domain.entity"})
@Slf4j
public class ServiceRegistryAutoConfiguration {

    @NacosInjected
    private NamingService namingService;

    public ServiceRegistryAutoConfiguration() {
    }

//    @ConditionalOnExpression("${msu.enable:false} and '${msu.deployMode}'=='OnPremise'")
//    @Bean
//    public HealthCheckSchedule healthCheckSchedule() {
//        return new HealthCheckSchedule(namingService);
//    }

}
