///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.msu.client;
//
//import com.alibaba.nacos.api.annotation.NacosInjected;
//import com.alibaba.nacos.api.exception.NacosException;
//import com.alibaba.nacos.api.naming.NamingService;
//import lombok.var;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.ArrayList;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Config.class)
//public class NacosTest {
//    @NacosInjected
//    private NamingService namingService;
//
//    @Test
//    public void testNacos() throws NacosException {
////        this.namingService.registerInstance("service1","192.168.1.1",5100);
////        this.namingService.registerInstance("service1","192.168.1.2",5200);
////        this.namingService.registerInstance("service1","192.168.1.3",5300);
////
////        this.namingService.selectOneHealthyInstance("service1");
//
//        this.namingService.registerInstance("service1","192.168.1.1",5100,"cluster1");
//        this.namingService.registerInstance("service1","192.168.1.4",5400,"cluster1");
//        this.namingService.registerInstance("service1","192.168.1.2",5200,"cluster2");
//        this.namingService.registerInstance("service1","192.168.1.3",5300,"cluster3");
//        var clusters = new ArrayList<String>();
//        clusters.add("cluster1");
//        this.namingService.selectOneHealthyInstance("service1",clusters);
//
//    }
//
//}
