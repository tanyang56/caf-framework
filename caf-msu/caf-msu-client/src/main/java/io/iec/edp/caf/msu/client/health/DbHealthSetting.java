/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.client.health;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * DB心跳检测的周期
 *
 * @author manwenxing01
 */
@ConfigurationProperties(prefix = "msu.dbhealth")
@Data
public class DbHealthSetting {

    //心跳周期（单位：秒）
    private int beatPeriod = 30;

    //下线周期（单位：秒）
    private int removePeriod = 2 * beatPeriod;

}
