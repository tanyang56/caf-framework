/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.client.discovery;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.msu.api.MsuService;
import io.iec.edp.caf.msu.api.client.ServiceDiscovery;
import io.iec.edp.caf.msu.api.entity.GspAppServerInstance;
import io.iec.edp.caf.msu.api.entity.ServiceUnitInfo;
import io.iec.edp.caf.msu.client.exception.ServiceUnitNotFoundException;
import io.iec.edp.caf.msu.common.domain.entity.GspSuEntity;
import io.iec.edp.caf.msu.common.domain.repository.SuRepository;
import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 服务中心-服务发现（DB版）
 * 返回一个健康的实例
 *
 * @date 2023-01-30
 */
@Slf4j
public class DbDiscoveryImpl implements ServiceDiscovery {

    private SuRepository suRepo;

    public DbDiscoveryImpl(SuRepository suRepo) {
        this.suRepo = suRepo;
    }

    @Override
    public String discover(String serviceUnitName, HashMap<String, String> eventContext) {
        //根据su查找所有app实例
        List<GspAppServerInstance> apps = findAppsBySu(serviceUnitName);

        //随机获取实例
        //apps = apps.stream().filter(GspAppServerInstance::getHealthy).collect(Collectors.toList());   过滤健康实例
        GspAppServerInstance instance = selectService(apps);
        String url = instance.getAppUrl();

        if (url == null || url.length() == 0) {
            log.error("ServiceCenter(DataBase) Can't Find Instance by su {}", serviceUnitName);
            throw new ServiceUnitNotFoundException("ServiceCenter(DataBase) Current su " + serviceUnitName + " is not available");
        }

        return url;
    }

    @Override
    public List<String> discoverAll(String serviceUnitName, HashMap<String, String> eventContext) {
        //根据su查找所有app实例
        List<GspAppServerInstance> apps = findAppsBySu(serviceUnitName);

        //所有实例的url
        List<String> urls = new ArrayList<>();
        apps.forEach(x -> {
            if (x.getAppUrl() != null && x.getAppUrl().length() > 0)
                urls.add(x.getAppUrl());
        });
        return urls;
    }

    /**
     * 访问主库,获取GSPSuInstance表的所有数据并去重
     */
    @Override
    public List<ServiceUnitInfo> getEnabledServiceUnitInfo() {
        List<ServiceUnitInfo> infos = null;

        try {
            //设置主库
            MultiTenantContextInfo contextInfo = new MultiTenantContextInfo();
            contextInfo.setMasterDb(true);
            MultiTenantContextHolder.set(contextInfo);

            //读取GSPSuInstance
            List<GspSuEntity> allSuEntity = this.suRepo.findAll();
            if (allSuEntity.size() > 0) {
                infos = new ArrayList<>();
                for (GspSuEntity entity : allSuEntity) {
                    //去重加入
                    if (infos.stream().noneMatch(x -> x.getName().equals(entity.getSu()))) {
                        ServiceUnitInfo info = new ServiceUnitInfo();
                        info.setName(entity.getSu());
                        info.setServiceUnitDes(entity.getSu());
                        infos.add(info);
                    }
                }
            }
        } finally {
            MultiTenantContextHolder.set(null);
        }
        return infos;
    }

    /**
     * 根据su获取所有实例
     *
     * @param su 服务单元编号
     * @return 所有实例
     */
    private List<GspAppServerInstance> findAppsBySu(String su) {
        MsuService msuService = SpringBeanUtils.getBean(MsuService.class);
        List<GspAppServerInstance> apps = msuService.getGspAppServerInfoBySu(su);
        //实例数量为0
        if (apps.size() == 0) {
            log.error("ServiceCenter(DataBase) Can't Find App by su {}", su);
            throw new ServiceUnitNotFoundException("ServiceCenter(DataBase) Current su " + su + " is not available");
        }
        return apps;
    }

    /**
     * 随机选择app实例
     *
     * @param apps app实例列表
     * @return 随机app实例
     */
    private GspAppServerInstance selectService(List<GspAppServerInstance> apps) {
        if (apps == null || apps.size() == 0)
            return null;
        int index = (int) (Math.random() * apps.size());

        return apps.get(index);
    }

}
