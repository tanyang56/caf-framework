/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.api;

import io.iec.edp.caf.msu.api.entity.ServiceUnitInfo;
import io.iec.edp.caf.msu.api.entity.ServiceUnitRegisterInfo;

import java.util.List;

/**
 * 服务单元感知服务
 *
 * @author Leon Huo
 */
public interface ServiceUnitAwareService {

    /**
     * 当前实例包含的所有的服务单元信息(包含启用和未启用)
     *
     * @return 所有的服务单元信息
     */
    List<ServiceUnitInfo> getAllServiceUnits();

    /**
     * 当前实例已启用的服务单元名称列表
     *
     * @return 已启用服务单元名称列表
     */
    List<String> getEnabledServiceUnits();

    /**
     * 当前已启用的服务单元信息列表
     *
     * @return 启用的服务单元信息
     */
    List<ServiceUnitInfo> getEnabledServiceUnitsInfo();

    /**
     * 服务注册中心：获取su注册信息
     *
     * @param appName
     * @param serviceName
     * @return 注册信息
     */
    ServiceUnitRegisterInfo getServiceUnitRegisterInfo(String appName, String serviceName);

    /**
     * 运行时服务注册
     * @param suInfos
     * @return
     */
    boolean registerServiceUnitsInfo(List<ServiceUnitInfo> suInfos);
}
