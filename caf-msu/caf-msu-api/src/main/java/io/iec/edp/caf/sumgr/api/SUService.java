/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.sumgr.api;

import io.iec.edp.caf.msu.api.MsuService;
import io.iec.edp.caf.msu.api.entity.GspAppServerInstance;
import io.iec.edp.caf.msu.api.entity.GspSuInstance;

import java.util.List;

@Deprecated
//todo 被io.iec.edp.caf.datapermission.runtime.core.config.DataPermissionRuntimeAutoConfig依赖
public interface SUService {
    /**
     * 获取一个AppServer的部署模式
     * @param appServerName appServer名称（注意这里不是指关键应用的app）
     * @return
     */
    //DeployMode getDeployMode(String appServerName);

    /**
     * 获取一个AppServer的IP
     * @param appServerName appServer名称（注意这里不是指关键应用的app）
     * @return
     */
    String getIpAddress(String appServerName);

    /**
     * 获取一个appServer的port
     * @param appServerName
     * @return
     */
    String getPort(String appServerName);

    /**
     * 判断一个Su的服务是否远程调用
     * @param serviceUnitName su的名称
     * @return
     */
    boolean isLocalInvoke(String serviceUnitName);

    /**
     * 获取远程appServerName的baseUrl
     * @param appServerName
     *
     * @return
     */
    String getRemoteBaseUrl(String appServerName);

    /**
     * 获取一个AppServer的信息
     * @param appServerName appServer名称
     * @return
     */
    List<GspAppServerInstance> getGspAppServerInfo(String appServerName);

    /**
     * 根据Su名称获取appServer信息
     * @param suName su名称
     * @return
     */
    List<GspAppServerInstance> getGspAppServerInfoBySu(String suName);

    /**
     * 获取一个Su的信息
     * @param suName su名称
     * @return
     */
    GspSuInstance getGspSuInfo(String suName);

    /**
     * 获取一个AppServer下包含的所有的Su
     * @param appServerName appserver名称
     * @return
     */
    List<GspSuInstance> getGspSuByAppServer(String appServerName);

//    Boolean isSameDbConInfo(String suName,int tenantId);
}
