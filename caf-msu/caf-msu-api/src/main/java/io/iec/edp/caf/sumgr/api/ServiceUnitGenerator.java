/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.sumgr.api;

import io.iec.edp.caf.commons.runtime.msu.entities.ServiceUnitYaml;
import io.iec.edp.caf.sumgr.api.entity.ServiceUnitInfo;

import java.io.File;

@Deprecated
public interface ServiceUnitGenerator {
    /**
     * Generate ServiceUnit.json.
     * @param destDir   The directory for the root of the package hierarchy
     *                          for generated files.
     */
    public Boolean exists(File destDir);

    /**
     * Generate ServiceUnit.json.
     * @param destDir   The directory for the root of the package hierarchy
     *                          for generated files. Can not be null.
     */
    public void generate(ServiceUnitInfo info, File destDir);

    /**
     * Generate service-unit.yaml.
     * @param msuCommonInfo msu information
     * @param destDir The directory for the root of the package hierarchy
     *                         for generated files. Can not be null.
     */
    public void generate(ServiceUnitYaml msuCommonInfo, File destDir);
}
