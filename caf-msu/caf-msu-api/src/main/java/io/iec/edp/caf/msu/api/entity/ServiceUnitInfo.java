/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.iec.edp.caf.commons.runtime.msu.entities.MsuCommonInfo;
import io.iec.edp.caf.msu.api.enums.MsuType;
import lombok.Data;

/**
 * @author Leon Huo
 */

@Data
public class ServiceUnitInfo extends MsuCommonInfo {

    private MsuType msuType;

    public ServiceUnitInfo(){

    }

    public ServiceUnitInfo(MsuCommonInfo commonInfo) {
        super(commonInfo);
        if ("Common".equalsIgnoreCase(commonInfo.getType()))
            this.msuType = MsuType.Common;
        else
            this.msuType = MsuType.Biz;
    }
}
