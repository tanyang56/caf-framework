/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.sumgr.api;

import io.iec.edp.caf.sumgr.api.entity.ServiceUnitInfo;

import java.util.List;

@Deprecated
//todo 被[io.iec.edp.caf.distribute.lockmanagement.config.LockManagementConfig依赖
public interface IServiceUnitAware  {
    /**
     * 当前Server中包含的所有的服务单元信息
     * @return
     */
    List<ServiceUnitInfo> getAllServiceUnits();

    /**
     * 当前已启用的服务单元名称列表
     * @return
     */
    List<String> getEnabledServiceUnits();
}
