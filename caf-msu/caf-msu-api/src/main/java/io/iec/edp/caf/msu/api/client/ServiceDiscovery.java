/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.msu.api.client;

import io.iec.edp.caf.msu.api.entity.ServiceUnitInfo;

import java.util.HashMap;
import java.util.List;

/**
 * 服务发现
 *
 * @author Leon Huo
 */
//铁塔已扩展此接口，不能轻易更改discover方法的调用。如果更改方法，需要通知其同步处理
public interface ServiceDiscovery {

    String discover(String serviceUnitName, HashMap<String, String> eventContext);

    List<String> discoverAll(String serviceUnitName, HashMap<String, String> eventContext);

    //返回系统启用的所有Su
    default List<ServiceUnitInfo> getEnabledServiceUnitInfo() {
        return null;
    }

}
