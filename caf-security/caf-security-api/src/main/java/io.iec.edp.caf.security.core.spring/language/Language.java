package io.iec.edp.caf.security.core.spring.language;

import lombok.Builder;
import lombok.Data;

import java.util.List;


/**
 * This is {@link Language}.
 *
 * @author zhangtianyi
 * @since 1.0.0
 */
@Data
@Builder
public class Language {

    private String urlLanguage;

    private String userDetailsLanguage;

    private List<String> headerLanguage;
}
