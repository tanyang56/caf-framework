/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rest.server.event;

import io.iec.edp.caf.commons.event.StartupCompletedEvent;
import io.iec.edp.caf.rest.RESTServiceRegistrar;
import io.iec.edp.caf.rest.server.RESTEndpointUtil;
import io.iec.edp.caf.rest.RESTEndpoint;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.Bus;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangyandong
 */
@Order(value = Ordered.HIGHEST_PRECEDENCE + 1)
@Slf4j
public class RestStartupCompletedListener implements ApplicationListener<StartupCompletedEvent> {

    Bus bus;

    public RestStartupCompletedListener(Bus bus) {
        this.bus = bus;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(StartupCompletedEvent myEvent) {
        RESTServiceRegistrar restServiceRegistrar = RESTServiceRegistrar.getSingleton();
        restServiceRegistrar.finishRegistry();
        List<RESTEndpoint> restEndpoints = new ArrayList<>(restServiceRegistrar.getEndpoints());
        for (RESTEndpoint endpoint : restEndpoints) {
            RESTEndpointUtil.createJAXRSServer(endpoint.getPath(), bus, endpoint.getServices());
        }
    }
}
