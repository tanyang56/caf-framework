/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rest;

import io.iec.edp.caf.multicontext.annotation.Collect;
import lombok.Builder;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This is {@link RESTEndpoint}.
 *
 * @author yisiqi
 * @since 1.0.0
 */

@Getter
@Collect
public class RESTEndpoint {

	private String path;
	private List<Object> services;

	private String applicationName;
	private String serviceUnitName;
	private String restServiceName;

	@Builder
	public RESTEndpoint(String path, Object ... services) {
		this.path = path;
		this.services = Collections.unmodifiableList(Arrays.asList(services));
	}

	@Builder
	public RESTEndpoint(String path, Collection<Object> services) {
		this(path, services.toArray());
	}

	/**
	 * 服务单元和path并非完全对应的场景使用
	 * @param applicationName
	 * @param serviceUnitName
	 * @param restServiceName
	 * @param path
	 * @param services
	 */
	@Builder
	public RESTEndpoint(String applicationName, String serviceUnitName, String restServiceName, String path,
						Object ... services){
		this(path, services);
		this.applicationName = applicationName;
		this.serviceUnitName = serviceUnitName;
		this.restServiceName = restServiceName;
	}

	/**
	 * 服务单元和path并非完全对应的场景使用
	 * @param applicationName
	 * @param serviceUnitName
	 * @param restServiceName
	 * @param path
	 * @param services
	 */
	@Builder
	public RESTEndpoint(String applicationName, String serviceUnitName, String restServiceName, String path,
						Collection<Object> services){
		this(applicationName, serviceUnitName, restServiceName, path, services.toArray());
	}

	/**
	 * 路径和应用服务单元完全一致的场景使用
	 * @param applicationName
	 * @param serviceUnitName
	 * @param restServiceName
	 * @param version
	 * @param services
	 */
	@Builder
	public RESTEndpoint(String applicationName, String serviceUnitName, String restServiceName, int version,
						Object ...  services){

		this(applicationName, serviceUnitName, restServiceName, new StringBuffer("/").append(applicationName).append("/").append(serviceUnitName).append(
				"/v").append(version).toString(), services);
		this.path = new StringBuffer("/").append(applicationName).append("/").append(serviceUnitName).append(
				"/v").append(version).toString();
	}

	/**
	 * 路径和应用服务单元完全一致的场景使用
	 * @param applicationName
	 * @param serviceUnitName
	 * @param restServiceName
	 * @param version
	 * @param services
	 */
	@Builder
	public RESTEndpoint(String applicationName, String serviceUnitName, String restServiceName, int version,
						Collection<Object> services){

		this(applicationName, serviceUnitName, restServiceName, version, services.toArray());
	}

}
