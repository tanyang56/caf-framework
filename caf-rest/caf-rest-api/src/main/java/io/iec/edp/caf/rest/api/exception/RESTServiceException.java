//package io.iec.edp.caf.rest.api.exception;
//
//import java.io.IOException;
//import java.utils.ArrayList;
//import java.utils.Collection;
//import java.utils.List;
//
///**
// * This is {@link RESTServiceException}.
// *
// * @author Yi Siqi
// * @since 0.0.1
// */
//public abstract class RESTServiceException extends IOException {
//
//	private final int code;
//	private final String error;
//	private final ErrorDescription desc;
//	private final List<String> params = new ArrayList<String>();
//
//	protected RESTServiceException(int category, String error, ErrorDescription desc) {
//		this.code = category + desc.getSerialNumber();
//		this.error = error;
//		this.desc = desc;
//	}
//
//	protected RESTServiceException(int category, String error, ErrorDescription desc, String param) {
//		this(category, error, desc);
//		this.params.add(param);
//	}
//
//	protected RESTServiceException(int category, String error, ErrorDescription desc, Collection<String> params) {
//		this(category, error, desc);
//		this.params.addAll(params);
//	}
//
//
//	private String jsonString;
//	public String toJsonString() {
//		if (jsonString == null || jsonString.trim().length() == 0) {
//			String paramsTpl = ",\"params\": [%s]";
//			StringBuilder paramsStr = new StringBuilder();
//			if (params.size() == 0) {
//				paramsStr = new StringBuilder();
//			} else if (params.size() == 1) {
//				paramsStr = new StringBuilder(String.format(paramsTpl, "\"" + params.get(0) + "\""));
//			} else {
//				paramsStr = new StringBuilder("\"" + params.get(0) + "\"");
//				for (int i = 1; i < params.size(); ++i) {
//					paramsStr.append(String.format(",\"%s\"", params.get(i)));
//				}
//				paramsStr = new StringBuilder(String.format(paramsTpl, paramsStr.toString()));
//			}
//			jsonString = String.format("{\"code\": %d,\"error\": \"%s\",\"error_description\": \"%s\"%s}",
//				code, error, desc.getMessage(), paramsStr.toString());
//		}
//		return jsonString;
//	}
//
//	private String messageString;
//	public String toMessageString() {
//		if (messageString == null || messageString.trim().length() == 0) {
//			messageString = String.format("Error: #%d %s. %s", code, error, desc.getMessage());
//		}
//		return messageString;
//	}
//
//
//}
