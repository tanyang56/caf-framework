/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.rest.api.exception;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.HashMap;

/**
 * @author ：LiuJianhua
 * @description :
 */
@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE)
public class WebApiException {
    private String Code;
    private int Level;
    private String Message;
    private String innerMessage;
    private OffsetDateTime date;
    private String Detail;
    private String RequestId;
    private HashMap<String, String> extensionMessage;
}
