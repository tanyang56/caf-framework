//package io.iec.edp.caf.rest.client.tests;
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//
//@SpringBootApplication
//@Configuration
//@EnableAutoConfiguration
//@ComponentScan({"io.iec.edp.caf"})
//@EntityScan({"io.iec.edp.caf"})
//public class Config {
//}
