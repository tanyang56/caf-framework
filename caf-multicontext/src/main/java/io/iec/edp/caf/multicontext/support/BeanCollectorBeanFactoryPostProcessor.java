/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.support;

import io.iec.edp.caf.multicontext.annotation.BeanCollector;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.Ordered;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class BeanCollectorBeanFactoryPostProcessor implements BeanFactoryPostProcessor, Ordered {

    private static List<String> beanCollectorNames = Collections.synchronizedList(new ArrayList<>());
    private BeanFactory beanFactory;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
        for (String beanName : beanFactory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
            if (beanDefinition instanceof AnnotatedBeanDefinition) {
                postProcess(beanFactory, beanName, (AnnotatedBeanDefinition) beanDefinition);
            }
        }
    }

    private void postProcess(ConfigurableListableBeanFactory beanFactory,
                             String beanName,
                             AnnotatedBeanDefinition beanDefinition) {

        //如果这个bean是个bean收集器，那么那就需要延迟加载，等所有的bean加载完了后，在加载自身去收集其他的bean
        if (checkMetadata(beanDefinition)) {
            beanDefinition.setLazyInit(true);
            beanCollectorNames.add(beanName);
        }
    }

    private boolean checkMetadata(AnnotatedBeanDefinition beanDefinition) {
        if (beanDefinition.getMetadata().hasAnnotation(BeanCollector.class.getName())) {
            return true;
        }

        if (beanDefinition.getFactoryMethodMetadata() instanceof AnnotatedTypeMetadata) {
            return beanDefinition.getFactoryMethodMetadata().getAnnotations().isPresent(BeanCollector.class.getName());
        }

        return false;
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public void instantiationBeanCollectors() {
        Assert.notNull(beanFactory, "beanFactory has not been injected");
        for (String beanName : this.beanCollectorNames) {
            beanFactory.getBean(beanName);
        }
    }
}
