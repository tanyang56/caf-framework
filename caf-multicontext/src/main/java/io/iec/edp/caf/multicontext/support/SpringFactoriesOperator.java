/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.support;

import org.springframework.core.io.support.SpringFactoriesLoader;

import java.lang.reflect.Field;
import java.util.Map;

public class SpringFactoriesOperator {

    public static void clearSpringFactoriesCache() {
        try {
            Field field = SpringFactoriesLoader.class.getDeclaredField("cache");
            field.setAccessible(true);
            Map map = (Map) field.get(null);
            map.clear();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
