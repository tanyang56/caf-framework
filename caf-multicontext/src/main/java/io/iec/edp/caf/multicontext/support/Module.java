/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.support;

import io.iec.edp.caf.multicontext.context.ModuleApplicationContext;
import io.iec.edp.caf.multicontext.factory.ModuleBeanFactory;

import java.util.List;

/**
 * 子模块对象 todo
 */
public class Module {

    private final String name;

    private final ClassLoader classLoader;

    private final List<String> paths;

    private final ModuleApplicationContext context;

    private final ModuleBeanFactory beanFactory;

    //todo  private int level 优先级

    public Module(String name, ClassLoader classLoader, List<String> path, ModuleApplicationContext context, ModuleBeanFactory beanFactory) {
        this.name = name;
        this.classLoader = classLoader;
        this.paths = path;
        this.context = context;
        this.beanFactory = beanFactory;
    }

    public String getName() {
        return name;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public List<String> getPath() {
        return paths;
    }

    public ModuleApplicationContext getContext() {
        return context;
    }

    public ModuleBeanFactory getBeanFactory() {
        return beanFactory;
    }
}
