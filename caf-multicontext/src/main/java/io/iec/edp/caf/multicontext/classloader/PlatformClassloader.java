/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.classloader;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import io.iec.edp.caf.commons.runtime.msu.ServiceUnitConfigService;
import io.iec.edp.caf.multicontext.support.JarUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.loader.LaunchedURLClassLoader;
import sun.misc.URLClassPath;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 平台类加载器
 * <p>
 * 此类暂时无扩展
 *
 * @author guowenchang
 */
@Slf4j
public class PlatformClassloader extends LaunchedURLClassLoader {

    /*
        需要将此类加载器注册为并行类加载器 否则在非双亲委派模型下可能会造成死锁
    */
    static {
        ClassLoader.registerAsParallelCapable();
    }

    private LaunchedURLClassLoader resourceLoader;

    private LaunchedURLClassLoader internalClassloader;

    //存各个su的path
    private final Map<String, URLClassPath> appClasspath = new HashMap<>();

    /**
     * Create a new {@link LaunchedURLClassLoader} instance.
     *
     * @param urls   the URLs from which to load classes and resources
     * @param parent the parent class loader for delegation
     */
    public PlatformClassloader(URL[] urls, ClassLoader parent) {
        super(new URL[0], parent);
        //单独构造一个resourceLoader以加载Platform自身资源，不加载各模块资源
        resourceLoader = new LaunchedURLClassLoader(urls, parent.getParent());
        //
        internalClassloader = (LaunchedURLClassLoader) parent;
    }

    public PlatformClassloader(ClassLoader parent) {
        super(new URL[0], parent);
        ServiceUnitConfigService.getEnableSu();
        resourceLoader = new LaunchedURLClassLoader(((LaunchedURLClassLoader)parent).getURLs(), parent.getParent());
        internalClassloader = (LaunchedURLClassLoader) parent;

    }

    public void addModuleURL(URL[] urls) {
        addUrl(internalClassloader, urls);
    }

    public void addPlatformURL(URL[] urls) {
        addUrl(internalClassloader, urls);
        addUrl(resourceLoader, urls);
    }

    private void addUrl(LaunchedURLClassLoader launchedURLClassLoader, URL[] urls) {
        URLClassPath ucp;
        try {
            Field ucpField = URLClassLoader.class.getDeclaredField("ucp");
            ucpField.setAccessible(true);
            ucp = (URLClassPath) ucpField.get(launchedURLClassLoader);
            for (URL url : urls) {
                ucp.addURL(url);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取所有su的path
     * @return allSuPath
     */
    public Map<String, URLClassPath> getAppClasspath(){return this.appClasspath;}

    /**
     * @param suName 获取某个su的path
     * @return suPath
     */
    public URLClassPath getSuClasspath(String suName) {
        return appClasspath.get(suName);
    }

    @Override
    public URL getResource(String var1) {
        return resourceLoader.getResource(var1);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        return resourceLoader.getResources(name);
    }

    @Override
    public URL findResource(String var1) {
        return resourceLoader.findResource(var1);
    }

    @Override
    public Enumeration<URL> findResources(String var1) throws IOException {
        return resourceLoader.findResources(var1);
    }
}
