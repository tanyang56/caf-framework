/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.config;

import lombok.Data;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 特殊模块
 * @author wangyandong
 */
@Data
public class SpecialModule {
    private String name;
    private List<String> includes;
    //存储模块下的su和对应的路径
    private Map<String, String> suMapping;

    public SpecialModule(){
        this.name = null;
        this.includes = null;
    }
    public SpecialModule(String name,List<String> includes){
        this.name = name;
        this.includes = includes;
    }
    public SpecialModule(String name,List<String> includes,Map<String, String> suMapping){
        this.name = name;
        this.includes = includes;
        this.suMapping = suMapping;
    }


}
