/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.hibernate;

import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 自定义定制器 用于将classloader加入Hibernate可用的classloader列表
 *
 * @author guowenchang
 */
public class CustomHibernatePropertiesCustomizer implements HibernatePropertiesCustomizer {

    private static List<ClassLoader> classLoaderList = new ArrayList<>();

    @Override
    public void customize(Map<String, Object> hibernateProperties) {
        hibernateProperties.put(org.hibernate.cfg.AvailableSettings.CLASSLOADERS, classLoaderList);
    }

    public static void addClassloader(ClassLoader classLoader) {
        classLoaderList.add(classLoader);
    }
}
