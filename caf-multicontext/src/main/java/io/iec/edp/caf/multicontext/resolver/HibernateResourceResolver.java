/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.resolver;

import io.iec.edp.caf.multicontext.support.Module;
import io.iec.edp.caf.multicontext.support.ModuleManager;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

public class HibernateResourceResolver extends PathMatchingResourcePatternResolver {

    private final ModuleManager moduleManager;

    private ClassLoader classLoader;

    public HibernateResourceResolver(ModuleManager moduleManager) {
        super();
        this.moduleManager = moduleManager;
    }

    public HibernateResourceResolver(ModuleManager moduleManager, ClassLoader classLoader) {
        super(new DefaultResourceLoader(classLoader));
        this.moduleManager = moduleManager;
        this.classLoader = classLoader;
    }

    protected Set<Resource> doFindAllClassPathResources(String path) throws IOException {
        Set<Resource> result = new LinkedHashSet<>(16);
        ClassLoader cl = getClassLoader();

        addToResult(path, result, cl);

        for (Module module : moduleManager) {
            ClassLoader classLoader = module.getClassLoader();
            addToResult(path, result, classLoader);
        }

        return result;
    }

    private void addToResult(String path, Set<Resource> result, ClassLoader classLoader) throws IOException {
        Enumeration<URL> resourceUrls;
        resourceUrls = (classLoader != null ? classLoader.getResources(path) : ClassLoader.getSystemResources(path));
        while (resourceUrls.hasMoreElements()) {
            URL url = resourceUrls.nextElement();
            result.add(convertClassLoaderURL(url));
        }
        if (!StringUtils.hasLength(path)) {
            // The above result is likely to be incomplete, i.e. only containing file system references.
            // We need to have pointers to each of the jar files on the classpath as well...
            addAllClassLoaderJarRoots(classLoader, result);
        }
    }

    @Override
    public ClassLoader getClassLoader() {
        if (this.classLoader != null) {
            return this.classLoader;
        }

        return super.getClassLoader();
    }
}
