/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.support;

/**
 * 当前线程对应模块上下文信息
 *
 * @author guowenchang
 */
public class LocalThreadModule {
//    private static ThreadLocal<Module> threadLocal = new InheritableThreadLocal<>();
    /**
     * 20220617：将InheritableThreadLocal改为ThreadLocal，因为并行启动下，
     * 在context的finishBeanFactoryInitialization
     * 时会设置上这个变量，因为可继承在finishBeanFactoryInitialization会触发epp那边quatz的相关启动线程池
     * 导致后续运行时epp里的调用其他su（比如msg）的rpc时会因为他们之前启动阶段的线程继承了这个上下文变量导致获取的
     * context一致是epp-module导致获取不大rpc的相关bean报错
     */
    private static ThreadLocal<Module> threadLocal = new ThreadLocal<>();


    private static Boolean multiContext = false;

    public static Module getModule() {
        return threadLocal.get();
    }

    public static void setModule(Module module) {
        threadLocal.set(module);
    }

    public static void purgeModule() {
        threadLocal.remove();
    }

    public static Boolean getMultiContext() {
        return multiContext;
    }

    public static void setMultiContext(Boolean multiContext) {
        LocalThreadModule.multiContext = multiContext;
    }
}
