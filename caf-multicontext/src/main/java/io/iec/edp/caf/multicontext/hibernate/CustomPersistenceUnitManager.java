/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.hibernate;

import io.iec.edp.caf.multicontext.resolver.HibernateResourceResolver;
import io.iec.edp.caf.multicontext.support.ModuleManager;
import org.springframework.core.io.ResourceLoader;
import org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager;

/**
 * 自定义PersistenceUnitManager 替换自带的ResourceLoader，提供从子module中获取entity的能力
 * todo rxb: 感觉这个没有，这个重置resourceloader好像在CafJpaRepositoriesRegistrar里弄了,下次可以去掉试试
 * @author guowenchang
 */
public class CustomPersistenceUnitManager extends DefaultPersistenceUnitManager {

    private ModuleManager moduleManager;

    public CustomPersistenceUnitManager(ModuleManager moduleManager) {
        super();
        this.moduleManager = moduleManager;
        this.setResourceLoader(new HibernateResourceResolver(moduleManager));
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
//      这里感觉直接super.setResourceLoader(resourceLoader);就行吧
        super.setResourceLoader(new HibernateResourceResolver(moduleManager));
    }
}
