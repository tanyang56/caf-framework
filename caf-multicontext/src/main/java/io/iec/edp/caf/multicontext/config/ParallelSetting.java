/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.multicontext.config;

import lombok.Data;

import java.util.List;

/**
 * 并行加载配置
 * @author wangyandong
 */
@Data
public class ParallelSetting {
    /**
     * 并行类型
     */
    private Principle principle;

    /**
     * 扫描路径
     */
    private List<String> basePaths;

    /**
     * 扫描路径
     */
    private List<String> includes;

    /**
     * 排除路径，仅支持扫描路径(includes)下一级路径
     */
    private List<String> excludes;

    /**
     * 特殊或例外模块定义
     */
    private List<SpecialModule> specialModules;

    /**
     * 特殊的一些bean，类似wf，msg的使用websocket的bean,这些bean的注册必须放到底座的里
     */
    private List<String> specilBeans;
}
