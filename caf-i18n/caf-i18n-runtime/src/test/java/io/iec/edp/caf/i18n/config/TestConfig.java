package io.iec.edp.caf.i18n.config;

import io.iec.edp.caf.commons.exception.api.GspExceptionMessageService;
import io.iec.edp.caf.commons.exception.core.IExceptionMessageService;
import io.iec.edp.caf.commons.exception.handler.ExceptionMessageService;
import io.iec.edp.caf.i18n.api.LanguageSuffixProvider;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;
import io.iec.edp.caf.i18n.exception.DefaultGspExceptionMessageService;
import io.iec.edp.caf.i18n.framework.api.language.ILanguageService;
import io.iec.edp.caf.i18n.framework.api.resource.IResourceLocalizer;
import io.iec.edp.caf.i18n.framework.localclient.i18nresource.IResourceLocalizerImpl;
import io.iec.edp.caf.i18n.framework.localclient.language.LanguageServiceImpl;
import io.iec.edp.caf.i18n.manager.LanguageSuffixProviderImpl;
import io.iec.edp.caf.i18n.manager.ResourceLocalizerImpl;
import io.iec.edp.caf.impl.ServiceUnitAwareServiceImpl;
import io.iec.edp.caf.msu.api.ServiceUnitAwareService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author manwenxing01
 */
@Configuration(proxyBeanMethods = false)
@EntityScan({"io.iec.edp.caf.i18n.*", "io.iec.edp.caf.commons.exception.*", "io.iec.edp.caf.impl.*", "io.iec.edp.caf.msu.api.*"})
public class TestConfig {

    @Bean
    public ServiceUnitAwareService returnServiceUnitAwareService() {
        return new ServiceUnitAwareServiceImpl();
    }

    @Bean
    public ResourceLocalizer returnResourceLocalizer() {
        return new ResourceLocalizerImpl();
    }

    @Bean
    public GspExceptionMessageService returnDefaultGspExceptionMessageService(ResourceLocalizer manager) {
        return new DefaultGspExceptionMessageService(manager);
    }

    @Bean
    public IResourceLocalizer returnOldResourceLocalizer(ResourceLocalizer resourceLocalizer) {
        return new IResourceLocalizerImpl(resourceLocalizer);
    }

    @Bean
    public IExceptionMessageService returnIExceptionMessageService(ResourceLocalizer resourceLocalizer) {
        return new ExceptionMessageService(resourceLocalizer);
    }

    @Bean
    @ConditionalOnMissingBean(ILanguageService.class)
    public ILanguageService sysInitLanguageService() {
        return new LanguageServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean(LanguageSuffixProvider.class)
    public LanguageSuffixProvider sysInitLanguageSuffixProvider() {
        return new LanguageSuffixProviderImpl();
    }

}
