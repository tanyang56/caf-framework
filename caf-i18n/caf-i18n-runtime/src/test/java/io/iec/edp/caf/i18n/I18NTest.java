//package io.iec.edp.caf.i18n;
//
//import io.iec.edp.caf.commons.runtime.CafEnvironment;
//import io.iec.edp.caf.commons.utils.SpringBeanUtils;
//import io.iec.edp.caf.i18n.api.ResourceLocalizer;
//import io.iec.edp.caf.i18n.config.TestConfig;
//import io.iec.edp.caf.msu.api.ServiceUnitAwareService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.ApplicationContext;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.lang.reflect.Field;
//
///**
// * @author manwenxing01
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = TestConfig.class)
//public class I18NTest {
//
//    @Autowired
//    private ResourceLocalizer localizer;
//
//    @Autowired
//    private ApplicationContext applicationContext;
//
//    @Before
//    public void setJunitEnvironment() throws NoSuchFieldException, IllegalAccessException {
//        //反射重新设置CafEnvironment的serverRTPath
//        Class<CafEnvironment> environmentClass = CafEnvironment.class;
//        Field path = environmentClass.getDeclaredField("serverRTPath");
//        path.setAccessible(true);
//        //设置单元测试文件路径
//        path.set(null, "D:\\1.公司源码\\java\\CAF Next\\caf-framework\\caf-i18n\\caf-i18n-runtime\\src\\test\\resources\\server");
//
//        //重新设置SpringBeanUtils的spring上下文
//        SpringBeanUtils.setApplicationContext(this.applicationContext);
//
//        //尝试获取su
//        try {
//            SpringBeanUtils.getBean(ServiceUnitAwareService.class).getEnabledServiceUnits();
//        } catch (Exception e) {
//            System.out.println("国际化资源接口-单元测试-Before()中报错");
//        }
//    }
//
//    @Test
//    public void getString() {
//        System.out.println(this.localizer.getString("ECP_CAF_EXCP_0144", "exception.properties", "pfcommon", "en"));
//    }
//
//}
