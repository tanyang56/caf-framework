/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.event;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;

import java.util.TimeZone;

/**
 * springboot启动事件监听(设置时区信息)
 *
 * @author Vincent Lin
 */
public class I18nApplicationEnvironmentPreparedListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    private static String currentTimeZone;

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        String timeZoneId = event.getEnvironment().getProperty("i18n.time-zone");
        if (timeZoneId == null || "".equals(timeZoneId)) {
            timeZoneId = TimeZone.getDefault().getID();
        }
        TimeZone.setDefault(TimeZone.getTimeZone(timeZoneId));
        System.setProperty("hibernate.jdbc.time_zone", timeZoneId);
        currentTimeZone = timeZoneId;
    }

    public static String getCurrentTimeZone() {
        return currentTimeZone;
    }
}
