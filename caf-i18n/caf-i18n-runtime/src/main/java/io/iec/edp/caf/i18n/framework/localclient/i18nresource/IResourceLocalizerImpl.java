/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.localclient.i18nresource;

import io.iec.edp.caf.i18n.api.ResourceLocalizer;
import io.iec.edp.caf.i18n.framework.api.resource.IResourceLocalizer;

import java.awt.*;
import java.util.ResourceBundle;

@Deprecated
public class IResourceLocalizerImpl implements IResourceLocalizer {

    //注入新目录的实现类对象
    private ResourceLocalizer resourceLocalizer;

    //构造注入
    public IResourceLocalizerImpl(ResourceLocalizer resourceLocalizer) {
        this.resourceLocalizer = resourceLocalizer;
    }

    @Override
    public String getString(String resourceID, String contentID, String currentSU, String langCode) {
        return this.resourceLocalizer.getString(resourceID, contentID, currentSU, langCode);
    }

    @Override
    public byte[] getBytes(String resourceID, String contentID, String currentSU, String langCode) {
        return this.resourceLocalizer.getBytes(resourceID, contentID, currentSU, langCode);
    }

    @Override
    public Image getImage(String resourceID, String contentID, String currentSU, String langCode) {
        return this.resourceLocalizer.getImage(resourceID, contentID, currentSU, langCode);
    }

    @Override
    public ResourceBundle getResourceSet(String metadataId, String currentSU, String langCode) {
        return this.resourceLocalizer.getResourceSet(metadataId, currentSU, langCode);
    }
}
