/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.exception;

import io.iec.edp.caf.commons.exception.api.GspExceptionMessageService;
import io.iec.edp.caf.i18n.I18nContext;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

/**
 * 实现异常信息国际化服务接口
 */
public class DefaultGspExceptionMessageService implements GspExceptionMessageService {

    private ResourceLocalizer manager;

    public DefaultGspExceptionMessageService (ResourceLocalizer manager){
        this.manager = manager;
    }

    @Override
    public String getExceptionMessage(String resourceFileName, String resourceCode, String serviceUnitCode) {
        String culture = I18nContext.current.getLanguage();
        String msgStr = this.manager.getString(resourceCode, resourceFileName, serviceUnitCode, culture);
        return msgStr == null || "".equals(msgStr) ? resourceCode : msgStr;
    }
}
