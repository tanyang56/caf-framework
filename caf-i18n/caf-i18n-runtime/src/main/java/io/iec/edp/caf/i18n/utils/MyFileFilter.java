/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.utils;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 读取文件过滤类
 *
 * @author Vincent Lin
 */
public class MyFileFilter implements FilenameFilter {

    private String fileExtension;

    public MyFileFilter(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public boolean accept(File dir, String name) {
        return name.endsWith("." + fileExtension);
    }
}
