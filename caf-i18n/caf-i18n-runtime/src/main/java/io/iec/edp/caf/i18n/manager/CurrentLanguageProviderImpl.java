/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.manager;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import io.iec.edp.caf.i18n.api.CurrentLanguageProvider;

/**
 * 默认的当前语言提供程序，当igix层存在时，此实现可以忽略
 *
 * @author wangyandong
 * @date 2021/08/30 16:35
 */
public class CurrentLanguageProviderImpl implements CurrentLanguageProvider {
    /**
     * 当前的登陆语言编号
     */
    @Override
    public String getLanguage() {
        //尝试从环境变量读取language
        return CafEnvironment.getEnvironment().getProperty("language", "zh-CHS");
    }
}
