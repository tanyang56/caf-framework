/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.handler;

import io.iec.edp.caf.commons.exception.core.IExceptionMessageService;
import io.iec.edp.caf.i18n.I18nContext;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

/**
 * @author lijing
 * @data 2019/8/23 17:49
 */
@Deprecated
public class ExceptionMessageService implements IExceptionMessageService {

    private ResourceLocalizer manager;

    public ExceptionMessageService(ResourceLocalizer localizer) {
        this.manager = localizer;
    }

    /**
     * 获取其他模块的国际化消息
     *
     * @param resourceFileName 元数据标识
     * @param messageCode      消息编号
     * @param serviceUnitCode  服务单元编号
     * @param culture          语言类型
     * @return 国际化消息
     */
    @Override
    public String getI18nMessage(String resourceFileName, String messageCode, String serviceUnitCode, String culture) {
        String msgStr = manager.getString(messageCode, resourceFileName, serviceUnitCode, culture);
        return msgStr == null || "".equals(msgStr) ? messageCode : msgStr;
    }

    /**
     * 获取语言
     *
     * @return 语言
     */
    @Override
    public String getLanguage() {
        try {
            String language = I18nContext.current.getLanguage();
            if (language == null || "".equals(language))
                language = "zh-CHS";
            return language;
        } catch (Exception e) {
            //TODO:兼容没有国际化上下文的情况
            return "zh-CHS";
        }
    }

    /**
     * 从异常的Resource文件中获取信息
     *
     * @param fileName    文件名
     * @param messageCode 消息
     * @param language    语言类型
     * @return 信息
     */
    @Override
    public String getExceptionInnerResource(String fileName, String messageCode, String language) {
        return messageCode;
    }
}
