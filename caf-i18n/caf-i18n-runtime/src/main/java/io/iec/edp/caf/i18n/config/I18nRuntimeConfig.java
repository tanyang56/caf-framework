/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.config;

import io.iec.edp.caf.i18n.api.CurrentLanguageProvider;
import io.iec.edp.caf.i18n.manager.CurrentLanguageProviderImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 提供给工具使用
 *
 * @author wangyandong
 * @date 2021/08/30 17:15
 *
 */
@Configuration(proxyBeanMethods = false)
public class I18nRuntimeConfig {
    @ConditionalOnMissingBean({CurrentLanguageProvider.class})
    @Bean
    public CurrentLanguageProvider getCurrentLanguageProvider() {
        return new CurrentLanguageProviderImpl();
    }

}
