/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.manager;

import io.iec.edp.caf.i18n.api.LanguageSuffixProvider;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 语种后缀接口CAF Framework默认实现
 *
 * @author manwenxing01
 * @date 2022-08-29
 */
public class LanguageSuffixProviderImpl implements LanguageSuffixProvider {

    private List<EcpLanguage> languages = new ArrayList<>();

    public LanguageSuffixProviderImpl() {
        EcpLanguage zh_chs = new EcpLanguage("zh-CHS", "zh-CHS", "简体中文", "CHS", 0, "简体中文", true, true, OffsetDateTime.now());
        EcpLanguage en = new EcpLanguage("en", "en", "英文", "EN", 1, "英文", true, true, OffsetDateTime.now());
        EcpLanguage zh_cht = new EcpLanguage("zh-CHT", "zh-CHT", "繁體中文", "CHT", 2, "繁体中文", true, true, OffsetDateTime.now());
        this.languages.add(zh_chs);
        this.languages.add(en);
        this.languages.add(zh_cht);
    }

    @Override
    public String getFieldSuffix(String langCode) {
        if (!"".equals(langCode))
            for (EcpLanguage lang : this.languages) {
                if (lang.getCode().equals(langCode))
                    return String.format("_%s", lang.getFieldSuffix());
            }
        return null;
    }

}
