/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.api;


import io.iec.edp.caf.i18n.entity.EcpLanguage;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;

/**
 * 语种管理接口：语种增，删，改，查
 *
 * @author Vincent Lin
 */
@Path("/languagemanager")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface LanguageManager {

    /**
     * 增：保存语种
     *
     * @param language 语种
     */
    @POST
    @Path("/save")
    void saveWithoutCheck(EcpLanguage language);

    /**
     * 删除：根据语种内码Id删除语种定义
     *
     * @param languageID 语种内码Id
     */
    @POST
    @Path("/delete")
    void delete(String languageID);

    /**
     * 保存语种
     *
     * @param language 语种
     */
    void save(EcpLanguage language);

    /**
     * 添加语种
     *
     * @param language 语种
     */
    void add(EcpLanguage language);

    /**
     * 查：根据语种Id获取语种
     *
     * @param languageID 语种内码
     * @return 指定语言的语种信息
     */
    EcpLanguage get(String languageID);

    /**
     * 查：根据语种code获取语种
     *
     * @param languageCode 语种code
     * @return 指定语言的语种信息
     */
    EcpLanguage getLanguageByCode(String languageCode);

    /**
     * 查：获取所有语种的列表
     *
     * @return java.utils.List<EcpLanguage> 所有语种列表
     */
    List<EcpLanguage> getAllLanguages();

    /**
     * 设置语种启用状态
     *
     * @param languageID 语种内码Id
     * @param enabled    启用状态
     */
    void setLanguageStatus(String languageID, boolean enabled);

    /**
     * 更新语种的排序顺序
     *
     * @param orders 语种编号与排序顺序的字典
     */
    void updateOrder(HashMap<String, Integer> orders);
}
