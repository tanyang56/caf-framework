/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.api.language;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Deprecated
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EcpLanguage implements Serializable, Cloneable, Comparable<EcpLanguage> {

    /**
     * 内码
     */
    @Id
    private String id;

    /**
     * 编号
     */
    private String code;

    /**
     * 语种名称
     */
    private String name;

    /**
     * 语种字段后缀
     */
    @Column(name = "fieldsuffix")
    @JsonProperty("fieldSuffix")
    private String fieldSuffix;

    /**
     * 排序顺序
     */
    @Column(name = "sortorder")
    private int order;

    /**
     * 语种描述
     */
    private String description;

    /**
     * 当前语种是否已启用的标识
     */
    private boolean enabled;

    /**
     * 当前语种是否属于系统内置
     */
    @Column(name = "sysinit")
    @JsonProperty("sysInit")
    private boolean sysInit;

    /**
     * 最后修改时间
     */
    @Column(name = "lastmodifiedtime")
    @JsonProperty("lastModifiedTime")
    @Nullable
    private OffsetDateTime lastModifiedTime;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(EcpLanguage o) {
        if (o == null)
            return 1;
        int value = this.order - o.order;
        return value;
    }
}
