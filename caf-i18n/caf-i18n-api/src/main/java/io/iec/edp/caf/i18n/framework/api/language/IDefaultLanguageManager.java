/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.api.language;

/**
 * @author Vincent Lin
 * @description 默认语言管理接口
 * 通过服务管理器获取服务接口
 * @date 2019/7/26
 */
@Deprecated
public interface IDefaultLanguageManager {

    /**
     * @param langCode 语种编号
     * @description 设置应用默认语言
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void setAppDefaultLanguage(String langCode);

    /**
     * @return String 默认语种编号
     * @description 获取应用的默认语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    String getAppDefaultLanguage();

    /**
     * @return java.lang.String 默认语种编号
     * @description 获取实例的默认语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    String getDefaultLanguage();

    /**
     * @param appCode  实例编号
     * @param langCode 语种编号
     * @description 设置实例默认语言
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void setDefaultLanguage(String appCode, String langCode);
}



