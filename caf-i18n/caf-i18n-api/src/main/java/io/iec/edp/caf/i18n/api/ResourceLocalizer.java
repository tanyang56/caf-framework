/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.api;

import java.awt.*;
import java.util.ResourceBundle;

/**
 * 国际化资源服务接口
 *
 * @author Vincent Lin
 * @date 2023-03-24
 */
public interface ResourceLocalizer {

    /**
     * 根据资源编号{code}、su{su}、语言{language}和资源文件名称/资源元数据id{resource}获取对应语言的文本资源
     *
     * @param code     资源编号
     * @param resource 资源文件名或资源元数据id
     * @param su       服务单元编号
     * @param language 语言
     * @return 对应语言文本资源。code或resource为空返回null，资源文件中无此code返回""
     */
    String getString(String code, String resource, String su, String language);

    /**
     * 根据资源编号{code}、su{su}、语言{language}和资源文件名称/资源元数据id{resource}获取对应语言的二进制资源
     *
     * @param code     资源编号
     * @param resource 资源文件名或资源元数据id
     * @param su       服务单元编号
     * @param language 语言
     * @return 对应语言二进制资源。code或resource为空返回null，资源文件中无此code返回""
     */
    byte[] getBytes(String code, String resource, String su, String language);

    /**
     * 根据资源编号{code}、su{su}、语言{language}和资源文件名称/资源元数据id{resource}获取对应语言的图片资源
     *
     * @param code     资源编号
     * @param resource 资源文件名或资源元数据id
     * @param su       服务单元编号
     * @param language 语言
     * @return 对应语言图片资源。code或resource为空返回null，资源文件中无此code返回""
     */
    Image getImage(String code, String resource, String su, String language);

    /**
     * 根据资源元数据id{metadataId}、su{su}、语言{language}获取对应语言的资源集
     *
     * @param metadataId 资源元数据id
     * @param su         服务单元编号
     * @param language   语言
     * @return 对应语言资源集
     */
    ResourceBundle getResourceSet(String metadataId, String su, String language);

}