/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.api.CurrentLanguageProvider;
import io.iec.edp.caf.i18n.api.I18nContextService;

import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Vincent Lin
 * @description 国际化上下文
 * 通过国际化上下文，可以获取当前登录语言、默认语言、系统时区等信息。
 * @date 2019/7/26
 */
public class I18nContext {

    private I18nContextService i18nServerContext;
    private CurrentLanguageProvider provider;

    public final static I18nContext current = new I18nContext();

    private I18nContext() {
    }

    /**
     * 获取内部国际化服务
     */
    public I18nContextService getService() {
        if (this.i18nServerContext == null)
            this.i18nServerContext = SpringBeanUtils.getBean(I18nContextService.class);
        return this.i18nServerContext;
    }

    /**
     * 当前的登陆语言编号
     */
    public String getLanguage() {
        if (provider == null)
            provider = SpringBeanUtils.getBean(CurrentLanguageProvider.class);
        return provider.getLanguage();
    }

    /**
     * 实例的默认语言编号
     */
    public String getInstLanguage() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getInstLanguage();
    }

    /**
     * 应用的默认语言编号
     */
    public String getAppLanguage() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getAppLanguage();
    }

    /**
     * 获取系统时区
     */
    public TimeZone getAppTimeZone() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getAppTimeZone();
    }

    /**
     * 客户端时区的编号
     */
    @Deprecated
    public String getUserTimeZoneId() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getUserTimeZoneId();
    }

    /**
     * 客户端时区的偏移
     */
    @Deprecated
    public double getUserTimeOffset() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getUserTimeOffset();
    }

    /**
     * 数据库存储使用的时区的编号
     */
    @Deprecated
    public String getInstTimeZoneId() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getInstTimeZoneId();
    }

    /**
     * 数据库存储使用时区偏移
     */
    @Deprecated
    public double getInstTimeOffset() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        return i18nServerContext.getInstTimeOffset();
    }

    /**
     * 当前国际化的区域性选项
     */
    public Locale getCurrentLocale() {
        if (i18nServerContext == null)
            i18nServerContext = getService();
        if (this.getLanguage() == "zh-CHS") {
            return Locale.SIMPLIFIED_CHINESE;
        } else if (this.getLanguage() == "en") {
            return Locale.ENGLISH;
        } else if (this.getLanguage() == "zh-CHT") {
            return Locale.TRADITIONAL_CHINESE;
        } else {
            return Locale.getDefault();
        }
    }

    /**
     * 用户国际化选项（屏蔽原因：framework层无法使用EcpUserI18NSetting）
     */
//    public EcpUserI18NSetting getUserI18NSettings() {
//        if (i18nServerContext == null)
//            i18nServerContext = getService();
//        return i18nServerContext.getUserI18NSettings();
//    }

}

