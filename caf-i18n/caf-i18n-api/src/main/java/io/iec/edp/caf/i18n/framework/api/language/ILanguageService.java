/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.api.language;

import javax.ws.rs.GET;
import java.util.List;

/**
 * @author Vincent Lin
 * @description 语种服务接口
 * 对外服务接口，主要适用场景：获取系统启用的语言列表、获取系统内置语言列表、获取语言对应的字段后缀。
 * 获取语种管理接口服务的方式有两种：
 * 1 根据上下文State获取，适合于登录后使用
 * GSPState state = GSPContext.Current.Session;//客户端改为UIState即可。
 * ILanguageService manager =
 * ServiceManager.Default.Resolve&lt;ILanguageService&gt;(new TypedParameter(typeof(GSPState),state))
 * 2 根据实例编号获取，适合于登录前使用的情况
 * String appCode = "m572";//改成实际的值即可。
 * ILanguageService manager =
 * ServiceManager.Default.Resolve&lt;ILanguageService&gt;(new TypedParameter(typeof(String),appCode))
 * 获取系统启用的语言
 * var enabledLanguages = manager.GetEnabledLanguages();
 * 获取系统内置的语言，安装盘初始时根据此生成多语列
 * var builtinLanguages = manager.GetBuiltinLanguages();
 * 获取语言对应的字段后缀
 * var language = I18nContext.Current.Language;
 * var suffix = manager.GetFieldSuffix(language);
 * var fieldName = String.format("Name{0}",suffix);
 * @date 2019/7/26
 */
@Deprecated
public interface ILanguageService {
    /**
     * @param langCode 语种编号
     * @return EcpLanguage 语种对象
     * @description 根据语种编号获取语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    EcpLanguage getLanguage(String langCode);

    /**
     * @return java.utils.List<EcpLanguage> 所有语种列表
     * @description 获取本系统已经启用的语种列表
     * @author Vincent Lin
     * @date 2019/7/26
     */
    @GET
    List<EcpLanguage> getEnabledLanguages();

    /**
     * @return java.utils.List<EcpLanguage> 所有语种列表
     * @description 获取内置的语言列表
     * @author Vincent Lin
     * @date 2019/7/26
     */
    List<EcpLanguage> getBuiltinLanguages();

    /**
     * @return java.utils.List<EcpLanguage> 所有语种列表
     * @description 获取所有语种的列表
     * @author Vincent Lin
     * @date 2019/7/26
     */
    @GET
    List<EcpLanguage> getAllLanguages();

    /**
     * @return java.lang.String 字段后缀
     * @description 根据当前登录语言获取对应的字段后缀
     * @author Vincent Lin
     * @date 2019/7/26
     */
    String getFieldSuffix();

    /**
     * @param langCode 语种编号
     * @return java.lang.String 字段后缀
     * @description 获取语种编号对应的字段后缀
     * @author Vincent Lin
     * @date 2019/7/26
     */
    String getFieldSuffix(String langCode);
}
