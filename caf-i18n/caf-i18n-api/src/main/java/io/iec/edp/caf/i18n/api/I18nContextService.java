/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.TimeZone;

/**
 * 国际化相关信息接口
 *
 * @author manwenxing01
 * @date 2021-08-20
 */
@Path("/context")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface I18nContextService {

    /**
     * 当前的登陆语言编号
     */
    String getLanguage();

    /**
     * 实例的默认语言编号
     */
    String getInstLanguage();

    /**
     * 应用的默认语言编号
     */
    String getAppLanguage();

    /**
     * 客户端时区的编号
     */
    @Deprecated
    String getUserTimeZoneId();

    /**
     * 客户端时区的偏移
     */
    @Deprecated
    double getUserTimeOffset();

    /**
     * 数据库存储使用的时区的编号
     */
    @Deprecated
    String getInstTimeZoneId();

    /**
     * 数据库存储使用时区偏移
     */
    @Deprecated
    double getInstTimeOffset();

    /**
     * 获取系统时区
     */
    TimeZone getAppTimeZone();

    /**
     * 用户国际化选项（屏蔽原因：framework层无法使用EcpUserI18NSetting）
     */
//    UserI18nSetting getUserI18NSettings();

    /**
     * 获取有效的语言列表
     */
    @GET
    @Path("/getlanguages")
    List<String> getAvailableLanguages();

    /**
     * 获取有效的国家地区列表
     */
    @GET
    @Path("/getcountries")
    List<String> getAvailableCountries();

    /**
     * 获取有效的时区列表
     */
    @GET
    @Path("/gettimezones")
    List<String> getAvailableTimeZones();

    /**
     * 返回数据库时区
     */
    public TimeZone getDBTimezone();
}
