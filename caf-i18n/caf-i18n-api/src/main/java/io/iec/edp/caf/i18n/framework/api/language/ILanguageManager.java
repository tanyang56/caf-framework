/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.api.language;

import javax.ws.rs.POST;
import java.util.HashMap;
import java.util.List;

/**
 * @author Vincent Lin
 * @description 语种管理接口
 * 内部管理用接口，外部调用请使用<see cref="ILanguageService"/>接口
 * 获取语种管理接口服务的方式有两种：
 * 1 根据上下文State获取，适合于登录后使用
 * GSPState state = GSPContext.Current.Session;//客户端改为UIState即可。
 * ILanguageManager manager =
 * ServiceManager.Default.Resolve<ILanguageManager>(new TypedParameter(typeof(GSPState),state))
 * 2 根据实例编号获取，适合于登录前使用的情况
 * String appCode = "m572";//改成实际的值即可。
 * ILanguageManager manager =
 * ServiceManager.Default.Resolve<ILanguageManager>(new TypedParameter(typeof(String),appCode))
 * @date 2019/7/26
 */
@Deprecated
public interface ILanguageManager {

    /**
     * @param language 语种
     * @description 添加语种定义
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void add(EcpLanguage language);

    /**
     * @param language 语种
     * @description 保存语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void save(EcpLanguage language);

    /**
     * @param language 语种
     * @description 保存语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    @POST
    void saveWithoutCheck(EcpLanguage language);

    /**
     * @param langID 语种内码
     * @description 根据语种内码删除语种定义
     * @author Vincent Lin
     * @date 2019/7/26
     */
    @POST
    void delete(String langID);

    /**
     * @param langID 语种内码
     * @return io.iec.edp.caf.i18n.framework.api.language.events.EcpLanguage
     * @description 获取语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    EcpLanguage get(String langID);

    /**
     * @param langCode 语种内码
     * @return EcpLanguage 指定语言的语种信息
     * @description 获取语种
     * @author Vincent Lin
     * @date 2019/7/26
     */
    EcpLanguage getLanguageByCode(String langCode);

    /**
     * @return java.utils.List<EcpLanguage> 所有语种列表
     * @description 获取所有语种的列表
     * @author Vincent Lin
     * @date 2019/7/26
     */
    List<EcpLanguage> getAllLanguages();

    /**
     * @param langID  语种内码
     * @param enabled 启用状态
     * @description 设置语种启用状态
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void setLanguageStatus(String langID, boolean enabled);

    /**
     * @param orders 语种编号与排序顺序的字典
     * @description 更新语种的排序顺序
     * @author Vincent Lin
     * @date 2019/7/26
     */
    void updateOrder(HashMap<String, Integer> orders);
}
