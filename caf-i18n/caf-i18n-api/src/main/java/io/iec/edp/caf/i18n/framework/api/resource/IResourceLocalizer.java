/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.framework.api.resource;

import java.awt.*;
import java.util.ResourceBundle;

/**
 * 国际化资源服务接口:此接口支持以metadata、*.resources存储的资源
 *
 * @author Vincent Lin
 * @date 2019/7/26
 */
@Deprecated
public interface IResourceLocalizer {

    /**
     * 根据资源文件、资源编号返回特定语言下的资源信息
     *
     * @param resourceID 资源编号
     * @param contentID  元数据编号或资源文件名
     * @param currentSU  当前服务单元编号
     * @param langCode   语种编号
     * @return java.lang.String 资源信息
     * @author Vincent Lin
     * @date 2019/7/26
     */
    String getString(String resourceID, String contentID, String currentSU, String langCode);

    /**
     * 根据资源文件、资源编号返回特定语言下的二进制资源信息
     *
     * @param resourceID 资源编号
     * @param contentID  元数据编号或资源文件名
     * @param currentSU  语种编号
     * @param langCode   当前服务单元
     * @return byte[] 二进制资源信息
     * @author Vincent Lin
     * @date 2019/7/26
     */
    byte[] getBytes(String resourceID, String contentID, String currentSU, String langCode);

    /**
     * 根据资源文件、资源编号返回特定语言下的图像资源信息
     *
     * @param resourceID 资源编号
     * @param contentID  元数据编号或资源文件名
     * @param currentSU  语种编号
     * @param langCode   当前服务单元
     * @return java.awt.Image 图像资源信息
     * @author Vincent Lin
     */
    Image getImage(String resourceID, String contentID, String currentSU, String langCode);

    /**
     * 元数据运行时获取资源集合
     *
     * @param metadataId 元数据ID
     * @param currentSU  当前su
     * @param langCode   语言
     * @return ResourceBundle 资源集
     * @author Vincent Lin
     */
    ResourceBundle getResourceSet(String metadataId, String currentSU, String langCode);

}
