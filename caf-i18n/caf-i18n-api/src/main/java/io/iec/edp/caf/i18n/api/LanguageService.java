/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.api;


import io.iec.edp.caf.i18n.entity.EcpLanguage;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 语种服务接口：对外服务接口
 * 适用场景：1：获取系统启用的语种集合   2：获取系统内置语种集合   3：获取U语种对应的字段后缀
 *
 * @author Vincent Lin
 */
@Path("/languageservice")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface LanguageService {

    /**
     * 获取系统已启用的语种
     *
     * @return java.utils.List<EcpLanguage> 语种集合
     */
    @GET
    @Path("/getenabledlanguages")
    List<EcpLanguage> getEnabledLanguages();

    /**
     * 获取系统所有语种
     *
     * @return java.utils.List<EcpLanguage> 语种集合
     */
    @GET
    @Path("/getalllanguages")
    List<EcpLanguage> getAllLanguages();

    /**
     * 获取系统内置的语言集合
     *
     * @return java.utils.List<EcpLanguage> 语种集合
     */
    List<EcpLanguage> getBuiltinLanguages();

    /**
     * 根据语种编号获取语种实体
     *
     * @param langCode 语种编号
     * @return EcpLanguage 语种实体
     */
    EcpLanguage getLanguage(String langCode);

    /**
     * 根据当前登录语言获取对应的语种字段后缀
     *
     * @return String 语种字段后缀
     */
    String getFieldSuffix();

    /**
     * 根据语种编号获取对应语种字段后缀
     *
     * @param langCode 语种编号
     * @return java.lang.String 语种字段后缀
     */
    String getFieldSuffix(String langCode);
}
