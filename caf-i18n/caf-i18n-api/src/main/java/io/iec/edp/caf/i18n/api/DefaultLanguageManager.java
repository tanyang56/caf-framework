/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.i18n.api;

/**
 * 应用级别：默认语言管理接口
 *
 * @author Vincent Lin
 */
public interface DefaultLanguageManager {

    /**
     * 设置应用默认语言
     *
     * @param langCode 语种编号
     */
    void setAppDefaultLanguage(String langCode);

    /**
     * 获取应用的默认语言
     *
     * @return String 默认语种编号
     */
    String getAppDefaultLanguage();

    /**
     * 设置实例默认语言
     *
     * @param appCode  实例编号
     * @param langCode 语种编号
     */
    void setDefaultLanguage(String appCode, String langCode);

    /**
     * 获取实例的默认语种
     *
     * @return java.lang.String 默认语种编号
     */
    String getDefaultLanguage();

}

