/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.core;

@Deprecated
public interface IExceptionMessageService {
    /**
     * 获取其他模块的国际化消息
     *
     * @param resourceFileName 元数据标识
     * @param message          消息
     * @param serviceUnitCode  服务单元编号
     * @param culture          语言类型
     * @return 国际化消息
     */
    public String getI18nMessage(String resourceFileName, String message, String serviceUnitCode, String culture);

    /**
     * 获取语言
     *
     * @return 语言
     */
    public String getLanguage();


    /**
     * 从异常的Resource文件中获取信息
     *
     * @param fileName 文件名
     * @param message  消息
     * @param culture  语言类型
     * @return 信息
     */
    public String getExceptionInnerResource(String fileName, String message, String culture);
}
