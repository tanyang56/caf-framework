/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.caf.commons;

public class Parent {
//    public static String str;
//
//    public static EntityObject obj;
//
//    public static ThreadLocal<String> threadLocal = new InheritableThreadLocal<>();
//    public static ThreadLocal<EntityObject> threadLocal2 = new InheritableThreadLocal<>();
//
//
//    public static final EntityObject objFinal=new EntityObject();

    public static final ThreadLocal<String> threadLocalFinal = new InheritableThreadLocal<>();
    public static final ThreadLocal<EntityObject> threadLocalFinal2 = new InheritableThreadLocal<>();

//    public static void setObjFinal(String s){
//        objFinal.str = s;
//    }

    public static void setObjThreadLocal(String s){
        threadLocalFinal.set(s);
    }

    public static void setObjThreadLocal2(EntityObject s){
        threadLocalFinal2.set(s);
    }

    public static Object getObjThreadLocal(){
        return threadLocalFinal.get();
    }

    public static EntityObject getObjThreadLocal2(){
        return threadLocalFinal2.get();
    }
}
