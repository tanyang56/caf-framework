///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.caf.commons;
//
//import io.iec.caf.commons.config.JsonConfig;
//import io.iec.edp.caf.common.JSONSerializer;
//import io.iec.edp.caf.commons.core.SerializerFactory;
//import io.iec.edp.caf.commons.core.enums.SerializeType;
//import lombok.var;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.sql.Date;
//import java.time.OffsetDateTime;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.Future;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = JsonConfig.class)
//public class JsonTest {
//
////    @NacosInjected
////    private NamingService namingService;
////
////    @Test
////    public void testNacos() throws NacosException {
////        this.namingService.registerInstance("service1","192.168.1.1",5100,"cluster1");
////        this.namingService.registerInstance("service1","192.168.1.2",5200,"cluster1");
////        this.namingService.registerInstance("service1","192.168.1.3",5300,"cluster1");
////
////        var instance = this.namingService.selectOneHealthyInstance("service1");
////
////    }
////    @Test
////    public void testStatic() throws ExecutionException, InterruptedException {
////
//////        Child.str = "123";
//////        EntityObject obj = new EntityObject();
//////        obj.str = "sss";
//////        Child.obj = obj;
//////
//////        System.out.println(Parent.str);
//////        System.out.println(Parent.obj.str);
//////
//////        Child.threadLocal.set("threadlocal");
//////        Child.threadLocal2.set(obj);
//////
//////        System.out.println(Parent.threadLocal.get());
//////        System.out.println(Parent.threadLocal2.get().str);
//////        System.out.println("///////////////////////////////////////////////////////////////////////////////////////////");
//////
//////        Child.setObjFinal("final 123");
////
////
////        ExecutorService executorService = Executors.newFixedThreadPool(1);
////
////        Parent.setObjThreadLocal("parent thread local");
////
////        Thread t = new Thread(() -> {
////            System.out.println(Child.getObjThreadLocal());
////
////            Child.setObjThreadLocal("threadlocal final 123");
////            EntityObject obj2 = new EntityObject();
////            obj2.str="threadlocal obj string";
////            Child.setObjThreadLocal2(obj2);
////
////            System.out.println(Parent.getObjThreadLocal());
////            System.out.println(Parent.getObjThreadLocal2().str);
////        });
////
////        Future future = executorService.submit(t);//子线程启动
////        future.get();//需要捕获两种异常
////        executorService.shutdown();
////
//////        System.out.println(Parent.objFinal.str);
////        System.out.println(Parent.getObjThreadLocal());
////        System.out.println(Parent.getObjThreadLocal2().str);
////    }
//
//    @Test
//    public void testNormalEntity() throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException {
////        String s="{displayValue:\"<p>[[${testfybo.billType}]]</p>\",\"parseValue\":\"<html> \\n <head></head> \\n <body> \\n  <p><span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.billType}\\\"> &nbsp;</span></span></p>  \\n </body>\\n</html>\",\"dataMap\":[{\"key\":\"[[${testfybo.billType}]]\",\"value\":\"<span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.billType}\\\"> &nbsp;</span></span>\"}]}";
//        String s="{displayValue:\"<p>[[${testfybo.billType}]]</p>\",\"parseValue\":\"<html> \\n <head></head> \\n <body> \\n  <p><span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.billType}\\\"> &nbsp;</span></span></p>  \\n </body>\\n</html>\",\"dataMap\":[{\"key\":\"[[${testfybo.billType}]]\",\"value\":\"<span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.billType}\\\"> &nbsp;</span></span>\"}]}";
//        if(s.startsWith("{displayValue")&&s.contains("parseValue")&&s.contains("dataMap")){
//            System.out.println("还会");
//        }
////        SelectDataMap dataMap=new SelectDataMap();
////        dataMap.setDisplayValue("ni");
////        Map<String,String> map=new HashMap<>();
////        map.put("key","key1");
////        map.put("value","valu1");
////        dataMap.setParseValue("pa");
////        String json= JSONSerializer.serialize(dataMap);
////        System.out.println(json);
//
//        SelectDataMap dataMap1=JSONSerializer.deserialize("{\n" +
//                "\t\"displayValue\": \"<table style=\\\"border-collapse: collapse; width: 100%; height: 36px;\\\" border=\\\"1\\\">\\n<tbody>\\n<tr style=\\\"height: 18px;\\\">\\n<td style=\\\"width: 50%; height: 18px;\\\">内码</td>\\n<td style=\\\"width: 50%; height: 18px;\\\">编号</td>\\n</tr>\\n<tr style=\\\"height: 18px;\\\">\\n<td style=\\\"width: 50%; height: 18px;\\\">[[${testfybo.id}]]</td>\\n<td style=\\\"width: 50%; height: 18px;\\\">[[${testfybo.billCode}]]</td>\\n</tr>\\n</tbody>\\n</table>\",\n" +
//                "\t\"parseValue\": \"<html> \\n <head></head> \\n <body> \\n  <table style=\\\"border-collapse: collapse; width: 100%; height: 36px;\\\" border=\\\"1\\\"> \\n   <tbody> \\n    <tr style=\\\"height: 18px;\\\"> \\n     <td style=\\\"width: 50%; height: 18px;\\\">内码</td> \\n     <td style=\\\"width: 50%; height: 18px;\\\">编号</td> \\n    </tr> \\n    <tr style=\\\"height: 18px;\\\" th:each=\\\"testfybo, iter:${testfybo}\\\"> \\n     <td style=\\\"width: 50%; height: 18px;\\\">[[${testfybo.id}]]</td> \\n     <td style=\\\"width: 50%; height: 18px;\\\">[[${testfybo.billCode}]]</td> \\n    </tr> \\n   </tbody> \\n  </table>  \\n </body>\\n</html>\",\n" +
//                "\t\"dataMap\": [\n" +
//                "\t\t{\n" +
//                "\t\t\t\"key\": \"[[${testfybo.id}]]\",\n" +
//                "\t\t\t\"value\": \"<span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.id}\\\"> &nbsp;</span></span>\"\n" +
//                "\t\t},\n" +
//                "\t\t{\n" +
//                "\t\t\t\"key\": \"[[${testfybo.billCode}]]\",\n" +
//                "\t\t\t\"value\": \"<span th:each=\\\"item:${testfybo}\\\">&nbsp;<span th:text=\\\"${item.billCode}\\\"> &nbsp;</span></span>\"\n" +
//                "\t\t}\n" +
//                "\t]\n" +
//                "}",SelectDataMap.class);
////        var s = "cs";
////        var i = 1;
////        var d1=  new Date(System.currentTimeMillis());
////        var d2 = OffsetDateTime.now();
////        SimpleEntity entity = new SimpleEntity();
////        entity.setS(s);
////        entity.setI(i);
////        entity.setD1(d1);
////        entity.setD2(d2);
////
////        var serializer = SerializerFactory.getSerializer(SerializeType.Json);
////        var deserializer = SerializerFactory.getDeserializer(SerializeType.Json);
////
////        var bytes = serializer.serializeToString(entity);
////        var copyentity = deserializer.deserialize(bytes,SimpleEntity.class);
////        assert copyentity.getS().equals(s);
////        assert copyentity.getI()==i;
////        //todo Date类型反序列化后没有时间了？
//////        assert copyentity.getD1()==d1;
////        assert copyentity.getD2().equals(d2);
////
////        ByteArrayOutputStream output = new ByteArrayOutputStream();
////        var serializestream = SerializerFactory.getSerializer(SerializeType.Json,output);
////
////        serializestream.serializeToStream(entity);
////        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
////        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Json,input);
////        copyentity = deserializerstream.deserialize(SimpleEntity.class);
////        assert copyentity.getS().equals(s);
////        assert copyentity.getI()==i;
//////        assert copyentity.getD1()==d1;
////        assert copyentity.getD2().equals(d2);
////
////        var bytes2 = serializer.serializeToString(d1);
////        var copyentity2 = deserializer.deserialize(bytes2,Date.class);
//////        assert copyentity2.getTime()==d1.getTime();
//    }
//
//    @Test
//    public void testList() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//        List<SimpleEntity> list = new ArrayList<>();
//        list.add(entity);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Json);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Json);
//
//        var bytes = serializer.serializeToString(list);
//        var copyentity = deserializer.deserialize(bytes, List.class,SimpleEntity.class);
//        assert ((List<SimpleEntity>)copyentity).get(0).getS().equals(s);
//        assert ((List<SimpleEntity>)copyentity).get(0).getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert ((List<SimpleEntity>)copyentity).get(0).getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Json,output);
//
//        serializestream.serializeToStream(list);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Json,input);
//        copyentity = deserializerstream.deserialize(List.class,SimpleEntity.class);
//        assert ((List<SimpleEntity>)copyentity).get(0).getS().equals(s);
//        assert ((List<SimpleEntity>)copyentity).get(0).getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert ((List<SimpleEntity>)copyentity).get(0).getD2().equals(d2);
//    }
//    @Test
//    public void testMap() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//        Map<String,SimpleEntity> map = new HashMap();
//        map.put("entity",entity);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Json);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Json);
//
//        var bytes = serializer.serializeToString(map);
//        var copyentity = deserializer.deserialize(bytes, Map.class,String.class,SimpleEntity.class);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getS().equals(s);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Json,output);
//
//        serializestream.serializeToStream(map);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Json,input);
//        copyentity = deserializerstream.deserialize(Map.class,String.class,SimpleEntity.class);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getS().equals(s);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD2().equals(d2);
//    }
//
//    @Test
//    public void testComplexEntity() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//
//        Map<String,SimpleEntity> map = new HashMap();
//        map.put("entity",entity);
//
//        List<SimpleEntity> list = new ArrayList<>();
//        list.add(entity);
//
//        ComplexEntity entity1= new ComplexEntity();
//        entity1.setEntity(entity);
//        entity1.setListEntity(list);
//        entity1.setMapEntity(map);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Json);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Json);
//
//        var bytes = serializer.serializeToString(entity1);
//        var copyentity = deserializer.deserialize(bytes, ComplexEntity.class);
//        assert copyentity.getEntity().getS().equals(s);
//        assert copyentity.getEntity().getI()==i;
//        assert copyentity.getListEntity().get(0).getS().equals(s);
//        assert copyentity.getListEntity().get(0).getI()==i;
//        assert copyentity.getMapEntity().get("entity").getS().equals(s);
//        assert copyentity.getMapEntity().get("entity").getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert copyentity.getEntity().getD2().equals(d2);
//        assert copyentity.getListEntity().get(0).getD2().equals(d2);
//        assert copyentity.getMapEntity().get("entity").getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Json,output);
//
//        serializestream.serializeToStream(entity1);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Json,input);
//        copyentity = deserializerstream.deserialize(ComplexEntity.class);
//        assert copyentity.getEntity().getS().equals(s);
//        assert copyentity.getEntity().getI()==i;
//        assert copyentity.getListEntity().get(0).getS().equals(s);
//        assert copyentity.getListEntity().get(0).getI()==i;
//        assert copyentity.getMapEntity().get("entity").getS().equals(s);
//        assert copyentity.getMapEntity().get("entity").getI()==i;
//        //todo Date类型反序列化后没有时间了？
////        assert copyentity.getD1()==d1;
//        assert copyentity.getEntity().getD2().equals(d2);
//        assert copyentity.getListEntity().get(0).getD2().equals(d2);
//        assert copyentity.getMapEntity().get("entity").getD2().equals(d2);
//    }
//
//}
