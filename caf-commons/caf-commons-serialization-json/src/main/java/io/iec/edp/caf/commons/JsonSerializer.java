/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons;

import com.fasterxml.jackson.databind.JavaType;
import io.iec.edp.caf.commons.core.api.DataSerializer;
import lombok.SneakyThrows;
import lombok.var;

import java.io.*;

/**
 * Json序列化器
 */
class JsonSerializer implements DataSerializer {
    private final BufferedWriter writer;
    private final BufferedReader reader;

    public JsonSerializer(OutputStream outstream, InputStream inputStream){
        this.writer = new BufferedWriter(new OutputStreamWriter(outstream));
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @SneakyThrows
    @Override
    public void serializeToStream(Object object) {
        var s = serializeToString(object);
        this.writer.write(s);
        this.writer.flush();
        this.writer.close();
    }

    @Override
    public String serializeToString(Object object) {
        return JacksonUtils.serialize(object);
    }

    @Override
    public byte[] serializeToByte(Object object) {
        var str = serializeToString(object);
        return str.getBytes();
    }

    @Override
    public <T> T deserialize(Class<T> clazz) throws IOException {
        var s = readLine();
        return deserialize(s,clazz);
    }

    @Override
    public <T> T deserialize(JavaType type) throws IOException {
        var s = readLine();
        return deserialize(type);
    }

    @Override
    public <T> T deserialize(Class<?> collectionClazz, Class<?>... elementClazzes) throws IOException {
        var s = readLine();
        return deserialize(s,collectionClazz,elementClazzes);
    }

    @Override
    public <T> T deserialize(String value, Class<T> clazz) {
        return JacksonUtils.deserialize(value,clazz);
    }

    @Override
    public <T> T deserialize(String value, JavaType type) {
        return JacksonUtils.deserialize(value,type);
    }

    @Override
    public <T> T deserialize(String value, Class<?> collectionClazz, Class<?>... elementClazzes) {
        return JacksonUtils.deserialize(value,collectionClazz,elementClazzes);
    }

    @Override
    public <T> T deserialize(byte[] value, Class<T> clazz) {
        var str = new String(value);
        return deserialize(str,clazz);
    }

    @Override
    public <T> T deserialize(byte[] value, JavaType type) {
        var str = new String(value);
        return deserialize(str,type);
    }

    @Override
    public <T> T deserialize(byte[] value, Class<?> collectionClazz, Class<?>... elementClazzes) {
        var str = new String(value);
        return deserialize(str,collectionClazz,elementClazzes);
    }

    @Override
    public Object getObjectMapper() {
        return JacksonUtils.getObjectMapper();
    }


    private String readLine() throws IOException, EOFException {
        String line = reader.readLine();
        if (line == null || line.trim().length() == 0) {
            throw new EOFException();
        }
        return line;
    }
}
