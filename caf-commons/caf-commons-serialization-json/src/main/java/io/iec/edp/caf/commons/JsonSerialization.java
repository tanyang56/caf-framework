/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons;

//import io.iec.edp.caf.commons.core.api.CafSerialization.DataDeserializer;
import io.iec.edp.caf.commons.core.api.CafSerialization;
import io.iec.edp.caf.commons.core.api.DataSerializer;
import io.iec.edp.caf.commons.core.entity.TypeConstants;
import lombok.var;

import java.io.*;

/**
 * Json Serialization
 */
public class JsonSerialization implements CafSerialization {
    /**
     * 序列化器的标识
     * 通过TypeConstants常量获取
     * @return
     */
    @Override
    public byte getId() {
        return TypeConstants.JSON_SERIALIZATION_ID;
    }

    @Override
    public DataSerializer getSerializer() {
        var bytes = new byte[2048];
        var input = new ByteArrayInputStream(bytes);
        var output = new ByteArrayOutputStream();
        return new JsonSerializer(output,input);
    }

    @Override
    public DataSerializer getDerializer() {
        var bytes = new byte[2048];
        var input = new ByteArrayInputStream(bytes);
        var output = new ByteArrayOutputStream();
        return new JsonSerializer(output,input);
    }

    @Override
    public DataSerializer getSerializer(OutputStream stream) {
        var bytes = new byte[2048];
        var input = new ByteArrayInputStream(bytes);
        return new JsonSerializer(stream,input);
    }

    @Override
    public DataSerializer getDerializer(InputStream stream) {
        var output = new ByteArrayOutputStream();
        return new JsonSerializer(output,stream);
    }
}
