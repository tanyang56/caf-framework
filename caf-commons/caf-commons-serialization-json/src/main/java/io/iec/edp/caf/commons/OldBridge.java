/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 临时的桥接类(后续老的不用了就可以删除了)
 * 为了不报序列化工具对外暴露出去
 */
@Deprecated
public class OldBridge {
    public static <T> String serialize(T obj ) {
        return JacksonUtils.serialize(obj);
    }

    public static <T>  T deserialize(String json , Class<T> clazz) {
        return JacksonUtils.deserialize(json,clazz);
    }

    public static <T> T deserialize(String json , Class<?> collectionClazz, Class<?>... elementClazzes) {
        return JacksonUtils.deserialize(json,collectionClazz,elementClazzes);
    }
    public static <T> T deserialize(String json, JavaType type){return JacksonUtils.deserialize(json,type);}

    public static ObjectMapper getObjectMapper() {
        return JacksonUtils.getObjectMapper();
    }
}
