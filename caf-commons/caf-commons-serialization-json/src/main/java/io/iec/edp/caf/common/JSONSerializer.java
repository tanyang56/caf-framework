/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common;


import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.iec.edp.caf.commons.OldBridge;

@Deprecated
public class JSONSerializer {

    public static <T> String serialize(T obj ) {
        return OldBridge.serialize(obj);
    }

    public static <T>  T deserialize(String json , Class<T> clazz) {
        return OldBridge.deserialize(json,clazz);
    }

    public static <T> T deserialize(String json , Class<?> collectionClazz, Class<?>... elementClazzes) {
        return OldBridge.deserialize(json,collectionClazz,elementClazzes);
    }
    public static <T> T deserialize(String json, JavaType type){return OldBridge.deserialize(json,type);}

    public static ObjectMapper getObjectMapper() {
        return OldBridge.getObjectMapper();
    }
}
