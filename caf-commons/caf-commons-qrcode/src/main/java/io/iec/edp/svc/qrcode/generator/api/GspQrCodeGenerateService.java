/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.svc.qrcode.generator.api;

import com.google.zxing.WriterException;

import java.awt.*;
import java.io.IOException;

@Deprecated
public interface GspQrCodeGenerateService {
    //默认大小，长宽为200
    byte[] generateQrCodeImage(String code) throws IOException, WriterException;

    byte[] generateQrCodeImageWithSize(String code, int width, int height) throws IOException, WriterException;

    byte[] generateQrCodeImageWithLogo(String code, byte[] image) throws IOException, WriterException;

    byte[] generateQrCodeImageWithLogoAndSize(String code, byte[] image, int width, int height) throws IOException, WriterException;

    byte[] generateQrCodeImageWithColor(String code, Color color) throws IOException, WriterException;

    byte[] generateQrCodeImageWithSizeAndColor(String code, int width, int height, Color color) throws IOException, WriterException;

    byte[] generateQrCodeImageWithLogoAndColor(String code, byte[] image, Color color) throws IOException, WriterException;

    byte[] generateQrCodeImageWithLogoAndSizeAndColor(String code, byte[] image, int width, int height, Color color) throws IOException, WriterException;
}
