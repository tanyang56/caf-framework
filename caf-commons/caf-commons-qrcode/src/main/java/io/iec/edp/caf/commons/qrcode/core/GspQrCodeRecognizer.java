/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.qrcode.core;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

/**
 * 二维码识别器
 *
 * @author Leon Huo
 */
public class GspQrCodeRecognizer {
    //根据传入的文件byte[]识别二维码
    public String recognizeQrCode(byte[] content) throws NotFoundException, IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(content);    //将b作为输入流；
        BufferedImage image = ImageIO.read(inputStream);

        return recognizeContentFromImage(image);
    }

    //根据传入的文件对象识别二维码
    public String recognizeQrCodeFile(File file) throws NotFoundException, IOException {
        BufferedImage image = ImageIO.read(file);
        return recognizeContentFromImage(image);
    }

    //识别图片中的二维码信息
    private String recognizeContentFromImage(BufferedImage image) throws NotFoundException {
        MultiFormatReader formatReader = new MultiFormatReader();

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));

        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        Result result = formatReader.decode(binaryBitmap, hints);
        return result.toString();

    }
}
