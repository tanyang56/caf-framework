/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.svc.qrcode.generator.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import io.iec.edp.svc.qrcode.generator.api.GspQrCodeGenerateService;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GspQrCodeGenerateServiceImpl implements GspQrCodeGenerateService {

    public GspQrCodeGenerateServiceImpl() {

    }

    private static final int Background = 0xFFEE82EE;

    @Override
    public byte[] generateQrCodeImage(String code) throws IOException, WriterException {
        return generateQrCodeImageWithSize(code, 200, 200);
    }

    @Override
    public byte[] generateQrCodeImageWithSize(String code, int width, int height) throws IOException, WriterException {
        return generateQrCodeImageWithSizeAndColor(code, width, height, Color.BLACK);
    }

    private BitMatrix generateQrCode(String code, int width, int height) throws WriterException {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        Map hints = new HashMap();
        //设置UTF-8， 防止中文乱码
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        //设置二维码四周白色区域的大小
        hints.put(EncodeHintType.MARGIN, 0);
        //设置二维码的容错性
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        //画二维码，记得调用multiFormatWriter.encode()时最后要带上hints参数，不然上面设置无效
        return multiFormatWriter.encode(code, BarcodeFormat.QR_CODE, width, height, hints);
    }

    @Override
    public byte[] generateQrCodeImageWithLogo(String code, byte[] image) throws IOException, WriterException {
        return generateQrCodeImageWithLogoAndSize(code, image, 200, 200);
    }

    @Override
    public byte[] generateQrCodeImageWithLogoAndSize(String code, byte[] image, int width, int height) throws IOException, WriterException {

        BitMatrix bitMatrix = generateQrCode(code, width, height);
        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(-16777215, -1);
        BufferedImage matrixImage = MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig);
        Graphics2D g2 = matrixImage.createGraphics();
        int matrixWidth = matrixImage.getWidth();
        int matrixHeight = matrixImage.getHeight();
        BufferedImage logo = ImageIO.read(new ByteArrayInputStream(image));
        g2.drawImage(logo, matrixWidth / 5 * 2, matrixHeight / 5 * 2, matrixWidth / 5, matrixHeight / 5, null);
        BasicStroke stroke = new BasicStroke(5.0F, 1, 1);
        g2.setStroke(stroke);
        RoundRectangle2D.Float round = new RoundRectangle2D.Float((matrixWidth / 5 * 2), (matrixHeight / 5 * 2), (matrixWidth / 5), (matrixHeight / 5), 20.0F, 20.0F);
        g2.setColor(Color.white);
        g2.draw(round);
        BasicStroke stroke2 = new BasicStroke(1.0F, 1, 1);
        g2.setStroke(stroke2);
        RoundRectangle2D.Float round2 = new RoundRectangle2D.Float((matrixWidth / 5 * 2 + 2), (matrixHeight / 5 * 2 + 2), (matrixWidth / 5 - 4), (matrixHeight / 5 - 4), 20.0F, 20.0F);
        g2.setColor(new Color(128, 128, 128));
        g2.draw(round2);
        g2.dispose();
        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        ImageIO.write(matrixImage, "png", pngOutputStream);
        matrixImage.flush();
        return pngOutputStream.toByteArray();

        //return generateQrCodeImageWithLogoAndSizeAndColor(code, image, width, height, Color.BLACK);
    }

    @Override
    public byte[] generateQrCodeImageWithColor(String code, Color color) throws IOException, WriterException {
        return generateQrCodeImageWithSizeAndColor(code, 200, 200, color);
    }

    @Override
    public byte[] generateQrCodeImageWithSizeAndColor(String code, int width, int height, Color color) throws IOException, WriterException {
        BitMatrix bitMatrix = generateQrCode(code, width, height);

        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(color.getRGB(), 0xFFFFFFFF);

        BufferedImage matrixImage = MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig);

        //开始画二维码
        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        ImageIO.write(matrixImage, "png", pngOutputStream);

        matrixImage.flush();

        //MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
        return pngOutputStream.toByteArray();
    }

    @Override
    public byte[] generateQrCodeImageWithLogoAndColor(String code, byte[] image, Color color) throws IOException, WriterException {
        return generateQrCodeImageWithLogoAndSizeAndColor(code, image, 200, 200, color);
    }

    @Override
    public byte[] generateQrCodeImageWithLogoAndSizeAndColor(String code, byte[] image, int width, int height, Color color) throws IOException, WriterException {
        BitMatrix bitMatrix = generateQrCode(code, width, height);

        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(color.getRGB(), 0xFFFFFFFF);

        BufferedImage matrixImage = MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig);

        Graphics2D g2 = matrixImage.createGraphics();

        int matrixWidth = matrixImage.getWidth();
        int matrixHeight = matrixImage.getHeight();

        BufferedImage logo = ImageIO.read(new ByteArrayInputStream(image));

        g2.drawImage(logo, matrixWidth / 5 * 2, matrixHeight / 5* 2, matrixWidth / 5, matrixHeight / 5, null);//绘制
        BasicStroke stroke = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g2.setStroke(stroke);// 设置笔画对象
        //指定弧度的圆角矩形
        RoundRectangle2D.Float round = new RoundRectangle2D.Float(matrixWidth / 5 * 2, matrixHeight / 5 * 2, matrixWidth / 5, matrixHeight / 5, 20, 20);
        g2.setColor(Color.white);
        g2.draw(round);// 绘制圆弧矩形

        //设置logo 有一道灰色边框
        BasicStroke stroke2 = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g2.setStroke(stroke2);// 设置笔画对象
        RoundRectangle2D.Float round2 = new RoundRectangle2D.Float(matrixWidth / 5 * 2 + 2, matrixHeight / 5 * 2 + 2, matrixWidth / 5 - 4, matrixHeight / 5 - 4, 20, 20);
        g2.setColor(new Color(128, 128, 128));
        g2.draw(round2);// 绘制圆弧矩形

        g2.dispose();

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        ImageIO.write(matrixImage, "png", pngOutputStream);
        matrixImage.flush();
        return pngOutputStream.toByteArray();
    }
}
