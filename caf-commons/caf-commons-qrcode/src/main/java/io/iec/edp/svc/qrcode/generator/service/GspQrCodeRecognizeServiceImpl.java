/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.svc.qrcode.generator.service;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import io.iec.edp.svc.qrcode.generator.api.GspQrCodeRecognizeService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

public class GspQrCodeRecognizeServiceImpl implements GspQrCodeRecognizeService {
    public GspQrCodeRecognizeServiceImpl() {
    }

    @Override
    public String recognizeQrCode(byte[] content) throws NotFoundException, IOException {
        MultiFormatReader formatReader = new MultiFormatReader();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(content);    //将b作为输入流；

        BufferedImage image = ImageIO.read(inputStream);

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
        //PlanarYUVLuminanceSource source=CameraManager.get().buildLuminanceSource(data,width,height);

        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        Result result = formatReader.decode(binaryBitmap, hints);
        return result.toString();
    }

    @Override
    public String recognizeQrCodeFile(File file) throws IOException, NotFoundException {
        MultiFormatReader formatReader = new MultiFormatReader();
        BufferedImage image = ImageIO.read(file);

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
        //PlanarYUVLuminanceSource source=CameraManager.get().buildLuminanceSource(data,width,height);

        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        Result result = formatReader.decode(binaryBitmap, hints);
        return result.toString();
    }
}
