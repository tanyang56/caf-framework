//package io.iec.edp.caf.commons.runtime;
//
//import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
//import org.springframework.context.ApplicationListener;
//import org.springframework.core.Ordered;
//
///**
// * 应用程序事件监听，以获取Environment
// */
//public class EnvironmentPreparedListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent>, Ordered {
//    @Override
//    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
//        CafEnvironment.setEnvironment(event.getEnvironment());
//    }
//
//    @Override
//    public int getOrder() {
//        return 1;
//    }
//}
