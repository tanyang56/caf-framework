/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.msu.entities;

import io.iec.edp.caf.commons.runtime.msu.enums.MsuCommonType;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 新的su描述文件
 */
@Data
public class ServiceUnitYaml {
    //SU描述格式（Scheme）的版本
    String api;

    //申明当前描述的是一个SU的基本信息
    String kind;

    //su信息
    MsuDescription metadata;

    //依赖关系信息
    MsuSpecification spec;

    //转为简单实体
    public MsuCommonInfo resolveMsuCommonInfo(){
        MsuCommonInfo msuCommonInfo = new MsuCommonInfo();
        msuCommonInfo.setPath(resolvePath());
        msuCommonInfo.setName(resolveName());
        msuCommonInfo.setNameChs(resolveNameChs());
        msuCommonInfo.setApplicationName(resolveApplicationName());
        msuCommonInfo.setApplicationNameChs(resolveApplicationNameChs());
        msuCommonInfo.setMsuCommonType(MsuCommonType.Biz);
        msuCommonInfo.setType("Biz");
        msuCommonInfo.setDependency(resolveDependency());

        return msuCommonInfo;
    }

    @Data
    public static class MsuSpecification {
        //所属App的信息
        ApplicationInfo application;

        //SU在安装盘的路径
        String path;

        Map<String,String> runtime;

        //必须依赖关系信息，不依赖无法启动
        List<MsuDescription> dependencies;
    }

    @Data
    public static  class ApplicationInfo {
        //SU所属的app
        String name;

        //SU所属的app显示名
        String alias;

        String version;
    }

    public String resolvePath(){
        return spec==null? null:spec.path;
    }

    public String resolveName(){
        return metadata==null?null:metadata.name;
    }
    public String resolveNameChs(){
        if(metadata==null||metadata.annotations==null) return null;
        else return metadata.annotations.get(MsuDescription.alias);
    }
    public String resolveApplicationName(){
        return (spec==null||spec.application==null)?null:spec.application.name;
    }
    public String resolveApplicationNameChs(){
        return (spec==null||spec.application==null)?null:spec.application.alias;
    }
    public List<String> resolveDependency(){
        List<String> dependency = new ArrayList<>();
        if(spec!=null&&spec.dependencies!=null){
            spec.dependencies.forEach(depen->{
                if(depen.name!=null) dependency.add(depen.name);
            });
        }
        return dependency;
    }
}
