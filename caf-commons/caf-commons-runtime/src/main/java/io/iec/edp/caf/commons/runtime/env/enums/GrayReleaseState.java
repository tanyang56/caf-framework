/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.env.enums;

/**
 * prepare 更新准备
 * update 应用更新
 * starting 服务启动中
 * finished 完成
 */
public enum GrayReleaseState {
    NORMAL(0),

    PREPARE_A(1),
    UPDATE_A(2),
    STARTING_A(3),
    FINISHED_A(4),

    PREPARE_B(11),
    UPDATE_B(12),
    STARTING_B(13),
    FINISHED_B(14),
    UNKNOWN(99);

    GrayReleaseState(int i) {

    }

    GrayReleaseState() {

    }

    /**
     * 检查状态更迭是否符合规范
     * @param current
     * @param target
     * @return
     */
    public static boolean changeEnable(GrayReleaseState current, GrayReleaseState target){
        //NORMAL状态仅支持更改为UPDATE状态
        if(current == NORMAL){
            return target == UPDATE_A || target == UPDATE_B;
        }else if(current == UPDATE_A||current == STARTING_A||current == FINISHED_A){
            //A状态仅支持更改为A
            return target == STARTING_A || target == FINISHED_A || target == UPDATE_A;
        }else if(current == UPDATE_B||current == STARTING_B||current == FINISHED_B){
            //B状态仅支持更改为B
            return target == STARTING_B || target == FINISHED_B || target == UPDATE_A;
        }
        return true;
    }
}
