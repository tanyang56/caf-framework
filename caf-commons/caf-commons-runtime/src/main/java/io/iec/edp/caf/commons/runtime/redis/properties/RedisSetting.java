package io.iec.edp.caf.commons.runtime.redis.properties;

import io.iec.edp.caf.commons.runtime.redis.enums.RedisMode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.connection.RedisNode;

import java.util.Set;

/**
 * redis配置信息
 *
 * @author manwenxing01
 * @date 2023-02-21
 */
@Setter
@Getter
public class RedisSetting {

    //redis连接模式
    private RedisMode mode;

    //集群模式节点
    private Set<RedisNode> clusterNodes;

    //哨兵模式master
    private String master;

    //最大转发数量
    private int maxRedirects = 5;

    //ipAddress
    private String host;

    //端口
    private int port;

    //密码
    private String password;

    //数据库
    private int database = 0;

}
