/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.environment.enums;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 应用版本:
 *
 * @author guowenchang
 */
public enum AppEdition {
    /**
     * 精简版/单机版
     */
    Compact("compact"),
    /**
     * 标准版-文件部署
     */
    StandardFile("standard-file"),
    /**
     * 标准版-文件部署-高可用
     */
    StandardFileHA("standard-file-ha"),
    /**
     * 标准版-镜像部署
     */
    StandardDocker("standard-docker"),
    /**
     * 分布式-文件部署
     */
    DistributedFile("distributed-file"),
    /**
     * 分布式-镜像部署
     */
    DistributedDocker("distributed-docker"),
    /**
     * 分布式-镜像部署-高可用
     */
    DistributedDockerHA("distributed-docker-ha"),

    /**
     * insuite集成模式
     */
    InSuite("InSuite");

    private final String name;

    private static final Logger logger = LoggerFactory.getLogger(AppEdition.class);

    AppEdition(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static AppEdition getMode(String s) {
        if (s == null) {
            s = "";
        }

        for (AppEdition edition : AppEdition.values()) {
            if (edition.getName().equalsIgnoreCase(s)) {
                return edition;
            }
        }

        logger.warn(String.format("未找到%s对应的部署模式 默认使用标准版模式启动", s));

        return AppEdition.StandardFile;
    }
}
