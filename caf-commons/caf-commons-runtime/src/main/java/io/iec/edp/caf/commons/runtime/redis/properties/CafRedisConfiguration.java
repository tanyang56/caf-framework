package io.iec.edp.caf.commons.runtime.redis.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * CAF redis配置集合（环境中使用redis配置的集合）
 *
 * @author manwenxing01
 * @date 2023-02-21
 */
@ConfigurationProperties(prefix = "spring.redis")
@Getter
@Setter
public class CafRedisConfiguration {

    //配置集合
    private Map<String, RedisSetting> independence;

    /**
     * 根据name获取redis配置
     *
     * @param name 缓存节点name
     */
    public RedisSetting get(String name) {
        return this.independence != null ? this.independence.get(name) : null;
    }

}
