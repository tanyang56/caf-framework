/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.msu.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.iec.edp.caf.commons.runtime.msu.enums.MsuCommonType;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 后续msu的定义更新在此进行
 */
@Data
public class MsuCommonInfo {

    @JsonProperty(value = "ApplicationName")
    private String applicationName;

    @JsonProperty(value = "ApplicationNameAlias")
    private String applicationNameChs;

    @JsonProperty(value = "ServiceUnitName")
    private String name;

    @JsonProperty(value = "ServiceUnitNameAlias")
    private String nameChs;

    @JsonProperty(value = "Type")
    private String type;

    @JsonProperty(value = "ApplicationPath")
    private String path;

    private List<String> dependency;

    private String serviceUnitDes;

    @JsonProperty(value = "MsuType")
    private MsuCommonType msuCommonType;

    public MsuCommonInfo(){

    }

    //将子类转为父类
    public MsuCommonInfo(MsuCommonInfo commonInfo){
        this.applicationName = commonInfo.applicationName;
        this.applicationNameChs = commonInfo.applicationNameChs;
        this.name = commonInfo.getName();
        this.nameChs = commonInfo.getNameChs();
        this.type = commonInfo.getType();
        this.path = commonInfo.getPath();
        this.dependency = commonInfo.getDependency();
        this.serviceUnitDes = commonInfo.getServiceUnitDes();
        this.msuCommonType = commonInfo.getMsuCommonType();
    }

    public ServiceUnitYaml convertToYaml(){
        ServiceUnitYaml serviceUnitYaml = new ServiceUnitYaml();

        //关键应用信息
        ServiceUnitYaml.ApplicationInfo applicationInfo = new ServiceUnitYaml.ApplicationInfo();
        applicationInfo.setName(this.applicationName);
        applicationInfo.setAlias(this.applicationNameChs);
        applicationInfo.setVersion("latest");

        //依赖信息
        Map<String,String> runtime = new HashMap<>();
        runtime.put("type",null);
        runtime.put("version","latest");
        ServiceUnitYaml.MsuSpecification msuSpecification = new ServiceUnitYaml.MsuSpecification();
        msuSpecification.setPath(this.path);
        msuSpecification.setApplication(applicationInfo);
        msuSpecification.setRuntime(runtime);
        List<MsuDescription> msuDescriptions = new ArrayList<>();
        if(this.dependency!=null){
            this.dependency.forEach(dependent->{
                MsuDescription msuDescription = new MsuDescription();
                msuDescription.setGroup("com.inspures");
                msuDescription.setName(dependent);
                msuDescription.setVersion("latest");
                msuDescriptions.add(msuDescription);
            });
        }
        msuSpecification.setDependencies(msuDescriptions);

        //su信息
        MsuDescription msuDescription = new MsuDescription();
        msuDescription.setName(this.name);
        Map<String,String> annotations = new HashMap<>();
        annotations.put("caf.inspures.com/alias",nameChs);
        annotations.put("caf.inspures.com/group","com.inspures.gs-cloud");
        annotations.put("caf.inspures.com/version","latest");
        msuDescription.setAnnotations(annotations);

        //最终实体
        serviceUnitYaml.setSpec(msuSpecification);
        serviceUnitYaml.setMetadata(msuDescription);
        serviceUnitYaml.setApi("caf.inspures.com/v1alpha1");
        serviceUnitYaml.setKind("ServiceUnit");

        return serviceUnitYaml;
    }
}
