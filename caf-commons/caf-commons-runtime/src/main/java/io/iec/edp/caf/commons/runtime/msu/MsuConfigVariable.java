/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.msu;

public class MsuConfigVariable {

    static public final String SU_JSON_FILE = "ServiceUnit.json";
    static public final String SU_YAML_FILE = "service-unit.yaml";
    static public final String SU_YML_FILE = "service-unit.yml";

    static public final String SU_CHILD_DEPENDENT = "child";
    static public final String SU_PARENT_DEPENDENT = "parent";

    //serviceunit.json
    //static final public String SU_JSON_SU_NAME = "ServiceUnitName";

    //caf_serviceunit.json
    static final public String CAF_JSON = "DeploymentConfiguration";
    static final public String CAF_JSON_STRATEGY = "Strategy";
    static final public String CAF_JSON_SU = "ServiceUnits";
    static final public String CAF_JSON_SU_NAME = "Name";
    static final public String CAF_JSON_SU_ENABLE = "Enabled";

    //application.yaml
    static final public String CAF_YAML = "msu-config";
    static final public String CAF_YAML_STRATEGY = "strategy";
    static final public String CAF_YAML_SU = "serviceunits";

}
