/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.app.manager.classloader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.loader.LaunchedURLClassLoader;
import sun.misc.URLClassPath;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 自定义主类加载器
 *
 * @author Leon Huo
 * @Date: 2021/5/20
 */
@Slf4j
public class CAFClassLoader extends LaunchedURLClassLoader {
    /*
     需要将此类加载器注册为并行类加载器 否则在非双亲委派模型下可能会造成死锁
    */
    static {
        ClassLoader.registerAsParallelCapable();
    }

    private final Map<String, URLClassPath> appClasspath = new HashMap<>();
    private final Lock lock = new ReentrantLock();
    private final ClassManager classManager = new ClassManager();
    private final Set<String> classesNotFound = new HashSet<>();
    private final boolean useJRebel = "true".equals(System.getProperty("loader.useJRebel"));
    //创建线程池
    private final ExecutorService threadPool = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
            60L, TimeUnit.SECONDS,
            new SynchronousQueue<Runnable>());

    public CAFClassLoader(URLClassLoader classLoader) {
        super(classLoader.getURLs(), classLoader);
    }

    //构造并注册
    public CAFClassLoader(URLClassLoader classloader, Map<String, String[]> appInfo) {
        this(classloader);
        for (Map.Entry<String, String[]> entry : appInfo.entrySet()) {
            this.register(entry.getKey(), entry.getValue());
        }
    }

    //注册一个应用
    public void register(String appName, String[] appPaths) {
        appName = toLowerCase(appName);

        try {
            lock.lock();
            if (!appClasspath.containsKey(appName)) {
                //获取当前su下所有的jar的url
                URL[] urls = getJarPathFromDirPaths(appPaths);
                URLClassPath urlClassPath = new URLClassPath(urls);
                appClasspath.put(appName, urlClassPath);
                for (URL url : urls) {
                    //注册当前su下所有的jar的url到LanuchURLClassLoader的Ucp里
                    super.addURL(url);
                }
            } else {
                throw new RuntimeException(appName + " has been registered");
            }

        } finally {
            lock.unlock();
        }
    }

    /**
     * 返回给定目录下所有的jar包URL
     * 只支持绝对路径
     */
    private URL[] getJarPathFromDirPaths(String[] paths) {
        try {
            List<URL> urlList = new ArrayList<>();
            for (String path : paths) {
                String root = cleanupPath(handleUrl(path));
                //判断是否绝对路径
                if (!(root.contains(":") || root.startsWith("/"))) {
                    throw new Exception("Unsupported URL:" + root);
                }

                File file = new File(root);
                if (file.isDirectory()) {
                    urlList.add(file.toURI().toURL());
                    urlList.addAll(getClassFromPath(file));
                } else if (file.getName().endsWith(".jar")) {
                    urlList.add(file.toURI().toURL());
                }
            }

            return new ArrayList<URL>(new HashSet<URL>(urlList)).toArray(new URL[0]);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("internal error");
        }
    }

    private static List<URL> getClassFromPath(File rootFile) throws MalformedURLException {
        List<URL> result = new ArrayList<>();
        if (rootFile.listFiles() == null) {
            log.error("Something is wrong with this directory,the directory path is:" + rootFile.getAbsolutePath());
        }
        for (File file : rootFile.listFiles()) {
            if (file.isDirectory()) {
                result.addAll(getClassFromPath(file));
            } else if (file.getName().toLowerCase().endsWith(".jar")) {
                result.add(file.toURI().toURL());
            }
        }
        return result;
    }

    private String cleanupPath(String path) {
        path = path.trim();
        // No need for current dir path
        if (path.startsWith("./")) {
            path = path.substring(2);
        }
        String lowerCasePath = path.toLowerCase(Locale.ENGLISH);
        if (lowerCasePath.endsWith(".jar") || lowerCasePath.endsWith(".zip")) {
            return path;
        }
        if (path.endsWith("/*")) {
            path = path.substring(0, path.length() - 1);
        } else {
            // It's a directory
            if (!path.endsWith("/") && !path.equals(".")) {
                path = path + "/";
            }
        }
        return path;
    }


    private String handleUrl(String path) throws UnsupportedEncodingException {
        if (path.startsWith("jar:file:") || path.startsWith("file:")) {
            path = URLDecoder.decode(path, "UTF-8");
            if (path.startsWith("file:")) {
                path = path.substring("file:".length());
                if (path.startsWith("//")) {
                    path = path.substring(2);
                }
            }
        }
        return path;
    }


    //注销一个应用
    //todo 此部分还需要更多验证 目前不推荐使用
    @Deprecated
    public void unregister(String appName) {
    }

    private String toLowerCase(String s) {
        return s.toLowerCase();
    }


    /**
     * 重写findClass方法 将未被父类加载器加载过的请求下发给子类加载器执行
     *
     * @throws ClassNotFoundException
     */
    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        //被spring增强或spring 通知类型的类永远不会加载到，增加缓存直接返回，避免扫描
        if (!useJRebel && (name.contains("$$Enhance") || this.classesNotFound.contains(name))) {
            throw new ClassNotFoundException(name);
        }

        Class<?> clazz;

        try {
            clazz = super.findClass(name);
            if (clazz != null)
                registerClass(name, clazz, appClasspath);
        } catch (ClassNotFoundException ex) {
            if (!useJRebel) {
                this.classesNotFound.add(name);
            }
            throw ex;
        }

        if (clazz == null) {
            throw new ClassNotFoundException(name);
        }

        return clazz;
    }

    public ClassManager getClassManager() {
        return classManager;
    }

    private void registerClass(String name, Class clazz, Map<String, URLClassPath> appClasspath) {
        //获取异步Future对象
        threadPool.submit((Callable<Void>) () -> {
            if (clazz != null) {
                String className = name.replace('.', '/').concat(".class");
                String appName = classManager.getAppName(className);
                if (appName == null) {
                    for (Map.Entry<String, URLClassPath> entry : appClasspath.entrySet()) {
                        if (entry.getValue().getResource(className) != null) {
                            appName = entry.getKey();
                            break;
                        }
                    }
                }
                if (appName != null) {
                    classManager.registryClass(name, clazz, appName);
                }
            }
            return null;
        });
    }

    public URLClassPath getSuClasspath(String suName) {
        return appClasspath.get(suName);
    }
}
