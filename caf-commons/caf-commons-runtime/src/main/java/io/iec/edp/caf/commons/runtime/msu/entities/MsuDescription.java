/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.msu.entities;

import lombok.Data;

import java.util.Map;

/**
 * yaml中对su的描述
 */
@Data
public class MsuDescription {
    public static final String alias = "caf.inspures.com/alias";
    public static final String annotationGroup = "caf.inspures.com/group";
    public static final String annotationVersion = "caf.inspures.com/version";

    /**
     * K8s需要的标签
     */
    Map<String,String> annotations;

    String group;

    //SU的名字/code
    String name;

    String version;

}
