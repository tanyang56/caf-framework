/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.runtime.env;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.iec.edp.caf.commons.runtime.FileOperator;
import io.iec.edp.caf.commons.runtime.env.entities.EnvironmentState;
import io.iec.edp.caf.commons.runtime.env.enums.GrayReleaseState;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;

@Slf4j
public class EnvironmentStateOperator {

    private static final String envStateFileName = "environment_state.json";

    //序列化工具
    private static final ObjectMapper objectMapper = new ObjectMapper();

    //获取配置
    public static EnvironmentState getCurrentEnvironmentState(String serverPath) {
        try{
            String filePath = serverPath + File.separator + "config" +
                    File.separator + "runtime" + File.separator + envStateFileName;
            String stateJson = FileOperator.readToBuffer(filePath);
            EnvironmentState currentEnvironmentState = null;
            if(stateJson!=null) currentEnvironmentState = objectMapper.readValue(stateJson,EnvironmentState.class);
            //检查获取的信息结果，不符合规范则初始化并重新生成该配置文件
            if(!checkEnvironmentStateIllegal(currentEnvironmentState)){
                if(currentEnvironmentState == null){
                    currentEnvironmentState = new EnvironmentState();
                    currentEnvironmentState.setGrayReleaseState(GrayReleaseState.NORMAL);
                }
                if(currentEnvironmentState.getGrayReleaseState()==null){
                    currentEnvironmentState.setGrayReleaseState(GrayReleaseState.NORMAL);
                }
                updateEnvironmentState(currentEnvironmentState,filePath);
            }
            return currentEnvironmentState;
        }catch (Exception e){
            throw new RuntimeException("",e);
        }

    }

    public static void updateCurrentEnvironmentState(EnvironmentState currentEnvironmentState,String serverPath){
        String filePath = serverPath + File.separator + "config" +
                File.separator + "runtime" + File.separator + envStateFileName;
        updateEnvironmentState(currentEnvironmentState,filePath);
    }

    //保存配置
    private static void updateEnvironmentState(EnvironmentState environmentState,String filePath){
        try{
            String saveVal = objectMapper.writeValueAsString(environmentState);
            FileOperator.saveNewFile(saveVal,filePath);
        }catch (Exception e){
            throw new RuntimeException("",e);
        }
    }

    /**
     * 检查配置信息是否规范
     * @param currentEnvironmentState 当前环境状态
     */
    private static boolean checkEnvironmentStateIllegal(EnvironmentState currentEnvironmentState){
        return currentEnvironmentState != null
                && currentEnvironmentState.getGrayReleaseState() != null;
    }

    static {
        //忽略 在json字符串中存在，但是在java对象中不存在对应属性的情况。防止错误
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    }
}
