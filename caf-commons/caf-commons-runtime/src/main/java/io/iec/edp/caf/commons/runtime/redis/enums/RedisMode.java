package io.iec.edp.caf.commons.runtime.redis.enums;

/**
 * redis模式
 *
 * @author manwenxing01
 */
public enum RedisMode {

    STANDALONE("standalone", "单体模式"),
    SENTINEL("sentinel", "哨兵模式"),
    CLUSTER("cluster", "集群模式");

    private String label;
    private String description;

    RedisMode(String label, String description) {
        this.label = label;
        this.description = description;
    }

}
