/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.environment;

import io.iec.edp.caf.common.environment.enums.AppEdition;
import io.iec.edp.caf.commons.runtime.CafEnvironment;

/**
 * 应用版本:
 *
 * @author guowenchang
 * @date 2020-08-31
 */
@Deprecated
public class EnvironmentUtil {


    public static AppEdition getAppEdition() {
        return CafEnvironment.getAppEdition();
    }

    public static String getServerRuntimePathName() {
        return CafEnvironment.getServerRuntimePathName();
    }


    /**
     * 获取启动类所在的目录(runtime)
     *
     * @return -
     */
    public static String getStartupPath() {
        return CafEnvironment.getStartupPath();
    }

    /**
     * 获取盘目录
     *
     * @return -
     */
    public static String getBasePath() {
        return CafEnvironment.getBasePath();
    }

    /**
     * 获取主应用目录(jstack)
     *
     * @return -
     */
    public static String getServerRTPath() {
        return CafEnvironment.getServerRTPath();
    }

    public static String getStringProperty(String property) {
        return CafEnvironment.getEnvironment().getProperty(property);
    }

    public static String getStringProperty(String property, String defaultValue) {
        return CafEnvironment.getEnvironment().getProperty(property, defaultValue);
    }


    public static <T> T getProperty(String property, Class<T> tClass) {
        return CafEnvironment.getEnvironment().getProperty(property, tClass);
    }

    public static <T> T getProperty(String property, Class<T> tClass, T defaultValue) {
        return CafEnvironment.getEnvironment().getProperty(property, tClass, defaultValue);
    }

    /**
     * 返回Port
     */
    public static Integer getPort() {
        return CafEnvironment.getPort();
    }
}
