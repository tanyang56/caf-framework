/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.core.api;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Serialization接口
 * 该接口用来获取序列化和反序列化器
 */
public interface CafSerialization {
    /**
     * 序列化器的标识
     * 通过TypeConstants常量获取
     * @return
     */
    byte getId();
    /**
     * 获取序列化器
     * @return
     */
    DataSerializer getSerializer();

    /**
     * 获取反序列化器
     * @return
     */
    DataSerializer getDerializer();

    /**
     * 获取带outputstream的序列化器
     * @param stream
     * @return
     */
    DataSerializer getSerializer(OutputStream stream);

    /**
     * 获取带inputstream的序列化器
     * @param stream
     * @return
     */
    DataSerializer getDerializer(InputStream stream);
}
