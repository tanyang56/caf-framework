/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.core.api;

import com.fasterxml.jackson.databind.JavaType;

import java.io.IOException;

/**
 * 序列化接口
 */
public interface DataSerializer {
    /**
     * 序列化到流
     * @param object
     */
    void serializeToStream(Object object);

    /**
     * 序列化到字符串
     * @param object
     * @return
     */
    String serializeToString(Object object);

    /**
     * 序列化到Byte数组
     * @param object
     * @return
     */
    byte[] serializeToByte(Object object);
    /**
     * 从流里反序列化
     * @param clazz 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(Class<T> clazz) throws IOException;
    /**
     * 从流里反序列化
     * @param type 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(JavaType type) throws IOException;
    /**
     * 从流里反序列化到泛型集合类型
     * @param collectionClazz 集合类型
     * @param elementClazzes 集合里泛型
     * @param <T> 返回值类型
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(Class<?> collectionClazz, Class<?>... elementClazzes) throws IOException;

    /**
     * 从字符串反序列化
     * @param value 反序列化值
     * @param clazz 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(String value, Class<T> clazz);

    /**
     * 从字符串反序列化
     * @param value 反序列化值
     * @param type 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(String value, JavaType type);

    /**
     * 从字符串反序列化到泛型集合类型
     * @param value 反序列化的值
     * @param collectionClazz 集合类型
     * @param elementClazzes 集合里泛型
     * @param <T> 返回值类型
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(String value, Class<?> collectionClazz, Class<?>... elementClazzes);
    /**
     * 从byte数组反序列化
     * @param value 反序列化值
     * @param clazz 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(byte[] value, Class<T> clazz);

    /**
     * 从字符串反序列化
     * @param value 反序列化值
     * @param type 返回的类型
     * @param <T> 返回值类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(byte[] value, JavaType type);

    /**
     * 从byte数组反序列化到泛型集合类型
     * @param value 反序列化的值
     * @param collectionClazz 集合类型
     * @param elementClazzes 集合里泛型
     * @param <T> 返回值类型
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    <T> T deserialize(byte[] value, Class<?> collectionClazz, Class<?>... elementClazzes);

    /**
     * 获取ObjectMapper
     * @return
     */
    Object getObjectMapper();
}
