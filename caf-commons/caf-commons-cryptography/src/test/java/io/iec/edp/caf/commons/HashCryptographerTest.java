/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.commons;

import io.iec.edp.caf.commons.cryptography.HashCryptographer;
import io.iec.edp.caf.commons.cryptography.enums.HashAlgorithmType;
import io.iec.edp.caf.commons.cryptography.generator.RandomSaltGenerator;
import io.iec.edp.caf.commons.cryptography.generator.StringFixedSaltGenerator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import io.iec.edp.caf.commons.utils.ByteUtils;

import java.nio.charset.StandardCharsets;

/**
 * 散列工具类单元测试
 *
 * @author guowenchang
 * @date 2020-03-10
 */
public class HashCryptographerTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private String plaintext = "哈希测试 浪潮国际";

    @Test
    public void sha256Test() {
        HashCryptographer cryptographer = HashCryptographer.getInstance(HashAlgorithmType.SHA256);
        String result = cryptographer.computeHash(plaintext);
        logger.info(result);
        org.junit.Assert.assertNotNull(result);
    }

    @Test
    public void sha512Test() {
        HashCryptographer cryptographer = HashCryptographer.getInstance(HashAlgorithmType.SHA512);
        String result = cryptographer.computeHash(plaintext);
        logger.info(result);
        Assert.notNull(result,"failed");
    }

    @Test
    public void sm3Test() {
        HashCryptographer cryptographer = HashCryptographer.getInstance(HashAlgorithmType.SM3);
        String result = cryptographer.computeHash(plaintext);
        logger.info(result);
        Assert.notNull(result,"failed");
    }

    @Test
    public void sha256WithSaltTest() {
        String salt = "salt";
        byte[] saltArray = salt.getBytes(StandardCharsets.UTF_8);
        HashCryptographer cryptographer = HashCryptographer.getInstance(HashAlgorithmType.SM3);
        //字符串方式
        String s1 = cryptographer.computeHash(plaintext, salt);
        //bytes
        byte[] s2 = cryptographer.computeHash(plaintext.getBytes(StandardCharsets.UTF_8), saltArray);
        //校验，将字符串转回bytes，与第二个进行比较
        Assert.isTrue(ByteUtils.equals(s2,ByteUtils.fromHexString(s1)), "test failed");
    }

    @Test
    public void saltTest() {
        byte[] salt1 = new RandomSaltGenerator().generateSalt(8);
        Assert.hasText(ByteUtils.toHexString(salt1),"failed");

        byte[] salt2 = new StringFixedSaltGenerator("hahahahaha").generateSalt(8);
        Assert.hasText(ByteUtils.toHexString(salt1),"failed");
    }
}
