/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.commons;

import io.iec.edp.caf.commons.cryptography.SymmetricCryptographer;
import io.iec.edp.caf.common.enums.SymmetricAlgorithmEnum;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * 加密工具单元测试
 *
 * @author guowenchang
 * @date 2020-03-09
 */
public class SymmetricCryptographerTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Test
    public void tripleDESTest() {
        String originText = "This is a test text";
        String key = "password";

        SymmetricCryptographer symmetricCryptographer = SymmetricCryptographer.getInstance(SymmetricAlgorithmEnum.TripleDES);

        byte[] encryptedBytes = symmetricCryptographer.encrypt(originText.getBytes(), key);
        byte[] decryptedBytes = symmetricCryptographer.decrypt(encryptedBytes, key);
        String decryptedText = new String(decryptedBytes);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));

        String encryptedText = symmetricCryptographer.encrypt(originText, key);
        logger.info(encryptedText);
        Assert.notNull(encryptedText);

        decryptedText = symmetricCryptographer.decrypt(encryptedText, key);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));
    }

    @Test
    public void rc2CryptoTest() {
        String originText = "This is a test text";
        String key = "pass";

        SymmetricCryptographer symmetricCryptographer = SymmetricCryptographer.getInstance(SymmetricAlgorithmEnum.RC2);

        byte[] encryptedBytes = symmetricCryptographer.encrypt(originText.getBytes(), key);
        byte[] decryptedBytes = symmetricCryptographer.decrypt(encryptedBytes, key);
        String decryptedText = new String(decryptedBytes);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));

        String encryptedText = symmetricCryptographer.encrypt(originText, key);
        logger.info(encryptedText);
        Assert.notNull(encryptedText);

        decryptedText = symmetricCryptographer.decrypt(encryptedText, key);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));
    }

    @Test
    public void aesCryptoTest() {
        String originText = "This is a test text";
        String key = "passwordA";

        SymmetricCryptographer symmetricCryptographer = SymmetricCryptographer.getInstance(SymmetricAlgorithmEnum.Rijndael);

        byte[] encryptedBytes = symmetricCryptographer.encrypt(originText.getBytes(), key);
        byte[] decryptedBytes = symmetricCryptographer.decrypt(encryptedBytes, key);
        String decryptedText = new String(decryptedBytes);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));

        String encryptedText = symmetricCryptographer.encrypt(originText, key);
        logger.info(encryptedText);
        Assert.notNull(encryptedText);

        decryptedText = symmetricCryptographer.decrypt(encryptedText, key);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));
    }

    @Test
    public void sm4CryptoTest() {
        String originText = "This is a test text";
        String key = "passwordA";

        SymmetricCryptographer symmetricCryptographer = SymmetricCryptographer.getInstance(SymmetricAlgorithmEnum.SM4);

        byte[] encryptedBytes = symmetricCryptographer.encrypt(originText.getBytes(), key);
        byte[] decryptedBytes = symmetricCryptographer.decrypt(encryptedBytes, key);
        String decryptedText = new String(decryptedBytes);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));

        String encryptedText = symmetricCryptographer.encrypt(originText, key);
        logger.info(encryptedText);
        Assert.notNull(encryptedText);

        decryptedText = symmetricCryptographer.decrypt(encryptedText, key);
        logger.info(decryptedText);
        Assert.notNull(decryptedText);
        Assert.isTrue(originText.equals(decryptedText));
    }
}