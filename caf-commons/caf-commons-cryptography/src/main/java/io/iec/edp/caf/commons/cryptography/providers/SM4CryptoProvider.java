/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.providers;

import io.iec.edp.caf.commons.cryptography.service.SymmetricCryptoProvider;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 * 3DES加密服务实现
 *
 * @author guowenchang
 */
public class SM4CryptoProvider implements SymmetricCryptoProvider {

    @Override
    public byte[] encrypt(byte[] byteIn, byte[] key, byte[] vector) {
        try {
            Cipher cipher = Cipher.getInstance("SM4/CBC/NoPadding", BouncyCastleProvider.PROVIDER_NAME);
            Key sm4Key = new SecretKeySpec(key, "SM4");
            cipher.init(Cipher.ENCRYPT_MODE, sm4Key, new IvParameterSpec(vector));
            return cipher.doFinal(byteIn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public byte[] decrypt(byte[] byteIn, byte[] key, byte[] vector) {
        try {
            Cipher cipher = Cipher.getInstance("SM4/CBC/NoPadding", BouncyCastleProvider.PROVIDER_NAME);
            Key sm4Key = new SecretKeySpec(key, "SM4");
            cipher.init(Cipher.DECRYPT_MODE, sm4Key, new IvParameterSpec(vector));
            return cipher.doFinal(byteIn);
            // 真正开始解密操作
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
