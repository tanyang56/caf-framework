/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.cryptography;

import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.crypto.digests.SM3Digest;
import io.iec.edp.caf.commons.utils.ByteUtils;
import java.nio.charset.StandardCharsets;

/**
 * 散列工具类
 *
 * @author guowenchang
 */
@Deprecated
public class HashCryptographer {

    /**
     * SHA1 字节数组
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha1(byte[] data) {
        return DigestUtils.sha1(data);
    }

    /**
     * SHA1 字符串
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha1(String data) {
        return DigestUtils.sha1(data);
    }

    /**
     * SHA1 字节数组
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha1Hex(byte[] data) {
        return DigestUtils.sha1Hex(data);
    }

    /**
     * SHA1 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha1Hex(String data) {
        return DigestUtils.sha1Hex(data);
    }

    /**
     * SHA256 字节数组
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha256(byte[] data) {
        return DigestUtils.sha256(data);
    }

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha256(String data) {
        return DigestUtils.sha256(data);
    }

    /**
     * SHA256 字节数组
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha256Hex(byte[] data) {
        return DigestUtils.sha256Hex(data);
    }

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha256Hex(String data) {
        return DigestUtils.sha256Hex(data);
    }

    /**
     * SHA512 字节数组
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha512(byte[] data) {
        return DigestUtils.sha512(data);
    }

    /**
     * SHA512 字符串
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sha512(String data) {
        return DigestUtils.sha512(data);
    }

    /**
     * SHA512 字节数组
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha512Hex(byte[] data) {
        return DigestUtils.sha512Hex(data);
    }

    /**
     * SHA512 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sha512Hex(String data) {
        return DigestUtils.sha512Hex(data);
    }

    /**
     * SM3 字节数组
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sm3(byte[] data) {
        return computeSM3Hash(data);
    }

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @return 散列值
     */
    public static byte[] sm3(String data) {
        return sm3(data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * SM3 字节数组
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sm3Hex(byte[] data) {
        return ByteUtils.toHexString(sm3(data));
    }

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public static String sm3Hex(String data) {
        return ByteUtils.toHexString(sm3(data));
    }


    /**
     * SHA1 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 散列值
     */
    public static byte[] sha1(byte[] data, byte[] salt) {
        return DigestUtils.sha1(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA1 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 散列值
     */
    public static byte[] sha1(String data, String salt) {
        return DigestUtils.sha1(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA1 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 16进制散列值
     */
    public static String sha1Hex(byte[] data, byte[] salt) {
        return DigestUtils.sha1Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA1 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    public static String sha1Hex(String data, String salt) {
        return DigestUtils.sha1Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA256 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 散列值
     */
    public static byte[] sha256(byte[] data, byte[] salt) {
        return DigestUtils.sha256(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 散列值
     */
    public static byte[] sha256(String data, String salt) {
        return DigestUtils.sha256(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA256 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 16进制散列值
     */
    public static String sha256Hex(byte[] data, byte[] salt) {
        return DigestUtils.sha256Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    public static String sha256Hex(String data, String salt) {
        return DigestUtils.sha256Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA512 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 散列值
     */
    public static byte[] sha512(byte[] data, byte[] salt) {
        return DigestUtils.sha512(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA512 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 散列值
     */
    public static byte[] sha512(String data, String salt) {
        return DigestUtils.sha512(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA512 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 16进制散列值
     */
    public static String sha512Hex(byte[] data, byte[] salt) {
        return DigestUtils.sha512Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SHA512 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    public static String sha512Hex(String data, String salt) {
        return DigestUtils.sha512Hex(mergeDataAndSalt(data, salt));
    }

    /**
     * SM3 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 散列值
     */
    public static byte[] sm3(byte[] data, byte[] salt) {
        return computeSM3Hash(mergeDataAndSalt(data, salt));
    }

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 散列值
     */
    public static byte[] sm3(String data, String salt) {
        return computeSM3Hash(mergeDataAndSalt(data, salt));
    }

    /**
     * SM3 字节数组
     *
     * @param data 输入数据
     * @param salt 盐值 字节数组
     * @return 16进制散列值
     */
    public static String sm3Hex(byte[] data, byte[] salt) {
        return ByteUtils.toHexString(sm3(mergeDataAndSalt(data, salt)));
    }

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    public static String sm3Hex(String data, String salt) {
        return ByteUtils.toHexString(sm3(mergeDataAndSalt(data, salt)));
    }

    /**
     * 国产加密算法SM3实现
     * 这里使用了bcprov-jdk15on-1.59这个jar包
     *
     * @param data 输入数据
     * @return 散列值
     */
    private static byte[] computeSM3Hash(byte[] data) {
        SM3Digest digest = new SM3Digest();
        digest.update(data, 0, data.length);
        byte[] result = new byte[digest.getDigestSize()];
        digest.doFinal(result, 0);
        return result;
    }

    private static byte[] mergeDataAndSalt(byte[] data, byte[] salt) {
        return mergeArray(data, salt);
    }

    private static byte[] mergeDataAndSalt(String data, String salt) {
        return (data + salt).getBytes(StandardCharsets.UTF_8);
    }

    private static byte[] mergeArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }
}
