/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common;

import io.iec.edp.caf.common.enums.AsymmetricAlgorithmEnum;
import io.iec.edp.caf.commons.cryptography.model.CafKeyPair;
import io.iec.edp.caf.commons.cryptography.service.AsymmetricCryptoProvider;
import io.iec.edp.caf.commons.cryptography.providers.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.Base64;

@Deprecated
public class AsymmetricCryptographer {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private AsymmetricCryptoProvider asymmetricCryptographyService;
    private AsymmetricAlgorithmEnum asymmetricAlgorithmEnum;

    /**
     * 私有构造参数
     *
     * @param asymmetricCryptographyService 加密方式
     */
    private AsymmetricCryptographer(AsymmetricAlgorithmEnum asymmetricCryptographyService) {
        this.asymmetricAlgorithmEnum = asymmetricCryptographyService;
        switch (asymmetricCryptographyService) {
            case SM2:
                this.asymmetricCryptographyService = new SM2CryptoProvider();
                break;
            case RSA:
                this.asymmetricCryptographyService = new RSACryptoProvider();
                break;
            case DSA:
                this.asymmetricCryptographyService = new DSACryptoProvider();
                break;

            default:
                throw new RuntimeException("Unknown Cryptographer");
        }
    }

    /**
     * 公有静态构造器, 返回加密工具实例
     *
     * @param asymmetricAlgorithmEnum 加密方式
     * @return 对象实例
     */
    public static AsymmetricCryptographer getInstance(AsymmetricAlgorithmEnum asymmetricAlgorithmEnum) {
        return new AsymmetricCryptographer(asymmetricAlgorithmEnum);
    }

    public CafKeyPair generateKeyPair() {
        return this.asymmetricCryptographyService.generateKeyPair();
    }

    /**
     * 使用公钥对明文加密
     *
     * @param byteIn 明文
     * @param key    公钥
     * @return 密文
     */
    public byte[] encryptWithPub(byte[] byteIn, String key) {
        byteIn = appendBottomZero(byteIn, this.asymmetricAlgorithmEnum.getBlockSize());
        byte[] keyByte = getLegalKey(key);
        return asymmetricCryptographyService.encrypt(byteIn, keyByte,false);
    }

    /**
     * 使用私钥对密文解密
     *
     * @param byteIn 密文
     * @param key    密码
     * @return 明文
     */
    public byte[] decryptWithPri(byte[] byteIn, String key) {
        byte[] keyByte = getLegalKey(key);
        return removeBottomZero(asymmetricCryptographyService.decrypt(byteIn, keyByte,false));
    }

    /**
     * 使用私钥对明文加密
     * 被运行框架的
     * @param byteIn 明文
     * @param key    公钥
     * @return 密文
     */
    public byte[] encryptWithPri(byte[] byteIn, String key) {
        byteIn = appendBottomZero(byteIn, this.asymmetricAlgorithmEnum.getBlockSize());
        byte[] keyByte = getLegalKey(key);
        return asymmetricCryptographyService.encrypt(byteIn, keyByte,true);
    }

    /**
     * 使用公钥对明文解密
     *
     * @param byteIn 密文
     * @param key    密码
     * @return 明文
     */
    public byte[] decryptWithPub(byte[] byteIn, String key) {
        byte[] keyByte = getLegalKey(key);
        return removeBottomZero(asymmetricCryptographyService.decrypt(byteIn, keyByte,true));
    }

    /**
     * 使用公钥对明文加密
     *
     * @param stringIn 明文
     * @param key      公钥
     * @return 密文
     */
    public String encryptWithPub(String stringIn, String key) {
        byte[] byteIn = stringIn.getBytes();
        byte[] byteOut = this.encryptWithPub(byteIn, key);
        return Base64.getEncoder().encodeToString(byteOut);
    }

    /**
     * 使用私钥对密文解密
     *
     * @param stringIn 密文
     * @param key      密码
     * @return 明文
     */
    public String decryptWithPri(String stringIn, String key) {
        byte[] byteIn = Base64.getDecoder().decode(stringIn);
        byte[] byteOut = this.decryptWithPri(byteIn, key);
        return new String(byteOut);
    }

    /**
     * 使用私钥对明文加密
     *
     * @param stringIn 明文
     * @param key      公钥
     * @return 密文
     */
    public String encryptWithPri(String stringIn, String key) {
        byte[] byteIn = stringIn.getBytes();
        byte[] byteOut = this.encryptWithPri(byteIn, key);
        return Base64.getEncoder().encodeToString(byteOut);
    }

    /**
     * 使用公钥对明文解密
     *
     * @param stringIn 密文
     * @param key      密码
     * @return 明文
     */
    public String decryptWithPub(String stringIn, String key) {
        byte[] byteIn = Base64.getDecoder().decode(stringIn);
        byte[] byteOut = this.decryptWithPub(byteIn, key);
        return new String(byteOut);
    }

    /**
     * 签名
     *
     * @param byteIn 原文
     * @param key    私钥
     * @return 签名
     */
    public String sign(byte[] byteIn, String key) {
        byte[] legalKey = getLegalKey(key);
        byte[] signByte = asymmetricCryptographyService.sign(byteIn, legalKey);
        return Base64.getEncoder().encodeToString(signByte);
    }

    /**
     * 验签
     *
     * @param byteIn 原文
     * @param sign   签名
     * @param key    公钥
     * @return 是否验证成功
     */
    public boolean verify(byte[] byteIn, String sign, String key) {
        return asymmetricCryptographyService.verify(byteIn, Base64.getDecoder().decode(sign), getLegalKey(key));
    }

    /**
     * 签名
     *
     * @param stringIn 原文
     * @param key      私钥
     * @return 签名
     */
    public String sign(String stringIn, String key) {
        return this.sign(stringIn.getBytes(), key);
    }

    /**
     * 验签
     *
     * @param stringIn 原文
     * @param sign     签名
     * @param key      公钥
     * @return 是否验证成功
     */
    public boolean verify(String stringIn, String sign, String key) {
        return this.verify(stringIn.getBytes(), sign, key);
    }

    /**
     * 将Base64格式的key字符串转为byte数组
     *
     * @param key 初始密钥
     * @return 合法密钥
     */
    private byte[] getLegalKey(String key) {
        return Base64.getDecoder().decode(key);
    }

    /**
     * 在byte数组后填充0x00 使其可以被分组大小整除
     *
     * @param bytes     明文
     * @param blockSize 分组大小
     * @return
     */
    private byte[] appendBottomZero(byte[] bytes, int blockSize) {
        int length = bytes.length;
        blockSize = blockSize / 8;

        //计算需填充长度
        if (length % blockSize != 0) {
            length = length + (blockSize - (length % blockSize));
        }

        //填充
        byte[] result = new byte[length];
        System.arraycopy(bytes, 0, result, 0, bytes.length);

        return result;
    }

    /**
     * 移除末尾的0x00
     *
     * @param bytes
     * @return
     */
    private byte[] removeBottomZero(byte[] bytes) {
        int endPosition = bytes.length;
        for (int i = bytes.length; i > 0; i--) {
            endPosition = i;
            if (bytes[i - 1] != 0x00) {
                break;
            }
        }

        byte[] result = new byte[endPosition];
        System.arraycopy(bytes, 0, result, 0, endPosition);

        return result;
    }
}
