/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.providers;

import io.iec.edp.caf.commons.cryptography.service.HashProvider;
import org.apache.commons.codec.digest.DigestUtils;
import io.iec.edp.caf.commons.utils.ByteUtils;
import java.nio.charset.StandardCharsets;

/**
 * @author wangyandong
 */
public class SHA384Provider implements HashProvider {

    /**
     * 计算散列值
     *
     * @param data 输入字节数组
     * @return 散列值
     */
    @Override
    public byte[] createHash(byte[] data) {
        return DigestUtils.sha384(data);
    }

    /**
     * 计算散列值
     *
     * @param data 输入字节数组
     * @param salt   盐值
     * @return 散列值
     */
    @Override
    public byte[] createHash(byte[] data, byte[] salt) {
        byte[] dataWithSalt = ByteUtils.concatenate(data,salt);
        return DigestUtils.sha384(dataWithSalt);
    }

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    @Override
    public String createHash(String data) {
        return DigestUtils.sha384Hex(data);
    }

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    @Override
    public String createHash(String data, String salt) {
        byte[] dataWithSalt = (data + salt).getBytes(StandardCharsets.UTF_8);
        return DigestUtils.sha384Hex(dataWithSalt);
    }
}
