/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.cryptography;

import io.iec.edp.caf.commons.cryptography.PropertiesEncryptor;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * 配置文件加解密工具类
 *
 * @author guowenchang
 */
@Deprecated
public class ConfigurationCryption {

    public static String encrypt(String originText, String password) {
        return PropertiesEncryptor.encrypt(originText,password);
    }

    public static String decrypt(String originText, String password) {
        return PropertiesEncryptor.decrypt(originText,password);
    }
}
