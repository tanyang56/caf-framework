/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.model;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

/**
 * 公钥/私钥对
 *
 * @author guowenchang
 */
public class CafKeyPair {
    private PublicKey publicKey;

    private PrivateKey privateKey;

    public CafKeyPair(KeyPair keyPair) {
        this.publicKey = keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
    }

    /**
     * 返回公钥
     * @return
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * 返回私钥
     * @return
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * 以Byte[]数组格式返回公钥
     * @return
     */
    public byte[] getPublicKeyBytes() {
        return this.publicKey.getEncoded();
    }

    /**
     * 以Byte[]数组格式返回私钥
     * @return
     */
    public byte[] getPrivateKeyBytes() {
        return this.privateKey.getEncoded();
    }

    /**
     * 以String格式返回公钥
     * @return
     */
    public String getPublicKeyString() {
        return Base64.getEncoder().encodeToString(this.getPublicKeyBytes());
    }

    /**
     * 以String格式返回私钥
     * @return
     */
    public String getPrivateKeyString() {
        return Base64.getEncoder().encodeToString(this.getPrivateKeyBytes());
    }
}
