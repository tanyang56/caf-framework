/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.service;

import io.iec.edp.caf.commons.cryptography.model.CafKeyPair;

/**
 * 不对称加密服务接口
 *
 * @author guowenchang
 */
public interface AsymmetricCryptoProvider {

    CafKeyPair generateKeyPair();

    /**
     * 使用公钥对明文加密
     *
     * @param byteIn    明文
     * @param publicKey 密钥
     * @return 密文
     */
    byte[] encrypt(byte[] byteIn, byte[] publicKey,boolean isReverse);

    /**
     * 使用私钥对密文解密
     *
     * @param byteIn     密文
     * @param privateKey 密钥
     * @return 明文
     */
    byte[] decrypt(byte[] byteIn, byte[] privateKey,boolean isReverse);

    /**
     * 签名
     *
     * @param byteIn 原文
     * @param key    私钥
     * @return 签名
     */
    byte[] sign(byte[] byteIn, byte[] key);

    /**
     * 验签
     *
     * @param byteIn 原文
     * @param sign   签名
     * @param key    公钥
     * @return 是否验证成功
     */
    boolean verify(byte[] byteIn, byte[] sign, byte[] key);
}
