/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.enums;

/**
 * 不对称加密算法枚举类
 * <p>
 * 目前支持以下加密方式
 * RSA
 * DSA
 * SM2
 * <p>
 * 注意此处填充模式均设为NoPadding(在外部手动填充0)
 *
 * @author guowenchang
 * @date 2020-03-23
 */
public enum AsymmetricAlgorithmEnum {

    DSA("DSA", 64),
    RSA("RSA", 128),
    SM2("SM2", 128);

    AsymmetricAlgorithmEnum(String algorithm, int blockSize) {
        this.algorithm = algorithm;
        this.blockSize = blockSize;
    }

    //加密算法名称
    private String algorithm;

    private int blockSize;

    public String getAlgorithm() {
        return algorithm;
    }

    public AsymmetricAlgorithmEnum getEnum() {
        return this;
    }

    public int getBlockSize() {
        return blockSize;
    }
}