/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common;

import io.iec.edp.caf.common.enums.SymmetricAlgorithmEnum;

@Deprecated
public class SymmetricCryptographer{

    private io.iec.edp.caf.commons.cryptography.SymmetricCryptographer symmetricCryptographer;

    private SymmetricCryptographer(SymmetricAlgorithmEnum symmetricAlgorithmEnum) {
        this.symmetricCryptographer = io.iec.edp.caf.commons.cryptography.SymmetricCryptographer.getInstance(symmetricAlgorithmEnum);
    }

    public static SymmetricCryptographer getInstance(SymmetricAlgorithmEnum symmetricAlgorithmEnum) {
        return new SymmetricCryptographer(symmetricAlgorithmEnum);
    }

    /**
     * 加密数据
     *
     * @param byteIn 明文
     * @param key    密码
     * @return 密文
     */
    public byte[] encrypt(byte[] byteIn, String key) {
        return symmetricCryptographer.encrypt(byteIn, key);
    }

    /**
     * 解密数据
     *
     * @param byteIn 密文
     * @param key    密码
     * @return 明文
     */
    public byte[] decrypt(byte[] byteIn, String key) {
        return symmetricCryptographer.decrypt(byteIn,key);
    }

    /**
     * 加密数据
     *
     * @param stringIn 明文
     * @param key      密码
     * @return 密文
     */
    public String encrypt(String stringIn, String key) {
        return symmetricCryptographer.encrypt(stringIn,key);
    }

    /**
     * 解密数据
     *
     * @param stringIn 密文
     * @param key      密码
     * @return 明文
     */
    public String decrypt(String stringIn, String key) {
        return symmetricCryptographer.decrypt(stringIn,key);
    }

}
