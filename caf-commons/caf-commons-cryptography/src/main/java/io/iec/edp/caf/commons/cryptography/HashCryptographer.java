/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography;

import io.iec.edp.caf.commons.cryptography.enums.HashAlgorithmType;
import io.iec.edp.caf.commons.cryptography.providers.SHA256Provider;
import io.iec.edp.caf.commons.cryptography.providers.SHA512Provider;
import io.iec.edp.caf.commons.cryptography.providers.SM3Provider;
import io.iec.edp.caf.commons.cryptography.service.HashProvider;

/**
 * 散列工具类
 *
 * @author guowenchang
 */
public class HashCryptographer {

    private HashProvider provider;


    /// <summary>
    /// 根据hash算法类型获取
    /// </summary>
    /// <param name="type"></param>
    private HashCryptographer(HashAlgorithmType type)
    {
        switch (type)
        {
            case SHA256:
                provider = new SHA256Provider();
                break;
            case SHA384:
                provider = new SHA512Provider();
                break;
            case SHA512:
                provider = new SHA512Provider();
                break;
            case SM3:
                provider = new SM3Provider();
                break;
            default:
                throw new IllegalArgumentException("HashAlgorithmType");
        }
    }



    /// <summary>
    /// 根据hash算法类型获取
    /// </summary>
    /// <param name="type"></param>
    /// <returns>与算法类型对应的Hash算法</returns>
    public static HashCryptographer getInstance(HashAlgorithmType type)
    {
        return new HashCryptographer(type);
    }

    /**
     * Computes the hash value of the plaintext.
     * @param data The plaintext in which you wish to hash.
     * @return The resulting hash.
     */
    public byte[] computeHash(byte[] data){
        return provider.createHash(data);
    }

    /**
     * Computes the hash value of the plaintext.
     * @param plaintext The plaintext in which you wish to hash.
     * @return The resulting hash.
     */
    public String computeHash(String plaintext){
        return provider.createHash(plaintext);
    }

    /**
     * Computes the hash value of the plaintext.
     * @param plaintext The plaintext in which you wish to hash.
     * @param salt 盐值
     * @return The resulting hash.
     */
    public String computeHash(String plaintext, String salt){
        return provider.createHash(plaintext,salt);
    }

    /**
     * Computes the hash value of the plaintext.
     * @param data The plaintext in which you wish to hash.
     * @return The resulting hash.
     */
    public byte[] computeHash(byte[] data, byte[] salt){
        return provider.createHash(data,salt);
    }
}
