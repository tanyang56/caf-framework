/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.common.enums;

/**
 * 对称加密算法枚举类
 * <p>
 * 目前支持以下加密方式
 * AES
 * DES
 * RC2
 * 3DES
 * SM4
 * <p>
 * 默认工作模式均为CBC
 * 注意此处填充模式均设为NoPadding(在外部手动填充0)
 *
 * @author guowenchang
 * @date 2020-03-09
 */
public enum SymmetricAlgorithmEnum {
    //            因为安全问题去除
    //标准加密算法
//    DES("DES", "DES/CBC/NoPadding", 64, 64, 64, 0),

    //3DES
    TripleDES("DESede", "AES/CBC/NoPadding", 64, 128, 192, 64),

    //可作为DES算法的建议替代算法
    RC2("RC2", "AES/CBC/NoPadding", 64, 40, 128, 8),

    //高级加密算法
    Rijndael("AES", "AES/CBC/NoPadding", 128, 128, 256, 64),

    //国产加密算法
    SM4("SM4", "SM4/CBC/NoPadding", 128, 128, 128, 0);

    SymmetricAlgorithmEnum(String algorithm, String transformation) {
        this.algorithm = algorithm;
        this.transformation = transformation;
        this.needLegalKey = false;
    }

    SymmetricAlgorithmEnum(String algorithm, String transformation, int blockSize, int minSize, int maxSize, int skipSize) {
        this.algorithm = algorithm;
        this.transformation = transformation;
        this.needLegalKey = true;
        this.blockSize = blockSize;
        this.maxSize = maxSize;
        this.minSize = minSize;
        this.skipSize = skipSize;
    }

    //加密算法名称
    private String algorithm;
    //在cipher初始化时的transformation参数
    private String transformation;
    //key长度是否有要求
    private Boolean needLegalKey;
    //加密算法分组区块大小
    private int blockSize = 0;
    //如果需要 key长度的要求参数
    private int minSize = 0;
    //如果需要 key长度的要求参数
    private int maxSize = 0;
    //如果需要 key长度的要求参数
    private int skipSize = 0;

    public String getAlgorithm() {
        return algorithm;
    }

    public String getTransformation() {
        return transformation;
    }

    public Boolean getNeedLegalKey() {
        return needLegalKey;
    }

    public int getMinSize() {
        return minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getSkipSize() {
        return skipSize;
    }

    public int getBlockSize() {
        return blockSize;
    }

    public SymmetricAlgorithmEnum getEnum() {
        return this;
    }
}