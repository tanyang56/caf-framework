/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.service;

/**
 * Hash算法接口
 * @author wangyandong
 */
public interface HashProvider {
    /**
     * 计算散列值
     * @param data 输入字节数组
     * @return 散列值
     */
    public byte[] createHash(byte[] data);

    /**
     * 计算散列值
     * @param data 输入字节数组
     * @param salt 盐值
     * @return 散列值
     */
    public byte[] createHash(byte[] data, byte[] salt);

    /**
     * SM3 字符串
     *
     * @param data 输入数据
     * @return 16进制散列值
     */
    public String createHash(String data);

    /**
     * SHA256 字符串
     *
     * @param data 输入数据
     * @param salt 盐值
     * @return 16进制散列值
     */
    public String createHash(String data, String salt);
}
