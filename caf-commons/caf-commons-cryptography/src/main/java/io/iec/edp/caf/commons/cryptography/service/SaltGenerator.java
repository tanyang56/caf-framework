/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.cryptography.service;

/**
 * Common interface for all salt generators which can be applied in digest or encryption operations.
 * Every implementation of this interface must be thread-safe.
 */
public interface SaltGenerator {

    /**
     * This method will be called for requesting the generation of a new salt of the specified length.
     * @param lengthBytes the requested length for the salt.
     * @return the generated salt.
     */
    byte[] generateSalt(int lengthBytes);
}