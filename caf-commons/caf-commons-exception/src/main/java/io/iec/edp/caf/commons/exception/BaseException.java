/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author ：LiuJianhua
 * @date ：Created in 2019/7/12 10:53
 * @description :异常基类
 */
@Deprecated
@Getter
@Setter
public abstract class BaseException extends Exception implements Serializable
{
    private static final long serialVersionUID = 8343048459443313229L;

    /**
     * 异常的唯一标识
     */
    private String id;

    /**
     * 异常编号
     */
    private String exceptionCode;

    /**
     * 异常级别
     */
    private ExceptionLevel level;

    /**
     * 是否业务异常
     */
    private boolean bizException;

    /**
     * 异常消息
     */
    private String message;

    /**
     * 异常消息中的参数
     */
    private String[] messageParams;

    /**
     * 服务单元编号
     */
    private String serviceUnitCode;

    /**
     * 国际化提示信息
     */
    private String i18nMessage="";

    /**
     * 资源文件
     */
    private String resourceFile;


    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param exceptionCode   异常编号
     * @param resourceFile    资源文件
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     */
    public BaseException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams,
                         Exception innerException)
    {
        super(exceptionCode, innerException);
        if (innerException instanceof BaseException)
        {
            this.id = ((BaseException) innerException).getId();
        } else
        {
            this.id = UUID.randomUUID().toString();
        }
        this.serviceUnitCode = serviceUnitCode;
        this.resourceFile = resourceFile;
        this.exceptionCode = exceptionCode;
        this.messageParams = messageParams;

        this.level = ExceptionLevel.Error;
        this.bizException = false;
    }


    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param exceptionCode   异常编号
     * @param resourceFile    资源文件
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     * @param level           异常级别
     */
    public BaseException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams,
                         Exception innerException, ExceptionLevel level)
    {
        this(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException);
        this.level = level;
        this.bizException = false;
    }

    /**
     * 传入异常编号和服务单元编号
     *
     * @param serviceUnitCode 服务单元编号
     * @param resourceFile    资源文件
     * @param exceptionCode   异常编号
     * @param messageParams   提示消息的上下文参数
     * @param innerException  异常类
     * @param level           异常级别
     * @param bizException    是否业务异常
     */
    public BaseException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams,
                         Exception innerException, ExceptionLevel level, boolean bizException)
    {
        this(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level);
        this.bizException = bizException;
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     */
    public BaseException(String serviceUnitCode, String exceptionCode, String message, Exception innerException)
    {
        super(message, innerException);
        if (innerException instanceof BaseException)
        {
            this.id = ((BaseException) innerException).getId();
        } else
        {
            this.id = UUID.randomUUID().toString();
        }
        this.exceptionCode = exceptionCode;
        this.message = message;
        this.serviceUnitCode = serviceUnitCode;
        this.level = ExceptionLevel.Error;
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     * @param level           异常级别
     */
    public BaseException(String serviceUnitCode, String exceptionCode, String message, Exception innerException,
                         ExceptionLevel level)
    {
        this(serviceUnitCode, exceptionCode, message, innerException);
        this.level = level;
    }

    /**
     * 传入异常编号和异常消息
     *
     * @param serviceUnitCode 服务单元编号
     * @param message         异常消息
     * @param exceptionCode   异常编号
     * @param innerException  异常类
     * @param level           异常级别
     * @param bizException    是否业务异常
     */
    public BaseException(String serviceUnitCode, String exceptionCode, String message, Exception innerException,
                         ExceptionLevel level,
                         boolean bizException)
    {
        this(serviceUnitCode, exceptionCode, message, innerException, level);
        this.bizException = bizException;
    }

    /**
     * 重写了Message的获取方法
     *
     * @return
     */
    @Override
    public String getMessage()
    {
        if (this.getI18nMessage().equals(""))
        {
            return super.getMessage();
        } else
        {
            return this.getI18nMessage();
        }
    }

    /**
     * 获取国际化的提示信息
     *
     * @return
     */
    public String getI18nMessage()
    {
        return this.getLocalizedMessage();
    }

    @Override
    public String getLocalizedMessage()
    {
        if (!this.i18nMessage.equals(""))
        {
            return this.i18nMessage;
        }
        String messageStr = "";//TODO:调用国际化服务，获取国际化消息
        return messageStr;
    }
}
