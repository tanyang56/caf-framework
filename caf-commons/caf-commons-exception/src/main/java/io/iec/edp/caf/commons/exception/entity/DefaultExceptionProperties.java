/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.entity;

/**
 * 默认的国际化资源文件地址
 *
 * @author guowenchang
 * @data 2019/8/16 16:39
 */
public class DefaultExceptionProperties {

    //默认的国际化资源文件地址
    public static final String SERVICE_UNIT = "caf";

    //默认的国际化资源文件地址
    public static final String RESOURCE_FILE = "exception.properties";
}

