/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.api;

/**
 * @author Leon Huo
 */
public interface GspExceptionMessageService {
    /**
     * 获取异常信息
     * @param resourceFileName  资源标识
     * @param resourceCode  资源编号
     * @param serviceUnitCode 服务单元编号
     * @return 异常消息
     */

    //TODO:与国际化集成获取信息
    String getExceptionMessage(String resourceFileName, String resourceCode, String serviceUnitCode);
}
