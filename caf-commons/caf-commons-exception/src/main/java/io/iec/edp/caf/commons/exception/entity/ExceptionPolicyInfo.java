/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 错误处理的配置项信息
 * @author lijing
 * @data 2019/8/16 17:29
 */
@Data
public class ExceptionPolicyInfo {
    /**
     * 策略名
     */
    private String name;

    /**
     * 业务策略的父级策略名
     */
    private String basePolicyName;

    /**
     * 错误定义对象描述信息
     */
    private String description;

    /**
     * handler列表
     */
    private String handlerList;

}
