/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.entity;

/**
 * 默认异常编号
 *
 * @author Leon Huo
 */
public class ExceptionErrorCode {
    /**
     * 处理程序为空
     */
    public static final String exceptionHandleIsNull = "ECP_CAF_EXCP_0101";

    /**
     * 处理实现类为空
     */
    public static final String handleClassIsNull = "ECP_CAF_EXCP_0102";

    /**
     * 反射错误
     */
    public static final String reflectError = "ECP_CAF_EXCP_0103";

    /**
     * 配置文件错误
     */
    public static final String exceptionConfigFileError = "ECP_CAF_EXCP_0104";

    /**
     * 包装异常错误
     */
    public static final String wrapExceptionError = "ECP_CAF_EXCP_0105";

    /**
     * 无效的类
     */
    public static final String invalidClass = "ECP_CAF_EXCP_0107";

    /**
     * 类未实现接口
     */
    public static final String classNotImplInterface = "ECP_CAF_EXCP_0108";

    /**
     * 客户端配置文件错误
     */
    public static final String clientConfigFileError = "ECP_CAF_EXCP_0109";

    /**
     * 缓存未实现
     */
    public static final String cacheNotFound = "ECP_CAF_EXCP_0110";

    /**
     * 网络错误
     */
    public static final String networkError = "ECP_CAF_EXCP_0111";

    /**
     * 连接关闭
     */
    public static final String connectionClose = "ECP_CAF_EXCP_0112";

    /**
     * 服务器未响应
     */
    public static final String serverNotReponse = "ECP_CAF_EXCP_0113";

    /**
     * 长度超长
     */
    public static final String overlengthError = "ECP_CAF_EXCP_0114";

    /**
     * 解析服务器错误
     */
    public static final String analyzeServerError = "ECP_CAF_EXCP_0115";

    /**
     * 服务器错误
     */
    public static final String serverError = "ECP_CAF_EXCP_0116";

    /**
     * 客返回数据不完整
     */
    public static final String reponseDataError = "ECP_CAF_EXCP_0117";

    /**
     * 请求取消
     */
    public static final String requestCancel = "ECP_CAF_EXCP_0118";

    /**
     * 缓存策略错误
     */
    public static final String cachePolicyError = "ECP_CAF_EXCP_0119";

    /**
     * 代理不允许请求
     */
    public static final String agentNotAllowRequest = "ECP_CAF_EXCP_0120";

    /**
     * SSL连接错误
     */
    public static final String sslConnectError = "ECP_CAF_EXCP_0121";

    /**
     * 请求数据返回错误
     */
    public static final String requestDataError = "ECP_CAF_EXCP_0122";

    /**
     * 无效的服务器返回
     */
    public static final String invalidServerReponse = "ECP_CAF_EXCP_0123";

    /**
     * 请求超时
     */
    public static final String requestTimeoutError = "ECP_CAF_EXCP_0124";

    /**
     * 无效的服务器证书
     */
    public static final String invalidServerCertificate = "ECP_CAF_EXCP_0125";

    /**
     * 网络连接失败
     */
    public static final String networkConnectFail = "ECP_CAF_EXCP_0126";

    /**
     * MSDTC错误
     */
    public static final String MSDTCError = "ECP_CAF_EXCP_0127";

    /**
     * COM+错误
     */
    public static final String ComPlusError = "ECP_CAF_EXCP_0128";

    /**
     * 文件名超长
     */
    public static final String fileNameOverlength = "ECP_CAF_EXCP_0129";

    /**
     * Oracle MTS错误
     */
    public static final String OraclMTSError = "ECP_CAF_EXCP_0130";

    /**
     * 分布式事务错误
     */
    public static final String DTCError = "ECP_CAF_EXCP_0131";

    /**
     * 网络提示
     */
    public static final String networkMessage = "ECP_CAF_EXCP_0132";

    /**
     * 询问的标题
     */
    public static final String askTitle = "ECP_CAF_EXCP_0133";

    /**
     * 消息的标题
     */
    public static final String infoTitle = "ECP_CAF_EXCP_0134";

    /**
     * 警告的标题
     */
    public static final String warningTitle = "ECP_CAF_EXCP_0135";

    /**
     * 错误的标题
     */
    public static final String errorTitle = "ECP_CAF_EXCP_0136";

    /**
     * 异常处理错误
     */
    public static final String exceptionHandleError = "ECP_CAF_EXCP_0137";

    /**
     * 默认的处理程序为NULL
     */
    public static final String defaultHandleIsNull = "ECP_CAF_EXCP_0138";

    /**
     * 值为空
     */
    public static final String valueIsNull = "ECP_CAF_EXCP_0139";

    /**
     * 未找到类
     */
    public static final String classNotFound = "ECP_CAF_EXCP_0140";

    /**
     * 业务上下文未找到
     */
    public static final String bizContextNotFound = "ECP_CAF_EXCP_0141";

    /**
     * 上下文销毁错误
     */
    public static final String bizContextDestroyFailed = "ECP_CAF_EXCP_0142";

    /**
     * 上下文构造器未找到
     */
    public static final String bizContextBuilderNotFound = "ECP_CAF_EXCP_0143";

    /**
     * RPC异常
     */
    public static final String rpcError = "ECP_CAF_EXCP_0144";

    /**
     * 未找到方法中的参数序列化信息
     */
    public static final String serializedParamNotFoundInMethod = "ECP_CAF_EXCP_0145";

    /**
     * 不支持此序列化方式
     */
    public static final String serializeMethodNotSupport = "ECP_CAF_EXCP_0146";

    /**
     * 找不到RPC远程调用地址
     */
    public static final String remoteRPCAddressNotFound = "ECP_CAF_EXCP_0147";

    /**
     * 远程调用获取服务接口信息失败
     */
    public static final String remoteServiceUnitNotFound = "ECP_CAF_EXCP_0148";

    /**
     * 本地调用获取服务接口信息失败
     */
    public static final String localServiceUnitNotFound = "ECP_CAF_EXCP_0149";

    /**
     * 未获取到服务
     */
    public static final String serviceNotFound = "ECP_CAF_EXCP_0150";

    /**
     * 非法RPC调用
     */
    public static final String illegalRPCCall = "ECP_CAF_EXCP_0151";

    /**
     * RpcParam缺失
     */
    public static final String rpcParamNotFound = "ECP_CAF_EXCP_0152";

    /**
     * MSU通讯异常
     */
    public static final String msuCommError = "ECP_CAF_EXCP_0153";

    /**
     * 未找到Service
     */
    public static final String serviceInstanceNotFound = "ECP_CAF_EXCP_0154";

    /**
     * 未找到SU
     */
    public static final String serviceUnitNotFound = "ECP_CAF_EXCP_0155";

    /**
     * 服务注册失败
     */
    public static final String serviceRegisterError = "ECP_CAF_EXCP_0156";

    /**
     * 服务注销失败
     */
    public static final String serviceDeregisterError = "ECP_CAF_EXCP_0157";

    /**
     * Nacos通讯异常
     */
    public static final String nacosError = "ECP_CAF_EXCP_0158";
}
