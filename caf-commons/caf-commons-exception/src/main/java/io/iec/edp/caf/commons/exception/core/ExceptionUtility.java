/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.core;

import io.iec.edp.caf.commons.exception.api.GspExceptionMessageService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.var;

/**
 * 异常工具类
 *
 * @author Leon Huo
 */
public class ExceptionUtility {

    /**
     * 返回异常的元数据标识
     */
    public static final String fileName = "GSP_CAF_EXCP.properties";


    /**
     * 传给日志的，记录模块名称的KEY
     */
    private static String moduleName = "ExceptionModuleName";


    /**
     * 异常ID
     */
    private static String id = "Id";

    /**
     * 异常编号
     */
    private static String exceptionCode = "ExceptionCode";

    /**
     * 从Message中获取异常提示信息
     *
     * @param message 消息
     * @return 异常提示信息
     */
    public static String getExceptionInnerMessage(String message) {
        return getExceptionMessage(fileName, message, "CAF");
    }


    private static GspExceptionMessageService exMessageService;

    /**
     * 从异常的Resource中获取异常提示信息
     *
     * @param fileName        资源文件
     * @param message         消息编号
     * @param serviceUnitCode 服务单元编号
     * @return 异常提示信息
     */
    public static String getExceptionMessage(String fileName, String message, String serviceUnitCode) {
        if (exMessageService == null)
            exMessageService = SpringBeanUtils.getBean(GspExceptionMessageService.class);
        //获取异常信息
        var result = exMessageService.getExceptionMessage(fileName, message, serviceUnitCode);
        if (result == null || result.isEmpty())
            return message;
        else
            return result;
    }

}