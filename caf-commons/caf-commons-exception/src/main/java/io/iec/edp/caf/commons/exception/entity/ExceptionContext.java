/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.entity;

import lombok.Data;

/**
 * 异常处理的上下文
 *
 * @author Leon Huo
 */
@Data
public class ExceptionContext {
    /**
     * 业务策略
     */
    private String policyName;

    /**
     * 异常
     */
    private Exception exception;

    /**
     * 构造函数
     *
     * @param exception  异常
     * @param policyName 业务策略
     */

    public ExceptionContext(Exception exception, String policyName) {
        this.exception = exception;
        this.policyName = policyName;
    }
}
