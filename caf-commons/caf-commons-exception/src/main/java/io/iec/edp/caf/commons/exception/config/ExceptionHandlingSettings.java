/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.config;

import io.iec.edp.caf.commons.exception.entity.ExceptionHandlerInfo;
import io.iec.edp.caf.commons.exception.entity.ExceptionPolicyInfo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author lijing
 */

@ConfigurationProperties(prefix = "exception-configuration")
@Data
public class ExceptionHandlingSettings {

    /**
     * 错误定义对象指定的是否使用某个错误处理对象的信息
     */
    private String defaultPolicyName="default";

    /**
     * 业务策略对象配置信息的集合
     */
    private List<ExceptionPolicyInfo> policies;

    /**
     *  错误处理对象配置信息的集合
     */
    private List<ExceptionHandlerInfo> handlers;
}
