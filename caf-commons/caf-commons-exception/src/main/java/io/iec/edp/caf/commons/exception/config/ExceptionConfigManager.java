/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.config;

import io.iec.edp.caf.commons.exception.entity.ExceptionHandlerInfo;
import io.iec.edp.caf.commons.exception.entity.ExceptionPolicyInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 异常处理配置管理类，负责合并Bean和 yaml 配置的合并
 *
 * @author wangyandong
 */
public class ExceptionConfigManager {
    private ExceptionHandlingSettings settings;
    private List<ExceptionHandlerInfo> handlerInfoList;
    private List<ExceptionPolicyInfo> policyInfoList;

    /**
     * 构造函数
     *
     * @param settings
     */
    public ExceptionConfigManager(ExceptionHandlingSettings settings, List<ExceptionHandlerInfo> handlerInfoList,
                                  List<ExceptionPolicyInfo> policyInfoList) {
        this.settings = settings;
        this.handlerInfoList = handlerInfoList;
        this.policyInfoList = policyInfoList;
        this.initialize();
    }

    private void initialize() {
        if (this.settings == null)
            return;
        if (this.settings.getPolicies() != null) {
            if (this.policyInfoList == null)
                this.policyInfoList = new ArrayList<>();

            for (ExceptionPolicyInfo policyData : this.settings.getPolicies()) {
                this.policyInfoList.removeIf(t -> t.getName().equals(policyData.getName()));
                this.policyInfoList.add(policyData);
            }
        }
        if (this.settings.getHandlers() != null) {
            if (this.handlerInfoList == null)
                this.handlerInfoList = new ArrayList<>();

            for (ExceptionHandlerInfo handlerData : this.settings.getHandlers()) {
                this.handlerInfoList.removeIf(t -> t.getName().equals(handlerData.getName()));
                this.handlerInfoList.add(handlerData);
            }
        }
    }

    public String getDefaultPolicyName() {
        return this.settings.getDefaultPolicyName();
    }


    /**
     * 检查配置文件是否正常，如果不正常且是客户端，则修复
     *
     * @return true正常 false不正常
     */
    public Map<Boolean, ExceptionHandlingSettings> checkConfigFileValid() {
        Map<Boolean, ExceptionHandlingSettings> map = new HashMap<>();
        map.put(this.settings != null, this.settings);
        return map;
    }

    /**
     * 从策略列表中获取策略信息
     *
     * @param policyName 策略名
     * @return 策略信息
     */
    public ExceptionPolicyInfo getExceptionPolicyInfo(String policyName) {
        if (this.policyInfoList == null || this.policyInfoList.size() == 0)
            return null;
        List<ExceptionPolicyInfo> exceptionPolicies = policyInfoList;
        //循环 exceptionPolicies 寻找第一个name与policyName相同的policy
        for (ExceptionPolicyInfo data : exceptionPolicies) {
            if (data.getName().equals(policyName)) {
                return data;
            }
        }
        return null;
    }

    /**
     * 从策略信息中获取handler信息
     *
     * @param handlerName handler名称
     * @return handler信息
     */
    public ExceptionHandlerInfo getExceptionHandlerInfo(String handlerName) {
        if (this.handlerInfoList == null || this.handlerInfoList.size() == 0)
            return null;
        List<ExceptionHandlerInfo> exceptionHandlers = this.handlerInfoList;
        for (ExceptionHandlerInfo data : exceptionHandlers) {
            if (data.getName().equals(handlerName)) {
                return data;
            }
        }
        return null;
    }
}
