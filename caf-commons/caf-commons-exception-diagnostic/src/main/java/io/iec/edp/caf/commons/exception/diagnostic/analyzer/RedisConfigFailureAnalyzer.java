/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.analyzer;

import io.iec.edp.caf.commons.exception.diagnostic.exception.RedisConfigException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

/**
 * Redis配置不正确异常解析器
 * @author wangyandong
 */
public class RedisConfigFailureAnalyzer extends AbstractFailureAnalyzer<RedisConfigException> {

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, RedisConfigException cause) {
        return new FailureAnalysis(cause.getMessage(),"Redis密码不允许为空，请为Redis配置合适密码后重新启动！",rootFailure);
    }
}