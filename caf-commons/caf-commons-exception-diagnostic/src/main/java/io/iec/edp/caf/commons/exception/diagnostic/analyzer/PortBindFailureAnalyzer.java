/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.analyzer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

import java.net.BindException;


@Slf4j
public class PortBindFailureAnalyzer extends AbstractFailureAnalyzer<BindException> {

    public PortBindFailureAnalyzer(){
        super();
    }
    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, BindException cause) {
        log.error("应用端口被占用，请关闭端口或调整yaml里端口后重试:"+rootFailure.getMessage(),cause);
        return new FailureAnalysis("启动端口绑定失败：" + rootFailure.getMessage()+" : "+cause.getMessage(),
                "请先关闭占用的端口",
                rootFailure);
    }
}
