/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.analyzer;

import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

import java.io.FileNotFoundException;

/**
 * 文件找不到异常，重点处理以下问题：
 * 1. Too many open files
 * 2.
 * @author wangyandong
 * @date 2021/09/01 14:11
 *
 */
public class FileNotFoundFailureAnalyzer extends AbstractFailureAnalyzer<FileNotFoundException> {

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, FileNotFoundException cause) {
        if(cause.getMessage().toLowerCase().contains("too many open files")) {
            return new FailureAnalysis("句柄数超出系统限制（Too many open files）",
                    "将文件句柄数调整为较大数值，一、临时修改：执行命令 ulimit -n number；二、将ulimit 值添加到/etc/profile文件中（适用于有root权限登录的系统）",
                    rootFailure);
        }
        return null;
    }
}
