/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.analyzer;

import io.iec.edp.caf.commons.exception.diagnostic.entity.ConstantInfos;
import lombok.var;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 找不到类型错误分析器
 * @author wangyandong
 */
public class ClassNotFoundFailureAnalyzer  extends AbstractFailureAnalyzer<ClassNotFoundException> {
    private static final LinkedHashMap<String,String> KeyApps = new LinkedHashMap<>();
    private static final LinkedHashMap<String, List<String>> KeyMsus = new LinkedHashMap<>();
    public ClassNotFoundFailureAnalyzer(){
        super();


//        KeyApps.put("ARAP","应收应付");
//        KeyApps.put("BA","BA");
//        KeyApps.put("BF","基础数据");
//        KeyApps.put("BP","业务中台");
//        KeyApps.put("CT","合同管理");
//        KeyApps.put("SASA","企业大脑");
//        KeyApps.put("EAM","资产管理");
//        KeyApps.put("EIS","电子影像");
//        KeyApps.put("ES","电子采购");
//        KeyApps.put("ERM","电子档案");
//        KeyApps.put("FI","财务会计");
//        KeyApps.put("FSSP","财务共享");
//        KeyApps.put("IHC","资金池");
//        KeyApps.put("IPS","投资管理");
//        KeyApps.put("MA","管理会计");
//        KeyApps.put("MDM","主数据管理");
//        KeyApps.put("PS","项目管理");
//        KeyApps.put("QM","质量管理");
//        KeyApps.put("SCM","供应链");
//        KeyApps.put("TAX","税务管理");
//        KeyApps.put("TM","资金管理");
    }
    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, ClassNotFoundException cause) {
        String className = cause.getMessage().toLowerCase();
        AtomicReference<String> action = new AtomicReference<>("");
        //first 基于su精确匹配
        var temp = className.split(".");
        String msu = "";
        if(temp.length>3){//默认命名空间的第四位是su
            msu = temp[3];
        }

        //caf的su，单独处理，因为有sys的也叫caf
        if(msu.length()>0 && msu.equalsIgnoreCase("caf")==false){
            var msuinfo = ConstantInfos.getMsuInfos();
            String finalMsu = msu;
            for (Map.Entry<String, List<String>> entry : msuinfo.entrySet()) {
                String k = entry.getKey();
                List<String> v = entry.getValue();
                if (v.contains(finalMsu.toLowerCase())) {
                    action.set("请联系" + k + "处理");
                    break;
                }
            }
        }

        //second 基于命名空间匹配
        var namespace = ConstantInfos.getNameSpace();
        final boolean[] isBreak = {false};
        for (Map.Entry<String, List<String>> entry : namespace.entrySet()) {
            String k = entry.getKey();
            List<String> v = entry.getValue();
            for (String y : v) {
                if (className.contains(y)) {
                    action.set("请联系" + k + "处理");
                    isBreak[0] = true;
                    break;
                }
            }
            if (isBreak[0]) {
                break;
            }
        }

        if(action.get().length()>0){

        }else if(className.startsWith("io.iec.edp") ||
                className.startsWith("com.inspur.edp") ||
                className.startsWith("com.inspur.igix")){
            action.set("请联系【产品支持部】或者【平台部】处理");
        }else if(className.startsWith("com.inspur.gs")){
//            String[] names = className.split(".");
//            if(names.length>=4){
//                String code = names[3].toUpperCase();
//                if(KeyApps.containsKey(code)) {
//                    action.set(String.format("请联系【%s】处理！", KeyApps.get(code)));
//                }else{
//                    action.set("请联系【产品部】处理！");
//                }
//            }else{
                action.set("请联系【产品部】处理！");
//            }
        }else if(className.startsWith("inspur")){
            action.set("请联系【项目二开】处理！");
        }
//        else if(className.startsWith("inspur.cb")){
//            action.set("请联系【预算管理】处理！");
//        }
        action.set(action.get()+" : No Such Class '"+className);
        return new FailureAnalysis("No Such Class：" + cause.getMessage(),
                action.get(),
                rootFailure);
    }
}