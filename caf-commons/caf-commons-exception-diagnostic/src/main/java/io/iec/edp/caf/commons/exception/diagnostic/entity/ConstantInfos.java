/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class ConstantInfos {
    public static LinkedHashMap<String, List<String>> getMsuInfos(){
        LinkedHashMap<String, List<String>> KeyMsus = new LinkedHashMap<>();
        KeyMsus.put("产品部财税产品部合并报表组",new ArrayList<String>(Arrays.asList("cfm")));
        KeyMsus.put("产品部财务共享开发组",new ArrayList<String>(Arrays.asList("pf","rs","ssp")));
        KeyMsus.put("产品部财务会计开发组",new ArrayList<String>(Arrays.asList("cfs","cfl","faf")));
        KeyMsus.put("平台查询平台组",new ArrayList<String>(Arrays.asList("mcf")));
        KeyMsus.put("城市服务",new ArrayList<String>(Arrays.asList("hxsl","hsxmscm","gsxmpbs","csmr","csmsfnd","cscm","cswo")));
        KeyMsus.put("低代码平台研发部",new ArrayList<String>(Arrays.asList("schd","sys","sg","devmain","nocode","cicd","svcm","afm","bcc")));
        KeyMsus.put("电子采购事业部",new ArrayList<String>(Arrays.asList("mk","sm")));
        KeyMsus.put("东方电气项目处",new ArrayList<String>(Arrays.asList("dsrmsu")));
        KeyMsus.put("风险内控开发处",new ArrayList<String>(Arrays.asList("eric","eddm","lm","ric","ams")));
        KeyMsus.put("产品部共享产品研发部",new ArrayList<String>(Arrays.asList("cbc","cbp","tr")));
        KeyMsus.put("产品部供应链与制造产品部供应链组",new ArrayList<String>(Arrays.asList("pufnd","pr","sct","invp","soc","ibp","sr","sacr")));
        KeyMsus.put("产品部管理会计开发组",new ArrayList<String>(Arrays.asList("masc","mace","mapfnd","mapc","ma","mamr","mapca","maim","mapa","macca","maia")));
        KeyMsus.put("建筑事业部管理组",new ArrayList<String>(Arrays.asList("jbz","jpm","jzkk","jss")));
        KeyMsus.put("产品部国资监管开发处",new ArrayList<String>(Arrays.asList("sasom","sers")));
        KeyMsus.put("产品部合同管理开发组",new ArrayList<String>(Arrays.asList("ese","ctu")));
        KeyMsus.put("混合集成平台研发部",new ArrayList<String>(Arrays.asList("wf","task")));
        KeyMsus.put("产品部集团司库开发组",new ArrayList<String>(Arrays.asList("lc","tmc","sdm")));
        KeyMsus.put("技术保障部",new ArrayList<String>(Arrays.asList("cbs","mlp","ipa","aim","cos","kg","emcdc","emcac","emcim","emcmc","emccc","emcpc")));
        KeyMsus.put("产品部架构与总体部",new ArrayList<String>(Arrays.asList("tiw","ias","tim","brpc","toim","tbr","bebc","df","bc","eis","ermfm")));
        KeyMsus.put("建筑事业部",new ArrayList<String>(Arrays.asList("jztm","jzfi","jztax","jzeis","jzdf","jzdi","jzarap","jzaa","jzsys")));
        KeyMsus.put("开发管理组",new ArrayList<String>(Arrays.asList("cdm","cifm","isf","itiam","ptm","pa")));
        KeyMsus.put("粮食央企事业部",new ArrayList<String>(Arrays.asList("coop")));
        KeyMsus.put("制造业事业部流程行业开发组",new ArrayList<String>(Arrays.asList("gmjt","zwqwm","zwqqm")));
        KeyMsus.put("平台物联网组",new ArrayList<String>(Arrays.asList("iotre","iotom","iotdm","iotda","iotvg","iotds","iotfnd","ioteg","iotvs","iottdb","iotdt")));
        KeyMsus.put("企业金融事业部",new ArrayList<String>(Arrays.asList("ftmscm","ftmsbl","ftmscust","ftmsri","ftmsiv","ftmseb","ftmsln")));
        KeyMsus.put("产品部企业资金开发组",new ArrayList<String>(Arrays.asList("fa","ta","ihcfnd")));
        KeyMsus.put("产品部全面预算开发组",new ArrayList<String>(Arrays.asList("jhpt","cb")));
        KeyMsus.put("融信事业部",new ArrayList<String>(Arrays.asList("akj","ajs","ays","azj","ajdfnd","acgpt")));
        KeyMsus.put("产品部生产质量开发组",new ArrayList<String>(Arrays.asList("qa","qc","pd","pp")));
        KeyMsus.put("数据中台产品部数据管理开发处",new ArrayList<String>(Arrays.asList("mdfd","damd","das","dadp","dapt","dasx","mdmt")));
        KeyMsus.put("战略大客户事业部数据与集成组",new ArrayList<String>(Arrays.asList("empubcom","empubsi","cs","vehicle","sms","wcb","odms","ctms","imm","survey","vms","oms","ccms")));
        KeyMsus.put("数据中台产品研发部",new ArrayList<String>(Arrays.asList("mlm","dast","dacx","dasv","dasm","daqm","daam","shm","lfdm","soeo","ffr","fer","fbr",
                "basa","cere","soa","tiol","jpbr","spm","sacm","bap","gis","rc","baoa","bakm","bair","ea","east","idxi","idji","fmd",
                "mdmfnd","csmd","mmd","pmd","mdc","mda","opmd","eabr","eabc","eabs")));
        KeyMsus.put("数字供应链研发部",new ArrayList<String>(Arrays.asList("crmfnd","svc","act","mkt","csm","sa","an","su","epfnd","es","epmm","hum","pusc",
                "po","pupr","mv","sn","whs","pv","codes","scmfnd","mds","avail","im","inters","batch","cond","barc","to","so","sapr",
                "ss","bizf","tms","psm","eamcm","eaminfo","ipsm")));
        KeyMsus.put("制造业事业部水务开发二组",new ArrayList<String>(Arrays.asList("zptom","ztiim","zpmam","zspsm","zecem","zamws","ziaoa","zsmss","zumlu","zjjss")));
        KeyMsus.put("制造业事业部水务开发一组",new ArrayList<String>(Arrays.asList("gsxmcpst","gsxmpay")));
        KeyMsus.put("产品部税务管理开发组",new ArrayList<String>(Arrays.asList("eptm","trm","tacc")));
        KeyMsus.put("企业金融事业部司库开发组",new ArrayList<String>(Arrays.asList("ftmsdip","ftmsfnd")));
        KeyMsus.put("产品部项目管理开发组",new ArrayList<String>(Arrays.asList("tpsm")));
        KeyMsus.put("中央大客户事业部",new ArrayList<String>(Arrays.asList("fcoe","fbse","claf","csoasop","cosa","coecc","ctiog","yzleis","fifm","fitf","fals","fpub","fpln")));
        KeyMsus.put("云原生平台研发部",new ArrayList<String>(Arrays.asList("ses","msg","ewp","prt","dfs")));
        KeyMsus.put("战略大客户事业部",new ArrayList<String>(Arrays.asList("obbudget","obqueryanalysis","obpre","obexecute","obcontrol","obp","is","jgempm","oop")));
        KeyMsus.put("产品部制造运营开发组",new ArrayList<String>(Arrays.asList("mom","hse","iniot")));
        KeyMsus.put("质量安全与技术管理部",new ArrayList<String>(Arrays.asList("ppsm","pmsm","rdpm","rdoa","tqi","qsa","apsm","scsm","pim")));
        KeyMsus.put("智慧国资事业部",new ArrayList<String>(Arrays.asList("search","prm","pbm","cgm","asm","blm","sim","parl","drm","nam","irm",
                "lavm","aem","dm","cim","fm","plm","bpe","gpc")));
        KeyMsus.put("智慧司库研发部",new ArrayList<String>(Arrays.asList("mgm","gls","bm","cc","cm","mt","bd","bl","am","tmfnd","fp","cmt","id",
                "il","ia","fsd","cps")));
        KeyMsus.put("智能财务研发部",new ArrayList<String>(Arrays.asList("fifnd","aa","aip","tv","gl","arapm","tibm","otf","eitf","tbd","tbs","tsaa",
                "vatf","cts","ctc","ctp","ctm","ctb")));
        KeyMsus.put("智能制造事业部",new ArrayList<String>(Arrays.asList("cem","zsam","zpbd","zpim","zpat","zeaf","zfom","zasm","zptm","gsxmbdpb","gsxmsmbd","gsxmasma","gsxmprmr",
                "gsxmwosg","gsxmwocs","gsxmwobd","gsxmwopn","gsxmwolv","gsxmwoms","gsxmwoms","gsxmwoom","gsxmmswl","gsxmmsws","gsxmmsmr","gsxmmsnr","gsxmmsci","gsxmmspa","gsxmmsbp",
                "gsxmmsam","gsxmmscf","gsxmmscb","gsxmcpcg","gsxmcpwm","gsxmcpcs","gsxmcpws","gsxmcpbd","gsxmcpot","gsxmcpcm","gsxmcprg","gsxmrcrp","gsxmrcrm","gsxmrcbd","gsxmrccm","GSXMRCEM",
                "ossi","crrcs","zsdd","zsdf","zsda","zluhfi","zluhscm","zluhma","zluhbf","zluhcommon","zluhinte","gsxmwgds","gsxmwgdm","gsxmpmeg","gsxmpmwf","gsxmmr","gsxmdimr")));

        KeyMsus.put("智能制造研发部",new ArrayList<String>(Arrays.asList("qm","me","obpre","ems")));
        KeyMsus.put("产品部主数据开发处",new ArrayList<String>(Arrays.asList("mdq")));
        KeyMsus.put("产品部资产管理开发组",new ArrayList<String>(Arrays.asList("eampm","eao")));
        KeyMsus.put("产品部总体与推广处",new ArrayList<String>(Arrays.asList("idi","dars","dafy")));

        return KeyMsus;
    }

    public static LinkedHashMap<String, List<String>> getNameSpace(){
        LinkedHashMap<String, List<String>> KeyMsus = new LinkedHashMap<>();
        KeyMsus.put("低代码平台部",new ArrayList<String>(Arrays.asList(".sysmanager.",".cef.",".bef.")));
        KeyMsus.put("集成平台部",new ArrayList<String>(Arrays.asList(".wf.",".task.")));
        KeyMsus.put("技术保障部",new ArrayList<String>(Arrays.asList(".databaseobject.",".stdcontrol.")));
        KeyMsus.put("预算管理",new ArrayList<String>(Arrays.asList("inspur.cb")));

        return KeyMsus;

    }
}
