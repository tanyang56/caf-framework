///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.commons.exception.diagnostic;
//
//import io.iec.edp.caf.commons.utils.StringUtils;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.config.BeanPostProcessor;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.lang.Nullable;
//
///**
// * @author wangyandong
// * @date 2021/09/01 13:51
// * @email wangyandong@inspur.com
// */
//public class DiagnosticBeanPostProcessor implements BeanPostProcessor {
//    @Nullable
//    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        if(bean instanceof DataSourceProperties){
//            DataSourceProperties propterties = (DataSourceProperties)bean;
//            if(StringUtils.isEmpty(propterties.getUrl())==null ||
//                    StringUtils.isEmpty(propterties.getUsername()) ||
//                    StringUtils.isEmpty(propterties.getPassword())){
//                throw new DataSourceCreationException("数据源配置不完整：url、user或password为空",propterties);
//            }
//            return bean;
//        }
//        return bean;
//    }
//
//    @Nullable
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        return bean;
//    }
//}