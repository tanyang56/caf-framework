/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.reporter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.boot.SpringBootExceptionReporter;
import org.springframework.boot.diagnostics.FailureAnalysis;
import org.springframework.boot.diagnostics.FailureAnalysisReporter;
import org.springframework.boot.diagnostics.FailureAnalyzer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.core.log.LogMessage;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 启动过程异常报告类
 * @author wangyandong
 * @date 2021/05/06 15:20
 *
 */
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class CAFBootExceptionReporter implements SpringBootExceptionReporter {
    private static final Log logger = LogFactory.getLog(CAFBootExceptionReporter.class);

    private final ClassLoader classLoader;

    private final List<FailureAnalyzer> analyzers;

    CAFBootExceptionReporter(ConfigurableApplicationContext context) {
        this(context, null);
    }

    CAFBootExceptionReporter(ConfigurableApplicationContext context, ClassLoader classLoader) {
        Assert.notNull(context, "Context must not be null");
        this.classLoader = (classLoader != null) ? classLoader : context.getClassLoader();
        this.analyzers = loadFailureAnalyzers(this.classLoader);
        prepareFailureAnalyzers(this.analyzers, context);
    }

    private List<FailureAnalyzer> loadFailureAnalyzers(ClassLoader classLoader) {
        List<String> analyzerNames = SpringFactoriesLoader.loadFactoryNames(FailureAnalyzer.class, classLoader);
        //去重
        analyzerNames =(List<String>) analyzerNames.stream().distinct().collect(Collectors.toList());
        List<FailureAnalyzer> analyzers = new ArrayList<>();
        for (String analyzerName : analyzerNames) {
            try {
                Constructor<?> constructor = ClassUtils.forName(analyzerName, classLoader).getDeclaredConstructor();
                ReflectionUtils.makeAccessible(constructor);
                analyzers.add((FailureAnalyzer) constructor.newInstance());
            }
            catch (Throwable ex) {
                logger.trace(LogMessage.format("Failed to load %s", analyzerName), ex);
            }
        }
        AnnotationAwareOrderComparator.sort(analyzers);
        return analyzers;
    }

    private void prepareFailureAnalyzers(List<FailureAnalyzer> analyzers, ConfigurableApplicationContext context) {
        for (FailureAnalyzer analyzer : analyzers) {
            prepareAnalyzer(context, analyzer);
        }
    }

    private void prepareAnalyzer(ConfigurableApplicationContext context, FailureAnalyzer analyzer) {
        if (analyzer instanceof BeanFactoryAware) {
            ((BeanFactoryAware) analyzer).setBeanFactory(context.getBeanFactory());
        }
        if (analyzer instanceof EnvironmentAware) {
            ((EnvironmentAware) analyzer).setEnvironment(context.getEnvironment());
        }
    }

    @Override
    public boolean reportException(Throwable failure) {
//        //异常通常有很多的嵌套，默认查找Analyzer的顺序是从外向内逐层匹配异常类型。
//        //问题：如果外层添加了异常分析器，则不会继续查找内层异常分析器，而内层才是最根本的原因。
//        List<Throwable> failures = this.getFailures(failure);
//        FailureAnalysis analysis = null;
//        for(int i=failures.size()-1; i>=0; i--) {
//            analysis = analyze(failures.get(i), this.analyzer);
//            if (analysis != null)
//                break;
//        }

        FailureAnalysis analysis = analyze(failure, this.analyzers);
        //经过所有的异常分析器之后，如果仍为空，按照通用的方式展示异常，不返回null
        if(analysis==null){
            analysis = this.getCommonAnalysis(failure);
        }
        return report(analysis, this.classLoader);
    }

    private FailureAnalysis analyze(Throwable failure, List<FailureAnalyzer> analyzers) {
        for (FailureAnalyzer analyzer : analyzers) {
            try {
                FailureAnalysis analysis = analyzer.analyze(failure);
                if (analysis != null) {
                    return analysis;
                }
            }
            catch (Throwable ex) {
                logger.debug(LogMessage.format("FailureAnalyzer %s failed", analyzer), ex);
            }
        }
        return null;
    }

    /**
     * 根据分析结果报告错误
     * @param analysis 分析结果，如果为null时
     * @param classLoader
     * @return 报告成功返回True；失败返回False，后续继续按logger.error方式输出异常
     */
    private boolean report(FailureAnalysis analysis, ClassLoader classLoader) {
        FailureAnalysisReporter reporter = new CAFFailureAnalysisReporter();
        reporter.report(analysis);
        return true;
    }

    /**
     * 将异常信息按层级转为List
     * @param failure
     * @return
     */
    private List<Throwable> getFailures(Throwable failure){
        List<Throwable> failures = new ArrayList<>();
        Throwable tempFailure = failure;
        do{
            failures.add(tempFailure);
            tempFailure = tempFailure.getCause();
        }while(tempFailure!=null);
        return failures;
    }

    private FailureAnalysis getCommonAnalysis(Throwable failure){
        StringBuilder sb = new StringBuilder();
        Throwable exception = failure;
        //记录外层
        String description = "";
        do{
            //如果上一个描述信息中不包含当前exception的信息，则补充进来
            if(!description.endsWith(exception.toString())){
                sb.append(exception.toString()).append(String.format("%n"));
            }
            description = exception.toString();
            exception = exception.getCause();
        }while(exception!=null);

        return new FailureAnalysis(sb.toString(),null,failure);
    }
}
