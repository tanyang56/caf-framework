/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.exception.diagnostic.analyzer;

import io.iec.edp.caf.commons.exception.diagnostic.exception.DataSourceCreationException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

/**
 * @author wangyandong
 * @date 2021/05/31 11:46
 *
 */
public class DataSourceFailureAnalyzer extends AbstractFailureAnalyzer<DataSourceCreationException> {

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, DataSourceCreationException cause) {
        String description = cause.getMessage();
        String action = "请使用EMC注册主库数据库实例或手动配置主库数据库实例信息！";
        return new FailureAnalysis(description, action, rootFailure);
    }
}