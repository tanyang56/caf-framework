///*
// * Copyright © OpenAtom Foundation.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *      http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
// * See the License for the specific language governing permissions and
// * limitations under the License.
// *
// */
//
//package io.iec.edp.caf.commons;
//
//import io.iec.edp.caf.commons.config.ProtobufConfig;
//import io.iec.edp.caf.commons.core.SerializerFactory;
//import io.iec.edp.caf.commons.core.enums.SerializeType;
//import lombok.var;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.io.*;
//import java.sql.Date;
//import java.time.OffsetDateTime;
//import java.util.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ProtobufConfig.class)
//public class ProtobufTest {
//    @Test
//    public void testNormalEntity() throws IllegalAccessException, ClassNotFoundException, InstantiationException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Protobuf);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Protobuf);
//
//        var bytes = serializer.serializeToByte(entity);
//        var copyentity = deserializer.deserialize(bytes,SimpleEntity.class);
//        assert copyentity.getS().equals(s);
//        assert copyentity.getI()==i;
//        //todo 序列化Date类型有问题
////        assert copyentity.getD1().equals(d1);
//        assert copyentity.getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Protobuf,output);
//
//        serializestream.serializeToStream(entity);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Protobuf,input);
//        copyentity = deserializerstream.deserialize(SimpleEntity.class);
//        assert copyentity.getS().equals(s);
//        assert copyentity.getI()==i;
//        //todo 序列化Date类型有问题
////        assert copyentity.getD1().equals(d1);
//        assert copyentity.getD2().equals(d2);
//
//        var bytes2 = serializer.serializeToByte(d1);
//        var copyentity2 = deserializer.deserialize(bytes2,Date.class);
////        assert copyentity2.getTime()==d1.getTime();
//    }
//
//    @Test
//    public void testList() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//        List<SimpleEntity> list = new ArrayList<>();
//        list.add(entity);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Protobuf);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Protobuf);
//
//        var bytes = serializer.serializeToByte(list);
//        var copyentity = deserializer.deserialize(bytes, List.class);
//        assert ((List<SimpleEntity>)copyentity).get(0).getS().equals(s);
//        assert ((List<SimpleEntity>)copyentity).get(0).getI()==i;
//        //todo 序列化Date类型有问题
////        assert ((List<SimpleEntity>)copyentity).get(0).getD1().equals(d1);
//        assert ((List<SimpleEntity>)copyentity).get(0).getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Protobuf,output);
//
//        serializestream.serializeToStream(list);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Protobuf,input);
//        copyentity = deserializerstream.deserialize(LinkedList.class);
//        assert ((List<SimpleEntity>)copyentity).get(0).getS().equals(s);
//        assert ((List<SimpleEntity>)copyentity).get(0).getI()==i;
//        //todo 序列化Date类型有问题
////        assert ((List<SimpleEntity>)copyentity).get(0).getD1().equals(d1);
//        assert ((List<SimpleEntity>)copyentity).get(0).getD2().equals(d2);
//    }
//    @Test
//    public void testMap() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//        Map<String,SimpleEntity> map = new HashMap();
//        map.put("entity",entity);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Protobuf);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Protobuf);
//
//        var bytes = serializer.serializeToByte(map);
//        var copyentity = deserializer.deserialize(bytes, Map.class);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getS().equals(s);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getI()==i;
//        //todo 序列化Date类型有问题
////        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD1().equals(d1);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Protobuf,output);
//
//        serializestream.serializeToStream(map);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Protobuf,input);
//        copyentity = deserializerstream.deserialize(Map.class);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getS().equals(s);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getI()==i;
//        //todo 序列化Date类型有问题
////        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD1().equals(d1);
//        assert ((Map<String,SimpleEntity>)copyentity).get("entity").getD2().equals(d2);
//    }
//
//    @Test
//    public void testComplexEntity() throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        var s = "cs";
//        var i = 1;
//        var d1=  new Date(System.currentTimeMillis());
//        var d2 = OffsetDateTime.now();
//        SimpleEntity entity = new SimpleEntity();
//        entity.setS(s);
//        entity.setI(i);
//        entity.setD1(d1);
//        entity.setD2(d2);
//
//        Map<String,SimpleEntity> map = new HashMap();
//        map.put("entity",entity);
//
//        List<SimpleEntity> list = new ArrayList<>();
//        list.add(entity);
//
//        ComplexEntity entity1= new ComplexEntity();
//        entity1.setEntity(entity);
//        entity1.setListEntity(list);
//        entity1.setMapEntity(map);
//
//        var serializer = SerializerFactory.getSerializer(SerializeType.Protobuf);
//        var deserializer = SerializerFactory.getDeserializer(SerializeType.Protobuf);
//
//        var bytes = serializer.serializeToByte(entity1);
//        var copyentity = deserializer.deserialize(bytes, ComplexEntity.class);
//        assert copyentity.getEntity().getS().equals(s);
//        assert copyentity.getEntity().getI()==i;
//        assert copyentity.getListEntity().get(0).getS().equals(s);
//        assert copyentity.getListEntity().get(0).getI()==i;
//        assert copyentity.getMapEntity().get("entity").getS().equals(s);
//        assert copyentity.getMapEntity().get("entity").getI()==i;
//        //todo 序列化Date类型有问题
////        assert copyentity.getEntity().getD1().equals(d1);
////        assert copyentity.getListEntity().get(0).getD1().equals(d1);
////        assert copyentity.getMapEntity().get("entity").getD1().equals(d1);
//        assert copyentity.getEntity().getD2().equals(d2);
//        assert copyentity.getListEntity().get(0).getD2().equals(d2);
//        assert copyentity.getMapEntity().get("entity").getD2().equals(d2);
//
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        var serializestream = SerializerFactory.getSerializer(SerializeType.Protobuf,output);
//
//        serializestream.serializeToStream(entity1);
//        ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
//        var deserializerstream = SerializerFactory.getDeserializer(SerializeType.Protobuf,input);
//        copyentity = deserializerstream.deserialize(ComplexEntity.class);
//        assert copyentity.getEntity().getS().equals(s);
//        assert copyentity.getEntity().getI()==i;
//        assert copyentity.getListEntity().get(0).getS().equals(s);
//        assert copyentity.getListEntity().get(0).getI()==i;
//        assert copyentity.getMapEntity().get("entity").getS().equals(s);
//        assert copyentity.getMapEntity().get("entity").getI()==i;
//        //todo 序列化Date类型有问题
////        assert copyentity.getEntity().getD1().equals(d1);
////        assert copyentity.getListEntity().get(0).getD1().equals(d1);
////        assert copyentity.getMapEntity().get("entity").getD1().equals(d1);
//        assert copyentity.getEntity().getD2().equals(d2);
//        assert copyentity.getListEntity().get(0).getD2().equals(d2);
//        assert copyentity.getMapEntity().get("entity").getD2().equals(d2);
//    }
//
//}
