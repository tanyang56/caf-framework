/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.utils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.lang.NonNull;

import java.util.*;

/**
 * This is {@link Page}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@SuppressWarnings("unused")
@Getter
@EqualsAndHashCode
public class Page<E> {

    private final List<E> content;

    private final int pageNumber;

    private final int pageSize;

    private final int totalPages;

    private final int totalElements;

    private final boolean first;

    private final boolean last;

    public Page(List<E> content, int pageNumber, int pageSize, int totalPages, int totalElements, boolean first, boolean last) {
        this.content = content;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
        this.first = first;
        this.last = last;
    }

    public static Page emptyPage(int pageNumber, int pageSize) {
        return new Page(new ArrayList(), pageNumber, pageSize, 0, 0, true, true);
    }

    public int size() {
        return content.size();
    }

    public boolean isEmpty() {
        return content.isEmpty();
    }

    public boolean contains(Object o) {
        return content.contains(o);
    }

    @NonNull
    public Iterator<E> iterator() {
        return content.iterator();
    }

    @NonNull
    public Object[] toArray() {
        return content.toArray();
    }

    @SuppressWarnings("SuspiciousToArrayCall")
    @NonNull
    public <T> T[] toArray(@NonNull T[] a) {
        return content.toArray(a);
    }

    public boolean add(E e) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public boolean remove(Object o) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public boolean containsAll(@NonNull Collection<?> c) {
        return content.containsAll(c);
    }

    public boolean addAll(@NonNull Collection<? extends E> c) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public boolean addAll(int index, @NonNull Collection<? extends E> c) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public boolean removeAll(@NonNull Collection<?> c) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public boolean retainAll(@NonNull Collection<?> c) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public void clear() {
        throw new java.lang.UnsupportedOperationException("");
    }

    public E get(int index) {
        return content.get(index);
    }

    public E set(int index, E element) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public void add(int index, E element) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public E remove(int index) {
        throw new java.lang.UnsupportedOperationException("");
    }

    public int indexOf(Object o) {
        return content.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        return content.lastIndexOf(o);
    }

    @NonNull
    public ListIterator<E> listIterator() {
        return content.listIterator();
    }

    @NonNull
    public ListIterator<E> listIterator(int index) {
        return content.listIterator(index);
    }

    @NonNull
    public List<E> subList(int fromIndex, int toIndex) {
        return content.subList(fromIndex, toIndex);
    }

    public boolean hasContent() {
        return content != null && !content.isEmpty();
    }

    public boolean hasNext() {
        return !isLast();
    }

    public boolean hasPrevious() {
        return !isFirst();
    }
}