//package io.iec.edp.caf.commons.utils;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.pdf.PdfCopy;
//import com.itextpdf.text.pdf.PdfImportedPage;
//import com.itextpdf.text.pdf.PdfReader;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.util.ArrayList;
//
///**
// * 先将setByteArrayOutputStreamToThreadLocal 将所有的字节输入流存入
// * eg: setXxx(aaa);
// *     setXxx(bbb);
// * 最后使用 getMergePdfByteArrayOutputStream 获得合并后的输出流,最后关闭close
// */
//public class CombinePdfHelper {
//
//    private static ThreadLocal<ArrayList<ByteArrayOutputStream>> localOutputStream = new ThreadLocal<>();
//
//    public  void setSingleStreamData(ByteArrayInputStream byteArrayInputStream) {
//        Document document = null;
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        try {
//            byte[] buffer = new byte[4096];
//            int m = 0;
//            while (-1 != (m = byteArrayInputStream.read(buffer))) {
//                output.write(buffer, 0, m);
//            }
//            document = new Document(new PdfReader(output.toByteArray()).getPageSize(1));
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            if (localOutputStream.get() == null) {
//                ArrayList<ByteArrayOutputStream> strings = new ArrayList<>();
//                strings.add(byteArrayOutputStream);
//                localOutputStream.set(strings);
//            } else {
//                ArrayList<ByteArrayOutputStream> strings = localOutputStream.get();
//                strings.add(byteArrayOutputStream);
//                localOutputStream.set(strings);
//            }
//            PdfCopy copy = new PdfCopy(document, byteArrayOutputStream);
//            document.open();
//            PdfReader reader = new PdfReader(output.toByteArray());
//            int n = reader.getNumberOfPages();
//            for (int j = 1; j <= n; j++) {
//                document.newPage();
//                PdfImportedPage page = copy.getImportedPage(reader, j);
//                copy.addPage(page);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (document != null) {
//                    document.close();
//                }
//                if (output != null) {
//                    output.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public  ByteArrayOutputStream getMergeStreamData() {
//        ArrayList<ByteArrayOutputStream> byteArrayOutputStreams = localOutputStream.get();
//        if (byteArrayOutputStreams == null) {
//            return null;
//        }
//        ArrayList<byte[]> filesBytes = new ArrayList<>();
//        for (ByteArrayOutputStream byteArrayOutputStream : byteArrayOutputStreams) {
//            filesBytes.add(byteArrayOutputStream.toByteArray());
//        }
//        Document document = null;
//        ByteArrayOutputStream byteArrayOutputStream = null;
//        try {
//            document = new Document(new PdfReader(filesBytes.get(0)).getPageSize(1));
//            byteArrayOutputStream = new ByteArrayOutputStream();
//            PdfCopy copy = new PdfCopy(document, byteArrayOutputStream);
//            document.open();
//            for (int i = 0; i < filesBytes.size(); i++) {
//                PdfReader reader = new PdfReader(filesBytes.get(i));
//                int n = reader.getNumberOfPages();
//                for (int j = 1; j <= n; j++) {
//                    document.newPage();
//                    PdfImportedPage page = copy.getImportedPage(reader, j);
//                    copy.addPage(page);
//                }
//            }
//            return byteArrayOutputStream;
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (document != null) {
//                    document.close();
//                }
//                if (byteArrayOutputStream != null) {
//                    byteArrayOutputStream.close();
//                }
//                localOutputStream.remove();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return byteArrayOutputStream;
//    }
//
//    // 关闭threadlock变量
//    public void close() {
//        localOutputStream.remove();
//    }
//}
