/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.utils;

public class ClassLoaderUtils {

    //非并行启动下直接获取当前classloader，并行启动下获取PlatformClassloader的parent
    public static ClassLoader getServiceClassLoader(){
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        //迭代寻找需要的classloader
        while(cl!=null){
            //不直接对classloader进行依赖 对类名进行强匹配
            //如果识别为并行启动 则加载parent
            if(cl.getClass().getName().contains("PlatformClassloader")){
                return cl.getParent();
            }
            //如果识别为非并行启动 则返回CafClassLoader
            if(cl.getClass().getName().contains("CAFClassLoader")){
                return cl;
            }
            cl = cl.getParent();
        }
        return Thread.currentThread().getContextClassLoader();
    }
}
