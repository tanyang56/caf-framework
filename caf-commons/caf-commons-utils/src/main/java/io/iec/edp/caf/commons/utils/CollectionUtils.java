/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.utils;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 集合工具集
 *
 * @author guowenchang
 */
public class CollectionUtils {

    /**
     * 是否为空
     *
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null ? true : collection.isEmpty();
    }

    /**
     * Map是否为空
     *
     * @param map
     * @return
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null ? true : map.isEmpty();
    }

    /**
     * list去重
     *
     * @param list
     */
    public static List removeDuplicate(List list) {
        return (List)list.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 判断数组中是否包含某个元素 忽略大小写
     * @param strList 数组
     * @param strings 元素信息 String[]
     * @return 结果 Boolean
     */
    public static boolean containStringIgnoreCase(List<String> strList,String... strings){
        if(strings.length==0) return false;
        for (String target:strings){
            for(String str:strList){
                if(str.equalsIgnoreCase(target)) return true;
            }
        }
        return false;
    }

}
