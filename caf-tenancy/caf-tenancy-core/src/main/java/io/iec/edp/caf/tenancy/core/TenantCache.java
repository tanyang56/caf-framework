/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package io.iec.edp.caf.tenancy.core;

import lombok.var;

/**
 * 租户分布式缓存实现类
 *
 * @author Hey Law
 * @create 2019年8月20日$
 */
public class TenantCache {

    public TenantCache()
    {
    }

//    /**
//     * https://www.cnblogs.com/boboshenqi/p/9403627.html
//     * 在多线程环境中，volatile能保证共享变量的可见性以及一定程度的有序性
//    */
//    private volatile IDistributedCache cache = null;
    private volatile MemoryCache cache = null;

    /// <summary>
    /// 内部分布式缓存访问对象
    /// </summary>
    public MemoryCache cache()
    {
        if (this.cache == null)
        {
            synchronized (TenantCache.class)
            {
                if (this.cache == null)
                {
//                    this.cache = cacheFactory.getDistributedCache("Multitenancy");
                    this.cache = new MemoryCache();
                }
            }
        }
        return this.cache;
    }

    /**
     * @Description: 根据key值返回缓存项
     * @param key 键值
     * @param clazz 类型
     * @return 缓存项
    */
    public <TData> TData get(String key, Class<TData> clazz)
    {
        if (!key.isEmpty() && this.cache() != null)
        {
            var ss = this.cache().get(key, clazz);
            return ss;
        }
        return null;
    }

    /**
     * @Description: 根据key值返回缓存项
     * @param hashKey hash键
     * @param key 键
     * @param clazz 类型
     * @return
    */
    public <TData> TData hashGet(String hashKey, String key, Class<TData> clazz)
    {
        if (!key.isEmpty() && this.cache() != null)
        {
            TData[] list = this.cache().hashMemberGet(hashKey, new String[] { key }, clazz);
            if (list != null && list.length > 0)
            {
                return list[0];
            }
        }
        return null;
    }

    /**
     * @Description: 将Tenant信息加入分布式缓存
     * @param cacheKey 缓存键值
     * @param cacheObject 缓存对象
     * @return
    */
    public void set(String cacheKey,Object cacheObject)
    {
        if (!cacheKey.isEmpty() && this.cache() != null)
        {
            this.cache().set(cacheKey, cacheObject);
        }
    }

    /**
     * @Description: 将Tenant信息加入分布式缓存
     * @param hashKey hash键
     * @param cacheKey 键
     * @param cacheObject 缓存对象
     * @return
     */
    public void hashSet(String hashKey, String cacheKey, Object cacheObject)
    {
        if (!cacheKey.isEmpty() && this.cache() != null)
        {
            this.cache().hashSet(hashKey, cacheKey, cacheObject);
        }
    }

    /**
     * @Description: 将Tenant信息从分布式缓存删除
     * @param cacheKey 缓存键值
     * @return
    */
    public void remove(String cacheKey)
    {
        if (!cacheKey.isEmpty() && this.cache() != null)
        {
            this.cache().remove(cacheKey);
        }
    }

    /**
     * @Description: 将Tenant信息从分布式缓存删除
     * @param hashKey hash键值
     * @return key 缓存键值
    */
    public void hashRemove(String hashKey,String key)
    {
        if (!hashKey.isEmpty() && !key.isEmpty() && this.cache() != null)
        {
            this.cache().hashRemove(hashKey, key);
        }
    }
}
