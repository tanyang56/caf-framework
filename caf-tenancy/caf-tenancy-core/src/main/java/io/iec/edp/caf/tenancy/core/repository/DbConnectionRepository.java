/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.repository;

import io.iec.edp.caf.tenancy.api.entity.DbConnectionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * DbConnectionRepository$
 *
 * @author Hey Law
 * @create 2019年8月21日19:13:54$
 */
public interface DbConnectionRepository extends JpaRepository<DbConnectionInfo, String> {

    @Query(value = "SELECT d.id,d.dbType,d.connectionString,d.lastModifiedTime FROM ecpdbconnection d, ecpsudbmapping s WHERE d.id = s.dbConnectionId and s.appInstanceId = ?1", nativeQuery = true)
    List<DbConnectionInfo> findByAppInstanceId(String appInstanceId);
}
