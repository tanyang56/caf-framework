/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.service;

import io.iec.edp.caf.tenancy.api.entity.ClusterApp;
import io.iec.edp.caf.tenancy.core.TenantCache;
import io.iec.edp.caf.tenancy.core.repository.ClusterAppRepository;
import io.iec.edp.caf.tenancy.core.utils.DataValidator;

/**
 * ClusterAppMgr
 *
 * @author Hey Law
 * @create 2019年8月21日17:57:08
 */
public class ClusterAppMgr {

    private final ClusterAppRepository instanceClusterRelationRepository;

    private final TenantCache tenantCache;

    public ClusterAppMgr(ClusterAppRepository instanceClusterRelationRepository, TenantCache tenantCache)
    {
        this.instanceClusterRelationRepository = instanceClusterRelationRepository;
        this.tenantCache = tenantCache;
    }

    /**
     * 根据应用实例标识获取集群编号
     *
     * @param appInstanceId 应用实例标识
     * @return
     * @Date: 2019/8/21
    */
    public String get(String appInstanceId)
    {
        DataValidator.checkForEmptyString(appInstanceId, "appInstanceId");
        //to do 暂时不用 去掉
        return "";
    }


    /**
     * 添加
     *
     * @param appInstanceId 应用实例标识
     * @param clusterId 集群Id
     * @return
     * @Date: 2019/8/21
    */
    public void add(String appInstanceId,String clusterId)
    {
        DataValidator.checkForEmptyString(appInstanceId, "appInstanceId");
        DataValidator.checkForEmptyString(clusterId, "clusterId");

        String cacheKey = this.getCacheKey(appInstanceId);

        ClusterApp clusterAppInfo = new ClusterApp();
        clusterAppInfo.setId(appInstanceId);
        clusterAppInfo.setClusterId(clusterId);
        this.instanceClusterRelationRepository.save(clusterAppInfo);

        tenantCache.set(cacheKey, clusterId);
    }

    /**
     * 更新租户信息
     *
     * @param appInstanceId 应用实例标识
     * @param clusterId 集群Id
     * @return
     * @Date: 2019/8/21
    */
    public boolean update(String appInstanceId, String clusterId)
    {
        DataValidator.checkForEmptyString(appInstanceId, "appInstanceId");
        DataValidator.checkForEmptyString(clusterId, "clusterId");

        String cacheKey = this.getCacheKey(appInstanceId);

        ClusterApp clusterAppInfo = new ClusterApp();
        clusterAppInfo.setId(appInstanceId);
        clusterAppInfo.setClusterId(clusterId);
        this.instanceClusterRelationRepository.save(clusterAppInfo);

        tenantCache.set(cacheKey, clusterId);

        return true;
    }

    /**
     * 删除
     *
     * @param id ClusterId
     * @return
     * @Date: 2019/8/21
    */
    public boolean remove(String id)
    {
        DataValidator.checkForEmptyString(id, "ClusterId");
        String cacheKey = this.getCacheKey(id);
        this.instanceClusterRelationRepository.deleteById(id);
        tenantCache.remove(cacheKey);
        return true;
    }

    /**
     * 删除租户信息
     *
     * @param id ClusterId
     * @return
     * @Date: 2019/8/21
    */
    public boolean removeFromCache(String id)
    {
        DataValidator.checkForEmptyString(id, "ClusterId");
        String cacheKey = this.getCacheKey(id);
        tenantCache.remove(cacheKey);
        return true;
    }

    /**
     * 根据租户标识获取缓存key
     *
     * @param id ClusterId
     * @return
     * @Date: 2019/8/21
     */
    private String getCacheKey(String id)
    {
        return String.format("ClusterRelation_%s", id);
    }
}
