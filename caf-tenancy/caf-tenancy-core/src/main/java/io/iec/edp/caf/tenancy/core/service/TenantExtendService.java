package io.iec.edp.caf.tenancy.core.service;

import io.iec.edp.caf.tenancy.api.ITenantExtendService;
import io.iec.edp.caf.tenancy.api.entity.TenantExtend;
import io.iec.edp.caf.tenancy.core.repository.TenantExtendRepository;

public class TenantExtendService implements ITenantExtendService {

    private final TenantExtendRepository extendRepository;

    public TenantExtendService(TenantExtendRepository extendRepository){
        this.extendRepository = extendRepository;
    }

    @Override
    public TenantExtend getTenantExtendByTenantId(Integer tenantId) {
        return extendRepository.findByTenantId(tenantId);
    }

    @Override
    public TenantExtend getTenantExtendByExtendId(String extendId) {
        return extendRepository.findByExtendTenant(extendId);
    }

    @Override
    public void saveTenantExtend(TenantExtend tenantExtend) {
        extendRepository.save(tenantExtend);
    }
}
