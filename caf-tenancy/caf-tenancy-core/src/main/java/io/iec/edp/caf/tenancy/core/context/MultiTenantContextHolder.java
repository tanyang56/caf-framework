/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.context;


import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.core.context.ICAFContextService;
import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
import lombok.extern.slf4j.Slf4j;

/**
 * TenantContext
 *
 * @author Hey Law
 */
@Slf4j
public class MultiTenantContextHolder {

    /**
     * @Description: 获取当前线程保存变量值
    */
    private static final InheritableThreadLocal<MultiTenantContextInfo> AsyncLocalContext = new InheritableThreadLocal();

    /**
     * @Description: 获取当前TenantContextInfo
    */
    public static MultiTenantContextInfo get()
    {
        MultiTenantContextInfo current = AsyncLocalContext.get();
        ICAFContextService context = SpringBeanUtils.getBean(ICAFContextService.class);
        if (current == null) {
            MultiTenantContextInfo multiTenantContextInfo = new MultiTenantContextInfo();
            if(context!=null) {
                multiTenantContextInfo.setTenantId(context.getTenantId());
                multiTenantContextInfo.setAppCode(context.getAppCode());
                multiTenantContextInfo.setServiceUnit(context.getCurrentSU());
                multiTenantContextInfo.setLanguage(context.getLanguage());
            }
//            AsyncLocalContext.set(multiTenantContextInfo);
            return multiTenantContextInfo;
        } else{
            //强制指定为当前上下文的su
            if(context!=null) {
                String su = context.getCurrentSU();
                if (su != null && !"".equals(su))
                    current.setServiceUnit(su);
            }
        }

        return current;
    }

    /**
     * @Description: 设置当前MultiTenantContextInfo
     */
    public static void set(MultiTenantContextInfo value)
    {
        synchronized (AsyncLocalContext)
        {
            MultiTenantContextInfo current = AsyncLocalContext.get();

            if (value == null)
            {
                if (current== null)
                {
                    return;
                }
                //取到外层的上下文，如果外层不为null，将外层设置为当前
                MultiTenantContextInfo outer = current.getOuterContexInfo();
                if (outer == null)
                {
                    AsyncLocalContext.set(null);
                    return;
                }
                AsyncLocalContext.set(outer);
            }
            else
            {
                value.setOuterContexInfo(current);
                AsyncLocalContext.set(value);
            }
        }
    }

    public static void purge()
    {
        synchronized (AsyncLocalContext)
        {
            AsyncLocalContext.remove();
        }
    }
}
