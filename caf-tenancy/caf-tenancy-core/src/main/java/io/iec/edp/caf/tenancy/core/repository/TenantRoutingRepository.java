/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.repository;

import io.iec.caf.data.jpa.repository.CafJpaRepository;
import io.iec.edp.caf.tenancy.api.entity.TenantRouting;
import org.springframework.data.jpa.repository.Query;

/**
 * TenantRoutingRepository
 *
 * @author guowenchang
 */
public interface TenantRoutingRepository extends CafJpaRepository<TenantRouting, Integer> {

    @Query(value = "select tenantId from ecptenantrouting " +
            "where suName = ?1 and tenantDim1 = ?2 " +
            "and current_date between startDate and endDate",
            nativeQuery = true)
    Integer findAvailableTenantId(String suName, String tenantDim1);

    @Query(value = "select tenantId from ecptenantrouting " +
            "where suName = ?1 and tenantDim1 = ?2 and tenantDim2 = ?3 " +
            "and current_date between startDate and endDate",
            nativeQuery = true)
    Integer findAvailableTenantId(String suName, String tenantDim1, String tenantDim2);
}
