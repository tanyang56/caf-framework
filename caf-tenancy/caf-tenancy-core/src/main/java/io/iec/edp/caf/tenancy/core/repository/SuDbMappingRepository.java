/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.repository;

import io.iec.edp.caf.tenancy.api.entity.SuDbMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


/**
 * SuDbMapping仓储
 *
 * @author Hey Law
 * @create 2019年8月19日$
 */
public interface SuDbMappingRepository extends JpaRepository<SuDbMapping, String> {

    /**
     * 根据AppInstanceId查找所有符合的SuDbMapping
     *
     * @return
     * @Date: 2019/8/21
    */
    List<SuDbMapping> findAllByAppInstanceId(String appInstanceId);


    /**
     * 根据应用实例和服务单元获取连接映射信息
     *
     * @param
     * @return
     * @Date: 2019/8/ 21
     */
    @Query(value = "SELECT id,appInstanceId,su,dbConnectionId,lastModifiedTime FROM ecpsudbmapping s WHERE s.appInstanceId = ?1 and (s.su=?2 or s.su ='*')", nativeQuery = true)
//    SuDbMapping findByAppInstanceIdAndSu(String appInstanceId, String su);
    List<SuDbMapping> findByAppInstanceIdAndSu(String appInstanceId, String su);

}
