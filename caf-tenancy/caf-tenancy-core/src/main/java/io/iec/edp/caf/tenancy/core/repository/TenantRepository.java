/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.repository;

import io.iec.caf.data.jpa.repository.CafJpaRepository;
import io.iec.edp.caf.tenancy.api.entity.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * EcpLanguageRepository$
 *
 * @author Hey Law
 * @create 2019年8月22日09:49:49$
 */
public interface TenantRepository extends CafJpaRepository<Tenant, Integer> {

    Tenant findByCode(String code);

    void deleteById(Integer id);
}
