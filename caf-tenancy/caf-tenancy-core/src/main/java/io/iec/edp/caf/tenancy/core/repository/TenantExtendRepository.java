package io.iec.edp.caf.tenancy.core.repository;

import io.iec.caf.data.jpa.repository.CafJpaRepository;
import io.iec.edp.caf.tenancy.api.entity.TenantExtend;

import java.util.List;

public interface TenantExtendRepository extends CafJpaRepository<TenantExtend, Integer> {
    TenantExtend findByTenantId(Integer tenantId);

    TenantExtend findByExtendTenant(String extendTenantId);
}
