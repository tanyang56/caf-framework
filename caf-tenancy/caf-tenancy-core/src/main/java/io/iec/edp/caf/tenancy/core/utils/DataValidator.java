/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.utils;

import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
import io.iec.edp.caf.tenancy.api.exception.DataValidatorException;
import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;

/**
 * DataValidator$
 *
 * @author Hey Law
 * @create 2019年8月21日14:20:11$
 */
public class DataValidator {

    public static void checkForEmptyString(String variable, String variableName) {
        if (variable.isEmpty()) {
            MultiTenantContextInfo multiTenantContextInfo = MultiTenantContextHolder.get();
            throw new DataValidatorException(multiTenantContextInfo.getServiceUnit(), "CheckForEmptyString", String.format("参数%s为空！",variableName), null, ExceptionLevel.Error, false);
        }
    }

    public static void checkForNullReference(Object variable, String variableName)
    {
        if (variable == null) {
            MultiTenantContextInfo multiTenantContextInfo = MultiTenantContextHolder.get();
            throw new DataValidatorException(multiTenantContextInfo.getServiceUnit(), "CheckForEmptyString", String.format("参数%s为null！",variableName), null, ExceptionLevel.Error, false);
        }
    }
}
