/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.extensions;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.data.source.DataSourceIdentifierResolver;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;

//todo 被[io/iec/edp/caf/security/core/spring/config/CafSecurityAutoConfiguration依赖
public class MultiTenantIdentifierResolver implements CurrentTenantIdentifierResolver {
    private DataSourceIdentifierResolver identifierResolver = null;
    @Override
    public String resolveCurrentTenantIdentifier() {
        if(this.identifierResolver ==null){
            identifierResolver = SpringBeanUtils.getBean(DataSourceIdentifierResolver.class);
        }
        return this.identifierResolver.resolveDataSourceIdentifier();
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
