/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.service;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.api.LanguageSuffixProvider;
import io.iec.edp.caf.tenancy.api.entity.Tenant;
import io.iec.edp.caf.tenancy.core.TenantCache;
import io.iec.edp.caf.tenancy.core.repository.TenantRepository;
import io.iec.edp.caf.tenancy.core.utils.DataValidator;
import lombok.var;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.persister.entity.PersistersContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TenantManager$
 *
 * @author Hey Law
 * @create 2019年8月22日09:54:37$
 */
public class TenantManager {

    private final TenantRepository repository;

    private final Log log = LogFactory.getLog(TenantManager.class);

//    private EcpLanguageRepository langRepository;

    //所有租户的缓存
    private final Map<String,List<Tenant>> allTenantsCache = new HashMap<>();

    //具体某一个租户的缓存（分布式缓存）
    private final TenantCache tenantCache;

    public TenantManager(TenantRepository repository, TenantCache tenantCache)
    {
        this.repository = repository;
//        this.langRepository = langRepository;
        this.tenantCache = tenantCache;
    }

    /// <summary>
    /// 根据租户标识获取详细信息
    /// </summary>
    /// <param name="tenantId">租户标识</param>
    /// <returns>租户信息</returns>
    /**
     * 根据租户标识获取详细信息
     *
     * @param tenantId 租户标识
     * @param language 语种编号
     * @return
     * @Date: 2019/8/22
     */
    public Tenant get(int tenantId, String language)
    {
        DataValidator.checkForNullReference(tenantId, "tenantId");

        String cacheKey = this.getCacheKey(language, tenantId);
        var tenant = tenantCache.get(cacheKey, Tenant.class);
        try
        {
            if (tenant == null) {
                this.setLanguageFieldSuffix(language);
//            String suffix = this.langRepository.findByCode(language).getFieldSuffix();
                var tmp = this.repository.findById(tenantId);
                if (tmp.isPresent()) {
                    tenant = tmp.get();
                }
                this.addCache(tenantId, language, tenant);
            }
        }
        catch (Exception ex)
        {
            tenant = null;
        }

        return tenant;
    }

    /**
     * 根据租户标识获取详细信息
     *
     * @param language 语种编号
     * @return
     * @Date: 2019/8/22
     */
    public List<Tenant> getAllTenants(String language)
    {
        //若已缓存
        if(allTenantsCache.containsKey(language)) return allTenantsCache.get(language);
        else{
//        String suffix = this.langRepository.findByCode(language).getFieldSuffix();
            this.setLanguageFieldSuffix(language);
            List<Tenant> allTenants = this.repository.findAll();
            allTenantsCache.put(language,allTenants);
            return allTenants;
        }
    }

    /// <summary>
    /// 根据租户编号获取详细信息
    /// </summary>
    /// <param name="tenantCode">租户编号</param>
    /// <returns>租户信息</returns>
    public Tenant getByCode(String tenantCode, String language) {
        DataValidator.checkForEmptyString(tenantCode, "tenantCode");

        this.setLanguageFieldSuffix(language);
        return this.repository.findByCode(tenantCode);
    }

    /**
     * 更新租户信息
     *
     * @param tenant 租户
     * @param language 语种编号
     * @return
     * @Date: 2019/8/22
     */
    public void add(Tenant tenant,String language)
    {
        DataValidator.checkForNullReference(tenant, "Tenant");

        if (tenant != null)
        {
            //        String suffix = this.langRepository.findByCode(language).getFieldSuffix();
            this.setLanguageFieldSuffix(language);
            this.repository.save(tenant);
            this.addCache(tenant.getId(), language, tenant);
        }
    }

    /**
     * 删除租户信息
     *
     * @param id 租户Id
     * @return
     * @Date: 2019/8/22
     */
    public boolean remove(int id)
    {
        DataValidator.checkForNullReference(id, "TenantId");

        this.repository.deleteById(id);
        this.removeCache(id);
        return true;
    }

    /**
     * 根据租户标识获取缓存key
     *
     * @param language 语种编号
     * @param tenantId 租户标识
     * @return
     * @Date: 2019/8/22
     */
    private String getCacheKey(String language,int tenantId)
    {
        return String.format("Tenant_%s_%s", language, tenantId);
    }

    /**
     * 加入缓存
     *
     * @param language 语种编号
     * @param tenantId 租户标识\
     * @param tenant  租户
     * @return
     * @Date: 2019/8/22
     */
    private void addCache(int tenantId, String language, Tenant tenant)
    {
        String cacheKey = this.getCacheKey(language, tenantId);
        tenantCache.set(cacheKey, tenant);

        //更新语言的索引,此处有并发问题，后续完善
        String cacheKey2 = String.format("Tenant_%s_index", tenantId);

        List<String> arr = new ArrayList<>();
        arr = tenantCache.get(cacheKey2,arr.getClass());
        if (arr == null) {
            arr = new ArrayList<>();
            tenantCache.set(cacheKey2, arr);
        }
    }

    /**
     * 删除缓存
     *
     * @param
     * @return
     * @Date: 2019/8/22
     */
    private void removeCache(int tenantId)
    {
        //更新语言的索引,此处有并发问题，后续完善
        String cacheKey = String.format("Tenant_%s_index", tenantId);

        List<String> arr = new ArrayList<>();
        arr = tenantCache.get(cacheKey,arr.getClass());
        if (arr == null)
        {
            arr = new ArrayList<>();
        }

        for (var item : arr)
        {
            tenantCache.remove(item);
        }
        tenantCache.remove(cacheKey);
    }
    /**
     * 设置语言后缀
     */
    private void setLanguageFieldSuffix(String language){
        LanguageSuffixProvider suffixProvider = SpringBeanUtils.getBean(LanguageSuffixProvider.class);
        String currentLangSuffix = suffixProvider.getFieldSuffix(language);

        String ctxLangSuffix = PersistersContext.getCurrentLangSuffix();
        if(!currentLangSuffix.equals(ctxLangSuffix))
            PersistersContext.setCurrentLangSuffix(currentLangSuffix);
    }
}
