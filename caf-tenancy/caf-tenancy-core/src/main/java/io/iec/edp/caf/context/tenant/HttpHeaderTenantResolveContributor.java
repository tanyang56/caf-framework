/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.context.tenant;

import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.tenancy.api.TenantResolveContributor;

import javax.servlet.http.HttpServletRequest;

/**
 * 从Header中获取租户
 *
 * @author guowenchang
 * @date 2021-01-27
 */
public class HttpHeaderTenantResolveContributor implements TenantResolveContributor {

    @Override
    public Integer resolveTenantId(HttpServletRequest request) {
        String tenantIdString = request.getHeader("TenantId");
        if (StringUtils.isEmpty(tenantIdString)) {
            return null;
        }

        return Integer.valueOf(tenantIdString);
    }
}
