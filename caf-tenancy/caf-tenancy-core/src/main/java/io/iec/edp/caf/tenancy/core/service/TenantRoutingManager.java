/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.service;

import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.tenancy.api.entity.TenantRouting;
import io.iec.edp.caf.tenancy.core.repository.TenantRoutingRepository;

import java.util.HashMap;

/**
 * 租户路由Manager
 *
 * @author guowenchang
 * @date 2021-01-15
 */
public class TenantRoutingManager {

    private final TenantRoutingRepository repository;

    //private final HashMap<String, TenantRouting> tenantRoutingCache = new HashMap<>();

    public TenantRoutingManager(TenantRoutingRepository repository) {
        this.repository = repository;
    }

    public Integer getTenantId(String suName, String tenantDim1, String tenantDim2) {

        if (StringUtils.isEmpty(tenantDim2)) {
            return repository.findAvailableTenantId(suName, tenantDim1);
        } else {
            return repository.findAvailableTenantId(suName, tenantDim1, tenantDim2);
        }
    }
}
