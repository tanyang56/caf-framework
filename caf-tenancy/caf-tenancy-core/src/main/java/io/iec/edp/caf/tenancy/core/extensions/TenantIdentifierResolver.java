/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.extensions;//package io.iec.edp.caf.tenancy.core.extensions;
//
//import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
//import io.iec.edp.caf.tenancy.api.entity.DbConnection;
//import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;
//import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
//
//
///**
// * 这个类是由Hibernate提供的用于识别tenantId的类，当每次执行sql语句被拦截就会调用这个类中的方法来获取tenantId
// * @author lanyuanxiaoyao
// * @version 1.0
// */
//public class TenantIdentifierResolver implements CurrentTenantIdentifierResolver{
//
//    @Override
//    public String resolveCurrentTenantIdentifier() {
//        MultiTenantContextInfo multiTenantContextInfo = MultiTenantContextHolder.get();
//
//        if(multiTenantContextInfo.isMasterDb())
//        {
//            return TenantDataSourceProvider.MASTERDB;
//        }
//
//        DbConnection dbConnectionInfo = multiTenantContextInfo.getDbConnectionInfo();
//
//        if (dbConnectionInfo == null)
//        {
//            //上下文不存在租户时，直接返回主库
//            Integer tenantId = multiTenantContextInfo.getTenantId();
//            if(tenantId<=0)
//                return TenantDataSourceProvider.MASTERDB;
//            return tenantId  + "," + multiTenantContextInfo.getAppCode()  + "," + multiTenantContextInfo.getServiceUnit();
//        }
//        else
//        {
//            return dbConnectionInfo.getDbType() + "," +dbConnectionInfo.getConnectionString();
//        }
//    }
//
//    @Override
//    public boolean validateExistingCurrentSessions() {
//        return true;
//    }
//}
