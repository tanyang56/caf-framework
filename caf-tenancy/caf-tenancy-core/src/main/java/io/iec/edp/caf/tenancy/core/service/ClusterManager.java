/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.core.service;

import io.iec.edp.caf.tenancy.core.TenantCache;
import io.iec.edp.caf.tenancy.core.repository.ClusterRepository;
import io.iec.edp.caf.tenancy.core.utils.DataValidator;
import io.iec.edp.caf.tenancy.api.entity.*;

import lombok.var;

/**
 * ClusterManager$
 *
 * @author Hey Law
 * @create 2019年8月22日09:31:42$
 */
public class ClusterManager {

    private final ClusterRepository repo;

    private final TenantCache tenantCache;

    public ClusterManager(ClusterRepository repo, TenantCache tenantCache)
    {
        this.repo = repo;
        this.tenantCache = tenantCache;
    }

    /// <summary>
    /// 根据租户标识获取详细信息
    /// </summary>
    /// <param name="tenantId">租户标识</param>
    /// <returns>租户信息</returns>
    /**
     * 根据租户标识获取详细信息
     *
     * @param id 集群定义标识
     * @return
     * @Date: 2019/8/22
    */
    public ClusterInfo get(String id)
    {
        DataValidator.checkForEmptyString(id, "ClusterId");

        String cacheKey = this.getCacheKey(id);
        var info = tenantCache.get(cacheKey, ClusterInfo.class);

        if (info == null) {
            info = this.repo.findById(id).get();
            if (info != null) {
                tenantCache.set(cacheKey, info);
            }
        }
        return info;
    }

    /// <summary>
    /// 添加
    /// </summary>
    /** 
     * 添加
     * 
     * @param info 集群定义
     * @return 
     * @Date: 2019/8/22
    */ 
    public void add(ClusterInfo info)
    {
        DataValidator.checkForNullReference(info, "ClusterInfo");
        String cacheKey = this.getCacheKey(info.getId());
        info = this.repo.save(info);
        tenantCache.set(cacheKey, info);
    }
    
    /** 
     * 更新集群定义信息
     * 
     * @param 
     * @return 
     * @Date: 2019/8/22
    */ 
    public boolean update(ClusterInfo info)
    {
        DataValidator.checkForNullReference(info, "ClusterInfo");

        String cacheKey = this.getCacheKey(info.getId());
        info = this.repo.save(info);
        tenantCache.set(cacheKey, info);
        return true;
    }

    /** 
     * 删除
     * 
     * @param id 集群定义标识
     * @return 
     * @Date: 2019/8/22
    */ 
    public boolean remove(String id)
    {
        DataValidator.checkForEmptyString(id, "ClusterId");

        String cacheKey = this.getCacheKey(id);
        this.repo.deleteById(id);
        tenantCache.remove(cacheKey);
        return true;
    }


    /** 
     * 删除租户信息
     * 
     * @param 
     * @return 
     * @Date: 2019/8/22
    */ 
    public boolean removeFromCache(String id)
    {
        DataValidator.checkForEmptyString(id, "ClusterId");
        String cacheKey = this.getCacheKey(id);
        tenantCache.remove(cacheKey);
        return true;
    }


    /**
     * 根据j集群标识获取缓存key
     *
     * @param
     * @return
     * @Date: 2019/8/22
    */
    private String getCacheKey(String id)
    {
        return String.format("Cluster_%s", id);
    }

}
