//package testservice;
//
//import io.iec.edp.caf.tenancy.api.entity.Tenant;
//
//import io.iec.edp.caf.tenancy.core.TenantCache;
//import io.iec.edp.caf.tenancy.core.repository.EcpLanguageRepository;
//import io.iec.edp.caf.tenancy.core.repository.TenantRepository;
//import io.iec.edp.caf.tenancy.core.utils.DataValidator;
//import lombok.var;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.utils.ArrayList;
//import java.utils.List;
//
///**
// * TenantManager$
// *
// * @author Hey Law
// * @create 2019年8月22日09:54:37$
// */
//@Service
//public class TenantManagerForTest {
//
//    @Autowired
//    private TenantRepository repository;
//
//    @Autowired
//    private EcpLanguageRepository langRepository;
//
//    @Autowired
//    private TenantCache tenantCache;
//
//    /// <summary>
//    /// 根据租户标识获取详细信息
//    /// </summary>
//    /// <param name="tenantId">租户标识</param>
//    /// <returns>租户信息</returns>
//    /**
//     * 根据租户标识获取详细信息
//     *
//     * @param tenantId 租户标识
//     * @param language 语种编号
//     * @return
//     * @Date: 2019/8/22
//    */
//    public Tenant get(int tenantId, String language)
//    {
//        DataValidator.CheckForNullReference(tenantId, "tenantId");
//
//        String cacheKey = this.getCacheKey(language, tenantId);
////        var tenant = tenantCache.get(cacheKey, Tenant.class);
//        Tenant tenant = null;
//        try
//        {
//            if (tenant == null)
//            {
////            String suffix = this.langRepository.findByCode(language).getFieldSuffix();
//              var tmp = this.repository.findById(tenantId);
//              if (tmp.isPresent())
//              {
//                  tenant = tmp.get();
//              }
////            this.addCache(tenantId, language, tenant);
//            }
//        }
//        catch (Exception ex)
//        {
//
//            tenant = null;
//        }
//
//        return tenant;
//    }
//
//    /**
//     * 根据租户标识获取详细信息
//     *
//     * @param language 语种编号
//     * @return
//     * @Date: 2019/8/22
//    */
//    public List<Tenant> getAllTenants(String language)
//    {
////        String suffix = this.langRepository.findByCode(language).getFieldSuffix();
//        return this.repository.findAll();
//    }
//
//    /**
//     * 更新租户信息
//     *
//     * @param tenant 租户
//     * @param language 语种编号
//     * @return
//     * @Date: 2019/8/22
//    */
//    public void add(Tenant tenant,String language)
//    {
//        DataValidator.CheckForNullReference(tenant, "Tenant");
//
//        if (tenant != null)
//        {
//            //        String suffix = this.langRepository.findByCode(language).getFieldSuffix();
//            this.repository.save(tenant);
//            this.addCache(tenant.getId(), language, tenant);
//        }
//    }
//
//    /**
//     * 删除租户信息
//     *
//     * @param id 租户Id
//     * @return
//     * @Date: 2019/8/22
//    */
//    public boolean remove(int id)
//    {
//        DataValidator.CheckForNullReference(id, "TenantId");
//
//        this.repository.deleteById(id);
//        this.removeCache(id);
//        return true;
//    }
//
//    /**
//     * 根据租户标识获取缓存key
//     *
//     * @param language 语种编号
//     * @param tenantId 租户标识
//     * @return
//     * @Date: 2019/8/22
//    */
//    private String getCacheKey(String language,int tenantId)
//    {
//        return String.format("Tenant_%s_%s", language, tenantId);
//    }
//
//    /**
//     * 加入缓存
//     *
//     * @param language 语种编号
//     * @param tenantId 租户标识\
//     * @param tenant  租户
//     * @return
//     * @Date: 2019/8/22
//    */
//    private void addCache(int tenantId, String language, Tenant tenant)
//    {
//        String cacheKey = this.getCacheKey(language, tenantId);
//        tenantCache.set(cacheKey, tenant);
//
//        //更新语言的索引,此处有并发问题，后续完善
//        String cacheKey2 = String.format("Tenant_%s_index", tenantId);
//
//        List<String> arr = new ArrayList<>();
//        tenantCache.get(cacheKey2,arr.getClass());
//        if (arr == null)
//            arr = new ArrayList<>();
//        tenantCache.set(cacheKey2, arr);
//    }
//
//    /**
//     * 删除缓存
//     *
//     * @param
//     * @return
//     * @Date: 2019/8/22
//    */
//    private void removeCache(int tenantId)
//    {
//        //更新语言的索引,此处有并发问题，后续完善
//        String cacheKey = String.format("Tenant_%s_index", tenantId);
//
//        List<String> arr = new ArrayList<>();
//        tenantCache.get(cacheKey,arr.getClass());
//        if (arr == null)
//        {
//            arr = new ArrayList<>();
//        }
//
//        for (var item : arr)
//        {
//            tenantCache.remove(item);
//        }
//        tenantCache.remove(cacheKey);
//    }
//
//}
