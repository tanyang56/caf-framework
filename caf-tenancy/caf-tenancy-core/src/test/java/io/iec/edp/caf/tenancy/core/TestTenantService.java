//package io.iec.edp.caf.tenancy.core;
//
//import io.iec.edp.caf.tenancy.api.TenantService;
//import io.iec.edp.caf.tenancy.api.context.IMultiTenancyContext;
//import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
//import io.iec.edp.caf.tenancy.api.context.MultiTenantContextManager;
//import io.iec.edp.caf.tenancy.api.entity.Tenant;
//import io.iec.edp.caf.tenancy.core.utils.AESUtil;
//import lombok.var;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import testservice.TenantManagerForTest;
//
///**
// * TestTenantService$
// *
// * @author Hey Law
// * @create 2019年8月26日13:50:21$
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = TenantConfiguration.class)
//public class TestTenantService {
//    @Autowired
//    private TenantService tenantService;
//
//    @Autowired
//    private MultiTenantContextManager multiTenantContextManager;
//
//    @Autowired
//    private TenantManagerForTest tenantManagerForTest;
//
//    @Test
//    public void getTenant() {
//        tenantService.getTenant(1,"23");
//    }
//
//    @Test
//    public void getDBConnInfo() {
//        var ss = tenantService.getDBConnInfo(10000, "10000", "test");
//    }
//
//    @Test
//    public void get() {
//        Tenant ssss前 = tenantService.getTenant(1,"23");
//
//        Tenant ssss前10000 = tenantService.getTenant(10000,"23");
//
//        Tenant ss = null;
//
//        try(IMultiTenancyContext multiTenancyContext = multiTenantContextManager.getCurrent())
//        {
//            MultiTenantContextInfo multiTenantContextInfo = new MultiTenantContextInfo();
//            multiTenantContextInfo.setTenantId(10000);
//            multiTenantContextInfo.setAppCode("10000");
//            multiTenantContextInfo.setServiceUnit("test");
//            multiTenancyContext.set(multiTenantContextInfo);
//
//            ss = tenantManagerForTest.get(1,"123");
//        }
//        catch (Throwable e) {
//            //异常通知
//            System.out.println("The method " + "get" + " occurs expection : " + e);
//            throw new RuntimeException(e);
//        }
//
//        Tenant ssss后 = tenantService.getTenant(1,"23");
//        Tenant ssss后10000 = tenantService.getTenant(10000,"23");
//
//        System.out.println(ssss前);
//        System.out.println(ssss前10000);
//        System.out.println(ss);
//        System.out.println(ssss后);
//        System.out.println(ssss后10000);
//
//    }
//
//    @Test
//    public void AESUtilTest() {
//        try
//        {
//            //9fLXOGW/jLfBvuloamPjAA==
////            var ss= AESUtil.aesEncrypt("01");
////            var res = AESUtil.aesDecrypt(ss);
//            var ss= AESUtil.encryptAES("01");
//            ss = null;
//        }
//        catch (Exception ex)
//        {
//            throw new RuntimeException(ex);
//        }
//    }
//
//
//}
