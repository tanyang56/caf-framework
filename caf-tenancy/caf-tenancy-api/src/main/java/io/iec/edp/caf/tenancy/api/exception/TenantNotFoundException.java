/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.exception;

/**
 * 在租户路由时 开发人员应手动catch该异常并做对应处理
 *
 * @author guowenchang
 * @date 2021-01-15
 */
public class TenantNotFoundException extends Exception {

    public TenantNotFoundException(String suName, String tenantDim1, String tenantDim2) {
        super("Su: " + suName + " TenantDim1: " + tenantDim1 + (tenantDim2 == null ? "" : " TenantDim2: " + tenantDim2));
    }
}
