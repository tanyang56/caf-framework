package io.iec.edp.caf.tenancy.api;

import io.iec.edp.caf.tenancy.api.entity.TenantExtend;

public interface ITenantExtendService {
    TenantExtend getTenantExtendByTenantId(Integer tenantId);

    TenantExtend getTenantExtendByExtendId(String extendId);

    void saveTenantExtend(TenantExtend tenantExtend);
}
