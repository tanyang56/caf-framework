/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Tenant路由维度表
 *
 * @author guowenchang
 */
@Entity
@Getter
@Setter
@Table(name = "ecptenantrouting")
public class TenantRouting implements Serializable {

    /**
     * 主键
     */
    @Id
    @Column
    private String id;

    /**
     * SU名称
     */
    @Column
    private String suName;

    /**
     * 维度值1
     */
    @Column
    private String tenantDim1;

    /**
     * 维度值2
     */
    @Column
    private String tenantDim2;

    /**
     * 生效时间
     */
    @Column
    private Date startDate;

    /**
     * 失效时间
     */
    @Column
    private Date endDate;

    /**
     * 租户Id
     */
    @Column
    private int tenantId;
}
