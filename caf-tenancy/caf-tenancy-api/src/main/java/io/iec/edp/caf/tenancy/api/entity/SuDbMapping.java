/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


/**
 * 服务单元~数据库连接映射关系
 *
 * @author Hey Law
 * @create 2019年8月19日$
 */
@Entity
@Data
@Table(name="ecpsudbmapping")
public class SuDbMapping  implements Serializable {

    /**
     *  唯一标识，无实际意义
    */
    @Id
    @Column
    private String id;

    /**
     *  应用实例标识
     */
    @Column
    private String appInstanceId;

    /**
     *  服务单元
     */
    @Column
    private String su;

    /**
     *  连接字符串编号
     */
    @Column
    private String dbConnectionId;


    /**
     *  最后更新时间
     */
    @Column
    private Date lastModifiedTime;

}
