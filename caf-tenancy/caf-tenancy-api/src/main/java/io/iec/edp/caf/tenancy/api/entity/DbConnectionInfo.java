/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

import io.iec.edp.caf.commons.dataaccess.DbType;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据库连接信息
 *
 * @author Hey Law
 * @create 2019年8月19日
 */
@Entity
@Data
@Table(name="ecpdbconnection")
public class DbConnectionInfo implements Serializable {

    /**
     * 唯一标识
     */
    @Id
    @Column
    private String id;

    /**
     * 数据库类型
     */
    @Column
    private DbType dbType;

    /**
     * 数据库连接字符串
     */
    @Column
    private String connectionString;

    /**
     * 最后更新时间
     */
    @Column
    private Date lastModifiedTime;


    @Column
    private String hashKey;

}
