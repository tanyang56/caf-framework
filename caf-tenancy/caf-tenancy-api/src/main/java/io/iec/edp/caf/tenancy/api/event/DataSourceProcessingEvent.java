/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.event;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

import javax.sql.DataSource;

/**
 * 数据源处理事件参数
 * @author wangyandong
 */
@Data
public class DataSourceProcessingEvent extends ApplicationEvent {
    private DataSource dataSource;

    public DataSourceProcessingEvent(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }
}
