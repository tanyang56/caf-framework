/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api;

import io.iec.edp.caf.data.source.UriInfo;
import io.iec.edp.caf.tenancy.api.entity.AppInstanceInfo;
import io.iec.edp.caf.tenancy.api.entity.DbConnectionInfo;
import io.iec.edp.caf.tenancy.api.entity.TenancyMode;
import io.iec.edp.caf.tenancy.api.entity.Tenant;

import java.io.IOException;
import java.util.List;

//todo 被io.iec.edp.caf.authentication.userpassword.core.config.CAFAuthUserPasswordAutoConfiguration依赖
public interface ITenantService {

    /**
     * 获取多租户模式
     * @return
     */
    TenancyMode getTenancyMode();

    /**
     * @Description: 获取所有租户信息
     * @param language 语言编号
     * @return 所有租户信息
     */
    List<Tenant> getAllTenants(String language);

    /**
     * @Description: 根据租户编号获取租户基本信息
     * @param tenantId 租户编号
     * @param language 语言编号
     * @return 租户基本信息
     */
    Tenant getTenant(int tenantId, String language);

    /**
     * 根据租户编号获取租户基本信息
     * @param tenantCode
     * @param language
     * @return
     */
    Tenant getByCode(String tenantCode, String language);

    /**
     * @Description: 根据租户编号获取租户应用信息
     * @param tenantId 租户编号
     * @param appInstanceCode 应用实例编号
     * @return 应用的URL地址
     */
    String getApplicationURL(int tenantId,String appInstanceCode);

    /**
     * @Description: 根据租户编号获取所有的租户实例基本信息(只获取实例基本信息)
     * @param tenantId 租户编号
     * @return 租户数据库信息
     */
    List<AppInstanceInfo> getAllAppInstInfos(int tenantId);

    /**
     * @Description: 根据租户编号获取所有的租户实例信息
     * @param tenantId 租户编号
     * @param appInstanceCode 应用实例编号
     * @return 租户数据库信息
     */
    AppInstanceInfo getAppInstanceInfo(int tenantId, String appInstanceCode);

    /**
     * @Description: 获取所有连接信息
     * @param tenantId 租户Id
     * @param appInstanceCode 应用实例
     * @return 数据库连接信息
     */
    List<DbConnectionInfo> getAllDBConnInfos(int tenantId, String appInstanceCode);

    /**
     *  根据租户、实例编号获取数据库编号
     * @param tenantId 租户编号
     * @param appInstanceCode 应用实例编号
     * @param su
     * @return
     */
    String getDBConnectionId(int tenantId, String appInstanceCode, String su);

    /**
     * @Description:  获取数据库连接信息
     * @param tenantId 租户Id
     * @param appInstanceCode 应用实例
     * @param su 服务单元
     * @return 数据库连接信息
     */
    DbConnectionInfo getDBConnInfo(int tenantId, String appInstanceCode, String su);

    /**
     * 切换租户Id
     * @param openId
     * @param tenantId
     * @throws IOException
     */
    void changeTenantId(String openId,String tenantId) throws IOException;

    /**
     * 新建租户数据库实例
     */
    UriInfo createDbInstance(String schemaName);


    void updateTenant(Tenant tenant,int oldTenantId);

    /**
     *
     * @param tenant 租户实体
     * @param dbStr 数据连接信息
     * @param appInsId 应用实例id
     * @param appInsName 应用实例名称
     * @param appInsDes 应用实例描述
     * @param suList su列表
     */
    void saveTenantInfo(Tenant tenant,String dbStr,String appInsId,
                               String appInsName,String appInsDes,List<String> suList);


}
