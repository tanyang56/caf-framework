/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.context;

import io.iec.edp.caf.tenancy.api.entity.DbConnectionInfo;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * 多租户上下文信息类
 *
 * @author Hey Law
 * @create 2019年8月20日$
 */
@Getter
@Setter
public class MultiTenantContextInfo {

    /**
     *  外层上下文
     */
    private MultiTenantContextInfo outerContexInfo;

    /**
     *  租户
     */
    private int tenantId;

    /**
     *  语言
     */
    private String language;

    /// <summary>
    /// 语言
    /// </summary>
    /**
     *  语言后缀
     */
    private String languageSuffix;

    /**
     *  连接信息
     */
    private String serviceUnit;

    /**
     *  应用实例
     */
    private String appCode;

    /**
     *  用户
     */
    private String userId;

    /**
     *  用户
     */
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    private boolean MasterDb;

    /**
     *  连接信息
     */
    private DbConnectionInfo dbConnectionInfo;

}
