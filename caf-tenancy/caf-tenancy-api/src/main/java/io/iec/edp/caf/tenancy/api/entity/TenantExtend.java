package io.iec.edp.caf.tenancy.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name="ecptenantextend")
public class TenantExtend {
    /**
     * 租户唯一标识
     */
    @Id
    @Column
    private String id;

    @Column
    private String extendDomain;

    @Column
    private String extendParam;

    @Column
    private String extendTenant;

    @Column
    private Integer tenantId;
}
