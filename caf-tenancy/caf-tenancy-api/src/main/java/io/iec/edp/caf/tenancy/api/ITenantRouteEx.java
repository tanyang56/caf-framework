/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api;

import io.iec.edp.caf.multicontext.annotation.Collect;
import io.iec.edp.caf.tenancy.api.exception.TenantNotFoundException;

/**
 * 租户路由服务扩展接口
 *
 * @author guowenchang
 * @date 2021-01-15
 */
@Collect
public interface ITenantRouteEx {

    /**
     * 返回一个SU名称 用于标记自己是哪个SU的租户解析器
     *
     * @return
     */
    String getSuName();

    /**
     * 路由接口
     *
     * @param tenantDim1 路由维度
     * @param tenantDim2 路由值
     * @return 租户id
     * @throws TenantNotFoundException 为了约束异常行为的一致性 找不到租户式应显式抛出异常
     */
    Integer route(String tenantDim1, String tenantDim2) throws TenantNotFoundException;
}
