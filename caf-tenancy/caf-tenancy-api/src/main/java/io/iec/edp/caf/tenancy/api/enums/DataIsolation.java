/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.enums;

/**
 * 多租户数据隔离模式枚举
 *
 * @author Hey Law
 * @create 2019年8月19日$
 */
public enum DataIsolation {

    /**
     *  非多租户，为默认值
    */
    None,

    /**
     *  一个租户一个Database
     */
    Database,

    /**
     *  一个租户一个 Schema
     */
    Schema,

    /**
     *  租户共享数据表
     */
    Discriminator
}
