/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

import io.iec.edp.caf.tenancy.api.enums.DataIsolation;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 应用实例信息
 *
 * @author Hey Law
 * @create 2019年8月19日$
 */
@Entity
@Data
@Table(name="ecpappinstance")
public class AppInstanceInfo implements Serializable {

    /**
     *  应用实例唯一标识
     */
    @Id
    @Column
    private String id;

    /**
     *  应用实例编号
     */
    @Column
    private String code;

    /**
     *  应用实例名称
     */
    @Column
    private String name;

    /**
     *  租户编号
     */
    @Column
    private int tenantId;

    /**
     *  数据隔离级别类型
     */
    @Column
    private DataIsolation dataIsolation;

    /**
     *  应用首页地址
     */
    @Column
    private String portalUrl;

    /**
     *  过期时间
     */
    @Column
    private Date expiredTime;

    /**
     *  实例状态
     */
    @Column
    private int status;

//    /// <summary>
//    /// 授权相关信息
//    /// </summary>
//    private LicenseInfo licenseInfo;

//    /**
//     *  服务单元数据访问映射关系
//     */
//    private List<SuDbMapping> suDBMappings;
//
//    /**
//     *  实例对应的数据库连接信息
//     */
//    private List<DbConnection> dbConnectionInfos;

    /**
     *  所属集群Id
     */
    @Column
    private String clusterId;

    /**
     *  最后更新时间
     */
    @Column
    private Date createdTime;

    @Column
    @Getter
    @Setter
    private String description;

}
