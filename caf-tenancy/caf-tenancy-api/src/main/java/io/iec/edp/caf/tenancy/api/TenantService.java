//package io.iec.edp.caf.tenancy.api;
//
//import io.iec.edp.caf.tenancy.api.entity.AppInstanceInfo;
//import io.iec.edp.caf.tenancy.api.entity.DbConnection;
//import io.iec.edp.caf.tenancy.api.entity.Tenant;
//
//import java.io.IOException;
//import java.util.List;
//
//public interface TenantService {
//
//    /**
//     * @Description: 获取所有租户信息
//     * @param language 语言编号
//     * @return 所有租户信息
//     * @Date: 2019/8/19
//    */
//    List<Tenant> getAllTenants(String language);
//
//    /**
//     * @Description: 根据租户编号获取租户基本信息
//     * @param tenantId 租户编号
//     * @param language 语言编号
//     * @return 租户基本信息
//     * @Date: 2019/8/19
//     */
//    Tenant getTenant(int tenantId, String language);
//
//    /**
//     * 根据租户编号获取租户基本信息
//     * @param tenantCode
//     * @param language
//     * @return
//     */
//    Tenant getByCode(String tenantCode, String language);
//
//    /**
//     * @Description: 根据租户编号获取租户应用信息
//     * @param tenantId 租户编号
//     * @param appInstanceCode 应用实例编号
//     * @return 应用的URL地址
//     * @Date: 2019/8/19
//     */
//    String getApplicationURL(int tenantId,String appInstanceCode);
//
//    /**
//     * @Description: 根据租户编号获取所有的租户实例基本信息(只获取实例基本信息)
//     * @param tenantId 租户编号
//     * @return 租户数据库信息
//     * @Date: 2019/8/21
//    */
//    List<AppInstanceInfo> getAllAppInstInfos(int tenantId);
//
//    /**
//     * @Description: 根据租户编号获取所有的租户实例信息
//     * @param tenantId 租户编号
//     * @param appInstanceCode 应用实例编号
//     * @return 租户数据库信息
//     * @Date: 2019/8/21
//    */
//    AppInstanceInfo getAppInstanceInfo(int tenantId, String appInstanceCode);
//
//    /**
//     * @Description: 获取所有连接信息
//     * @param tenantId 租户Id
//     * @param appInstanceCode 应用实例
//     * @return 数据库连接信息
//     * @Date: 2019/8/19
//    */
//    List<DbConnection> getAllDBConnInfos(int tenantId, String appInstanceCode);
//
//    /**
//     *  根据租户、实例编号获取数据库编号
//     * @param tenantId 租户编号
//     * @param appInstanceCode 应用实例编号
//     * @param su
//     * @return
//     */
//    String getDBConnectionId(int tenantId, String appInstanceCode, String su);
//
//    /**
//     * @Description:  获取数据库连接信息
//     * @param tenantId 租户Id
//     * @param appInstanceCode 应用实例
//     * @param su 服务单元
//     * @return 数据库连接信息
//     * @Date: 2019/8/19
//    */
//    DbConnection getDBConnInfo(int tenantId, String appInstanceCode, String su);
//
//    /**
//     * 切换租户Id
//     * @param openId
//     * @param tenantId
//     * @throws IOException
//     */
//    void changeTenantId(String openId,String tenantId) throws IOException;
//}
