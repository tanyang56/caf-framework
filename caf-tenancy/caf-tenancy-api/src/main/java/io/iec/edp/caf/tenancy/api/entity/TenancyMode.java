/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

/**
 * 多租户模式：独享模式、正常租户模式（租户间隔离）、集团多租户模式
 * @author wangyandong
 * @date 2021/06/22 17:37
 *
 */
public enum TenancyMode {
    /**
     * 独享
     */
    exclusive,
    /**
     * 隔离
     */
    isolated,
    /**
     * 集团
     */
    group
}
