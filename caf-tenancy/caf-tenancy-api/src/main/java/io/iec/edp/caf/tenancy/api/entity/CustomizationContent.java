/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 个性化定制内容
 *
 * @author Hey Law
 * @create 2019年8月19日$
 */
@Entity
@Data
@Table(name="customizationcontent")
public class CustomizationContent  implements Serializable {

    /**
     *  定制化内容唯一标识
    */
    @Id
    @Column
    private String id;

    /**
     *  租户Id
     */
    @Column
    private int tenantId;

    /**
     *  实例编号，为空标识实例无关
     */
    @Column
    private String appInstCode;

    /**
     *  所属服务单元
     */
    @Column
    private String su;

    /**
     *  内容分类
     */
    @Column
    private String catalog;

    /**
     *  二进制内容
     */
    @Column
    private byte[] content;

}
