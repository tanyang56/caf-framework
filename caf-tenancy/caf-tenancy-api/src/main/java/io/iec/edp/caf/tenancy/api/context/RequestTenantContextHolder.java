/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.tenancy.api.context;

/**
 * 请求级上下文
 *
 * @author guowenchang
 */
public class RequestTenantContextHolder {

    private static final InheritableThreadLocal<RequestTenantContextInfo> context = new InheritableThreadLocal();

    public static RequestTenantContextInfo get() {
        return context.get();
    }

    /**
     * 设置上下文
     *
     * @param value
     */
    public static void set(RequestTenantContextInfo value) {
        synchronized (context) {
            RequestTenantContextInfo current = context.get();
            value.setParentContext(current);
            context.set(value);
        }
    }

    /**
     * 还原上下文
     */
    public static void restore() {
        synchronized (context) {
            RequestTenantContextInfo current = context.get();
            if (current != null) {
                context.set(current.getParentContext());
            }
        }
    }

    public static void purge() {
        context.remove();
    }
}
