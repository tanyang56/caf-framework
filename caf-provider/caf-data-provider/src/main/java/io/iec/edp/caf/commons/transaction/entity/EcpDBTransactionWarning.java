/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.commons.transaction.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * 数据库事务预警数据
 *
 * @author manwenxing01
 * @date 2022-03-22
 */
@Data
@Entity
@Table(name = "ecpdbtransactionwarning")
@NoArgsConstructor
public class EcpDBTransactionWarning implements Serializable, Cloneable {

    public EcpDBTransactionWarning(String path, String message, OffsetDateTime time, String type) {
        this.path = path;
        this.message = message;
        this.createdTime = time;
        this.type = type;
    }

    //主键
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    //请求路径
    private String path;

    //描述信息
    @Deprecated
    private String description;

    //描述信息
    private String message;

    //服务类型
    private String type;

    //创建时间
    @Column(name = "createdtime")
    private OffsetDateTime createdTime;

    //判断主要属性不为空
    public boolean notEmpty() {
        return !"".equals(this.message) && !"".equals(this.path) && !"".equals(this.type) && this.createdTime != null;
    }

}
