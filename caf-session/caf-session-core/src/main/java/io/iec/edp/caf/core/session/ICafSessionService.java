/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;


import java.util.HashMap;
import java.util.List;

/**
 * Session服务接口
 *
 * @author wangyandong
 */
//todo 被预警依赖
public interface ICafSessionService {

    /**
     * 创建Session
     *
     * @param tenantId    租户标识
     * @param userId      用户标识
     * @param language    语言编号
     * @param sessionType session类型
     * @return 新创建的Session
     */
    CafSession create(Integer tenantId, String userId, String language, HashMap<String, String> items, SessionType sessionType);

    /**
     * 创建CafSession
     *
     * @param tenantId    租户标识
     * @param userId      用户标识
     * @param userCode    用户编号
     * @param userName    用户名称
     * @param language    语言
     * @param items       存储信息
     * @param sessionType Session类型
     * @return CafSession
     */
    CafSession create(Integer tenantId, String userId, String userCode, String userName, String language, HashMap<String, String> items, SessionType sessionType);

    /**
     * 根据Session标识获取CafSession
     *
     * @param sessionId Session标识
     * @return CafSession
     */
    CafSession findById(String sessionId);

    /**
     * 更新Session
     *
     * @param session CafSession
     */
    void update(CafSession session);

    /**
     * 删除Session
     *
     * @param sessionId CafSession的id
     */
    void deleteById(String sessionId);

    /**
     * 判断Session是否过期
     *
     * @param sessionId CafSession的id
     * @return 是否过期boolean
     */
    boolean isExpired(String sessionId);

    /**
     * 批量判断session是否过期，返回过期的sessionId列表
     *
     * @param sessionIds CafSession的id有序列表
     * @return 过期的session列表
     */
    List<String> filterExpired(List<String> sessionIds);
}
