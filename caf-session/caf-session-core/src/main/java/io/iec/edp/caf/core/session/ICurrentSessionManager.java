/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

/**
 * 当前Session管理接口，适用于依赖Session但层次低于Context的组件使用
 * @author wangyandong
 * @date 2021/05/19 15:16
 *
 */
@Deprecated
public interface ICurrentSessionManager {
    /**
     * 获取当前的Session对象
     */
    CafSession getCurrentSession();

    /**
     * 获取深复制的session对象
     * @return
     */
    CafSession getCopyedCurrentSession();

    /**
     * 获取当前的Session对象
     */
    void setCurrentSession(CafSession session);

    /**
     * 设置当前线程池里线程的session，不动Spring securityContextHolder里的session
     * @param session
     */
    void setCurrentThreadPoolSession(CafSession session);

    /**
     * 清理当前Session
     */
    void clearSession();

    /**
     * 清理当前前线程池里线程的session，不动Spring securityContextHolder里的session
     */
    void clearCurrentThreadPoolSession();

    /**
     * 设置当前Session的语言信息
     * @param language 语言code
     */
    void setCurrentSessionLanguage(String language);
}
