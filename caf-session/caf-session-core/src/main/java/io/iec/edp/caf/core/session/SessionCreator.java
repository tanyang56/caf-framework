/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import java.util.HashMap;
import java.util.List;

/**
 * Session创建接口
 * @author wangyandong
 * @date 2021/05/19 11:50
 *
 */
public interface SessionCreator {

    /**
     * 返回匹配的Session类型
     * @return
     */
    List<SessionType> getMatchingSessionTypes();

    /**
     * 创建 CafSession
     * @param sessionId 会话标识,
     * @param tenantId 租户标识
     * @param userId 用户Id
     * @param userCode 用户编号
     * @param userName 用户名
     * @param language 语言
     * @param items 其它参数
     * @return
     */
    CafSession create(String sessionId, Integer tenantId, String userId, String userCode, String userName, String language, HashMap<String,String> items);
}
