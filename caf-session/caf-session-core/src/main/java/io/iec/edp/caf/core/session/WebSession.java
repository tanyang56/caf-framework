/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import lombok.Getter;
import lombok.NonNull;

import java.time.ZonedDateTime;

/**
 * This is {@link WebSession}.
 * WebSession的基类
 * @author yisiqi
 * @since 1.0.0 CafSession
 */
@Getter
//todo 被io.iec.edp.caf.runtime.core.manager.FrameworkFilterListener依赖
public abstract class WebSession extends CafSession {
	//principalId等同于userCode
	@NonNull
	private final String principalId;
	@NonNull
	private final String remoteAddress;
	@NonNull
	private final int remotePort;

	@NonNull
	private final String origin;
	@NonNull
	private final String localAddress;
	@NonNull
	private final int localPort;
	@NonNull
	private final String contextPath;


	protected WebSession(
			String sessionId,
			ZonedDateTime creationTime,
			Integer tenantId,
			String userId,
			@NonNull String principalId,
			String userName,
			@NonNull String remoteAddress,
			@NonNull int remotePort,
			String origin,
			@NonNull String localAddress,
			@NonNull int localPort,
			@NonNull String contextPath,
			String language,
			SessionType sessionType
	) {
		super(sessionId, creationTime,tenantId,userId,principalId,userName,language,sessionType);
		this.principalId = principalId;
		this.remoteAddress = remoteAddress;
		this.remotePort = remotePort;
		this.origin = (origin == null || origin.length() == 0) ? "localhost" : origin;
		this.localAddress = localAddress;
		this.localPort = localPort;
		this.contextPath = contextPath;
	}
}
