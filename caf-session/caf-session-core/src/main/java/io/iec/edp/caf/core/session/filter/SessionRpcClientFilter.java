/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session.filter;

import io.iec.edp.caf.core.session.CafSessionContextHolder;
import io.iec.edp.caf.rpc.api.annotation.RpcClientInterceptor;
import io.iec.edp.caf.rpc.api.filter.RpcClientFilter;
import io.iec.edp.caf.rpc.api.support.ConstanceVarible;
import lombok.var;

import java.util.Map;

@RpcClientInterceptor
public class SessionRpcClientFilter implements RpcClientFilter {

    @Override
    public void doOutFilter(Map<String, Object> reqContext) {
        var session  = CafSessionContextHolder.getCurrentSession();
        var sessionid = "";
        if(session!=null){
            sessionid = session.getId();
        }
        reqContext.put(ConstanceVarible.SESSION_ID, sessionid);
    }

    @Override
    public void doInFilter(Map<String, Object> repContext) {

    }

    @Override
    public void doFilter(Map<String, Object> reqContext, Map<String, Object> repContext) {

    }
}
