/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import java.util.List;

/**
 * session持有者接口，不同类型的session有不同的实现
 * @author wangyandong
 */
public interface CafSessionHolder {
    /**
     * 返回匹配的Session类型
     * @return
     */
    List<SessionType> getMatchingSessionTypes();

    /**
     * 获取当前的Session对象
     */
    CafSession getCurrentSession();

    /**
     * 获取深复制后的CafSession
     * @return
     */
    CafSession getCopyedCurrentSession();

    /**
     * 获取当前的Session对象
     */
    void setCurrentSession(CafSession session);

    /**
     * 只给线程池里线程的本地变量设置session，不动SecurityContextHolder里的session
     * @param session
     */
    void setCurrentThreadPoolSession(CafSession session);

    /**
     * 清理当前Session
     */
    void clear();

    /**
     * 只清理线程池里线程的本地变量设置session，不动SecurityContextHolder里的session
     */
    void clearCurrentThreadPoolSession();
}
