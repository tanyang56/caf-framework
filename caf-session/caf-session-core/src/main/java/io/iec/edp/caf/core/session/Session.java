/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * This is {@link Session}.
 * Session和BizContext的共同基类
 *
 * @author yisiqi
 * @since 1.0.0
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
//todo 被com.inspur.edp.bef.core.session.BefBizContextBuilder依赖
public abstract class Session implements Serializable {

	protected static final long serialVersionUID = -4737116536165380594L;

	@NonNull
	protected String id;

	@NonNull
	protected ZonedDateTime creationDate;

}
