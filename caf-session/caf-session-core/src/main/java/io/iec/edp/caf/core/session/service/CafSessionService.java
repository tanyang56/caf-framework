package io.iec.edp.caf.core.session.service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * TODO 临时位置，临时名称，等待迁移至统一module
 * CafSessionService
 *
 * @author manwenxing01
 * @date 2023-04-27 15:20
 */
@Path("")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CafSessionService {

    /**
     * session修改语言
     *
     * @param languageCode 语言编号
     */
    @GET
    @Path("language/{languageCode}")
    boolean setLanguage(@PathParam("languageCode") String languageCode);

}
