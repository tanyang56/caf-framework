/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;

import java.time.ZonedDateTime;
import java.util.HashMap;

/**
 * WebSession和BSession的共同基类
 *
 * @author wangyandong
 */
//todo 被io/iec/edp/caf/context/config/BizContextAutoConfiguration依赖
@Getter
@Slf4j
public abstract class CafSession extends Session implements Authentication {
    //	@NonNull
    private final String userId;
    private final String userCode;
    //	@NonNull
    private final String userName;
    //	@NonNull
    private final Integer tenantId;
    //	@NonNull
    private String language;

    private final HashMap<String, String> items;

    private final SessionType sessionType;

    protected CafSession(
            String sessionId,
            ZonedDateTime creationTime,
            Integer tenantId,
            String userId,
            String userCode,
            String userName,
            @NonNull String language,
            SessionType sessionType
    ) {
        super(sessionId, creationTime);
        this.tenantId = tenantId;
        this.userId = userId;
        this.userCode = userCode;
        this.userName = userName;
        this.language = language;
        this.sessionType = sessionType;
        this.items = new HashMap<>();

        if (log.isDebugEnabled()) {
            log.debug("Current session: tenant {}, userId {}, userCode {}, userName {}", tenantId, userId, userCode, userName, new Exception("This is a log info not an exception."));
        }
    }

    public String getSysOrgId() {
        return null;
    }

    public String getSysOrgCode() {
        return null;
    }

    public String getSysOrgName() {
        return null;
    }

    public String getPhone() {
        return null;
    }

    /**
     * 根据Key值返回变量值
     *
     * @param key
     * @return
     */
    public String get(String key) {
        if (key != null && !"".equals(key))
            return items.get(key);
        return null;
    }

    /**
     * 添加Session变量
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public void put(String key, String value) {
        if (key != null && !"".equals(key))
            this.items.put(key, value);
    }

    //设置language信息
    public void setLanguage(@NonNull String language) {
        this.language = language;
    }
}
