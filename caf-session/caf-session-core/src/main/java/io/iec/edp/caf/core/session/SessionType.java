/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import java.util.HashMap;

/**
 * Session类型
 * @author wangyandong
 * @date 2019/12/14 13:36
 *
 */
public enum SessionType {
    /**
     * WebSession
     */
    web(0),
    /**
     * 后台Session
     */
    backend(1),
    /**
     * 移动框架Session
     */
    mobile(2);

    int value;
    private static HashMap<Integer, SessionType> mappings;

    SessionType(int val){
        this.value = val;
        getMappings().put(value, this);
    }

    public int getValue(){
        return  this.value;
    }

    public static SessionType forValue(int value) {
        return (SessionType)getMappings().get(value);
    }

    private static synchronized HashMap<Integer, SessionType> getMappings() {
        if (mappings == null) {
            mappings = new HashMap();
        }

        return mappings;
    }
}
