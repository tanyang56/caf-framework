/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

/**
 * Session保留键值
 * @author wangyandong
 * @date 2019/12/14 11:38
 *
 */
public class SessionReservedKeyCollection {

    /**
     * 租户id
     */
    public final static String SESSION_TENANT_ID = "tenantId";

    /**
     * 用户标识
     */
    public final static String SESSION_USER_ID = "userId";

    /**
     * 用户编号
     */
    public final static String SESSION_USER_CODE = "userCode";

    /**
     * 用户名称
     */
    public final static String SESSION_USER_NAME = "userName";

    /**
     * 语言编号
     */
    public final static String SESSION_LANGUAGE = "language";

    /**
     * SessionType
     */
    public final static String SessionType = "SessionType";

    /**
     * 实例编号
     */
    public final static String AppCode = "AppCode";

    /**
     * 组织标识
     */
    public final static String SysOrgId = "SysOrgId";

    /**
     * 组织标号
     */
    public final static String SysOrgCode = "SysOrgCode";

    /**
     * 组织名称
     */
    public final static String SysOrgName = "SysOrgName";
}
