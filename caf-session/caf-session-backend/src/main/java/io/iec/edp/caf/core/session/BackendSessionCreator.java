/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author wangyandong
 * @date 2021/08/25 10:53
 */
public class BackendSessionCreator implements SessionCreator {
    /**
     * 返回匹配的Session类型
     *
     * @return
     */
    @Override
    public List<SessionType> getMatchingSessionTypes() {
        List<SessionType> list = new ArrayList<>();
        list.add(SessionType.backend);
        return list;
    }

    /**
     * 创建 CafSession
     *
     * @param sessionId 会话标识,
     * @param tenantId  租户标识
     * @param userId    用户Id
     * @param userCode  用户编号
     * @param userName  用户名
     * @param language  语言
     * @param items     其它参数
     * @return
     */
    @Override
    public CafSession create(String sessionId, Integer tenantId, String userId, String userCode, String userName, String language, HashMap<String, String> items) {
        CafSession session = new CafBackendSession(sessionId, ZonedDateTime.now(), tenantId, userId, userCode, userName, language);
        if (items != null && items.size() > 0)
            session.getItems().putAll(items);
        return session;
    }
}
