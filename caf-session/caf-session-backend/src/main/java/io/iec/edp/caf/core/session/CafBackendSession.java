/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.core.session;

//import io.iec.edp.caf.security.core.CurrentUserDetails;
//import io.iec.edp.caf.security.core.UserDetails;
//import io.iec.edp.caf.security.core.UserDetailsSource;
//import io.iec.edp.caf.security.core.details.PersonalUser;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.time.ZonedDateTime;
import java.util.Collection;

/**
 * Caf后端Session
 * @author wangyandong
 * @date 2019/12/14 14:10
 *
 */
public class CafBackendSession extends CafSession {

    @Getter
    private final boolean authenticated;

//    private final Object userDetails;

    public CafBackendSession(
            String sessionId,
            ZonedDateTime creationTime,
            Integer tenantId,
            String userId,
            String userCode,
            String userName,
            String language
    ) {
        super(sessionId, creationTime,tenantId,userId,userCode,userName,language, SessionType.backend);
        this.authenticated = true;

//        PersonalUser person = PersonalUser.builder()
//                .boundLocalUser(true)
//                .userDetailsSource(UserDetailsSource.LOCAL)
//                .build();
//
//        userDetails = CurrentUserDetails.builder()
//                .user(person)
//                .build();
//        userDetails = new Object();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getDetails() {
        //bsession不支持这个
        return null;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public boolean isAuthenticated(){
        return this.authenticated;
    }

    @Override
    public Object getCredentials(){
        return null;
    }

    @Override
    public Object getPrincipal()
    {
        return this.getId();
    }

    @Override
    public String getName() {
        return null;
    }
}
