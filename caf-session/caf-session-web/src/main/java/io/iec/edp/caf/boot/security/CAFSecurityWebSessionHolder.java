/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.boot.security;

import io.iec.edp.caf.core.session.CafSession;
import io.iec.edp.caf.core.session.WebSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This is {@link CAFSecurityWebSessionHolder}.
 *
 * @author yisiqi
 * @since 1.0.0
 */
@SuppressWarnings("unused")
//todo 被io.iec.edp.caf.audit.core.manager.GspAuditManagerImpl.getClientAddress依赖
public class CAFSecurityWebSessionHolder {

    static {
        //设置session的存储为线程传递模式
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    public static WebSession getSession() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof WebSession)
            return (WebSession) authentication;
        return null;
    }

    public static CafSession getCafSession() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication instanceof CafSession)
            return (CafSession) authentication;
        return null;
    }

}
