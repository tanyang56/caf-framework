/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.session.filter;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

abstract class CafOnCommittedResponseWrapper extends HttpServletResponseWrapper {
    private boolean disableOnCommitted;
    private long contentLength;
    private long contentWritten;

    CafOnCommittedResponseWrapper(HttpServletResponse response) {
        super(response);
    }

    public void addHeader(String name, String value) {
        if ("Content-Length".equalsIgnoreCase(name)) {
            this.setContentLength(Long.parseLong(value));
        }

        super.addHeader(name, value);
    }

    public void setContentLengthLong(long len) {
        this.setContentLength(len);
        super.setContentLengthLong(len);
    }

    public void setContentLength(int len) {
        this.setContentLength((long)len);
        super.setContentLength(len);
    }

    private void setContentLength(long len) {
        this.contentLength = len;
        this.checkContentLength(0L);
    }

    private void disableOnResponseCommitted() {
        this.disableOnCommitted = true;
    }

    protected abstract void onResponseCommitted();

    public final void sendError(int sc) throws IOException {
        this.doOnResponseCommitted();
        super.sendError(sc);
    }

    public final void sendError(int sc, String msg) throws IOException {
        this.doOnResponseCommitted();
        super.sendError(sc, msg);
    }

    public final void sendRedirect(String location) throws IOException {
        this.doOnResponseCommitted();
        super.sendRedirect(location);
    }

    public ServletOutputStream getOutputStream() throws IOException {
        return new CafOnCommittedResponseWrapper.SaveContextServletOutputStream(super.getOutputStream());
    }

    public PrintWriter getWriter() throws IOException {
        return new CafOnCommittedResponseWrapper.SaveContextPrintWriter(super.getWriter());
    }

    public void flushBuffer() throws IOException {
        this.doOnResponseCommitted();
        super.flushBuffer();
    }

    private void trackContentLength(boolean content) {
        this.checkContentLength(content ? 4L : 5L);
    }

    private void trackContentLength(char content) {
        this.checkContentLength(1L);
    }

    private void trackContentLength(Object content) {
        this.trackContentLength(String.valueOf(content));
    }

    private void trackContentLength(byte[] content) {
        this.checkContentLength(content != null ? (long)content.length : 0L);
    }

    private void trackContentLength(char[] content) {
        this.checkContentLength(content != null ? (long)content.length : 0L);
    }

    private void trackContentLength(int content) {
        this.trackContentLength(String.valueOf(content));
    }

    private void trackContentLength(float content) {
        this.trackContentLength(String.valueOf(content));
    }

    private void trackContentLength(double content) {
        this.trackContentLength(String.valueOf(content));
    }

    private void trackContentLengthLn() {
        this.trackContentLength("\r\n");
    }

    private void trackContentLength(String content) {
        this.checkContentLength((long)content.length());
    }

    private void checkContentLength(long contentLengthToWrite) {
        this.contentWritten += contentLengthToWrite;
        boolean isBodyFullyWritten = this.contentLength > 0L && this.contentWritten >= this.contentLength;
        int bufferSize = this.getBufferSize();
        boolean requiresFlush = bufferSize > 0 && this.contentWritten >= (long)bufferSize;
        if (isBodyFullyWritten || requiresFlush) {
            this.doOnResponseCommitted();
        }

    }

    private void doOnResponseCommitted() {
        if (!this.disableOnCommitted) {
            this.onResponseCommitted();
            this.disableOnResponseCommitted();
        }

    }

    private class SaveContextServletOutputStream extends ServletOutputStream {
        private final ServletOutputStream delegate;

        SaveContextServletOutputStream(ServletOutputStream delegate) {
            this.delegate = delegate;
        }

        public void write(int b) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(b);
            this.delegate.write(b);
        }

        public void flush() throws IOException {
            CafOnCommittedResponseWrapper.this.doOnResponseCommitted();
            this.delegate.flush();
        }

        public void close() throws IOException {
            CafOnCommittedResponseWrapper.this.doOnResponseCommitted();
            this.delegate.close();
        }

        public boolean equals(Object obj) {
            return this.delegate.equals(obj);
        }

        public int hashCode() {
            return this.delegate.hashCode();
        }

        public void print(boolean b) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(b);
            this.delegate.print(b);
        }

        public void print(char c) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(c);
            this.delegate.print(c);
        }

        public void print(double d) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(d);
            this.delegate.print(d);
        }

        public void print(float f) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(f);
            this.delegate.print(f);
        }

        public void print(int i) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(i);
            this.delegate.print(i);
        }

        public void print(long l) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength((float)l);
            this.delegate.print(l);
        }

        public void print(String s) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(s);
            this.delegate.print(s);
        }

        public void println() throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println();
        }

        public void println(boolean b) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(b);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(b);
        }

        public void println(char c) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(c);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(c);
        }

        public void println(double d) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(d);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(d);
        }

        public void println(float f) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(f);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(f);
        }

        public void println(int i) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(i);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(i);
        }

        public void println(long l) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength((float)l);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(l);
        }

        public void println(String s) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(s);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(s);
        }

        public void write(byte[] b) throws IOException {
            CafOnCommittedResponseWrapper.this.trackContentLength(b);
            this.delegate.write(b);
        }

        public void write(byte[] b, int off, int len) throws IOException {
            CafOnCommittedResponseWrapper.this.checkContentLength((long)len);
            this.delegate.write(b, off, len);
        }

        public String toString() {
            return this.getClass().getName() + "[delegate=" + this.delegate.toString() + "]";
        }

        public boolean isReady() {
            return this.delegate.isReady();
        }

        public void setWriteListener(WriteListener writeListener) {
            this.delegate.setWriteListener(writeListener);
        }
    }

    private class SaveContextPrintWriter extends PrintWriter {
        private final PrintWriter delegate;

        SaveContextPrintWriter(PrintWriter delegate) {
            super(delegate);
            this.delegate = delegate;
        }

        public void flush() {
            CafOnCommittedResponseWrapper.this.doOnResponseCommitted();
            this.delegate.flush();
        }

        public void close() {
            CafOnCommittedResponseWrapper.this.doOnResponseCommitted();
            this.delegate.close();
        }

        public boolean equals(Object obj) {
            return this.delegate.equals(obj);
        }

        public int hashCode() {
            return this.delegate.hashCode();
        }

        public String toString() {
            return this.getClass().getName() + "[delegate=" + this.delegate.toString() + "]";
        }

        public boolean checkError() {
            return this.delegate.checkError();
        }

        public void write(int c) {
            CafOnCommittedResponseWrapper.this.trackContentLength(c);
            this.delegate.write(c);
        }

        public void write(char[] buf, int off, int len) {
            CafOnCommittedResponseWrapper.this.checkContentLength((long)len);
            this.delegate.write(buf, off, len);
        }

        public void write(char[] buf) {
            CafOnCommittedResponseWrapper.this.trackContentLength(buf);
            this.delegate.write(buf);
        }

        public void write(String s, int off, int len) {
            CafOnCommittedResponseWrapper.this.checkContentLength((long)len);
            this.delegate.write(s, off, len);
        }

        public void write(String s) {
            CafOnCommittedResponseWrapper.this.trackContentLength(s);
            this.delegate.write(s);
        }

        public void print(boolean b) {
            CafOnCommittedResponseWrapper.this.trackContentLength(b);
            this.delegate.print(b);
        }

        public void print(char c) {
            CafOnCommittedResponseWrapper.this.trackContentLength(c);
            this.delegate.print(c);
        }

        public void print(int i) {
            CafOnCommittedResponseWrapper.this.trackContentLength(i);
            this.delegate.print(i);
        }

        public void print(long l) {
            CafOnCommittedResponseWrapper.this.trackContentLength((float)l);
            this.delegate.print(l);
        }

        public void print(float f) {
            CafOnCommittedResponseWrapper.this.trackContentLength(f);
            this.delegate.print(f);
        }

        public void print(double d) {
            CafOnCommittedResponseWrapper.this.trackContentLength(d);
            this.delegate.print(d);
        }

        public void print(char[] s) {
            CafOnCommittedResponseWrapper.this.trackContentLength(s);
            this.delegate.print(s);
        }

        public void print(String s) {
            CafOnCommittedResponseWrapper.this.trackContentLength(s);
            this.delegate.print(s);
        }

        public void print(Object obj) {
            CafOnCommittedResponseWrapper.this.trackContentLength(obj);
            this.delegate.print(obj);
        }

        public void println() {
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println();
        }

        public void println(boolean x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(char x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(int x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(long x) {
            CafOnCommittedResponseWrapper.this.trackContentLength((float)x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(float x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(double x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(char[] x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(String x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public void println(Object x) {
            CafOnCommittedResponseWrapper.this.trackContentLength(x);
            CafOnCommittedResponseWrapper.this.trackContentLengthLn();
            this.delegate.println(x);
        }

        public PrintWriter printf(String format, Object... args) {
            return this.delegate.printf(format, args);
        }

        public PrintWriter printf(Locale l, String format, Object... args) {
            return this.delegate.printf(l, format, args);
        }

        public PrintWriter format(String format, Object... args) {
            return this.delegate.format(format, args);
        }

        public PrintWriter format(Locale l, String format, Object... args) {
            return this.delegate.format(l, format, args);
        }

        public PrintWriter append(CharSequence csq) {
            CafOnCommittedResponseWrapper.this.checkContentLength((long)csq.length());
            return this.delegate.append(csq);
        }

        public PrintWriter append(CharSequence csq, int start, int end) {
            CafOnCommittedResponseWrapper.this.checkContentLength((long)(end - start));
            return this.delegate.append(csq, start, end);
        }

        public PrintWriter append(char c) {
            CafOnCommittedResponseWrapper.this.trackContentLength(c);
            return this.delegate.append(c);
        }
    }
}
