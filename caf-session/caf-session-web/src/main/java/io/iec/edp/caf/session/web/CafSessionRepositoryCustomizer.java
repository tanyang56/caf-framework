/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.session.web;

import io.iec.edp.caf.session.repo.CafSessionRepository;
import org.springframework.boot.autoconfigure.session.SessionProperties;
import org.springframework.session.config.SessionRepositoryCustomizer;

import java.time.Duration;

/**
 * caf SessionRepository(redis)的SessionRepositoryCustomizer，设置session超时时间和namespace
 * 参考自类：RedisHttpSessionConfiguration 和 SpringBootRedisHttpSessionConfiguration
 *
 * @author wangyandong
 * @date 2020/07/22 17:26
 */
public class CafSessionRepositoryCustomizer implements SessionRepositoryCustomizer<CafSessionRepository> {

    //session在redis中命名空间
    private final static String NameSpace = "caf-session";

    //spring.session配置节
    private SessionProperties sessionProperties;

    public CafSessionRepositoryCustomizer(SessionProperties sessionProperties) {
        this.sessionProperties = sessionProperties;
    }

    public void customize(CafSessionRepository sessionRepository) {
        //设置session超时时间
        Duration timeout = sessionProperties.getTimeout();
        if (timeout != null) {
            sessionRepository.setDefaultMaxInactiveInterval((int) timeout.getSeconds());
        }

        //设置redis-session的命名空间
        sessionRepository.setRedisKeyNamespace(NameSpace);
    }
}
