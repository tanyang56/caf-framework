/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.session.repo;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import io.iec.edp.caf.session.holder.CafSecurityAPIKeyHolder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.*;
import org.springframework.session.events.SessionCreatedEvent;
import org.springframework.session.events.SessionDeletedEvent;
import org.springframework.session.events.SessionExpiredEvent;
import org.springframework.util.Assert;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.Map.Entry;

/**
 * 重写 {RedisIndexedSessionRepository}，过期时间移动调整为lua脚本执行
 *
 * @author manwenxing01
 * @date 2023-02-24
 */
public class CafSessionRepository implements FindByIndexNameSessionRepository<CafSessionRepository.RedisSession>, MessageListener {
    private static final Log logger = LogFactory.getLog(CafSessionRepository.class);
    private static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
    public static final int DEFAULT_DATABASE = 0;
    public static final String DEFAULT_NAMESPACE = "spring:session";
    private int database = 0;
    private String namespace = "spring:session:";
    private String sessionCreatedChannelPrefix;
    private String sessionDeletedChannel;
    private String sessionExpiredChannel;
    private final RedisOperations<Object, Object> sessionRedisOperations;
    private final CafRedisSessionExpirationPolicy expirationPolicy;
    private ApplicationEventPublisher eventPublisher = (event) -> {
    };
    private Integer defaultMaxInactiveInterval;
    private IndexResolver<Session> indexResolver = new DelegatingIndexResolver(new IndexResolver[]{new PrincipalNameIndexResolver()});
    private RedisSerializer<Object> defaultSerializer = new JdkSerializationRedisSerializer();
    private FlushMode flushMode;
    private SaveMode saveMode;

    public CafSessionRepository(RedisOperations<Object, Object> sessionRedisOperations) {
        this.flushMode = FlushMode.ON_SAVE;
        this.saveMode = SaveMode.ON_SET_ATTRIBUTE;
        Assert.notNull(sessionRedisOperations, "sessionRedisOperations cannot be null");
        this.sessionRedisOperations = sessionRedisOperations;
        this.expirationPolicy = new CafRedisSessionExpirationPolicy(sessionRedisOperations, this::getExpirationsKey, this::getSessionKey);
        this.configureSessionChannels();
    }

    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        Assert.notNull(applicationEventPublisher, "applicationEventPublisher cannot be null");
        this.eventPublisher = applicationEventPublisher;
    }

    public void setDefaultMaxInactiveInterval(int defaultMaxInactiveInterval) {
        this.defaultMaxInactiveInterval = defaultMaxInactiveInterval;
    }

    public void setIndexResolver(IndexResolver<Session> indexResolver) {
        Assert.notNull(indexResolver, "indexResolver cannot be null");
        this.indexResolver = indexResolver;
    }

    public void setDefaultSerializer(RedisSerializer<Object> defaultSerializer) {
        Assert.notNull(defaultSerializer, "defaultSerializer cannot be null");
        this.defaultSerializer = defaultSerializer;
    }

    public void setFlushMode(FlushMode flushMode) {
        Assert.notNull(flushMode, "flushMode cannot be null");
        this.flushMode = flushMode;
    }

    public void setSaveMode(SaveMode saveMode) {
        Assert.notNull(saveMode, "saveMode must not be null");
        this.saveMode = saveMode;
    }

    public void setDatabase(int database) {
        this.database = database;
        this.configureSessionChannels();
    }

    private void configureSessionChannels() {
        this.sessionCreatedChannelPrefix = this.namespace + "event:" + this.database + ":created:";
        this.sessionDeletedChannel = "__keyevent@" + this.database + "__:del";
        this.sessionExpiredChannel = "__keyevent@" + this.database + "__:expired";
    }

    public RedisOperations<Object, Object> getSessionRedisOperations() {
        return this.sessionRedisOperations;
    }

    public void save(RedisSession session) {
        if (!apiKey()) {
            //持久化非caf API key场景
            session.save();
            if (session.isNew) {
                String sessionCreatedKey = this.getSessionCreatedChannel(session.getId());
                this.sessionRedisOperations.convertAndSend(sessionCreatedKey, session.delta);
                session.isNew = false;
            }
        }
    }

    /**
     * ApiKey场景 && ApiKey不持久化
     */
    private boolean apiKey() {
        return CafSecurityAPIKeyHolder.get() != null && CafSecurityAPIKeyHolder.get() && !Boolean.parseBoolean(CafEnvironment.getEnvironment().getProperty("caf-boot.websession.apikeyPersistence", "false"));
    }

    public void cleanupExpiredSessions() {
        this.expirationPolicy.cleanExpiredSessions();
    }

    public RedisSession findById(String id) {
        return this.getSession(id, false);
    }

    public Map<String, RedisSession> findByIndexNameAndIndexValue(String indexName, String indexValue) {
        if (!PRINCIPAL_NAME_INDEX_NAME.equals(indexName)) {
            return Collections.emptyMap();
        } else {
            String principalKey = this.getPrincipalKey(indexValue);
            Set<Object> sessionIds = this.sessionRedisOperations.boundSetOps(principalKey).members();
            Map<String, RedisSession> sessions = new HashMap(sessionIds.size());
            Iterator var6 = sessionIds.iterator();

            while (var6.hasNext()) {
                Object id = var6.next();
                RedisSession session = this.findById((String) id);
                if (session != null) {
                    sessions.put(session.getId(), session);
                }
            }

            return sessions;
        }
    }

    private RedisSession getSession(String id, boolean allowExpired) {
        Map<Object, Object> entries = this.getSessionBoundHashOperations(id).entries();
        if (entries.isEmpty()) {
            return null;
        } else {
            MapSession loaded = this.loadSession(id, entries);
            if (!allowExpired && loaded.isExpired()) {
                return null;
            } else {
                RedisSession result = new RedisSession(loaded, false);
                result.originalLastAccessTime = loaded.getLastAccessedTime();
                return result;
            }
        }
    }

    private MapSession loadSession(String id, Map<Object, Object> entries) {
        MapSession loaded = new MapSession(id);
        Iterator var4 = entries.entrySet().iterator();

        while (var4.hasNext()) {
            Entry<Object, Object> entry = (Entry) var4.next();
            String key = (String) entry.getKey();
            if ("creationTime".equals(key)) {
                loaded.setCreationTime(Instant.ofEpochMilli((Long) entry.getValue()));
            } else if ("maxInactiveInterval".equals(key)) {
                loaded.setMaxInactiveInterval(Duration.ofSeconds((long) (Integer) entry.getValue()));
            } else if ("lastAccessedTime".equals(key)) {
                loaded.setLastAccessedTime(Instant.ofEpochMilli((Long) entry.getValue()));
            } else if (key.startsWith("sessionAttr:")) {
                loaded.setAttribute(key.substring("sessionAttr:".length()), entry.getValue());
            }
        }

        return loaded;
    }

    public void deleteById(String sessionId) {
        RedisSession session = this.getSession(sessionId, true);
        if (session != null) {
            this.cleanupPrincipalIndex(session);
            this.expirationPolicy.onDelete(session);
            String expireKey = this.getExpiredKey(session.getId());
            this.sessionRedisOperations.delete(expireKey);
            session.setMaxInactiveInterval(Duration.ZERO);
            this.save(session);
        }
    }

    public RedisSession createSession() {
        MapSession cached = new MapSession();
        if (this.defaultMaxInactiveInterval != null) {
            cached.setMaxInactiveInterval(Duration.ofSeconds((long) this.defaultMaxInactiveInterval));
        }

        RedisSession session = new RedisSession(cached, true);
        session.flushImmediateIfNecessary();
        return session;
    }

    public void onMessage(Message message, byte[] pattern) {
        byte[] messageChannel = message.getChannel();
        byte[] messageBody = message.getBody();
        String channel = new String(messageChannel);
        if (channel.startsWith(this.sessionCreatedChannelPrefix)) {
            Map<Object, Object> loaded = (Map) this.defaultSerializer.deserialize(message.getBody());
            this.handleCreated(loaded, channel);
        } else {
            String body = new String(messageBody);
            if (body.startsWith(this.getExpiredKeyPrefix())) {
                boolean isDeleted = channel.equals(this.sessionDeletedChannel);
                if (isDeleted || channel.equals(this.sessionExpiredChannel)) {
                    int beginIndex = body.lastIndexOf(":") + 1;
                    int endIndex = body.length();
                    String sessionId = body.substring(beginIndex, endIndex);
                    RedisSession session = this.getSession(sessionId, true);
                    if (session == null) {
                        logger.warn("Unable to publish SessionDestroyedEvent for session " + sessionId);
                        return;
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("Publishing SessionDestroyedEvent for session " + sessionId);
                    }

                    this.cleanupPrincipalIndex(session);
                    if (isDeleted) {
                        this.handleDeleted(session);
                    } else {
                        this.handleExpired(session);
                    }
                }

            }
        }
    }

    private void cleanupPrincipalIndex(RedisSession session) {
        String sessionId = session.getId();
        Map<String, String> indexes = this.indexResolver.resolveIndexesFor(session);
        String principal = (String) indexes.get(PRINCIPAL_NAME_INDEX_NAME);
        if (principal != null) {
            this.sessionRedisOperations.boundSetOps(this.getPrincipalKey(principal)).remove(new Object[]{sessionId});
        }
    }

    private void handleCreated(Map<Object, Object> loaded, String channel) {
        String id = channel.substring(channel.lastIndexOf(":") + 1);
        Session session = this.loadSession(id, loaded);
        this.publishEvent(new SessionCreatedEvent(this, session));
    }

    private void handleDeleted(RedisSession session) {
        this.publishEvent(new SessionDeletedEvent(this, session));
    }

    private void handleExpired(RedisSession session) {
        this.publishEvent(new SessionExpiredEvent(this, session));
    }

    private void publishEvent(ApplicationEvent event) {
        try {
            this.eventPublisher.publishEvent(event);
        } catch (Throwable var3) {
            logger.error("Error publishing " + event + ".", var3);
        }
    }

    public void setRedisKeyNamespace(String namespace) {
        Assert.hasText(namespace, "namespace cannot be null or empty");
        this.namespace = namespace.trim() + ":";
        this.configureSessionChannels();
    }

    String getSessionKey(String sessionId) {
        return this.namespace + "sessions:" + sessionId;
    }

    String getPrincipalKey(String principalName) {
        return this.namespace + "index:" + FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME + ":" + principalName;
    }

    String getExpirationsKey(long expiration) {
        return this.namespace + "expirations:" + expiration;
    }

    private String getExpiredKey(String sessionId) {
        return this.getExpiredKeyPrefix() + sessionId;
    }

    private String getSessionCreatedChannel(String sessionId) {
        return this.getSessionCreatedChannelPrefix() + sessionId;
    }

    private String getExpiredKeyPrefix() {
        return this.namespace + "sessions:expires:";
    }

    public String getSessionCreatedChannelPrefix() {
        return this.sessionCreatedChannelPrefix;
    }

    public String getSessionDeletedChannel() {
        return this.sessionDeletedChannel;
    }

    public String getSessionExpiredChannel() {
        return this.sessionExpiredChannel;
    }

    private BoundHashOperations<Object, Object, Object> getSessionBoundHashOperations(String sessionId) {
        String key = this.getSessionKey(sessionId);
        return this.sessionRedisOperations.boundHashOps(key);
    }

    static String getSessionAttrNameKey(String attributeName) {
        return "sessionAttr:" + attributeName;
    }

    final class RedisSession implements Session {
        private final MapSession cached;
        private Instant originalLastAccessTime;
        private Map<String, Object> delta = new HashMap();
        private boolean isNew;
        private String originalPrincipalName;
        private String originalSessionId;

        RedisSession(MapSession cached, boolean isNew) {
            this.cached = cached;
            this.isNew = isNew;
            this.originalSessionId = cached.getId();
            Map<String, String> indexes = CafSessionRepository.this.indexResolver.resolveIndexesFor(this);
            this.originalPrincipalName = (String) indexes.get(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME);
            if (this.isNew) {
                this.delta.put("creationTime", cached.getCreationTime().toEpochMilli());
                this.delta.put("maxInactiveInterval", (int) cached.getMaxInactiveInterval().getSeconds());
                this.delta.put("lastAccessedTime", cached.getLastAccessedTime().toEpochMilli());
            }

            if (this.isNew || CafSessionRepository.this.saveMode == SaveMode.ALWAYS) {
                this.getAttributeNames().forEach((attributeName) -> {
                    this.delta.put(CafSessionRepository.getSessionAttrNameKey(attributeName), cached.getAttribute(attributeName));
                });
            }
        }

        public void setLastAccessedTime(Instant lastAccessedTime) {
            this.cached.setLastAccessedTime(lastAccessedTime);
            this.delta.put("lastAccessedTime", this.getLastAccessedTime().toEpochMilli());
            this.flushImmediateIfNecessary();
        }

        public boolean isExpired() {
            return this.cached.isExpired();
        }

        public Instant getCreationTime() {
            return this.cached.getCreationTime();
        }

        public String getId() {
            return this.cached.getId();
        }

        public String changeSessionId() {
            return this.cached.changeSessionId();
        }

        public Instant getLastAccessedTime() {
            return this.cached.getLastAccessedTime();
        }

        public void setMaxInactiveInterval(Duration interval) {
            this.cached.setMaxInactiveInterval(interval);
            this.delta.put("maxInactiveInterval", (int) this.getMaxInactiveInterval().getSeconds());
            this.flushImmediateIfNecessary();
        }

        public Duration getMaxInactiveInterval() {
            return this.cached.getMaxInactiveInterval();
        }

        public <T> T getAttribute(String attributeName) {
            T attributeValue = this.cached.getAttribute(attributeName);
            if (attributeValue != null && CafSessionRepository.this.saveMode.equals(SaveMode.ON_GET_ATTRIBUTE)) {
                this.delta.put(CafSessionRepository.getSessionAttrNameKey(attributeName), attributeValue);
            }

            return attributeValue;
        }

        public Set<String> getAttributeNames() {
            return this.cached.getAttributeNames();
        }

        public void setAttribute(String attributeName, Object attributeValue) {
            this.cached.setAttribute(attributeName, attributeValue);
            this.delta.put(CafSessionRepository.getSessionAttrNameKey(attributeName), attributeValue);
            this.flushImmediateIfNecessary();
        }

        public void removeAttribute(String attributeName) {
            this.cached.removeAttribute(attributeName);
            this.delta.put(CafSessionRepository.getSessionAttrNameKey(attributeName), null);
            this.flushImmediateIfNecessary();
        }

        private void flushImmediateIfNecessary() {
            if (CafSessionRepository.this.flushMode == FlushMode.IMMEDIATE) {
                this.save();
            }

        }

        private void save() {
            this.saveChangeSessionId();
            this.saveDelta();
        }

        private void saveDelta() {
            if (!this.delta.isEmpty()) {
                String sessionId = this.getId();
                CafSessionRepository.this.getSessionBoundHashOperations(sessionId).putAll(this.delta);
                String principalSessionKey = CafSessionRepository.getSessionAttrNameKey(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME);
                String securityPrincipalSessionKey = CafSessionRepository.getSessionAttrNameKey("SPRING_SECURITY_CONTEXT");
                if (this.delta.containsKey(principalSessionKey) || this.delta.containsKey(securityPrincipalSessionKey)) {
                    if (this.originalPrincipalName != null) {
                        String originalPrincipalRedisKey = CafSessionRepository.this.getPrincipalKey(this.originalPrincipalName);
                        CafSessionRepository.this.sessionRedisOperations.boundSetOps(originalPrincipalRedisKey).remove(new Object[]{sessionId});
                    }

                    Map<String, String> indexes = CafSessionRepository.this.indexResolver.resolveIndexesFor(this);
                    String principal = indexes.get(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME);
                    this.originalPrincipalName = principal;
                    if (principal != null) {
                        String principalRedisKey = CafSessionRepository.this.getPrincipalKey(principal);
                        CafSessionRepository.this.sessionRedisOperations.boundSetOps(principalRedisKey).add(new Object[]{sessionId});
                    }
                }

                this.delta = new HashMap(this.delta.size());

                Long originalExpiration = this.originalLastAccessTime != null ? this.originalLastAccessTime.plus(this.getMaxInactiveInterval()).toEpochMilli() : null;
                CafSessionRepository.this.expirationPolicy.onExpirationUpdated(originalExpiration, this);
            }
        }

        private void saveChangeSessionId() {
            String sessionId = this.getId();
            if (!sessionId.equals(this.originalSessionId)) {
                if (!this.isNew) {
                    String originalSessionIdKey = CafSessionRepository.this.getSessionKey(this.originalSessionId);
                    String sessionIdKey = CafSessionRepository.this.getSessionKey(sessionId);

                    try {
                        CafSessionRepository.this.sessionRedisOperations.rename(originalSessionIdKey, sessionIdKey);
                    } catch (NonTransientDataAccessException var8) {
                        this.handleErrNoSuchKeyError(var8);
                    }

                    String originalExpiredKey = CafSessionRepository.this.getExpiredKey(this.originalSessionId);
                    String expiredKey = CafSessionRepository.this.getExpiredKey(sessionId);

                    try {
                        CafSessionRepository.this.sessionRedisOperations.rename(originalExpiredKey, expiredKey);
                    } catch (NonTransientDataAccessException var7) {
                        this.handleErrNoSuchKeyError(var7);
                    }
                }

                this.originalSessionId = sessionId;
            }
        }

        private void handleErrNoSuchKeyError(NonTransientDataAccessException ex) {
            if (!"ERR no such key".equals(NestedExceptionUtils.getMostSpecificCause(ex).getMessage())) {
                throw ex;
            }
        }
    }
}