package io.iec.edp.caf.session.holder;

/**
 * 是否api key类型请求
 *
 * @author manwenxing01
 */
public class CafSecurityAPIKeyHolder {

    private static final ThreadLocal<Boolean> threadLocal = new ThreadLocal<>();

    public static void set(Boolean value) {
        threadLocal.set(value);
    }

    public static Boolean get() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }

}
